# gearo

## Deployment

To deploy to staging server use next command:

`ansible-playbook -i hosts deploy.yml -l staging`

To deploy on production:

`ansible-playbook -i hosts deploy.yml -l production`

## Development

Working development branch - `develop`.    
To sort imports properly:

`isort -rc ./`

## Development (before start)

- Before create advertisement all **GeoLibraries** should be installed (https://docs.djangoproject.com/en/1.11/ref/contrib/gis/install/geolibs/)
- (see above) Version of **GDAL** should be 2.1+ (lower version will not work with Ubuntu 16.04)
- Before create advertisement **Redis** should be installed (`apt-get install redis-server` on Ubuntu or `brew install redis-server` on MacOS)
- Before download invoices **wkhtmltopdf** should be installed (`apt-get install wkhtmltopdf` on Ubuntu or `brew install wkhtmltopdf` on MacOS)
- For tests **Redis** server should be started (`redis-server`)

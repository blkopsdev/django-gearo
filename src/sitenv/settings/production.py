from .base import *

ALLOWED_HOSTS = ['gearo.de']

SITE_ID = 3

DEBUG = False

BASE_URL = "https://gearo.de"

EMAIL_BACKEND = 'post_office.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'outbound.gearo.de'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'no-reply@gearo.de'
EMAIL_HOST_PASSWORD = 'INKWr8w4P4c4'

NOTIFICATIONS_SERVER = {
    "host": "gearo.de",
    "secret": "EaQ=gQam@RH%CgrfpV9uBe6d",
    "ssl": True,
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'devlive1',
        'USER': 'devlive',
        'PASSWORD': '6zj9qYZj4a3v',
        'HOST': '92.222.77.76',
        'PORT': '3306',

    },
    'master1': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'devlive1',
        'USER': 'devlive',
        'PASSWORD': '6zj9qYZj4a3v',
        'HOST': '92.222.77.76',
        'PORT': '3306',
    },
    'master2': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'devlive1',
        'USER': 'gearolive',
        'PASSWORD': 'K$PK{#*t&ek(',
        'HOST': '92.222.91.218',
        'PORT': '3306',
    }
}

DATABASE_ROUTERS = ['gearo.DatabaseRouter.DatabaseRouter']

INSTALLED_APPS += ['opbeat.contrib.django']

MIDDLEWARE_CLASSES += ['opbeat.contrib.django.middleware.OpbeatAPMMiddleware']

HAYSTACK_CONNECTIONS = {
    "default": {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://92.222.77.77:9310/',
        'INDEX_NAME': 'gearo',
    }
}

# Mangopay settings
MANGOPAY_USE_SANDBOX = False
MANGOPAY_BASE_URL = 'https://api.mangopay.com'
MANGOPAY_CLIENT_ID = 'gearoprod'
MANGOPAY_PASSPHRASE = 'LMSFfo7ksfGhRfE4kcSbHYPMSZwXsyJuii7sW2cgqKxdHWSFYt'

MANGOPAY_GEARO_WALLET_BALANCE_CACHE_TIMEOUT = 30 * 60  # 30 minutes

PAYMENT_HOST = 'gearo.de'
PAYMENT_USES_SSL = True
PAYMENT_MODEL = 'gearo.Payment'
PAYMENT_VARIANTS = {
    'paypal': ('payments.paypal.PaypalProvider', {
        'client_id': 'AU6PIdiFT_do9VOty8QM1lwqCtacJryxvQmp4rj3VgdPY2fsD_HjBIIxDUrHaWnUoM6Lcp_wky1l09uO',
        'secret': 'EGUGl9Pt7_wt7nmoF7-2pzMmZLGjR9r_ccKTB_ht2UUFO6MHdYwKh4_lWZe_NVQUn9g9_1vyV3JXxXPu',
        'endpoint': 'https://api.paypal.com',
        'capture': False
    }),
    'mangopay_card': ('gearo.mangopay.MangopayProvider', {}),
    'mangopay_sofort': ('gearo.mangopay.MangopayProvider', {}),
    'mangopay_paypal': ('gearo.mangopay.MangopayProvider', {}),
}

MEDIA_URL = "https://images.gearo.de/"

MEDIA_ROOT = '/opt/data/media'

STATIC_ROOT = '/opt/data/static'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'gearo_log_format': {
            'format': '[%(asctime)s] %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'gearo_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/opt/projects/logs/gearo.log',
            'formatter': 'gearo_log_format',
        },
        'channels_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/opt/projects/logs/channels.log',
        },
        'celery_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/opt/projects/logs/tasks.log',
            'formatter': 'gearo_log_format',
        },
        'opbeat': {
            'level': 'WARNING',
            'class': 'opbeat.contrib.django.handlers.OpbeatHandler',
        },
        'mangopay_calls_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/opt/projects/logs/mangopay_calls.log',
            'formatter': 'gearo_log_format',
        },
    },
    'loggers': {
        'gearo': {
            'handlers': ['gearo_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.channels': {
            'handlers': ['channels_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'celery': {
            'handlers': ['celery_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'mangopay_calls': {
            'handlers': ['mangopay_calls_logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

OPBEAT = {
    'ORGANIZATION_ID': 'fc241d48db194a8ea0e16a4a8c184b01',
    'APP_ID': 'c9e5c39f41',
    'SECRET_TOKEN': 'f4d5a724542d6510cdf6d43161de76a331b880bd',
}

CELERY_ALWAYS_EAGER = False

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "ROUTING": "gearo.routing.channel_routing",
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': None
    }
}

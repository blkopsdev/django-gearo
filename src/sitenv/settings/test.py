from .base import *

MIGRATION_MODULES = {'gearo': None}

CONSTANCE_REDIS_CONNECTION = 'redis://localhost:6379/1'

CELERY_TASK_ALWAYS_EAGER = True

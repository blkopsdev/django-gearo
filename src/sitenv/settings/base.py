from datetime import timedelta
import os

from django.contrib import messages

from celery.schedules import crontab

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i#@59d0u_!r&r&n$m3o%(028gk^33jqbp1h=b=tpdkkuxa=6q-'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

BASE_URL = "http://localhost:8000"

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'graphos',
    'email_login',
    'post_office',
    'haystack',
    'gearo',
    'gearo_tasks',
    'easy_thumbnails',
    'taggit',
    'django_js_reverse',
    'compressor',
    'constance',
    'channels',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'phonenumber_field',
    'django_countries',
]

AUTHENTICATION_BACKENDS = (
    'email_login.auth_backend.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'sitenv.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.core.context_processors.media',
                'django.contrib.messages.context_processors.messages',
                'gearo.context_processors.settings',
                'gearo.context_processors.notifications',
                'gearo.context_processors.allowed_countries',
            ],
        },
    },
]

WSGI_APPLICATION = 'sitenv.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, 'whoosh_index'),
    },
}

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

DOWNLOADVIEW_BACKEND = 'django_downloadview.nginx.XAccelRedirectMiddleware'

DOWNLOADVIEW_RULES = [
    {
        'source_url': '/media/nginx/',
        'destination_url': '/nginx-optimized-by-middleware/',
    },
]

PDF_ROOT = '/opt/nginx/sitenv/templates'

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'gearo', 'media')

STATIC_ROOT = os.path.join(BASE_DIR, 'gearo', 'static')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'gearo', 'static', 'gearo')
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

SITE_ID = 1

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'gearo_log_format': {
            'format': '[%(asctime)s] %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'gearo_log_format',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'celery': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'gearo': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'gearo_tasks': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

# Mangopay settings
MANGOPAY_USE_SANDBOX = True
MANGOPAY_BASE_URL = 'https://api.sandbox.mangopay.com'
MANGOPAY_CLIENT_ID = 'gearogoesmangopay'
MANGOPAY_PASSPHRASE = 'BKmUc1mAiqX0WOHxfn4LMOVYz66ZjHdN9MAPD04XzUxTTYS85k'

MANGOPAY_GEARO_WALLET_BALANCE_CACHE_TIMEOUT = 900  # 15 minutes in seconds

PAYMENT_HOST = 'localhost:8000'
PAYMENT_USES_SSL = False
PAYMENT_MODEL = 'gearo.Payment'
PAYMENT_VARIANTS = {
    'paypal': ('payments.paypal.PaypalProvider', {
        'client_id': 'AUHuyU3OB4BrBD8pd9wWK3eZMfEz-mCHvYrEtuM0lKQQpokgYORSRV5dK_IwaCCnVYgc1A15QsFTbGgg',
        'secret': 'ELGMQqKUWFlrSVYSKrDh7MoLTEPpcMy0ZQY3a5qLpH55ewWb1DRI7B8ldX6sWdNVnuerypuSxLGfaKxo',
        'endpoint': 'https://api.sandbox.paypal.com',
        'capture': False
    }),
    'mangopay_card': ('gearo.mangopay.MangopayProvider', {}),
    'mangopay_sofort': ('gearo.mangopay.MangopayProvider', {}),
    'mangopay_paypal': ('gearo.mangopay.MangopayProvider', {}),
}

NEXMO_API_KEY = '512e4669'
NEXMO_API_SECRET = 'Mmr2gCNpNajC84PuhMSiB519'

GOOGLE_MAPS_KEY = 'AIzaSyCIMfxTcsiB_IrD4QYB3g5nnO1ndYjYsc8'

NOTIFICATIONS_SERVER = {
    "host": "localhost:8005",
    "secret": "C4&6]4<@CK#P86",
    "ssl": False,
}

THUMBNAIL_QUALITY = 95

THUMBNAIL_ALIASES = {
    'gearo': {
        'ad_m': {'size': (130, 74)},
        'ad_s': {'size': (400, 225)},
        'ad_l': {'size': (690, 395)},
        'ad_xs': {'size': (120, 65)},
        'avatar': {'size': (215, 215)}
    },
}

THUMBNAIL_AD_ALIAS = 'ad_m'
THUMBNAIL_AVATAR_ALIAS = 'avatar'
THUMBNAIL_TARGET = 'gearo'

ATTACHMENT_PLACEHOLDER = os.path.join(STATIC_URL, 'images', 'placeholder_image.png')
AVATAR_PLACEHOLDER = os.path.join(STATIC_URL, 'images', 'placeholderavatar.jpg')

ADVERTISEMENT_MAX_ATTACHMENTS = 6

MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}

LOGIN_URL = '/login/'

TAGGIT_CASE_INSENSITIVE = True

CELERYBEAT_SCHEDULE = {
    'change-status': {
        'task': 'gearo_tasks.tasks.changestatus.change_status',
        'schedule': crontab(hour=3, minute=0),
    },
    'combine-ranking': {
        'task': 'gearo_tasks.tasks.combineranking.combine_ranking',
        'schedule': crontab(hour=6, minute=0),
    },
    'force-reject': {
        'task': 'gearo_tasks.tasks.forcereject.force_reject',
        'schedule': timedelta(minutes=15)
    },
    'response-limit': {
        'task': 'gearo_tasks.tasks.responselimit.response_limit',
        'schedule': crontab(hour=3, minute=0),
    },
    'review-mail': {
        'task': 'gearo_tasks.tasks.reviewmail.review_mail',
        'schedule': timedelta(minutes=15),
    },
    'ranking-user': {
        'task': 'gearo_tasks.tasks.usertasks.ranking_user',
        'schedule': crontab(hour=4, minute=0),
    },
    'send-reminder-emails-one-day-rental': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_reminder_emails',
        'schedule': crontab(hour=15, minute=0),
        'kwargs': {'one_day_rental': True}
    },
    'send-reminder-emails-multiple-days-rental': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_reminder_emails',
        'schedule': crontab(hour=6, minute=0)
    },
    'send-response-limit-long-term-email-reminder': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_response_limit_long_term_reminders',
        'schedule': timedelta(minutes=30),
    },
    'send-response-limit-short-term-email-reminder': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_response_limit_short_term_reminders',
        'schedule': timedelta(minutes=30),
    },
    'send-response-limit-reminder': {
        'task': 'gearo_tasks.tasks.sms.send_response_limit_reminder',
        'schedule': timedelta(hours=1),
    },
    'send-response-limit-short-term-sms-reminder': {
        'task': 'gearo_tasks.tasks.sms.send_response_limit_short_term_reminders',
        'schedule': timedelta(minutes=30),
    },
    'send-notifications-for-unread-chat-messages': {
        'task': 'gearo_tasks.tasks.notifications.unread_chat_messages',
        'schedule': timedelta(minutes=15)
    },
    'check_mangopay_users_documents_status': {
        'task': 'gearo_tasks.tasks.mangopay.check_mp_document_status',
        'schedule': timedelta(minutes=15),
    },
    'check_mp_payouts_statuses': {
        'task': 'gearo_tasks.tasks.mangopay.check_mp_payouts_statuses',
        'schedule': timedelta(hours=2),
    },
    'check_mp_payouts_succeeded_statuses': {
        'task': 'gearo_tasks.tasks.mangopay.check_mp_payouts_succeeded_statuses',
        'schedule': timedelta(hours=2),
    },
}

CELERY_ALWAYS_EAGER = True

DASHBOARD_CHART_OPTIONS = {
    'colors': [
        "#edc240",
        "#afd8f8",
        "#cb4b4b",
        "#4da74d",
        "#9440ed",
        "#1f77b4",
        "#ff7f0e",
        "#2ca02c",
        "#d62728",
        "#8c564b",
        "#e377c2",
        "#7f7f7f",
        "#bcbd22",
        "#17becf",
    ],
}

# Number of hours before pickup when cancellation is not available
TRANSACTION_CANCELLATION_DOWNTIME = 20

# Number of days when transaction insurance is valid
INSURANCE_DURATION = 15

ITEMS_PER_PAGE = 20

JS_REVERSE_OUTPUT_PATH = os.path.join(BASE_DIR, 'gearo', 'static', 'gearo', 'js')

CONSTANCE_CONFIG = {
    'INSURANCE_RATE': (6.0, ''),
    'INSURANCE_COMMISSION': (0.5, ''),
    # Value that will be added to each insurance commission value (in EUROs)
    'ADDITION_TO_INSURANCE_COMMISSION': (1.0, ''),
    'FIRST_RATE': (10.4, ''),
    'SECOND_RATE': (2.5, ''),
    'VAT_RATE_DE': (19.0, ''),  # GERMANY VAT RATE
    'VAT_RATE_AT': (20.0, ''),  # AUSTRIA VAT RATE
    'GEARO_FEE': (15.0, ''),
    'REVIEW_GREAT_POINTS': (100, ''),
    'REVIEW_ALRIGHT_POINTS': (60, ''),
    'REVIEW_NOGO_POINTS': (25, ''),
    'CANCELED_POINTS': (15, ''),
    # If lessor close to next limits, he/she should change KYC lever from "Light" to "Regular"
    'LIMIT_CASH_IN': (2200.00, ''),  # EUR
    'LIMIT_CASH_OUT': (800.00, ''),  # EUR
}

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgiref.inmemory.ChannelLayer",
        "ROUTING": "gearo.routing.channel_routing",
    },
}

LOGIN_REDIRECT_URL = '/'

SOCIALACCOUNT_EMAIL_REQUIRED = False

SOCIALACCOUNT_QUERY_EMAIL = True

SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'VERIFIED_EMAIL': False,
    }
}

ACCOUNT_ADAPTER = 'gearo.adapter.AccountAdapter'

SOCIALACCOUNT_ADAPTER = 'gearo.adapter.SocialAccountAdapter'

# Disable email verification in Django-allauth, since we have own one.
ACCOUNT_EMAIL_VERIFICATION = 'none'

# This setting limit list of tags in auto-complete widget
# on 'inserieren/' page.
# It used in 'TagAutocompleteView'
TAG_AUTOSUGGEST_MAX_ITEMS = 10

# List of allowed countries (by country codes) for addresses, phones, countries fields
ALLOWED_COUNTRIES = ['DE', 'AT']

# This setting need for 'CountryField()' from django_countries
# Show next countries first in countries list
COUNTRIES_FIRST = ALLOWED_COUNTRIES

# Notifications settings
GEARO_NOTIFICATION_SHORT_TERM = 2  # hours
GEARO_NOTIFICATION_LONG_TERM = 12  # hours

# Time after which "SUCCEEDED" status for Mangopay PayOut is considered as confirmed forever
SUCCEEDED_STATUS_CONFIRMATION_WAITING_TIME = 2  # weeks

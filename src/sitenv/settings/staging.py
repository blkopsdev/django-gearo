from .base import *

DEBUG = False

SITE_ID = 2

ALLOWED_HOSTS = ['stage.gearo.de']

BASE_URL = "https://stage.gearo.de"

EMAIL_BACKEND = 'post_office.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'outbound.gearo.de'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'no-reply@gearo.de'
EMAIL_HOST_PASSWORD = 'INKWr8w4P4c4'

NOTIFICATIONS_SERVER = {
    "host": "stage.gearo.de",
    "secret": "EaQ=gQam@RH%CgrfpV9uBe6d",
    "ssl": True,
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'gearo_staging',
        'USER': 'gearo',
        'PASSWORD': 'Vcx66wEvrv37m2DLmHdy93k6',
        'HOST': 'localhost',
        'PORT': '3306',

    }
}

MEDIA_ROOT = '/var/www/gearo/media'

STATIC_ROOT = '/var/www/gearo/static'

PDF_ROOT = '/opt/projects/gearo/src/templates/'

INSTALLED_APPS += ['opbeat.contrib.django']

MIDDLEWARE_CLASSES += ['opbeat.contrib.django.middleware.OpbeatAPMMiddleware']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'django': {
            'format': 'django: %(message)s',
        },
        'gearo_log_format': {
            'format': '[%(asctime)s] %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'gearo_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/opt/projects/logs/gearo.log',
            'formatter': 'gearo_log_format',
        },
        'celery_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/opt/projects/logs/tasks.log',
            'formatter': 'gearo_log_format',
        },
        'loggly': {
             'level': 'DEBUG',
             'class': 'logging.handlers.SysLogHandler',
             'facility': 'local7',
             'formatter': 'django',
             'address': '/dev/log',
        },
        'opbeat': {
            'level': 'WARNING',
            'class': 'opbeat.contrib.django.handlers.OpbeatHandler',
        },
        'mangopay_calls_logfile': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/opt/projects/logs/mangopay_calls.log',
            'formatter': 'gearo_log_format',
        },
    },
    'loggers': {
        'gearo': {
            'handlers': ['gearo_logfile', 'loggly', 'opbeat'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'celery': {
            'handlers': ['celery_logfile', 'loggly', 'opbeat'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'mangopay_calls': {
            'handlers': ['mangopay_calls_logfile', 'loggly'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

HAYSTACK_CONNECTIONS = {
    "default": {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://localhost:9200/',
        'INDEX_NAME': 'gearo',
    }
}

OPBEAT = {
    'ORGANIZATION_ID': 'fc241d48db194a8ea0e16a4a8c184b01',
    'APP_ID': 'c293ce46df',
    'SECRET_TOKEN': 'f4d5a724542d6510cdf6d43161de76a331b880bd',
}

PAYMENT_HOST = 'stage.gearo.de'
PAYMENT_USES_SSL = True

BROKER_URL = 'amqp://gearo:Z8puhRztcx@localhost:5672/'

CELERYBEAT_SCHEDULE = {
    'change-status': {
        'task': 'gearo_tasks.tasks.changestatus.change_status',
        'schedule': timedelta(minutes=15),
    },
    'combine-ranking': {
        'task': 'gearo_tasks.tasks.combineranking.combine_ranking',
        'schedule': timedelta(minutes=15),
    },
    'force-reject': {
        'task': 'gearo_tasks.tasks.forcereject.force_reject',
        'schedule': timedelta(minutes=15)
    },
    'response-limit': {
        'task': 'gearo_tasks.tasks.responselimit.response_limit',
        'schedule': timedelta(minutes=15),
    },
    'review-mail': {
        'task': 'gearo_tasks.tasks.reviewmail.review_mail',
        'schedule': timedelta(minutes=15),
    },
    'ranking-user': {
        'task': 'gearo_tasks.tasks.usertasks.ranking_user',
        'schedule': timedelta(minutes=15),
    },
    'send-reminder-emails-one-day-rental': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_reminder_emails',
        'schedule': timedelta(minutes=15),
        'kwargs': {'one_day_rental': True}
    },
    'send-reminder-emails-multiple-days-rental': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_reminder_emails',
        'schedule': timedelta(minutes=15),
    },
    'send-response-limit-long-term-email-reminder': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_response_limit_long_term_reminders',
        'schedule': timedelta(minutes=30),
    },
    'send-response-limit-short-term-email-reminder': {
        'task': 'gearo_tasks.tasks.bookingreminders.send_response_limit_short_term_reminders',
        'schedule': timedelta(minutes=30),
    },
    'send-response-limit-reminder': {
        'task': 'gearo_tasks.tasks.sms.send_response_limit_reminder',
        'schedule': timedelta(hours=1),
    },
    'send-response-limit-short-term-sms-reminder': {
        'task': 'gearo_tasks.tasks.sms.send_response_limit_short_term_reminders',
        'schedule': timedelta(minutes=30),
    },
    'send-notifications-for-unread-chat-messages': {
        'task': 'gearo_tasks.tasks.notifications.unread_chat_messages',
        'schedule': timedelta(minutes=15)
    },
    'check_mangopay_users_documents_status': {
        'task': 'gearo_tasks.tasks.mangopay.check_mp_document_status',
        'schedule': timedelta(minutes=15),
    },
}

CELERY_ALWAYS_EAGER = False

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "ROUTING": "gearo.routing.channel_routing",
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': None
    }
}

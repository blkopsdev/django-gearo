from django.conf.urls import include, url

urlpatterns = [
    url(r'^', include('gearo.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url('^payments/', include('payments.urls')),
]

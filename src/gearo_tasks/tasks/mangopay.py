import logging
from datetime import date, timedelta
from decimal import Decimal as D

from django.conf import settings
from django.contrib.auth.models import User
from django.utils.timezone import now

from constance import config
from mangopay.constants import DOCUMENTS_STATUS_CHOICES, DOCUMENTS_TYPE_CHOICES
from mangopay.resources import BankWirePayOut, Document, Page
from post_office import mail

from gearo.celery import app
from gearo.helper.mangopay import (
    GEARO_STATUS_CHOICES, create_mangopay_user, create_mangopay_wallet, encode_file_to_base64_str,
    get_gearo_payout, get_mangopay_user, get_mangopay_wallet, make_payout,
    remove_temp_files, update_mangopay_user_info, update_platform_fee_wallet_balance,
)
from gearo.models import GearoPayOut, GearoUserDocument, Transactions

logger = logging.getLogger(__name__)

IDENTITY_PROOF = DOCUMENTS_TYPE_CHOICES.identity_proof

CREATED = DOCUMENTS_STATUS_CHOICES.created
VALIDATION_ASKED = DOCUMENTS_STATUS_CHOICES.validation_asked
VALIDATED = DOCUMENTS_STATUS_CHOICES.validated
REFUSED = DOCUMENTS_STATUS_CHOICES.refused


@app.task
def create_mangopay_user_and_wallet(user_id):
    user = User.objects.get(id=user_id)
    if not user.billing_info.is_mangopay_user_id_existing and user.email:
        create_mangopay_user(user)
    if not user.billing_info.is_mangopay_user_wallet_id_existing and user.email:
        create_mangopay_wallet(user)


@app.task
def update_mp_user_info(user_id, params_for_update):
    user = User.objects.get(id=user_id)
    update_mangopay_user_info(user, params_for_update)


@app.task
def update_mp_wallet_balance(user_id):
    user = User.objects.get(id=user_id)
    mp_wallet = get_mangopay_wallet(user)

    user.billing_info.mangopay_current_wallet_balance = mp_wallet.balance.amount / D(100)
    user.billing_info.save()

    mp_user = get_mangopay_user(user)
    emoney = mp_user.get_emoney()

    # The amount of money that has been debited from this users
    payin_emoney = emoney.debited_emoney.amount / D(100)
    # The amount of money that has been credited to this user
    payout_emoney = emoney.credited_emoney.amount / D(100)

    if payin_emoney > D(config.LIMIT_CASH_IN) and user.billing_info.payin_limit_reached_email_sent_at is None:
        send_cash_limit_email.delay(user_id)
    if payout_emoney > D(config.LIMIT_CASH_OUT) and user.billing_info.payout_limit_reached_email_sent_at is None:
        send_cash_limit_email.delay(user_id, 'cash_out')

    update_platform_fee_wallet_balance()


@app.task
def send_cash_limit_email(user_id, limit_type='cash_in'):
    user = User.objects.get(id=user_id)
    if limit_type == 'cash_in':
        template = 'limit_pay_in_reminder'
        user.billing_info.payin_limit_reached_email_sent_at = now()
    else:
        template = 'limit_pay_out_reminder'
        user.billing_info.payout_limit_reached_email_sent_at = now()
    user.billing_info.is_payment_limit_reached = True
    user.billing_info.save()
    context = {'first_name': user.first_name}
    mail.send(user.email, 'no-reply@gearo.de', template=template, context=context)


@app.task
def send_mp_document_for_verification(user_id, document_type, *pages):
    user = User.objects.get(id=user_id)
    mp_user = get_mangopay_user(user)

    document = Document(type=document_type, user=mp_user)
    document.save()
    document_id = document.get_pk()

    for page in pages:
        with open(page, 'rb') as f:
            document_page = Page(document=document, file=encode_file_to_base64_str(f), user=mp_user)
            document_page.save()

    document.status = VALIDATION_ASKED
    document.save()

    GearoUserDocument.objects.create(
        user=user, document_type=document_type, document_id=document_id, document_status=VALIDATION_ASKED)

    logger.debug(
        'Sent validation asked request for "%s" document with id - %s, '
        'for Mangopay User with id - %s, gearo user - "%s"',
        document_type, document_id, mp_user.get_pk(), user.email,
    )

    remove_temp_files(pages)


@app.task
def check_mp_document_status():
    unverified_documents = GearoUserDocument.objects.filter(document_status=VALIDATION_ASKED)

    for gearo_user_document in unverified_documents:
        document_id = gearo_user_document.document_id
        document_type = gearo_user_document.document_type
        mangopay_document = Document.get(document_id)

        user = gearo_user_document.user
        model_field = '{}_status'.format(document_type.lower())

        if mangopay_document.status == VALIDATED:
            gearo_user_document.document_status = VALIDATED
            gearo_user_document.save()

            setattr(user.billing_info, model_field, VALIDATED)

            logger.debug(
                '"%s" document with id - %s was VALIDATED, user id - %s, user email - %s',
                document_type, document_id, user.id, user.email,
            )

        if mangopay_document.status == REFUSED:
            refused_reason_type = mangopay_document.refused_reason_type
            refused_reason_message = mangopay_document.refused_reason_message

            gearo_user_document.document_status = REFUSED
            gearo_user_document.refused_reason_type = refused_reason_type
            gearo_user_document.refused_reason_message = refused_reason_message
            gearo_user_document.save()

            # If status of document is CREATED, user will see buttons for documents uploading.
            # If document will be REFUSED, we will set status of document again to CREATED,
            # so buttons for documents uploading will be active again.
            setattr(user.billing_info, model_field, CREATED)

            logger.error(
                '"%s" document with id - %s was REFUSED, refused reason type - %s, refused reason message - %s, '
                'user id - %s, user email - %s',
                document_type, document_id, refused_reason_type, refused_reason_message, user.id, user.email,
            )

        user.billing_info.save()


@app.task
def make_mp_payout(transaction_id):
    transaction = Transactions.objects.get(id=transaction_id)
    payout = make_payout(transaction)

    gearo_payout = get_gearo_payout(transaction)

    gearo_payout.payout_id = payout.get_pk()
    gearo_payout.payout_status = payout.status
    gearo_payout.result_code = payout.result_code
    gearo_payout.result_message = payout.result_message
    gearo_payout.save()

    # Update lessor's wallet balance after PayOut was made
    lessor = transaction.lessor
    update_mp_wallet_balance(lessor.id)


@app.task
def check_mp_payouts_statuses():
    created_gearo_payouts = GearoPayOut.objects.filter(payout_status=GEARO_STATUS_CHOICES.created)

    for gearo_payout in created_gearo_payouts:
        payout_id = gearo_payout.payout_id
        transaction = gearo_payout.transaction
        lessor_email = transaction.lessor.email
        payout = BankWirePayOut.get(payout_id)

        if payout.status == GEARO_STATUS_CHOICES.succeeded:
            gearo_payout.payout_status = GEARO_STATUS_CHOICES.succeeded

            logger.debug(
                'Mangopay PayOut with id - %s was SUCCEEDED for transaction id - %s (for lessor "%s")',
                payout_id, transaction.id, lessor_email,
            )

        if payout.status == GEARO_STATUS_CHOICES.failed:
            gearo_payout.payout_status = GEARO_STATUS_CHOICES.failed
            result_code = payout.result_code
            result_message = payout.result.message
            gearo_payout.result_code = result_code
            gearo_payout.result_message = result_message

            logger.error(
                'Mangopay PayOut with id - %s was FAILED for transaction id - %s (for lessor "%s"), '
                'Result code: %s, Result message: %s',
                payout_id, transaction.id, lessor_email, result_code, result_message,
            )

        gearo_payout.save()


@app.task
def check_mp_payouts_succeeded_statuses():
    """
    If a payout is successfully treated (the payout becomes SUCCEEDED),
    it can later be reaccredited for a variety of reasons.

    In this case, we will have `result_code` and `result_message`

    We will check payouts that not older than `period_to_check`
    """

    waiting_time = settings.SUCCEEDED_STATUS_CONFIRMATION_WAITING_TIME
    period_to_check = date.today() - timedelta(weeks=waiting_time)
    succeeded_gearo_payouts = GearoPayOut.objects.filter(
        payout_status=GEARO_STATUS_CHOICES.succeeded, created_at__lt=period_to_check,
    )

    for gearo_payout in succeeded_gearo_payouts:
        payout_id = gearo_payout.payout_id
        transaction = gearo_payout.transaction
        lessor_email = transaction.lessor.email
        payout = BankWirePayOut.get(payout_id)

        result_code_was_changed = payout.result_code != gearo_payout.result_code
        result_message_was_changed = payout.result_message != gearo_payout.result_message

        if result_code_was_changed and result_message_was_changed:
            gearo_payout.payout_status = GEARO_STATUS_CHOICES.succeeded_failed
            result_code = payout.result_code
            result_message = payout.result.message
            gearo_payout.result_code = result_code
            gearo_payout.result_message = result_message

            logger.error(
                'Mangopay PayOut with id - %s was FAILED after was SUCCEEDED '
                'for transaction id - %s (for lessor "%s"), Result code: %s, Result message: %s',
                payout_id, transaction.id, lessor_email, result_code, result_message,
            )

            gearo_payout.save()

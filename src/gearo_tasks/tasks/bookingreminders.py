import logging
from datetime import date, timedelta

from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db.models import F
from django.utils import timezone

from post_office import mail

from gearo.celery import app
from gearo.helper.absolute_url import absolute_url
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)


def _get_email_context(transaction, is_return_reminder=False):
    lessor = transaction.lessor.gearouser
    lessor_phone_number = getattr(lessor.phone_mobile, 'raw_input', '')
    tenant = transaction.applicant.gearouser
    tenant_phone_number = getattr(tenant.phone_mobile, 'raw_input', '')
    context = {
        'firstname_lessor': transaction.lessor.first_name,
        'firstname_tenant': transaction.applicant.first_name,
        'button_url': settings.BASE_URL + static('images/create_protocol_button.png'),
        'create_protocol_url': absolute_url('handingover', transaction.id),
        'chatroom': transaction.get_chat_url(),
        'phone_lessor': lessor_phone_number,
        'phone_tenant': tenant_phone_number,
    }
    if is_return_reminder:
        context['create_protocol_url'] = absolute_url('returnproto', transaction.id)
    return context


def _get_reminders_context(transaction):
    return {
        'firstname_lessor': transaction.lessor.first_name,
        'firstname_tenant': transaction.applicant.first_name,
        'responselimit': transaction.get_responselimit(),
        'bookingmanlink': absolute_url("maninq", transaction.id),
    }


@app.task
def send_reminder_emails(one_day_rental=False):
    today_pickup_lookup_kwargs = {
        'rent_start': date.today(),
        'status__in': Transactions.ACTIVE_STATUSES,
        'pickup_reminders_sent_at__isnull': True,
    }
    if one_day_rental:
        today_pickup_lookup_kwargs['rent_end'] = date.today()
    else:
        today_pickup_lookup_kwargs['rent_end__gt'] = date.today()
    today_pickup_transactions = Transactions.objects.filter(**today_pickup_lookup_kwargs)

    if one_day_rental:
        today_return_transactions = today_pickup_transactions
    else:
        today_return_transactions = Transactions.objects.filter(
            rent_start__lt=date.today(),
            rent_end=date.today(),
            status__in=Transactions.ACTIVE_STATUSES,
            return_reminders_sent_at__isnull=True,
        )

    for transaction in today_pickup_transactions:
        context = _get_email_context(transaction)
        mail.send(
            transaction.lessor.email,
            'no-reply@gearo.de',
            template='bookingreminderlessor',
            context=context,
        )
        logger.debug(
            'Sent today pickup reminder email for transaction #%s, ad #%s to Ad lessor %s.',
            transaction.id, transaction.ad.id, transaction.lessor.email,
        )
        mail.send(
            transaction.applicant.email,
            'no-reply@gearo.de',
            template='bookingremindertenant',
            context=context,
        )
        logger.debug(
            'Sent today pickup reminder email for transaction #%s, ad #%s to Ad tenant %s.',
            transaction.id, transaction.ad.id, transaction.applicant.email,
        )
        transaction.pickup_reminders_sent_at = timezone.now()
        transaction.save()

    for transaction in today_return_transactions:
        context = _get_email_context(transaction, is_return_reminder=True)
        mail.send(
            transaction.lessor.email,
            'no-reply@gearo.de',
            template='returnreminderlessor',
            context=context,
        )
        logger.debug(
            'Sent today return reminder email for transaction #%s, ad #%s to Ad lessor %s.',
            transaction.id, transaction.ad.id, transaction.lessor.email,
        )
        mail.send(
            transaction.applicant.email,
            'no-reply@gearo.de',
            template='returnremindertenant',
            context=context,
        )
        logger.debug(
            'Sent today return reminder email for transaction #%s, ad #%s to Ad tenant %s.',
            transaction.id, transaction.ad.id, transaction.applicant.email,
        )
        transaction.return_reminders_sent_at = timezone.now()
        transaction.save()


@app.task
def send_response_limit_long_term_reminders():
    """
    Send reminder `term` hours before response limit ends.

    These reminders will not be send for transactions that was created
    in period shorter than `term` hours before response_limit.
    """

    term = settings.GEARO_NOTIFICATION_LONG_TERM

    range_start = timezone.now() + timedelta(hours=term)
    range_end = timezone.now() + timedelta(hours=term + 1)
    transactions = Transactions.objects.filter(
        response_limit__range=(range_start, range_end),
        created_at__lt=(F('response_limit') - timedelta(hours=term)),
        status=Transactions.STATUS_WAITING,
        response_limit_reminder_long_email_sent_at__isnull=True,
    )
    for transaction in transactions:
        context = _get_reminders_context(transaction)
        mail.send(
            transaction.lessor.email,
            'no-reply@gearo.de',
            template='booking_reminder_1',
            context=context,
        )
        transaction.response_limit_reminder_long_email_sent_at = timezone.now()
        transaction.save()

        logger.debug(
            'Sent %s hours reminder email for transaction #%s, ad #%s to Ad lessor %s.',
            term, transaction.id, transaction.ad.id, transaction.lessor.email,
        )


@app.task
def send_response_limit_short_term_reminders():
    """
    Send reminder `term` hours before response limit ends.
    """

    term = settings.GEARO_NOTIFICATION_SHORT_TERM

    range_start = timezone.now() + timedelta(hours=term)
    range_end = timezone.now() + timedelta(hours=term + 1)
    transactions = Transactions.objects.filter(
        response_limit__range=(range_start, range_end),
        status=Transactions.STATUS_WAITING,
        response_limit_reminder_short_email_sent_at__isnull=True,
    )
    for transaction in transactions:
        context = _get_reminders_context(transaction)
        mail.send(
            transaction.lessor.email,
            'no-reply@gearo.de',
            template='booking_reminder_2',
            context=context,
        )
        transaction.response_limit_reminder_short_email_sent_at = timezone.now()
        transaction.save()

        logger.debug(
            'Sent %s hours reminder email for transaction #%s, ad #%s to Ad lessor %s.',
            term, transaction.id, transaction.ad.id, transaction.lessor.email,
        )

from django.contrib.auth.models import User
from django.core.files.base import ContentFile

import requests
from allauth.socialaccount.models import SocialAccount
from constance import config

from gearo.celery import app
from gearo.helper import emails
from gearo.models.gearo_user import GearoUser
from gearo.models.transactions import Transactions
from gearo.models.userreviews import UserReviews

FACEBOOK_API_PICTURE_URL = 'https://graph.facebook.com/%s/picture?type=square&width=400&height=400'


@app.task
def send_welcome_email(user_id):
    user = User.objects.get(id=user_id)
    emails.send_welcome_email(user)


@app.task
def ranking_user():
    users = GearoUser.objects.all()
    for user in users:
        tmp_reviews = UserReviews.objects.filter(user=user.user)
        tmp_count = len(tmp_reviews)
        tmp_user_ranking = 0
        tmp_all_points = 0
        tmp_good = 0
        tmp_alright = 0
        tmp_nogo = 0

        if len(tmp_reviews) > 0:
            # calculate average userreview and count review stats
            for review in tmp_reviews:
                if review.rating == "great":
                    tmp_good += 1
                    tmp_all_points += config.REVIEW_GREAT_POINTS
                elif review.rating == "alright":
                    tmp_alright += 1
                    tmp_all_points += config.REVIEW_ALRIGHT_POINTS
                elif review.rating == "nogo":
                    tmp_nogo += 1
                    tmp_all_points += config.REVIEW_NOGO_POINTS

            tmp_avg = tmp_all_points/tmp_count
            user.user_review = tmp_avg

            # calc user ranking points
            if tmp_nogo > 5:
                tmp_user_ranking -= 5
            else:
                tmp_user_ranking -= tmp_nogo
            if tmp_alright > 30:
                tmp_user_ranking -= 3
            else:
                tmp_user_ranking -= tmp_alright
            if tmp_good > 3:
                tmp_user_ranking += 3
            else:
                tmp_user_ranking += tmp_good

        canceled_count = Transactions.objects.filter(ad__owner=user.user, status=Transactions.STATUS_CANCELLED).count()
        tmp_user_ranking -= canceled_count * config.CANCELED_POINTS

        if tmp_user_ranking < 0:
            tmp_user_ranking = 0

        user.user_ranking = tmp_user_ranking
        user.save()


@app.task
def fetch_facebook_avatar(socialaccount_id):
    social_account = SocialAccount.objects.get(id=socialaccount_id)
    profile = social_account.user.profile
    if not profile.avatar:
        url = FACEBOOK_API_PICTURE_URL % social_account.uid
        response = requests.get(url)
        if response.status_code == 200:
            avatar_content = ContentFile(response.content)
            profile.avatar.save('userpic.jpg', avatar_content)

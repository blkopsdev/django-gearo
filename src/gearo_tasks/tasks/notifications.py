from post_office import mail

from gearo.celery import app
from gearo.helper import notifications
from gearo.models import Chatrooms, Notifications


@app.task
def send_notification(notification_id, notification_type='add'):
    notification = Notifications.objects.get(id=notification_id)
    notifications.send_notification(notification, notification_type=notification_type)


@app.task
def unread_chat_messages():
    chatrooms = Chatrooms.objects.filter(messages__is_read=False, messages__is_notification_sent=False).distinct()
    for chatroom in chatrooms:
        unread_messages = chatroom.messages.filter(is_read=False, is_notification_sent=False)
        grouped_unread_messages = {}
        for message in unread_messages:
            if message.recipient_id in grouped_unread_messages:
                grouped_unread_messages[message.recipient_id]['messages'].append(message.message)
            else:
                grouped_unread_messages[message.recipient_id] = {
                    'messages': [message.message],
                    'recipient': message.recipient,
                    'sender': message.sender,
                    'transaction': message.room.transaction
                }

        for grouped_message in grouped_unread_messages.values():
            mail.send(grouped_message['recipient'].email, 'no-reply@gearo.de', template='newmessage',
                      context={'firstname': grouped_message['recipient'].first_name,
                               'firstname_sender': grouped_message['sender'].first_name,
                               'messages': grouped_message['messages'],
                               'chatroom': grouped_message['transaction'].get_chat_url()}
                      )

        unread_messages.update(is_notification_sent=True)

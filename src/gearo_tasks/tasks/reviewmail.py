import logging
from datetime import date, timedelta

from django.core.urlresolvers import reverse
from django.utils import timezone

from post_office import mail

from gearo.celery import app
from gearo.helper.gentoken import gentoken
from gearo.models.tokens import Tokens
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)


@app.task
def review_mail():
    transactions = Transactions.objects.filter(status=Transactions.STATUS_FINISHED, review_email_sent_at__isnull=True)
    for transaction in transactions:
        # send mail to applicant
        token_app = Tokens(
            user=transaction.applicant,
            token=gentoken(16, True, "Tokens", "token"),
            expire_date=(date.today() + timedelta(days=15)),
            kind=3,
        )
        token_app.save()
        context = {
            'firstname': transaction.applicant.first_name,
            'firstname_lessor': transaction.lessor.first_name,
            'review_great': "http://gearo.de" + reverse(
                "reviewgreat", kwargs={"inqid": transaction.id, "token": token_app.token}),
            'review_right': "http://gearo.de" + reverse(
                "reviewalright", kwargs={"inqid": transaction.id, "token": token_app.token}),
            'review_nogo': "http://gearo.de" + reverse(
                "reviewnogo", kwargs={"inqid": transaction.id, "token": token_app.token}),
        }
        mail.send(transaction.applicant.email, 'no-reply@gearo.de', template='reviewapplicant', context=context)
        logger.debug(
            'Sent review email for transaction #%s, ad #%s to Ad applicant %s',
            transaction.id, transaction.ad.id, transaction.applicant.email,
        )

        # send mail to owner
        token_ad = Tokens(
            user=transaction.lessor,
            token=gentoken(16, True, "Tokens", "token"),
            expire_date=(date.today() + timedelta(days=15)),
            kind=3,
        )
        token_ad.save()
        context = {
            'firstname': transaction.lessor.first_name,
            'firstname_applicant': transaction.applicant.first_name,
            'review_great': "http://gearo.de" + reverse(
                "reviewgreat", kwargs={"inqid": transaction.id, "token": token_ad.token}),
            'review_right': "http://gearo.de" + reverse(
                "reviewalright", kwargs={"inqid": transaction.id, "token": token_ad.token}),
            'review_nogo': "http://gearo.de" + reverse(
                "reviewnogo", kwargs={"inqid": transaction.id, "token": token_ad.token}),
        }
        mail.send(transaction.lessor.email, 'no-reply@gearo.de', template='reviewlessor', context=context)
        logger.debug(
            'Sent review email for transaction #%s, ad #%s to Ad owner %s',
            transaction.id, transaction.ad.id, transaction.lessor.email,
        )
        transaction.review_email_sent_at = timezone.now()
        transaction.save()

import logging

from django.db.models import Avg, F
from django.utils import timezone

from gearo.celery import app
from gearo.models.gearo_user import GearoUser
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)


@app.task
def response_limit():
    users = GearoUser.objects.all()
    for user in users:
        try:
            calc = Transactions.objects.filter(processed_at__isnull=False, ad__owner=user.user) \
                                       .aggregate(avgtime=Avg(F('processed_at') - F('created_at')) / 3600)

            user.responsetime = calc.get("avgtime")
            user.save()
        except Exception as e:
            logger.error('Error on response time calculation - %s', e)

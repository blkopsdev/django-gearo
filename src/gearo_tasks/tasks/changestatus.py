import logging
from datetime import date

from django.utils import timezone

from gearo.celery import app
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)


@app.task
def change_status():
    logger.debug('Updating transactions statuses.')
    rent_start = Transactions.objects.filter(rent_start=date.today(), status=Transactions.STATUS_ACCEPTED) \
                                     .update(status="handing")
    rent_end = Transactions.objects.filter(rent_end=date.today(), status=Transactions.STATUS_ACTIVE) \
                                   .update(status="return")
    logger.debug('Set "handing" status to %s transactions, "return" to %s transactions', rent_start, rent_end)

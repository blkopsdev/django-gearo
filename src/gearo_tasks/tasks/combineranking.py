import datetime

from gearo.celery import app
from gearo.models.advertisement import Advertisement


@app.task
def combine_ranking():
    ads = Advertisement.objects.all()
    datetoday = datetime.date.strftime(datetime.date.today(), "%d.%m.%Y")
    for ad in ads:
        if datetoday not in ad.all_not_available_dates:
            ad.ad_points += 10
        else:
            ad.ad_points -= 5

        if ad.owner.gearouser.user_ranking + ad.ad_points > 0:
            ad.ranking = ad.owner.gearouser.user_ranking + ad.ad_points
        else:
            ad.ranking = 0

        ad.save()

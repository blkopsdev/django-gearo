from gearo.celery import app
from gearo.helper.locations import get_location
from gearo.models import Advertisement
from gearo.search_indexes import AdvertisementIndex


@app.task
def update_advertisements(user_id):
    for advertisement in Advertisement.objects.filter(owner_id=user_id, is_removed=False):
        AdvertisementIndex().update_object(instance=advertisement)


@app.task
def update_advertisement(ad_id):
    ad = Advertisement.objects.get(id=ad_id)
    ad.pickup_city = get_location(ad.lat, ad.lng)
    ad.set_slug()
    ad.save()
    AdvertisementIndex().update_object(instance=ad)

import logging

from django.conf import settings
from django.utils import timezone

import nexmo
from payments import get_payment_model
from post_office import mail

from gearo.celery import app
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)
Payment = get_payment_model()


@app.task
def force_reject():
    logger.debug('Check expired transactions.')
    transactions = Transactions.objects.filter(
        response_limit__lt=timezone.now(), processed_at__isnull=True,
        status__in=(Transactions.STATUS_WAITING, Transactions.STATUS_ACCEPTED)
    )

    for trans in transactions:
        trans.set_status(Transactions.STATUS_REJECTED)

        try:
            payment = Payment.objects.get(gearo_transaction=trans)
            logger.debug('Expired transaction #%s, released payment #%s.', trans.id, payment.id)
        except Payment.DoesNotExist:
            payment = None
            logger.error('Payment for transaction #%s does not exist.', trans.id)

        if payment:
            if payment.status in ['preauth', 'confirmed']:
                trans.reject()
            else:
                logger.error(
                    'Did not release payment #%s for transaction #%s due to its status - "%s".',
                    payment.id, trans.id, payment.status,
                )

        # send mail to ad owner
        mail.send(
            trans.lessor.email,
            'no-reply@gearo.de',
            template='forcereject',
            context={'firstname': trans.lessor.first_name, 'firstname_applicant': trans.applicant.first_name})

        # send mail to applicant
        mail.send(
            trans.applicant.email,
            'no-reply@gearo.de',
            template='transactionreject',
            context={'firstname': trans.applicant.first_name, 'firstname_lessor': trans.lessor.first_name})

        client = nexmo.Client(key=settings.NEXMO_API_KEY, secret=settings.NEXMO_API_SECRET)

        text = (
            '{} hat deine Anfrage leider abgelehnt. Deine Zahlung wird nicht '
            'eingezogen. Buche jetzt neues Equipment auf gearo.de'
        ).format(trans.lessor.first_name)
        gearo_user = trans.applicant.gearouser
        if gearo_user.phone_mobile and gearo_user.phone_verified:
            client.send_message({'from': 'Gearo', 'to': gearo_user.phone_mobile.raw_input, 'text': text})

import logging
from datetime import timedelta

from django.conf import settings
from django.template.loader import render_to_string
from django.utils import timezone

from gearo.celery import app
from gearo.helper import sms
from gearo.models import Transactions

logger = logging.getLogger(__name__)


@app.task
def send_sms(phone_number, text):
    sms.send_sms(phone_number, text)


@app.task
def send_response_limit_reminder():
    response_limit_range = (timezone.now() - timedelta(hours=3), timezone.now() - timedelta(hours=2))
    transactions = Transactions.objects.filter(
        response_limit__range=response_limit_range,
        status=Transactions.STATUS_WAITING,
        response_limit_reminder_sent_at__isnull=True,
    )
    for transaction in transactions:
        sms_text = render_to_string('gearo/advertisements/response_limit_reminder.txt', {'trans': transaction})

        mobile_phone = transaction.lessor.gearouser.phone_mobile.raw_input
        sms.send_sms(mobile_phone, sms_text)

        transaction.response_limit_reminder_sent_at = timezone.now()
        transaction.save()

        logger.debug(
            'Sent response limit reminder sms for transaction #%s, ad #%s to Ad lessor %s.',
            transaction.id, transaction.ad.id, transaction.lessor.email,
        )


@app.task
def send_response_limit_short_term_reminders():

    term = settings.GEARO_NOTIFICATION_SHORT_TERM

    range_start = timezone.now() + timedelta(hours=term)
    range_end = timezone.now() + timedelta(hours=term + 1)
    transactions = Transactions.objects.filter(
        response_limit__range=(range_start, range_end),
        status=Transactions.STATUS_WAITING,
        response_limit_reminder_short_sms_sent_at__isnull=True,
    )
    for transaction in transactions:
        sms_text = render_to_string(
            'gearo/advertisements/response_limit_short_term_reminder.txt', {'trans': transaction})
        mobile_phone = transaction.lessor.gearouser.phone_mobile.raw_input
        sms.send_sms(mobile_phone, sms_text)

        transaction.response_limit_reminder_short_sms_sent_at = (timezone.now())
        transaction.save()

        logger.debug(
            'Sent %s hours reminder sms for transaction #%s, ad #%s to Ad lessor %s.',
            term, transaction.id, transaction.ad.id, transaction.lessor.email,
        )

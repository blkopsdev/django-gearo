import logging

from django.contrib.auth.models import User
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from allauth.account.signals import user_signed_up
from allauth.socialaccount.signals import social_account_added
from mangopay.api import request_error, request_finished, request_started


from gearo.helper.emails import send_welcome_email
from gearo.models.gearo_profile import GearoProfile
from gearo.models.gearo_user import GearoUser
from gearo.models.notifications import Notifications
from gearo.models.user_payment import UserPayment
from gearo.search_indexes import AdvertisementIndex
from gearo_tasks.tasks import advertisement
from gearo_tasks.tasks.mangopay import create_mangopay_user_and_wallet
from gearo_tasks.tasks.notifications import send_notification
from gearo_tasks.tasks.usertasks import fetch_facebook_avatar

from .signals import advertisement_removed, advertisement_updated


logger = logging.getLogger('mangopay_calls')


@receiver(request_started)
def log_started_requests(sender, url, data, headers, method):
    logger.debug('%s | %s | %s | %s', method, url, data, headers)


@receiver(request_finished)
def log_finished_requests(sender, url, data, headers, method, result, laps):
    logger.info('%s | %s | %s | %s | %s', method, result, url, data, headers)


@receiver(request_error)
def log_error_requests(sender, url, status_code, headers):
    logger.error('%s | %s | %s', status_code, url, headers)


@receiver(post_save, sender=Notifications)
def send_add_notifications(sender, **kwargs):
    send_notification.delay(kwargs['instance'].id)


@receiver(post_delete, sender=Notifications)
def send_post_delete_notifications(sender, **kwargs):
    send_notification.delay(kwargs['instance'].id, notification_type='remove')


@receiver(advertisement_updated)
def update_advertisement(instance, **kwargs):
    advertisement.update_advertisement.delay(instance.id)


@receiver(advertisement_removed)
def remove_advertisement(instance, **kwargs):
    AdvertisementIndex().remove_object(instance=instance)


@receiver(post_save, sender=User)
def create_user_account_and_profile(instance, **kwargs):
    if kwargs.get('created', None):
        GearoProfile.objects.create(user=instance)
        GearoUser.objects.create(user=instance)
        UserPayment.objects.create(user=instance)


@receiver(user_signed_up)
def set_facebook_verification_after_signup(request, user, sociallogin, **kwargs):
    if not user.gearouser.email_verified:
        send_welcome_email(user)
    user.gearouser.facebook_verified = True
    user.gearouser.save()
    fetch_facebook_avatar.delay(sociallogin.account.id)


@receiver(social_account_added)
def set_facebook_verification_after_connect(request, sociallogin, **kwargs):
    gearo_user = sociallogin.user.gearouser
    gearo_user.facebook_verified = True
    gearo_user.save()


@receiver(post_save, sender=UserPayment)
def create_mp_user_and_wallet(instance, **kwargs):
    if not instance.is_mangopay_user_id_existing:
        create_mangopay_user_and_wallet.delay(instance.user.id)

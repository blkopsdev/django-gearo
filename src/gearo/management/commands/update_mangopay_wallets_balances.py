from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

import mangopay
from mangopay.api import APIRequest

from gearo.helper.mangopay import get_mangopay_wallet, update_platform_fee_wallet_balance
from gearo_tasks.tasks.mangopay import update_mp_wallet_balance

mangopay.client_id = settings.MANGOPAY_CLIENT_ID
mangopay.passphrase = settings.MANGOPAY_PASSPHRASE

handler = APIRequest(sandbox=True)


class Command(BaseCommand):
    help = "Updates balances of wallets for users that have wallets"

    def handle(self, *args, **options):

        # Counters to show in output
        all_users = User.objects.all()
        all_users_count = all_users.count()
        updated_mp_wallets_balances_count = 0

        for user in all_users:
            # `try-except` used for cases when instances of `GearoUser` or `UserPayment` do not exist
            try:
                mangopay_wallet = get_mangopay_wallet(user)
                if mangopay_wallet is not None:
                    update_mp_wallet_balance(user.id)
                    updated_mp_wallets_balances_count += 1

            except ObjectDoesNotExist:
                pass

        update_platform_fee_wallet_balance()

        self.stdout.write(self.style.SUCCESS('Number of all registered users: %s' % all_users_count))
        self.stdout.write(self.style.SUCCESS('Number of updated wallets: %s' % updated_mp_wallets_balances_count))

from django.core.management.base import BaseCommand

from gearo.models import GearoUser


class Command(BaseCommand):
    help = "Set `phone_verified` to `False` if phone field is not filled."

    def handle(self, *args, **options):

        users_with_verified_phones = GearoUser.objects.filter(phone_verified=True)
        number_of_fixes = 0

        for user in users_with_verified_phones:
            if not user.phone_mobile:
                user.phone_verified = False
                user.phone_verified_status = user.STATUS_NONE
                user.save()

                number_of_fixes += 1

                self.stdout.write(self.style.SUCCESS('Phone verification was disabled for user: %s' % user.user.email))

        self.stdout.write(self.style.SUCCESS('Total number of fixes was made: %s' % number_of_fixes))

from django.core.management.base import BaseCommand

from easy_thumbnails.files import generate_all_aliases

from gearo.models.attachments import Attachments


class Command(BaseCommand):
    help = "Genarates thumbnails for all aliases for all Ad attachments."

    def handle(self, *args, **options):
        for attachment in Attachments.objects.exclude(ad__isnull=True):
            generate_all_aliases(attachment.image, include_global=True)
            print('Generated thumbnails for attachment #%s, Ad #%s' % (attachment.id, attachment.ad.id))

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

import mangopay
from mangopay.api import APIRequest

from gearo.helper.mangopay import create_mangopay_user, create_mangopay_wallet

mangopay.sandbox = settings.MANGOPAY_USE_SANDBOX
mangopay.client_id = settings.MANGOPAY_CLIENT_ID
mangopay.passphrase = settings.MANGOPAY_PASSPHRASE

handler = APIRequest(sandbox=settings.MANGOPAY_USE_SANDBOX)


class Command(BaseCommand):
    help = "Provides mangopay user primary keys for users that do not have them."

    def add_arguments(self, parser):
        parser.add_argument('initial_users_qty', type=int)

    def handle(self, *args, **options):

        all_users = User.objects.all().order_by('-last_login')
        counter = options.get('initial_users_qty', 20)
        users = []
        for user in all_users:
            try:
                if not user.billing_info.is_mangopay_user_id_existing:
                    users.append(user)
                    counter -= 1
                if counter == 0:
                    break
            except User.billing_info.RelatedObjectDoesNotExist:
                self.stdout.write(self.style.ERROR('User "%s" does not have "billing_info"' % user))

        created_mp_users_count = 0
        created_natural_users_count = 0
        created_legal_users_count = 0
        created_wallets_count = 0

        for user in users:
            try:
                create_mangopay_user(user)
                if user.gearouser.is_business:  # user is LegalUser (represents a business or an organization)
                    created_legal_users_count += 1
                    created_mp_users_count += 1
                    self.stdout.write(self.style.SUCCESS('For user "%s" was created MP LegalUser' % user))
                else:  # user is NaturalUser (represents a person)
                    created_natural_users_count += 1
                    created_mp_users_count += 1
                    self.stdout.write(self.style.SUCCESS('For user "%s" was created MP NaturalUser' % user))
            except Exception as e:
                self.stdout.write(self.style.ERROR(
                    'For user "%s" MP user was not created. Reason: %s' % (user, e)))

            if user.billing_info.is_mangopay_user_id_existing and \
                    not user.billing_info.is_mangopay_user_wallet_id_existing:
                try:
                    create_mangopay_wallet(user)
                    created_wallets_count += 1
                    self.stdout.write(self.style.SUCCESS('For user "%s" was created MP Wallet' % user))
                except Exception as e:
                    self.stdout.write(self.style.ERROR(
                        'For user "%s" MP wallet was not created. Reason: %s' % (user, e)))

        self.stdout.write(self.style.SUCCESS('Number of all checked users: %s' % len(users)))
        self.stdout.write(self.style.SUCCESS('Number of created MP users: %s' % created_mp_users_count))
        self.stdout.write(self.style.SUCCESS('Number of created NaturalUsers: %s' % created_natural_users_count))
        self.stdout.write(self.style.SUCCESS('Number of created LegalUsers: %s' % created_legal_users_count))
        self.stdout.write(self.style.SUCCESS('Number of created Wallets: %s' % created_wallets_count))

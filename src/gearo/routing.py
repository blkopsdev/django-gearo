from channels import route_class

from .consumers import ChatConsumer, NotificationConsumer

channel_routing = [
    route_class(ChatConsumer, path=r"^/ws/chat/"),
    route_class(NotificationConsumer, path=r"^/ws/notification/"),
]

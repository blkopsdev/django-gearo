"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.conf import settings
from django.core.urlresolvers import reverse

from haystack import indexes
from haystack.exceptions import SkipDocument

from gearo.models.advertisement import Advertisement
from gearo.models.gearo_profile import GearoProfile


class AdvertisementIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    location = indexes.LocationField(model_attr='get_location')
    title = indexes.EdgeNgramField(model_attr="title")
    tags = indexes.MultiValueField()
    description = indexes.EdgeNgramField(model_attr="description")
    ranking = indexes.FloatField(model_attr="ranking")
    category = indexes.CharField()
    not_available_dates = indexes.MultiValueField(model_attr="not_available_dates")
    hired_dates = indexes.MultiValueField(model_attr="hired_dates")
    thumbnail_url = indexes.CharField(model_attr="thumbnail_url")
    absolute_url = indexes.CharField(model_attr="get_absolute_url")
    profile_url = indexes.CharField()
    profile_avatar_url = indexes.CharField()
    priceday = indexes.IntegerField()
    lat = indexes.FloatField(model_attr='lat')
    lng = indexes.FloatField(model_attr='lng')
    include_mwst = indexes.BooleanField()

    def get_model(self):
        return Advertisement
    
    def prepare_category(self, obj):
        return obj.category.name

    def prepare_tags(self, obj):
        return list(obj.tags.names())

    def prepare_profile_url(self, obj):
        return reverse('publicprofil', args=(obj.owner.username,))

    def prepare_profile_avatar_url(self, obj):
        try:
            return obj.owner.profile.avatar_thumbnail_url
        except GearoProfile.DoesNotExist:
            pass
        return settings.AVATAR_PLACEHOLDER

    def prepare_priceday(self, obj):
        return float("%.0f" % obj.priceday)

    def prepare_include_mwst(self, obj):
        return getattr(obj, 'include_mwst', None)

    def index_queryset(self, using=None):
        queryset = super(AdvertisementIndex, self).index_queryset(using=using)
        return queryset.filter(is_removed=False)

    def read_queryset(self, using=None):
        queryset = super(AdvertisementIndex, self).read_queryset(using=using)
        return queryset.filter(is_removed=False)

    def build_queryset(self, *args, **kwargs):
        queryset = super(AdvertisementIndex, self).build_queryset(*args, **kwargs)
        return queryset.filter(is_removed=False)

    def prepare(self, obj):
        if obj.is_removed:
            raise SkipDocument
        return super(AdvertisementIndex, self).prepare(obj)

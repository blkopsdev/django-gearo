from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class GearoConfig(AppConfig):
    label = 'gearo'
    name = 'gearo'
    verbose_name = _('Gearo')

    def ready(self):
        from . import receivers

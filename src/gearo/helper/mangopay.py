import base64
import json
import logging
import os
import tempfile
from datetime import date

from django.conf import settings
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from dateutil.parser import parse
from mangopay.constants import (
    BANK_ACCOUNT_TYPE_CHOICES, DIRECT_DEBIT_TYPE_CHOICES, DOCUMENTS_TYPE_CHOICES, LEGAL_USER_TYPE_CHOICES,
    STATUS_CHOICES
)
from mangopay.exceptions import APIError
from mangopay.fields import CharField
from mangopay.query import InsertQuery, SelectQuery
from mangopay.resources import (
    BankAccount, BankWirePayOut, CardRegistration, ClientWallet,
    DirectDebitWebPayIn, LegalUser, Money, NaturalUser, PreAuthorization, Wallet
)
from mangopay.utils import Address
from slugify import slugify

from gearo.models.gearo_user import GearoUser
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)

# Because PayOut could fail after it’s passed to "SUCCEEDED" we add one new status for this case
GEARO_STATUS_CHOICES = STATUS_CHOICES + (('SUCCEEDED_FAILED', 'succeeded_failed', 'Succeeded_Failed'),)
FAILED_STATUSES = (GEARO_STATUS_CHOICES.failed, GEARO_STATUS_CHOICES.succeeded_failed)

GEARO_DIRECT_DEBIT_TYPE_CHOICES = DIRECT_DEBIT_TYPE_CHOICES + ('PAYPAL', 'paypal', 'Paypal')

BUSINESSES = [business[0] for business in GearoUser.BUSINESSES]

KYC_DOCUMENTS = {
    'identity_proof': DOCUMENTS_TYPE_CHOICES.identity_proof,
    'registration_proof': DOCUMENTS_TYPE_CHOICES.registration_proof,
    'articles_of_association': DOCUMENTS_TYPE_CHOICES.articles_of_association,
    'shareholder_declaration': DOCUMENTS_TYPE_CHOICES.shareholder_declaration,
}

BUSINESS_DOCUMENTS_FIELDS_NAMES = ['registration_proof', 'articles_of_association', 'shareholder_declaration']


class PayPalPayIn(DirectDebitWebPayIn):
    direct_debit_type = CharField(api_name='DirectDebitType', default=GEARO_DIRECT_DEBIT_TYPE_CHOICES.paypal)

    class Meta:
        url = {
            InsertQuery.identifier: '/payins/paypal/web',
            SelectQuery.identifier: '/payins'
        }


WEB_PAYIN_MAPPING = {
    'mangopay_sofort': {'class': DirectDebitWebPayIn, 'mp_payment_variant': GEARO_DIRECT_DEBIT_TYPE_CHOICES.sofort},
    'mangopay_paypal': {'class': PayPalPayIn, 'mp_payment_variant': GEARO_DIRECT_DEBIT_TYPE_CHOICES.paypal},
}


def get_platform_fee_wallet_balance():
    """
    Get cached GEARO FEES wallet balance in the following form: "EUR xx.xx"
    """
    fee_wallet_balance = cache.get('fee_wallet_balance')
    if fee_wallet_balance is not None:
        return fee_wallet_balance

    update_platform_fee_wallet_balance()


def update_platform_fee_wallet_balance():
    """
    Set to cache GEARO FEES wallet balance in the following form: "EUR xx.xx"
    """
    timeout = settings.MANGOPAY_GEARO_WALLET_BALANCE_CACHE_TIMEOUT
    try:
        wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        fee_wallet_balance = wallet.balance / 100
        cache.set('fee_wallet_balance', fee_wallet_balance, timeout)
    except APIError:
        pass


def create_mangopay_legal_user(user):
    # LegalUser represents a business or an organization

    gearo_user = user.gearouser
    address_info = gearo_user.get_bill_address()

    legal_user = LegalUser(
        name=address_info['business_name'] or "Firmenname",
        legal_person_type=LEGAL_USER_TYPE_CHOICES.business,
        headquarters_address=get_headquarters_address(user),
        legal_representative_first_name=address_info['first_name'] or user.first_name,
        legal_representative_last_name=address_info['last_name'] or user.last_name,
        legal_representative_address=get_user_address(user),
        legal_representative_birthday=user.billing_info.birthday or date(1970, 1, 1),
        legal_representative_nationality=user.billing_info.nationality.code or 'DE',
        legal_representative_country_of_residence=user.billing_info.country_of_residence.code or 'DE',
        person_type='LEGAL',
        email=user.email,
        tag='Inner user id - {}'.format(user.id),
    )

    legal_user.save()

    logger.debug('Created Mangopay Legal User with id - %s, (user "%s")', legal_user.get_pk(), user.email)

    return legal_user


def create_mangopay_natural_user(user):
    # NaturalUser represents a person

    natural_user = NaturalUser(
        first_name=user.first_name,
        last_name=user.last_name,
        address=get_user_address(user),
        birthday=user.billing_info.birthday or date(1970, 1, 1),
        nationality=user.billing_info.nationality.code or 'DE',
        country_of_residence=user.billing_info.country_of_residence.code or 'DE',
        person_type='NATURAL',
        email=user.email,
        income_range='6',
        tag='Inner user id - {}'.format(user.id),
    )

    natural_user.save()

    logger.debug('Created Mangopay Natural User with id - %s, (user "%s")', natural_user.get_pk(), user.email)

    return natural_user


def create_mangopay_user(user):
    if user.gearouser.is_business:
        new_mp_user = create_mangopay_legal_user(user)
        user.billing_info.mangopay_l_user_id = new_mp_user.get_pk()
    else:
        new_mp_user = create_mangopay_natural_user(user)
        user.billing_info.mangopay_n_user_id = new_mp_user.get_pk()

    user.billing_info.save()

    return new_mp_user


def create_mangopay_wallet(user):
    mp_user = get_mangopay_user(user)

    wallet = Wallet(
        owners=[mp_user],
        description='Wallet of {} {}'.format(user.first_name, user.last_name),
        currency='EUR',
        tag='Inner user id - {}'.format(user.id),
    )

    wallet.save()

    if user.gearouser.is_business:
        user.billing_info.mangopay_l_user_wallet_id = wallet.id
        user.billing_info.save()

        logger.debug(
            'Created Mangopay Wallet with id - %s for Mangopay Legal User with id - %s (user "%s")',
            wallet.id, mp_user.id, user.email,
        )

    else:
        user.billing_info.mangopay_n_user_wallet_id = wallet.id
        user.billing_info.save()

        logger.debug(
            'Created Mangopay Wallet with id - %s for Mangopay Natural User with id - %s (user "%s")',
            wallet.id, mp_user.id, user.email,
        )

    return wallet


def create_mangopay_user_bank_account(user, data=None):
    mp_user = get_mangopay_user(user)
    billing_info = user.billing_info

    if data is None:
        data = {
            'first_name': billing_info.first_name, 'last_name': billing_info.last_name,
            'iban': billing_info.iban, 'bic': billing_info.bic,
        }

    bank_account = BankAccount(
        user=mp_user,
        owner_name='{} {}'.format(data['first_name'], data['last_name']),
        owner_address=get_user_address(user),
        type=BANK_ACCOUNT_TYPE_CHOICES.iban,
        iban=data['iban'],
        bic=data['bic'],
    )
    try:
        bank_account.save()
    except APIError as e:
        exception_data = json.loads(str(e))
        errors = exception_data['errors']
        errors_str = json.dumps(errors)
        billing_info.mangopay_bank_account_error = errors_str
        billing_info.save()
        return errors_str

    user.billing_info.mangopay_bank_account_error = None
    user.billing_info.save()

    bank_account_id = bank_account.get_pk()
    if user.gearouser.is_business:
        user.billing_info.mangopay_l_user_bank_account_id = bank_account_id
        user.billing_info.save()

        logger.debug(
            'Created Mangopay Bank Account IBAN with id - %s for Mangopay Legal User with id - %s (user "%s")',
            bank_account_id, mp_user.id, user.email,
        )

    else:
        user.billing_info.mangopay_n_user_bank_account_id = bank_account.get_pk()
        user.billing_info.save()

        logger.debug(
            'Created Mangopay Bank Account IBAN with id - %s for Mangopay Natural User with id - %s (user "%s")',
            bank_account_id, mp_user.id, user.email,
        )

    return bank_account


def update_mp_bank_account_info(user, data=None):
    """
    Mangopay BankAccount cannot be "updated". So if user change his/her bank details, we need to deactivate previous
    Mangopay BankAccount and create new with new bank details.

    If user did not have Mangopay BankAccount, it will be created.

    Mangopay Note: Once deactivated, a bank account can't be reactivated afterwards.
    """
    previous_bank_account = get_mangopay_user_bank_account(user)
    if previous_bank_account is not None:
        previous_bank_account.deactivate()
        user.billing_info.mangopay_n_user_bank_account_id = None
        user.billing_info.mangopay_l_user_bank_account_id = None
        user.billing_info.save()

        logger.debug(
            'Deactivated Mangopay Bank Account IBAN with id - %s for Mangopay User with id - %s (user "%s")',
            previous_bank_account.get_pk(), previous_bank_account.user.id, user.email,
        )

    return create_mangopay_user_bank_account(user, data)


def get_mangopay_user(user):
    if user.gearouser.is_business and user.billing_info.mangopay_l_user_id:
        return LegalUser.get(user.billing_info.mangopay_l_user_id)
    elif not user.gearouser.is_business and user.billing_info.mangopay_n_user_id:
        return NaturalUser.get(user.billing_info.mangopay_n_user_id)


def get_mangopay_user_by_id(user, mangopay_user_id):
    if user.billing_info.mangopay_l_user_id == mangopay_user_id:
        return LegalUser.get(mangopay_user_id)
    else:
        return NaturalUser.get(mangopay_user_id)


def get_mangopay_wallet(user):
    if user.gearouser.is_business and user.billing_info.mangopay_l_user_wallet_id:
        return Wallet.get(user.billing_info.mangopay_l_user_wallet_id)
    elif not user.gearouser.is_business and user.billing_info.mangopay_n_user_wallet_id:
        return Wallet.get(user.billing_info.mangopay_n_user_wallet_id)


def get_mangopay_user_bank_account(user):
    l_user_id = user.billing_info.mangopay_l_user_id
    l_bank_account_id = user.billing_info.mangopay_l_user_bank_account_id
    n_user_id = user.billing_info.mangopay_n_user_id
    n_bank_account_id = user.billing_info.mangopay_n_user_bank_account_id
    if user.gearouser.is_business and l_user_id and l_bank_account_id:
        return BankAccount.get(reference=l_bank_account_id, user_id=l_user_id)
    elif not user.gearouser.is_business and n_user_id and n_bank_account_id:
        return BankAccount.get(reference=n_bank_account_id, user_id=n_user_id)


def update_mangopay_user_info(user, params_for_update):
    mp_user = get_mangopay_user(user)
    is_user_business = user.gearouser.is_business

    if 'email' in params_for_update:
        # email field has the same name for both Natural and Legal users
        mp_user.email = params_for_update['email']

    if 'birthday' in params_for_update:
        birthday = parse(params_for_update['birthday']).date()
        if is_user_business:
            mp_user.legal_representative_birthday = birthday
        else:
            mp_user.birthday = birthday

    if 'nationality' in params_for_update:
        if is_user_business:
            mp_user.legal_representative_nationality = params_for_update['nationality']
        else:
            mp_user.nationality = params_for_update['nationality']

    if 'country_of_residence' in params_for_update:
        if is_user_business:
            mp_user.legal_representative_country_of_residence = params_for_update['country_of_residence']
        else:
            mp_user.country_of_residence = params_for_update['country_of_residence']

    if 'business_name' in params_for_update and is_user_business:
        mp_user.name = params_for_update['business_name']

    if 'address_changed' in params_for_update:
        if is_user_business:
            mp_user.legal_representative_address = get_user_address(user)
        else:
            mp_user.address = get_user_address(user)

    if 'business_address_changed' in params_for_update and is_user_business:
        mp_user.headquarters_address = get_headquarters_address(user)

    if 'business_type' in params_for_update and is_user_business:
        if params_for_update['business_type'] in BUSINESSES:
            legal_person_type = LEGAL_USER_TYPE_CHOICES.business
        else:
            legal_person_type = LEGAL_USER_TYPE_CHOICES.soletrader
        mp_user.legal_person_type = legal_person_type

    mp_user.save()

    logger.debug(
        'For Mangopay User with id - %s (user "%s"), were changed params: %s',
        mp_user.get_pk(), user.email, params_for_update,
    )

    return mp_user


def get_card_pre_registration_data(user):
    mp_user = get_mangopay_user(user)
    card_registration = CardRegistration(user=mp_user, currency='EUR')
    card_registration.save()
    data = {
        'mp_client_id': settings.MANGOPAY_CLIENT_ID,
        'mp_base_url': settings.MANGOPAY_BASE_URL,
        'access_key': card_registration.access_key,
        'preregistration_data': card_registration.preregistration_data,
        'card_registration_url': card_registration.card_registration_url,
        'card_registration_id': card_registration.get_pk(),
        'card_type': 'CB_VISA_MASTERCARD',
    }

    logger.debug(
        'Created Mangopay Card Registration with id - %s for Mangopay User with id - %s (user "%s")',
        card_registration.get_pk(), mp_user.get_pk(), user.email,
    )

    return data


def get_preauthorization(preauthorization_id):
    try:
        preauthorization = PreAuthorization.get(preauthorization_id)
    except Exception as e:
        logger.debug('Error during getting Mangopay Card PreAuthorization - %s', e)
        raise

    return preauthorization


def money_to_cents(money):
    return int(money * 100)


def encode_file_to_base64_str(document_file):
    return base64.b64encode(document_file.read())


def create_temp_file(document_file):
    with tempfile.NamedTemporaryFile(delete=False) as f:
        for chunk in document_file.chunks():
            f.write(chunk)
    return f.name


def remove_temp_files(list_of_file_names):
    for file_name in list_of_file_names:
        if os.path.exists(file_name):
            os.remove(file_name)


def get_mp_document(user, document_type):
    """
    Return the last instance of `GearoUserDocument` related to user.
    The last because ID proof document can be "REFUSED" by Mangopay
    and in this case new ID proof document can be added and sent for verification.

    We will keep in DB all instances of `GearoUserDocument` (maybe for cases of disputes),
    but should return always just the last one.
    """
    documents = user.documents
    return documents.filter(document_type=document_type).last()


def get_user_address(user):
    gearo_user = user.gearouser

    postal_code = allowed_zip_code(gearo_user.zip) if gearo_user.zip else '11111'
    user_address = Address(
        address_line_1=gearo_user.street or 'AddressLine1',
        city=gearo_user.place or 'City',
        postal_code=postal_code,
        country=gearo_user.country.code or 'DE',
    )
    return user_address


def get_headquarters_address(user):
    gearo_user = user.gearouser
    address = gearo_user.get_bill_address()

    headquarters_postal_code = allowed_zip_code(address['zip']) if address['zip'] else '11111'
    headquarters_address = Address(
        address_line_1=address['street'] or 'AddressLine1',
        city=address['place'] or 'City',
        postal_code=headquarters_postal_code,
        country=address['country_code'],
    )
    return headquarters_address


def allowed_zip_code(zip_code):
    """
    Return zip code converted to allowed characters (alphanumeric, spaces and dashes)

    `slugify` will remove all "_" (underscore), so firstly we should convert them to " " (space)
    """
    zip_code = zip_code.replace('_', ' ')
    return slugify(zip_code, ok='-', lower=False, spaces=True, only_ascii=True, )


def make_payout(transaction):
    """
    Make Mangopay PayOut to Bank Account that lessor indicated in "Bank information" page.

    """
    lessor = transaction.lessor
    mp_user = get_mangopay_user(lessor)
    mp_wallet = get_mangopay_wallet(lessor)
    debited_funds = money_to_cents(transaction.payout)

    bank_account = get_mangopay_user_bank_account(lessor)

    payout = BankWirePayOut(
        author=mp_user,
        debited_funds=Money(amount=debited_funds, currency='EUR'),
        fees=Money(amount='0', currency='EUR'),
        debited_wallet=mp_wallet,
        bank_account=bank_account,
        bank_wire_ref="Payment for rent of {} for {} day(s)".format(transaction.ad, transaction.calculated_duration),
    )

    payout.save()
    payout_id = payout.get_pk()
    payout_status = payout.status

    logger.debug(
        'Created Mangopay PayOut with id - %s, with status - %s, for transaction id - %s (for lessor "%s")',
        payout_id, payout_status, transaction.id, lessor.email,
    )

    return payout


def get_gearo_payout(transaction):
    """
    Return the last instance of `GearoPayOut` related to user.
    The last because PayOut can be "FAILED" (or "SUCCEEDED" and later "FAILED")
    and in this case new PayOut can created.

    We will keep in DB all instances of `GearoPayOut` (maybe for cases of disputes),
    but should return always just the last one.
    """
    return transaction.payouts.last()


def get_gearo_payout_status(transaction):
    payout_button_html = format_html(
        '<a class="button" style="background: {button_color};" href="{url}">Make PayOut</a>',
        button_color='#0eaf9c', url=reverse('mangopay_payout', args=(transaction.id,)),
    )
    break_html = format_html("<br><br>")

    gearo_payout = get_gearo_payout(transaction)
    if gearo_payout is not None:
        if gearo_payout.payout_status in FAILED_STATUSES:
            return format_html(gearo_payout.payout_status.upper()) + break_html + payout_button_html
        else:
            return format_html(gearo_payout.payout_status.upper())

    elif transaction.status == Transactions.STATUS_FINISHED:
        billing_info = transaction.lessor.billing_info
        if not billing_info.is_mangopay_bank_account_id_existing:
            return format_html('<div style="color: red;">{}</div>', 'NO MP BANK ACCOUNT')
        else:
            return payout_button_html

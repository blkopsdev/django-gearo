"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from datetime import date, datetime, timedelta

from dateutil import parser


def alldays(startdate, enddate):
    """return all dates between dates in a list, maybe disadvantageous written - too much pain killer"""
    semilist = list()
    if isinstance(startdate, str) and isinstance(enddate, str):
        d1 = parser.parse(startdate)
        d2 = parser.parse(enddate)
    elif isinstance(startdate, date) and isinstance(enddate, date):
        d1 = startdate
        d2 = enddate
    else:
        raise ValueError("Start and end date have to be an date object or an string in format " + dateformat)

    delta = d2 - d1

    for i in range(delta.days + 1):
        semilist.append(datetime.strftime(d1 + timedelta(days=i), "%d.%m.%Y"))
        
    return semilist

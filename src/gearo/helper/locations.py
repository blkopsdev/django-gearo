from django.conf import settings

import googlemaps

gmaps_client = googlemaps.Client(key=settings.GOOGLE_MAPS_KEY)


def get_locality(address_components):
    for component in address_components:
        if 'locality' in component['types']:
            return component['long_name']
    return


def get_location(lat, lng):
    locations = gmaps_client.reverse_geocode((lat, lng), )
    if len(locations) > 0:
        location = locations[0]
        pickup_location = get_locality(location['address_components'])
        if not location:
            pickup_location = location['formatted_address']
        return pickup_location

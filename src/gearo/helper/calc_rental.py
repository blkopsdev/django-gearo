from decimal import Decimal as D
from decimal import getcontext

from constance import config

getcontext().prec = 10


def calc_rental(duration, ad, discount=None):

    country_code = ad.owner.gearouser.country_code
    priceday = D(ad.priceday)
    # Sometimes user can indicate just price per day and in this case `pricethree` and `priceweek` will be empty (None)
    # and we will not be able to convert them to `Decimal`
    pricethree = D(ad.pricethree) if ad.pricethree is not None else None
    priceweek = D(ad.priceweek) if ad.priceweek is not None else None

    vat_rate_key = 'VAT_RATE_{}'.format(country_code)
    vat_rate = D(getattr(config, vat_rate_key))

    if duration in range(3, 7) and pricethree is not None:
        debt = D((pricethree / D('3')) * int(duration))
        long_term_benefit = D(duration * priceday) - ((pricethree / D('3')) * duration)
    elif duration >= 7 and priceweek is not None:
        debt = D((priceweek / D('7')) * int(duration))
        long_term_benefit = D(duration * priceday) - ((priceweek / D('7')) * duration)
    else:
        debt = D(priceday * int(duration))
        long_term_benefit = D('0')

    if discount is not None:
        debt = D(debt / D('100') * (D('100') - int(discount)))

    netto = debt / (D('100') + vat_rate) * D('100')
    vat = debt - netto
    daily_rent_price = D(priceday * duration)

    return {
        "netto": netto,
        "vat": vat,
        "brutto": debt,
        "duration": duration,
        "long_term_benefit": long_term_benefit,
        "daily_rent_price": daily_rent_price,
    }

from decimal import Decimal as D
from decimal import getcontext
from math import ceil

from django.conf import settings

from constance import config

getcontext().prec = 10


def calc_insurance(duration=1, ad=None):

    country_code = ad.owner.gearouser.country_code

    ins_rate = D(config.INSURANCE_RATE)
    first_rate = D(config.FIRST_RATE)
    second_rate = D(config.SECOND_RATE)
    vat_rate_de = D(config.VAT_RATE_DE)
    vat_rate_at = D(config.VAT_RATE_AT)
    ins_commission_rate = D(config.INSURANCE_COMMISSION)
    ins_addition = D(config.ADDITION_TO_INSURANCE_COMMISSION)

    ins = D('0')
    ins_sum = D('0')
    ins_commission = D('0')
    ins_commission_vat = D('0')
    ins_commission_sum = D('0')
    ins_debt = D('0')
    ins_debt_sum = D('0')

    if ad.category.is_insured:
        value = D(ad.articleofvalue)
        # Insurance calculates per 15 days which gives 24
        # (12 months each divided into 2 weeks)
        m = 24
        ins = value * ins_rate / D('100') / m
        ins_sum = ins + (ins * vat_rate_de / D('100'))  # insurance + VAT in DE

        ins_debt = (ins + value * ins_commission_rate / D('100') / m) * D('1.19')

        if duration > 1:
            for i in range(2, (duration + 1)):
                if i in range(2, 11):
                    ins_debt += ins_debt * (first_rate / D('100'))
                else:
                    ins_debt += ins_debt * (second_rate / D('100'))

        # Insurance valid for 15 days,
        # need to prolonged if duration is more than month.
        if duration > settings.INSURANCE_DURATION:
            factor = ceil(duration / settings.INSURANCE_DURATION)
            ins_sum *= factor
            ins *= factor

        ins_commission = ins_debt - ins_sum + ins_addition
        ins_commission_vat = ins_commission * vat_rate_de / D('100')
        ins_commission_sum = ins_commission_vat + ins_commission
        ins_debt_sum_base = ins_debt + ins_commission_vat

        if country_code == "AT":
            # For lessor from Austria ins_debt_sum
            # should be increased by the amount of VAT in Austria
            ins_debt_sum_base += ins_debt_sum_base * (vat_rate_at / D('100'))

        ins_debt_sum = ins_debt_sum_base + ins_addition

    return {
        "insurance": ins,
        "insurance_vat": ins * vat_rate_de / D('100'),
        "insurance_sum": ins_sum,
        "insurance_debt": ins_debt,
        "insurance_commission": ins_commission,
        "insurance_commission_vat": ins_commission_vat,
        "insurance_commission_sum": ins_commission_sum,
        "insurance_debt_sum": ins_debt_sum,
    }

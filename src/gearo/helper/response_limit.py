from datetime import datetime, time, timedelta


def round_datetime_to_nearest_30_minutes(dt, delta=timedelta(minutes=30)):
    return dt + (datetime.min - dt) % delta


def get_response_limit(rent_start_date):
    """
    Calculates and returns default response limit (`datetime`)
    """

    # Convert `date` object to `datetime` object
    rent_start_datetime = datetime.combine(rent_start_date, datetime.min.time())

    if rent_start_date == datetime.today().date():
        response_limit = datetime.now() + timedelta(hours=3)
    else:
        # Half the difference between time inquiry was made and time the rent will start (in hours)
        half_diff = (rent_start_datetime - datetime.now()) / timedelta(hours=1) / 2
        response_limit = datetime.now() + timedelta(hours=half_diff)
        # if calculated time earlier than 10:00, we set it to 10:00
        if response_limit.time() < time(10, 0, 0, 0):
            response_limit = response_limit.replace(hour=10, minute=0, second=0, microsecond=0)
        # if calculated time later than 20:00, we set it to 21:00
        if response_limit.time() > time(20, 0, 0, 0):
            response_limit = response_limit.replace(hour=21, minute=0, second=0, microsecond=0)

    rounded_response_limit = round_datetime_to_nearest_30_minutes(response_limit)

    return rounded_response_limit

from decimal import Decimal as D
from decimal import getcontext

from constance import config

getcontext().prec = 10


def calc_transaction_data(rental, insurance, ad):

    vat_rate_de = D(config.VAT_RATE_DE)
    vat_rate_at = D(config.VAT_RATE_AT)

    ins = insurance

    country_code = ad.owner.gearouser.country_code

    if country_code == 'DE':
        # If lessor from Germany (DE)
        # next variables will be calculated as previously
        insurance_profit = ins["insurance"] + ins["insurance_commission"]
        netto_total = rental["netto"] + insurance_profit
        vat = netto_total * vat_rate_de / D('100')

    else:
        # If lessor from Austria (AT), we will use existed fields in
        # Transactions model, but make special calculations for them

        # insurance_price_netto (in tables)
        insurance_profit = (ins["insurance_debt_sum"] / (D('100') + vat_rate_at) * D('100'))
        # netto_total_special (in tables)
        netto_total = rental["netto"] + insurance_profit
        # vat_special (in tables)
        vat = netto_total * vat_rate_at / D('100')

    total = netto_total + vat  # total_special for lessor from Austria (in tables)
    vat_special = ins["insurance_commission_vat"] + rental["vat"]
    netto_total_special = ins["insurance_commission"] + rental["netto"]
    subgoal = ins["insurance_commission"] + rental["netto"] + ins["insurance_commission_vat"] + rental["vat"]

    return {
        'vat_special': vat_special,
        'netto_total_special': netto_total_special,
        'subgoal': subgoal,
        'insurance_profit': insurance_profit,
        'netto_total': netto_total,
        'vat': vat,
        'total': total,
    }

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
import re

from django.contrib.auth.models import User


def generateusername(firstname=None, lastname=None):
    # remove all non alphabet chars
    regex = re.compile('[^a-zA-Z]')
    firstname = regex.sub('', firstname)
    lastname = regex.sub('', lastname)
    
    username = firstname+lastname[0]
    if User.objects.filter(username=username).exists():
        iteration = 1
        username += str(iteration)
        while User.objects.filter(username=username).exists():
            iteration += 1
            username += str(iteration)
        
    return username

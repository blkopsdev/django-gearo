"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from gearo.models.advertisement import Advertisement


def get_random_ads(num):
    return Advertisement.objects.filter(is_removed=False).order_by('?')[:num]

import copy

from django.core.exceptions import ObjectDoesNotExist

from gearo.helper.csv_stream import StreamCSV


class CSVWriter(object):

    def __init__(self, queryset, headers, fields, special_fields=None):
        self.queryset = queryset
        self.headers = headers
        self.fields = fields
        self.special_fields = special_fields

    def get_rows(self):
        values_fields = set(self.fields)
        if self.special_fields:
            values_fields -= set(self.special_fields)
        data_queryset = copy.deepcopy(self.queryset).values(*list(values_fields))
        data = {}
        for row in data_queryset:
            data[row['id']] = row

        for obj in self.queryset:
            row_values = []
            row = data[obj.id]
            for field_name in self.fields:
                if field_name in row:
                    row_values.append(row[field_name])
                else:
                    if field_name in self.special_fields:
                        csv_writer_method = getattr(self, field_name)
                        row_values.append(csv_writer_method(obj=obj))
            yield row_values

    def get_csv(self):
        gearo_csv = StreamCSV()
        gearo_csv.heading = [self.headers[f] for f in self.fields]
        gearo_csv.data_generator = self.get_rows()
        return gearo_csv

    def lessor_is_business(self, obj):
        # because instance of `GearoUser` (related to instance of `User`)
        # may not exists, we should use "try-except".
        try:
            lessor = obj.ad.owner.gearouser
            return 'ja' if lessor.is_business else 'nein'
        except ObjectDoesNotExist:
            return 'nein'

    def lessor_is_business_with_vat(self, obj):
        # because instance of `GearoUser` (related to instance of `User`)
        # may not exists, we should use "try-except".
        try:
            lessor = obj.ad.owner.gearouser
            return 'ja' if lessor.is_business_with_vat else 'nein'
        except ObjectDoesNotExist:
            return 'nein'

    def transaction_is_first(self, obj):
        return 'ja' if obj.is_first() else 'nein'

from decimal import Decimal as D

from constance import config


def calc_payout(lessor, articlecost_sum):
    articlecost_sum_fee = articlecost_sum * D(config.GEARO_FEE) / D('100')

    if lessor.gearouser.country_code == 'DE':
        payout = (articlecost_sum - (articlecost_sum_fee + articlecost_sum_fee * D(config.VAT_RATE_DE) / D('100')))
    else:
        payout = articlecost_sum - articlecost_sum_fee

    return payout

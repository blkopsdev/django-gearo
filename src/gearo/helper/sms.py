from django.conf import settings

import nexmo

client = nexmo.Client(key=settings.NEXMO_API_KEY, secret=settings.NEXMO_API_SECRET)


def send_sms(phone_number, text):
    client.send_message({'from': 'gearo', 'to': phone_number, 'text': text})

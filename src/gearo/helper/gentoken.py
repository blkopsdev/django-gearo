"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
import string

from django.apps import apps
from django.utils.crypto import get_random_string


def gentoken(length, unique=False, model="", field="", numbers=False):
    if type(length) != int:
        raise ValueError("Parameter have to be an integer")

    params = {'length': length}
    if not numbers:
        params['allowed_chars'] = string.ascii_letters

    token = get_random_string(**params)

    # checking uniqueness if necessary
    if unique:
        if len(model) <= 0:
            raise ValueError("Parameter 'model' can't be null ") 
        if len(field) <= 0:
            raise ValueError("Parameter 'field' can't be null ") 

        instance = apps.get_model(app_label="gearo", model_name=model)
        while instance.objects.filter(**{field: token}).exists():
            token = get_random_string(**params)

    return token

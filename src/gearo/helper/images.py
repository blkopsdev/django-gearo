import os.path
from tempfile import NamedTemporaryFile

from django.conf import settings

from easy_thumbnails.alias import aliases
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from PIL import Image

CROPPED_IMAGE_PREFIX = 'cropped'


def crop_image(image, crop_x, crop_y, width, height, rotate=None):
    filename, ext = os.path.splitext(image.name)
    cropped_image_filename = '%s_%s%s' % (filename, CROPPED_IMAGE_PREFIX, ext)
    im = Image.open(image)
    temp_image = NamedTemporaryFile()
    box = (crop_x, crop_y, width + crop_x, height + crop_y)
    processed_im = im.crop(box)
    if rotate:
        processed_im = processed_im.rotate(-int(rotate))

    if processed_im.mode in ['1', 'P']:
        processed_im = processed_im.convert('RGB')

    processed_im.save(temp_image, 'JPEG')
    temp_image.seek(0)
    return cropped_image_filename, temp_image


def generate_avatar_thumbnail(image):
    thumbnailer = get_thumbnailer(image)
    alias = aliases.get(settings.THUMBNAIL_AVATAR_ALIAS, target=settings.THUMBNAIL_TARGET)
    thumbnailer.get_thumbnail(alias)
    return thumbnail_url(image, settings.THUMBNAIL_AVATAR_ALIAS)

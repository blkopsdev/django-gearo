from post_office import mail

from gearo.helper.absolute_url import absolute_url
from gearo.helper.gentoken import gentoken
from gearo.models import Tokens


def send_welcome_email(user):
    token = Tokens(user=user, token=gentoken(16), kind=1)
    token.save()

    mail.send(
        user.email,
        'no-reply@gearo.de',
        template='welcomemail',
        context={'firstname': user.first_name, "mailactivate": absolute_url("confirmmail", token.token)},
        priority='now',
    )

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.conf import settings
from django.core.urlresolvers import reverse


def absolute_url(name, *args):
    return settings.BASE_URL + reverse(name, args=args)

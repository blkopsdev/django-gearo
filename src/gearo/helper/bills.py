import logging

from django.contrib.auth import get_user_model
from django.utils import timezone

from post_office import mail

from gearo.helper.absolute_url import absolute_url
from gearo.models.billsystem import Billsystem
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)
User = get_user_model()


def generate_invoice(trans):
    logger.debug('Generate invoice for transaction #%s', trans.id)

    lessor = trans.lessor
    tenant = trans.applicant

    gearo_user = lessor.gearouser
    bill_count = gearo_user.bill_count + 1
    gearo_user.bill_count = bill_count
    gearo_user.save()

    new_bill = Billsystem.objects.create(
        type="invoice", count=bill_count, transaction=trans, creator=lessor
    )
    trans.status = Transactions.STATUS_FINISHED
    trans.finished_at = timezone.now()
    trans.bill_created = True
    trans.bill_number = new_bill.id
    trans.save()

    new_bill.generate_invoice()

    # send email to user
    mail.send(trans.applicant.email, 'no-reply@gearo.de', template='invoiceapplicant',
              context={'firstname_lessor': lessor.first_name,
                       'firstname_tenant': tenant.first_name,
                       'invoice_link': absolute_url("renderpdf", trans.bill_number)})

    # send info to ad owner
    mail.send(trans.lessor.email, 'no-reply@gearo.de', template='invoicelessor',
              context={'firstname_lessor': lessor.first_name,
                       'firstname_tenant': tenant.first_name,
                       'invoice_link': absolute_url("renderpdf", trans.bill_number)})

    admin_emails = list(User.objects.filter(is_staff=True).values_list('email', flat=True))

    # send invoice to the admins
    invoice_filename = 'invoice_%s.pdf' % new_bill.id
    mail.send(admin_emails, 'no-reply@gearo.de', template='invoiceadmins',
              context={'firstname_lessor': lessor.first_name,
                       'transaction_id': trans.id,
                       'invoice_link': absolute_url("renderpdf", trans.bill_number)},
              attachments={invoice_filename: new_bill.invoice})

    return new_bill

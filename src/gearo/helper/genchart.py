import calendar
import operator

from django.conf import settings
from django.utils import timezone

from graphos.renderers import flot
from graphos.sources.model import SimpleDataSource


def daterange(start_date, end_date):
    """Get all dates between start_date and end_dates."""
    for n in range(int((end_date - start_date).days)):
        yield start_date + timezone.timedelta(n)


def monthrange(year):
    """Get pairs of first and last days of each month in the specified year."""
    for month in range(1, 13):
        start_date = timezone.datetime(year, month, 1).date()
        if month == 12:
            end_date = timezone.datetime(year+1, 1, 1).date()
        else:
            end_date = timezone.datetime(year, month+1, 1).date()
        end_date = end_date - timezone.timedelta(days=1)
        yield (start_date, end_date)


def months_tick_labels():
    """Generate ticks labels with month names."""
    labels = []
    for month in range(1, 13):
        labels.append([month, calendar.month_abbr[month]])
    return labels


def month_days_tick_labels(month, year):
    """Generate ticks labels with day numbers for a specified month-year."""
    num_days = calendar.monthrange(year=year, month=month)[1]
    labels = [day for day in range(1, num_days+1) if day % 2]
    return labels


def get_data_for_each_day_of_month(month, year, vlist, operator):
    """Get data for each day in month in the specified year."""
    data = []
    today = timezone.now().date()

    start_date = timezone.datetime(year, month, 1).date()
    if month == 12:
        end_date = timezone.datetime(year+1, 1, 1).date()
    else:
        end_date = timezone.datetime(year, month+1, 1).date()

    if today >= start_date:
        for single_date in daterange(start_date, end_date):
            if single_date <= today:
                data.append([
                    single_date.day,
                    sum([v[1] for v in vlist if operator(v[0], single_date)])
                ])
    return data


def get_data_for_each_month_of_year(year, vlist):
    """Get data for each month in the specified year."""
    data = []
    today = timezone.now().date()

    for dates in monthrange(year):
        if today >= dates[0]:
            data.append([
                dates[0].month,
                sum([v[1] for v in vlist if dates[0] <= v[0] <= dates[1]])
            ])
    return data


def get_integral_data_by_the_last_day_of_each_month(year, vlist):
    """Get integral data by the last day of each month in the specified year.
    """
    data = []
    today = timezone.now().date()

    for dates in monthrange(year):
        if today >= dates[0]:
            data.append([
                dates[0].month,
                sum([v[1] for v in vlist if v[0] <= dates[1]])
            ])
    return data


def get_data_for_month_year(month, year, vlist):
    """Get input data for charts. Get data for a given month and/or year. If
    month is equal to 0, generate info for the year. If month is between 1 and
    12, generate info for the month."""

    if month:
        data = get_data_for_each_day_of_month(
            month=month,
            year=year,
            vlist=vlist,
            operator=operator.eq
        )

        data_total = get_data_for_each_day_of_month(
            month=month,
            year=year,
            vlist=vlist,
            operator=operator.le
        )

    else:
        data = get_data_for_each_month_of_year(
            year=year,
            vlist=vlist
        )

        data_total = get_integral_data_by_the_last_day_of_each_month(
            year=year,
            vlist=vlist
        )
    return data, data_total


def generate_chart(chart_class, data, id='chart_div', label=False, height=300,
                   width=600,
                   xlabel=None, xmin=None, xmax=None, xticks=None,
                   ylabel=None, ymin=None, ymax=None, yticks=None):
    """Generate chart from data."""
    chart_options = {
        'colors': settings.DASHBOARD_CHART_OPTIONS['colors'],
        'legend': {
            'show': label,
        },
        'yaxis': {
            'ticks': yticks,
            'tickDecimals': 0,
            'min': ymin,
            'max': ymax,
            'axisLabel': ylabel,
            'axisLabelUseCanvas': False
        },
        'xaxis': {
            'ticks': xticks,
            'tickDecimals': 0,
            'min': xmin,
            'max': xmax,
            'axisLabel': xlabel,
            'axisLabelUseCanvas': False
        },
        'grid': {
            'hoverable': True,
            'clickable': True
        }
    }

    chart = chart_class(
        SimpleDataSource(data=data),
        height=height,
        width=width,
        html_id=id,
        options=chart_options
    )
    return chart


def generate_dashboard_line_charts(month, year, obj_name, vlist=None,
                                   data=None, data_total=None, label=False):
    """Generate two charts for dashboard view. One differential, the other
    integral."""
    if vlist:
        data, data_total = get_data_for_month_year(
            month=month,
            year=year,
            vlist=vlist
        )
        data.insert(0, ['time', 'num'])
        data_total.insert(0, ['time', 'num'])

    if month:
        xlabel = calendar.month_name[month]
        xmax = calendar.monthrange(year=year, month=month)[1]
        xticks = month_days_tick_labels(month=month, year=year)
    else:
        xlabel = 'Months'
        xmax = 12
        xticks = months_tick_labels()

    chart = generate_chart(
        chart_class=flot.LineChart,
        data=data,
        id='chart_div',
        xlabel=xlabel,
        xmin=1,
        xmax=xmax,
        xticks=xticks,
        ylabel=obj_name,
        label=label
    )

    chart_total = generate_chart(
        chart_class=flot.LineChart,
        data=data_total,
        id='chart_div_total',
        xlabel=xlabel,
        xmin=1,
        xmax=xmax,
        xticks=xticks,
        ylabel=obj_name,
        label=label
    )

    return chart, chart_total


def elementwise_sum_two_data_lists(main_list, aux_list, position=1):
    """Returns a new list of lists where each item has length of position and is
    created by taking first position-1 elements from the main_list item and the
    last element is the sum of main_list position item and aux_list position
    item."""
    result_list = []
    if main_list:
        for index in range(len(main_list)):
            new_element = main_list[index][:position]
            new_element.append(main_list[index][position] +
                               aux_list[index][position])
            result_list.append(new_element)
    return result_list


def chart_data_from_data_lists(list_of_data_lists):
    """Creates chart data from multiple lists of data."""
    result_list = []

    if list_of_data_lists[0]:
        for index in range(len(list_of_data_lists[0])):
            new_element = list_of_data_lists[0][index][:1]
            for data_list in list_of_data_lists:
                new_element.append(data_list[index][1])
            result_list.append(new_element)
    return result_list

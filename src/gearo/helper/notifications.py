import datetime
import hashlib
import hmac
import json
import logging
import ssl
import time

from django.conf import settings

from websocket import create_connection

logger = logging.getLogger('gearo.helper.notifications')

NOTIFICATIONS_PEPPER = (
    "FRp57M", "Vnrr", "Ee4qwU", "vd24aE", "VE9f", "n5TLmx", "nLfz", "2LQgRQ", "6BqD", "bpxrju", "4NyX3U", "HpXAgX",
    "6BqD", "5vTN66", "x8CUqL", "2v22", "dhrC7h", "nye8", "nEwb5D", "Wr5d", "YZXJEU", "EGHbqE", "uwrd", "rMCTxR",
    "wBZL5W", "KQjZ", "Uhsgjn", "kj2V", "L9dzHH", "KvqF", "kceTdu"
)

NOTIFICATION_SERVER_SCHEMA = "wss://" if settings.NOTIFICATIONS_SERVER["ssl"] else "ws://"
NOTIFICATION_SERVER_URL = NOTIFICATION_SERVER_SCHEMA + settings.NOTIFICATIONS_SERVER["host"]


def send_notification(instance, notification_type='add'):
    connection_params = {}
    notification_url = '%s/ws/notification/%s/' % (NOTIFICATION_SERVER_URL, instance.user.gearouser.notification_token)
    if settings.NOTIFICATIONS_SERVER["ssl"]:
        connection_params['sslopt'] = {"cert_reqs": ssl.CERT_NONE}
    websocket = create_connection(notification_url, **connection_params)

    etime = str(int(time.time()))

    pepper_index = datetime.datetime.today().day - 1
    key = settings.NOTIFICATIONS_SERVER["secret"] + NOTIFICATIONS_PEPPER[pepper_index] + etime
    key = bytes(key.encode('ascii'))
    message = instance.user.gearouser.notification_token
    message = bytes(message.encode('ascii'))
    message_id = hmac.new(key, message, hashlib.sha256).hexdigest()

    message = {
        "id": message_id,
        "type": notification_type,
        "time": etime,
        "room": instance.user.gearouser.notification_token
    }

    websocket.send(json.dumps(message))
    logger.debug('Sent notification "%s" #%s', notification_type, instance.id)
    websocket.close()

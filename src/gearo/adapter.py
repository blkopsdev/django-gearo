from datetime import timedelta

from django.contrib import messages
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.utils.timezone import now

from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from post_office import mail

from gearo.helper.absolute_url import absolute_url
from gearo.helper.generateusername import generateusername
from gearo.helper.gentoken import gentoken
from gearo.models import Tokens


class AccountAdapter(DefaultAccountAdapter):
    def populate_username(self, request, user):
        if not user.id:
            user.username = generateusername(user.first_name, user.last_name)


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def process_manual_signup(self, request, sociallogin):
        user = User.objects.get(email=sociallogin.email_addresses[0].email)
        sociallogin.user = user
        super(SocialAccountAdapter, self).process_manual_signup(request, sociallogin)
        expire_date = now() + timedelta(hours=1)
        new_token = Tokens(user=user, token=gentoken(16), kind=5, expire_date=expire_date)
        new_token.save()

        url = absolute_url('socialaccount_login_confirmation', new_token.token)
        mail.send(
            user.email, 'no-reply@gearo.de', template='facebookloginconfirm',
            context={'first_name': user.first_name, 'facebook_email_confirmation_link': url}
        )

        messages.warning(request, 'Bitte bestätige, dass du dich über Facebook bei gearo anmelden möchtest.')
        return redirect('search_results')

    def populate_username(self, request, user):
        if not user.id:
            user.username = generateusername(user.first_name, user.last_name)

    def get_connect_redirect_url(self, request, socialaccount):
        return reverse_lazy('search_results')

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.forms import ModelForm, inlineformset_factory
from django.utils.safestring import mark_safe

from taggit.forms import TagWidget

from gearo.models.advertisement import Advertisement, UnavailabilityPeriod

DESCRIPTION_PLACEHOLDER = mark_safe("Beschreibe dein Equipment so pr&auml;zise wie m&ouml;glich und f&uuml;ge wichtige "
                                    "Infos f&uuml;r den Mieter hinzu.")
PLACESEARCH_PLACEHOLDER = mark_safe('Stra&szlig;e, Hausnummer, PLZ, Stadt')
PLACESEARCH_WIDGET_ATTRS = {
    'id': 'placesearch', 'required': '', 'class': 'form-control', 'data-parsley-errors-container': '#error_placesearch',
    'placeholder': PLACESEARCH_PLACEHOLDER, 'data-parsley-is-coordinates-set': ''
}
ATTACHMENTS_WIDGET_ATTRS = {
    'id': 'attachments_field', 'class': 'hidden', 'required': '', 'data-parsley-min-attachments': 1,
    'data-parsley-errors-container': '#chooseimages_errors',
    'data-parsley-error-message': 'Bitte füge mindestens ein Bild deinem Angebot hinzu.'
}


class AddAdvertFrom(ModelForm):
    attachments = forms.CharField(widget=forms.TextInput(attrs=ATTACHMENTS_WIDGET_ATTRS))

    class Meta:
        model = Advertisement
        fields = ['category', 'title', 'tags', 'description', "priceday", 'pricethree', 'priceweek', 'articleofvalue',
                  'lat', 'lng', "time_mo_start", "time_mo_end", "time_di_start", "time_di_end",
                  "time_mi_start", "time_mi_end", "time_do_start", "time_do_end", "time_fr_start", "time_fr_end",
                  "time_sa_start", "time_sa_end", "time_so_start", "time_so_end", "pickup_location"]
        
        widgets = {
            'category': forms.Select(attrs={'class': 'form-control', "data-parsley-errors-container":
                                            "#error_category", "required": ""}),
            'title': forms.TextInput(attrs={"class": "form-control", "data-parsley-errors-container": "#error_title",
                                            "required": "",
                                            "placeholder": "z.B. Canon 5D MK III mit 35mm Festbrennweite"}),
            'tags': TagWidget(attrs={'data-parsley-min-tags': 3, 'class': 'hidden', 'required': '',
                                     'data-parsley-errors-container': '#error_tags'}),
            'priceday': forms.NumberInput(attrs={"class": "form-control",
                                                 "data-parsley-errors-container": "#error_priceday",
                                                 "required": "", "data-parsley-validation-threshold": "1",
                                                 "data-parsley-trigger": "keyup",
                                                 "data-parsley-type": "number", "step": "0.01",
                                                 "placeholder": "z.B. 48.00 "}),
            'pricethree': forms.NumberInput(attrs={"class": "form-control", "data-parsley-trigger": "keyup",
                                                   "data-parsley-errors-container": "#error_priceday",
                                                   "data-parsley-validation-threshold": "1",
                                                   "data-parsley-type": "number", "step": "0.01",
                                                   "placeholder": "z.B. 130.00 "}),
            'priceweek': forms.NumberInput(attrs={"class": "form-control", "data-parsley-trigger": "keyup",
                                                  "data-parsley-errors-container": "#error_priceday",
                                                  "data-parsley-validation-threshold": "1",
                                                  "data-parsley-type": "number", "step": "0.01",
                                                  "placeholder": "z.B. 186.00 "}),
            'articleofvalue': forms.NumberInput(attrs={"class": "form-control", "placeholder": "z.B. 2500.00 ",
                                                       "data-parsley-errors-container": "#error_articleofvalue",
                                                       "required": ""}),
            'description': forms.Textarea(attrs={"rows": 15, "cols": 54, 'required': '',
                                                 "placeholder": DESCRIPTION_PLACEHOLDER}),
            'lat': forms.HiddenInput(),
            'lng': forms.HiddenInput(),
            'time_mo_start': forms.TextInput(attrs={'class': ' times'}),
            'time_mo_end': forms.TextInput(attrs={'class': 'times'}),
            'time_di_start': forms.TextInput(attrs={'class': 'times'}),
            'time_di_end': forms.TextInput(attrs={'class': 'times'}),
            'time_mi_start': forms.TextInput(attrs={'class': 'times'}),
            'time_mi_end': forms.TextInput(attrs={'class': 'times'}),
            'time_do_start': forms.TextInput(attrs={'class': 'times'}),
            'time_do_end': forms.TextInput(attrs={'class': 'times'}),
            'time_fr_start': forms.TextInput(attrs={'class': 'times'}),
            'time_fr_end': forms.TextInput(attrs={'class': 'times'}),
            'time_sa_start': forms.TextInput(attrs={'class': 'times'}),
            'time_sa_end': forms.TextInput(attrs={'class': 'times'}),
            'time_so_start': forms.TextInput(attrs={'class': 'times'}),
            'time_so_end': forms.TextInput(attrs={'class': 'times'}),
            'pickup_location': forms.TextInput(attrs=PLACESEARCH_WIDGET_ATTRS)
        }

    def __init__(self, *args, **kwargs):
        super(AddAdvertFrom, self).__init__(*args, **kwargs)
        if not kwargs.get('instance', None):
            self.fields['priceday'].widget.attrs['disabled'] = 'disabled'
            self.fields['pricethree'].widget.attrs['disabled'] = 'disabled'
            self.fields['priceweek'].widget.attrs['disabled'] = 'disabled'


class UnavailabilityPeriodForm(forms.ModelForm):
    class Meta:
        model = UnavailabilityPeriod
        fields = ('start', 'end')

    widgets = {
        'start': forms.HiddenInput(attrs={'class': 'hide'}),
        'end': forms.HiddenInput(attrs={'class': 'hide'})
    }

UnavailabilityPeriodFormSet = inlineformset_factory(
    Advertisement, UnavailabilityPeriod, form=UnavailabilityPeriodForm, extra=0
)

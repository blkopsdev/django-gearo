"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.utils.safestring import mark_safe


class ResetPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Passwort",
                                                                 "aria-describedby":"basic-addon1"}))
    password_repeat = forms.CharField(widget=forms.PasswordInput(attrs={"class": "form-control",
                                                                        "placeholder": "Passwort wiederholen",
                                                                        "aria-describedby":"basic-addon1"}))
    
    def clean(self):
        cleaned_data = super(ResetPasswordForm, self).clean()
        password = cleaned_data.get('password')
        password_repeat = cleaned_data.get('password_repeat')
        
        if password != password_repeat:
            self.add_error('password', mark_safe("Passw&ouml;rter stimmen nicht &uuml;berein"))

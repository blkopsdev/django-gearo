"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.utils.safestring import mark_safe


# datepicker failed if preset date
# "value": datetime.datetime.strftime(datetime.datetime.now(), "%d.%m.%Y")
class InquireForm(forms.Form):
    startdate = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control datepicker-field dateicon",
                                                              "placeholder": "Abholung"}), required=True)
    enddate = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control datepicker-field dateicon",
                                                            "placeholder": mark_safe("R&uuml;ckgabe")}), required=True)
    pickup_after_3pm = forms.BooleanField(label='Abholung vor 15 Uhr', initial=False, required=False)
    return_before_12pm = forms.BooleanField(label='Rückgabe nach 12 Uhr', initial=False, required=False)

    def __init__(self, *args, **kwargs):
        instance = kwargs.pop('instance', None)
        super(InquireForm, self).__init__(*args, **kwargs)
        if instance and instance.category.is_billed_daily:
            self.fields['startdate'].widget.attrs['placeholder'] = 'Buchung von'
            self.fields['enddate'].widget.attrs['placeholder'] = 'Buchung bis'

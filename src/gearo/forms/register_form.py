"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.contrib.auth.models import User


# TODO: add parsley.js dataattributes
class RegisterForm(forms.Form):
    firstname = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "placeholder":"Vorname",
                                                              "aria-describedby":"basic-addon1"}), required=True,
                                error_messages={'required': 'Diese Eingabe ist erforderlich'})
    lastname = forms.CharField(widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Nachname",
                                                             "aria-describedby":"basic-addon1"}), required=True,
                               error_messages={'required': 'Diese Eingabe ist erforderlich'})
    
    email = forms.EmailField(widget=forms.TextInput(attrs={"class": "form-control", "placeholder":"E-Mail-Adresse",
                                                           "aria-describedby": "basic-addon1"}), required=True,
                             error_messages={'required': 'Diese Eingabe ist erforderlich',
                                             'invalid': 'Bitte gibt eine gueltige E-Mail-Adresse an'})
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control", "placeholder": "Passwort",
                                                                 "aria-describedby": "basic-addon1"}),
                               required=True, error_messages={'required': 'Diese Eingabe ist erforderlich'})
    terms = forms.BooleanField(widget=forms.CheckboxInput(), required=True,
                               error_messages={'required': 'Um fortfahren zu k&ouml;nnen, musst du zustimmen.'})
    
    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        email = cleaned_data.get('email')

        if User.objects.filter(email=email).exists():
            self.add_error('email', 'Es existiert bereits ein Account mit dieser E-Mail-Adresse')

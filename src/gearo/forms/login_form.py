from django import forms


class LoginForm(forms.Form):
    email = forms.CharField(widget=forms.TextInput(
        attrs={"class": "form-control", "placeholder": "E-Mail-Adresse", "aria-describedby": "basic-addon1"}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={"class": "form-control", "placeholder": "Passwort", "aria-describedby": "basic-addon1"}))

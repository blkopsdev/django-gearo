"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.forms import ModelForm

from post_office.models import EmailTemplate


class EmailTemplateFrom(ModelForm):
    class Meta:
        model = EmailTemplate
        fields = ["name", "description", "subject", "html_content"]
        
        widgets = {
                   'description': forms.TextInput(),
                   'html_content': forms.Textarea(attrs={"class": "tinymce"}),
                   }

from django import forms


class RentCalcForm(forms.Form):
    ad_id = forms.HiddenInput()
    start_date = forms.DateField()
    end_date = forms.DateField()
    pickup_after_3pm = forms.BooleanField(required=False)
    return_before_12pm = forms.BooleanField(required=False)

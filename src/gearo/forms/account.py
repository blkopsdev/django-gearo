import json

from django import forms
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe

from dateutil.parser import parse

from gearo.helper.mangopay import (
    BUSINESS_DOCUMENTS_FIELDS_NAMES, get_mangopay_user_bank_account, update_mp_bank_account_info)
from gearo.models.gearo_profile import GearoProfile
from gearo.models.user_payment import BUSINESS_TYPES, UserPayment
from gearo.models.gearo_user import GearoUser
from gearo_tasks.tasks.mangopay import create_mangopay_user_and_wallet, update_mp_user_info

ADDRESS_FIELDS_NAMES = ['street', 'zip', 'place', 'country']
BUSINESS_ADDRESS_FIELDS_NAMES = ['street_bill', 'zip_bill', 'place_bill', 'country_bill']
BANK_DETAILS_FIELDS_NAMES = ['first_name', 'last_name', 'iban', 'bic']
DOCUMENTS_FIELDS_NAMES = ['document_upside', 'document_downside'] + BUSINESS_DOCUMENTS_FIELDS_NAMES
REQUIRED_FIELD_MESSAGE = 'Du musst alle Felder ausfüllen'
UNSTYLED_FIELDS_NAMES = DOCUMENTS_FIELDS_NAMES + ['gender']


class StyleFormMixin(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for fname, field in self.fields.items():
            if fname not in UNSTYLED_FIELDS_NAMES:  # For these fields 'class' will be added in related form
                field.widget.attrs['class'] = 'form-control'
            if fname != 'phone_mobile':  # For this field error message indicated in `GearoPhoneNumberField`
                field.error_messages['required'] = REQUIRED_FIELD_MESSAGE

        for fname, placeholder_text in self.placeholders.items():
            self.fields[fname].widget.attrs['placeholder'] = placeholder_text

        for fname in self.required_fields:
            self.fields[fname].required = True

        if hasattr(self, 'checkbox_css_ids'):
            for fname, css_id in self.checkbox_css_ids.items():
                self.fields[fname].widget.attrs['id'] = css_id
                self.fields[fname].widget.attrs['class'] = 'onoffswitch-checkbox'

    def check_required_fields(self, cleaned_data, required_fields, message=REQUIRED_FIELD_MESSAGE):
        for field_name in required_fields:
            if not cleaned_data.get(field_name, None):
                self.add_error(field_name, message)


class ProfileForm(StyleFormMixin):
    placeholders = {
        'self_description': mark_safe('Gib Anderen eine kurze Information &uuml;ber dich selbst. '
                                      'Erz&auml;hl an was f&uuml;r Projekten du gerne arbeitest, '
                                      'oder was du beruflich schon gemacht hast.'),
        'homepage': 'Gib deine Homepage URL ein',
        'vimeo': 'Gib deine Vimeo URL ein',
        'youtube': 'Gib deine Youtube URL ein',
        'instagram': 'Gib deine Instagram URL ein',
        'facebook': 'Gib deine Facebook URL ein',
        'twitter': 'Gib deine Twitter URL ein',
        'linkedin': 'Gib deine LinkedIn URL ein',
        'flickr': 'Gib deine Flickr URL ein'
    }
    required_fields = ['avatar', 'self_description']

    class Meta:
        model = GearoProfile
        fields = [
            'avatar', 'self_description',
            'homepage', 'vimeo', 'youtube', 'instagram', 'facebook', 'twitter', 'linkedin', 'flickr',
        ]
        widgets = {
            'avatar': forms.FileInput(attrs={'class': 'upload'}),
        }


class PersonalInfoForm(StyleFormMixin):
    placeholders = {
        'birthday': 'Geburtstag'
    }
    required_fields = ['first_name', 'last_name', 'birthday', 'gender']

    birthday = forms.CharField(
        widget=forms.DateInput(format='%d.%m.%Y', attrs={'class': 'form-control datepicker-field dateicon'}))
    gender = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': ''}), choices=GearoUser.GENDER_CHOICES)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'birthday', 'gender']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            self.initial['birthday'] = self.instance.billing_info.birthday
            self.initial['gender'] = self.instance.gearouser.gender

    def save(self, commit=True):
        super().save(commit=True)
        birthday = self.cleaned_data.get('birthday', None)
        gender = self.cleaned_data.get('gender', None)
        self.instance.billing_info.birthday = parse(birthday, dayfirst=True).date()
        self.instance.billing_info.save()
        self.instance.gearouser.gender = gender
        self.instance.gearouser.save()
        return self.instance


class ContactInfoForm(StyleFormMixin):
    placeholders = {'phone': 'Festnetznummer'}
    required_fields = ['phone_mobile']

    class Meta:
        model = GearoUser
        fields = ['phone', 'phone_mobile']


class IdentificationInfoForm(StyleFormMixin):
    placeholders = {
        'id_document_number': 'Personalausweisnummer',
    }
    required_fields = ['nationality', 'country_of_residence', 'id_document_number']

    # Fields related to Identity Proof document
    document_upside = forms.ImageField(widget=forms.ClearableFileInput(attrs={'class': 'upload'}), required=False)
    document_downside = forms.ImageField(widget=forms.ClearableFileInput(attrs={'class': 'upload'}), required=False)

    registration_proof = forms.ImageField(
        widget=forms.ClearableFileInput(attrs={'multiple': True, 'class': 'upload'}), required=False)
    articles_of_association = forms.ImageField(
        widget=forms.ClearableFileInput(attrs={'multiple': True, 'class': 'upload'}), required=False)
    shareholder_declaration = forms.ImageField(
        widget=forms.ClearableFileInput(attrs={'multiple': True, 'class': 'upload'}), required=False)

    class Meta:
        model = UserPayment
        fields = [
            'country_of_residence', 'nationality', 'id_document_number', 'document_upside', 'document_downside',
        ]

    def clean(self):
        cd = super().clean()
        document_upside = cd.get('document_upside', None)
        document_downside = cd.get('document_downside', None)
        registration_proof = cd.get('registration_proof', None)
        articles_of_association = cd.get('articles_of_association', None)

        if not all([document_upside, document_downside]):
            self.add_error('document_upside', 'Bitte lade Vorder- und Rückseite deines Personalausweises hoch.')

        user = self.instance.user
        business_type = user.gearouser.get_business_type_name()
        if self.instance.is_payment_limit_reached:
            if business_type in BUSINESS_TYPES and not registration_proof:
                self.add_error('registration_proof', 'Bitte lade den Handelsregisterauszug hoch.')
            if business_type == BUSINESS_TYPES[0] and not articles_of_association:
                self.add_error('articles_of_association', 'Bitte lade den Gesellschaftervertrag hoch.')


class AddressInfoForm(StyleFormMixin):
    checkbox_css_ids = {
        'onoffswitch_bill': 'billonoffswitch',
        'onoffswitch_trader': 'traderonoffswitch',
        'calc_mwst': 'calctaxonoffswitch',
    }
    placeholders = {
        'business_name': 'Firmierung',
        'street': mark_safe('Stra&szlig;e'),
        'zip': 'PLZ',
        'place': 'Ort',
        'business_name_bill': 'Firmierung',
        'first_name_bill': 'Vorname',
        'last_name_bill': 'Nachname',
        'street_bill': mark_safe('Stra&szlig;e'),
        'zip_bill': 'PLZ',
        'place_bill': 'Ort',
        'tax_number': 'Steuernummer',
        'tax_ident_number': 'Umsatzsteuer – Identifikationsnummer',
    }
    required_fields = ['street', 'zip', 'place', 'country']
    required_fields_address = [
        'first_name_bill', 'last_name_bill', 'business_name_bill',
        'street_bill', 'zip_bill', 'place_bill', 'country_bill',
    ]

    business_name = forms.CharField(required=False)

    class Meta:
        model = GearoUser
        fields = [
            'business_name', 'street', 'zip', 'place', 'country', 'onoffswitch_bill', 'business_name_bill',
            'first_name_bill', 'last_name_bill', 'street_bill', 'zip_bill', 'place_bill', 'country_bill', 'trader',
            'calc_mwst', 'onoffswitch_trader', 'business_type', 'tax_number', 'tax_ident_number',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            self.initial['business_name'] = self.instance.user.profile.business_name

    def clean(self):
        cd = super().clean()

        if cd.get('onoffswitch_bill', None):
            self.check_required_fields(
                cd, self.required_fields_address, 'Bitte trag die komplette Rechnungsadresse ein')

        if cd.get('onoffswitch_trader', None):
            tax_num_entered = cd.get('tax_number', None)
            tax_ident_num_entered = cd.get('tax_ident_number', None)
            if not tax_num_entered and not tax_ident_num_entered:
                self.add_error(
                    'tax_ident_number', 'Bitte trag eine Steuernummer oder Umsatzsteuer – Identifikationsnummer ein')
            if not cd['business_type']:
                self.add_error('business_type', REQUIRED_FIELD_MESSAGE)

    def save(self, commit=True):
        super().save(commit=True)
        profile = self.instance.user.profile
        profile.business_name = self.cleaned_data.get('business_name', None)
        profile.save()
        if self.has_changed():
            self.update_changed_data(self.changed_data)
        return self.instance

    def update_changed_data(self, changed_data):
        # If user becomes a business - we should create Mangopay Legal User for it.
        # If user becomes not business - we should create Mangopay Natural User for it.
        # Mangopay User will be created just in case user does not have one.
        if 'onoffswitch_trader' in changed_data:
            # We should do next tasks synchronously, because before page will be reloaded
            # all MP data should be created.
            create_mangopay_user_and_wallet(self.instance.user.id)
            billing_info = self.instance.user.billing_info
            if not billing_info.is_mangopay_bank_account_id_existing:
                update_mp_bank_account_info(self.instance.user)

        params_for_update = {}
        if 'business_name_bill' in changed_data:
            params_for_update['business_name'] = self.cleaned_data.get('business_name_bill', None)

        if any([field_name for field_name in ADDRESS_FIELDS_NAMES if field_name in changed_data]):
            params_for_update['address_changed'] = True

        if any([field_name for field_name in BUSINESS_ADDRESS_FIELDS_NAMES if field_name in changed_data]):
            params_for_update['business_address_changed'] = True

        if 'business_type' in changed_data:
            params_for_update['business_type'] = self.cleaned_data.get('business_type', None)

        if params_for_update:
            update_mp_user_info.delay(self.instance.user.id, params_for_update)

        self.instance.trader = self.cleaned_data.get('onoffswitch_trader', None)
        self.instance.save()


class BankInfoForm(StyleFormMixin):
    placeholders = {
        'first_name': 'Vorname',
        'last_name': 'Nachname',
        'iban': 'IBAN',
        'bic': 'BIC',
    }
    required_fields = []  # We will make our own checks in `clean()`
    required_fields_names = ['first_name', 'last_name']

    class Meta:
        model = UserPayment
        fields = ['first_name', 'last_name', 'iban', 'bic']

    def clean(self):
        cd = super().clean()
        first_name = cd.get('first_name', None)
        last_name = cd.get('last_name', None)
        iban = cd.get('iban', None)
        bic = cd.get('bic', None)

        if any([iban, bic]):
            self.check_required_fields(cd, self.required_fields_names)

        user = self.instance.user
        is_bank_details_changed = set(BANK_DETAILS_FIELDS_NAMES).intersection(set(self.changed_data))
        if is_bank_details_changed or not self.instance.is_mangopay_bank_account_id_existing:
            bank_account_data = {'first_name': first_name, 'last_name': last_name, 'iban': iban, 'bic': bic}
            result = update_mp_bank_account_info(user, bank_account_data)
            if isinstance(result, str):
                bank_account_errors = json.loads(result)
                if bank_account_errors is not None:
                    for field, error in bank_account_errors.items():
                        if field in ['IBAN', 'BIC']:
                            self.add_error(field.lower(), 'Bitte überprüfe deine {}'.format(field))
                        else:
                            self.add_error(None, error)  # will be added to `form.non_field_errors`


class EmailChangeForm(StyleFormMixin):
    placeholders = {}
    required_fields = ['email']

    class Meta:
        model = User
        fields = ['email']

    def clean(self):
        cd = super().clean()
        initial_email = self.initial.get('email', None)
        new_email = cd.get('email', None)
        user_with_this_email_exists = User.objects.filter(email__iexact=new_email).exists()
        if all([new_email, initial_email]):
            if self.initial['email'] != new_email and user_with_this_email_exists:
                self.add_error('email', 'Es existiert bereits ein Account mit dieser E-Mail-Adresse')


class PasswordChangeForm(StyleFormMixin):
    placeholders = {
        'old_password': 'Dein aktuelles Passwort',
        'new_password': 'Dein neues Passwort',
    }
    required_fields = []

    old_password = forms.CharField(widget=forms.PasswordInput(), required=False)
    new_password = forms.CharField(widget=forms.PasswordInput(), required=False)

    class Meta:
        model = User
        fields = ['old_password', 'new_password']

    def clean(self):
        cd = super().clean()
        new_password = cd.get('new_password', None)
        old_password = cd.get('old_password', None)

        if old_password and not self.instance.check_password(old_password):
            self.add_error('old_password', 'Falsches Passwort. Bitte versuche es erneut.')
        if new_password and not old_password:
            self.add_error('old_password', 'Dein aktuelles Passwort.')
        if old_password and not new_password:
            self.add_error('new_password', 'Bitte gib dein neues Passwort an.')
        if all([new_password, old_password]) and new_password == old_password:
            self.add_error('new_password', 'Dein neues Passwort kann nicht dasselbe wie dein altes Passwort sein.')

    def save(self, commit=True):
        self.instance.set_password(self.cleaned_data.get('new_password', None))
        return super().save()

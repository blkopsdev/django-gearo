"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.forms import ModelForm

from gearo.models.faq_categorie import FaqCategory


class FaqCategoryFrom(ModelForm):
    class Meta:
        model = FaqCategory
        fields = ['category', 'parent']

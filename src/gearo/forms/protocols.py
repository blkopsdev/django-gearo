from django import forms

from gearo.models import *

TEXTAREA_ATTRS = {'rows': 1}
ATTACHMENT_WIDGET_ATTRS = {
    'id': 'attachments_field', 'required': '', 'data-parsley-min-attachments': 2,
    'data-parsley-errors-container': '#attachment_errors',
    'data-parsley-error-message': 'Lade mindestens zwei Fotos des Equipments hoch.'
}


class BaseProtocolForm(forms.ModelForm):
    placeholders = {
        'serial_number': 'Nummer hier eingeben',
        'identity_number': 'Nummer hier eintragen',
        'defects': 'Mängel oder Besonderheiten hier eintragen',
        'return_time': '--:--'
    }
    attachments = forms.CharField(widget=forms.TextInput(attrs=ATTACHMENT_WIDGET_ATTRS), required=False)

    class Meta:
        fields = '__all__'
        model = Rohprotocol
        widgets = {
            'lessor_signature': forms.HiddenInput(),
            'applicant_signature': forms.HiddenInput(),
            'undamaged': forms.HiddenInput(),
            'serial_number': forms.Textarea(attrs=TEXTAREA_ATTRS),
            'defects': forms.Textarea(attrs=TEXTAREA_ATTRS)
        }

    def __init__(self, *args, **kwargs):
        is_created = kwargs.pop('is_created', False)
        super(BaseProtocolForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            attrs = {'class': 'form-control'}
            placeholder = self.placeholders.get(field_name, None)
            if field_name == 'return_time':
                attrs = {
                    'size': 5, 'class': 'timepicker', 'data-parsley-errors-container': '#return_time_errors'
                }
            if is_created:
                attrs['disabled'] = 'disabled'
            if field_name in self.required_fields:
                field.required = True
                attrs['required'] = ''
            if placeholder:
                attrs['placeholder'] = placeholder
            field.widget.attrs.update(**attrs)


class HandingOverProtocolForm(BaseProtocolForm):
    required_fields = ('identity_card', 'identity_number', 'lessor_signature', 'applicant_signature', 'return_time')

    class Meta(BaseProtocolForm.Meta):
        fields = ('identity_card', 'identity_number', 'serial_number', 'defects',
                  'lessor_signature', 'applicant_signature', 'return_time', 'undamaged')


class ReturnProtocolForm(BaseProtocolForm):
    required_fields = ('lessor_signature', 'applicant_signature')

    class Meta(BaseProtocolForm.Meta):
        fields = ('defects', 'lessor_signature', 'applicant_signature', 'undamaged')

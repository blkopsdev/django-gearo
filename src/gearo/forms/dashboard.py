from django import forms
from django.utils import timezone

from gearo.models.transactions import Transactions

MONTHS = [
    (0, '------'),
    (1, 'January'),
    (2, 'February'),
    (3, 'March'),
    (4, 'April'),
    (5, 'May'),
    (6, 'June'),
    (7, 'July'),
    (8, 'August'),
    (9, 'September'),
    (10, 'October'),
    (11, 'November'),
    (12, 'December')
]

YEARS = [
    (2016, 2016),
    (2017, 2017),
    (2018, 2018),
    (2019, 2019),
    (2020, 2020),
]

FEES = (
    ('total', "Total"),
    ('insurance', "Insurance"),
    ('rental', "Rental"),
)


def get_status_choices():
    """Make a list of status choices with the extra All option."""
    choices = list(Transactions.STATUS_CHOICES)
    choices.insert(0, ('all', 'Alle'))
    return choices


class DashboardDateForm(forms.Form):
    """Form to choose time period for dashboard views."""
    month = forms.TypedChoiceField(
        choices=MONTHS,
    )
    year = forms.TypedChoiceField(
        choices=YEARS,
    )

    def clean_month(self):
        now = timezone.now()
        month = self.cleaned_data['month']
        try:
            month = int(month)
        except TypeError:
            month = now.month
        if month not in range(0, 13):
            month = now.month
        return month

    def clean_year(self):
        now = timezone.now()
        year = self.cleaned_data['year']
        try:
            year = int(year)
        except TypeError:
            year = now.year
        return year


class DashboardStatusForm(forms.Form):
    """Form to choose a transation status for dashboard views."""
    status = forms.TypedChoiceField(
        choices=get_status_choices()
    )


class DashboardFeeDateForm(forms.Form):
    """Form to choose dates and type of fee for dashboard views."""
    month = forms.TypedChoiceField(
        choices=MONTHS,
    )
    year = forms.TypedChoiceField(
        choices=YEARS,
    )
    fees = forms.MultipleChoiceField(
        label="Fees",
        choices=FEES,
        widget=forms.CheckboxSelectMultiple()
    )

    def clean_month(self):
        now = timezone.now()
        month = self.cleaned_data['month']
        try:
            month = int(month)
        except TypeError:
            month = now.month
        if month not in range(0, 13):
            month = now.month
        return month

    def clean_year(self):
        now = timezone.now()
        year = self.cleaned_data['year']
        try:
            year = int(year)
        except TypeError:
            year = now.year
        return year

    def clean_fees(self):
        fees = self.cleaned_data['fees']
        if not fees:
            fees = ['total']
        return fees

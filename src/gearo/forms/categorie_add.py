"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.forms import ModelForm

from gearo.models.advertisement_categories import AdvertisementCategory


class CategorieAdd(ModelForm):
    parent = forms.ModelChoiceField(queryset=AdvertisementCategory.objects.all(), to_field_name="id", required=False)

    class Meta:
        model = AdvertisementCategory
        fields = ["active", "name", "secret_name", "parent"]
    
    def clean_parent(self):
        if self.cleaned_data['parent'] is not None:
            return self.cleaned_data['parent'].id

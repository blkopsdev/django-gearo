"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.forms import ModelForm

from gearo.models.sites import Sites


class SiteForm(ModelForm):
    class Meta:
        model = Sites
        fields = ['title', 'content']
        
        widgets = {
                   'content': forms.Textarea(attrs={"class": "tinymce"}),
                   }

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.utils.safestring import mark_safe


class AddAdQuestionForm(forms.Form):
    question = forms.CharField(
        widget=forms.Textarea(
            attrs={"class": "form-control", "required": "required", "rows": 5,
                   "placeholder": mark_safe("Ist noch etwas unklar? Frage hier direkt den Eigent&uuml;mer!",)
            }
        )
    )

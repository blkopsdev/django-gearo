"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.forms import ModelForm

from gearo.models.discount_voucher import Voucher


class AddvoucherFrom(ModelForm):
    class Meta:
        model = Voucher
        exclude = ['created_at', 'activations']

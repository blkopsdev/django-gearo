"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django import forms
from django.forms import ModelForm

from gearo.models.faq import Faq


class AddQuestionFrom(ModelForm):
    class Meta:
        model = Faq
        fields = ['category', 'question', 'answer']
        
        widgets = {
                   'answer': forms.Textarea(attrs={"class": "tinymce"}),
                   }

from django.contrib.staticfiles import finders
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.urlresolvers import reverse_lazy

from django_webtest import WebTest
from nose_parameterized import parameterized

from gearo.models import Attachments, ProtocolAttachment
from gearo.tests.mixins import AttachmentTestMixin


class AttachmentUploadTests(AttachmentTestMixin, WebTest):

    @parameterized.expand([
        ('ad', Attachments),
        ('protocol', ProtocolAttachment)
    ])
    def test_upload_attachment(self, attachment_type, attachment_model):
        self.assertEquals(attachment_model.objects.count(), 0)
        url = reverse_lazy('imageupload', args=(attachment_type,))
        response = self.app.post(
            url,
            upload_files=(('attachmentupload', finders.find('gearo/images/logo.png')),),
            user=self.user
        )
        self.assertTrue(response.json['success'])
        self.assertEquals(attachment_model.objects.count(), 1)


class DeleteAttachmentTests(AttachmentTestMixin, WebTest):

    def setUp(self):
        super(DeleteAttachmentTests, self).setUp()
        uploaded_file = SimpleUploadedFile(name='test.png', content=b'123')
        self.ad_attachment = Attachments.objects.create(original_image=uploaded_file, owner=self.user)
        self.protocol_attachment = ProtocolAttachment.objects.create(original_image=uploaded_file, owner=self.user)

    @parameterized.expand([
        ('ad', Attachments, 0, 1),
        ('protocol', ProtocolAttachment, 1, 0)
    ])
    def test_remove_ad_attachment(self, attachment_type, attachment_model,
                                  ad_attachments, protocol_attachments):
        url = reverse_lazy('uploaddelete', args=(attachment_type,))
        attachment_id = attachment_model.objects.first().id
        response = self.app.post(url, params={'attachment_id': attachment_id}, user=self.user)
        self.assertTrue(response.json['success'])
        self.assertEquals(Attachments.objects.count(), ad_attachments)
        self.assertEquals(ProtocolAttachment.objects.count(), protocol_attachments)

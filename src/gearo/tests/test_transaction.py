from datetime import date, datetime, timedelta
from decimal import Decimal as D

from django.core.urlresolvers import reverse_lazy
from django.utils import timezone

from django_webtest import WebTest
from freezegun import freeze_time

from gearo.helper.calc_insurance import calc_insurance
from gearo.helper.response_limit import round_datetime_to_nearest_30_minutes
from gearo.models import Transactions
from gearo.tests.mixins import BaseAdvertisementTestMixin


class TransactionTest(BaseAdvertisementTestMixin, WebTest):
    csrf_checks = False

    def test_transaction_is_first_method(self):
        transaction_1 = self._create_inquiry(
            startdate=date(2017, 5, 22).strftime("%d.%m.%Y"), enddate=date(2017, 5, 24).strftime("%d.%m.%Y"))
        self.assertTrue(transaction_1.is_first())
        transaction_1.status = Transactions.STATUS_FINISHED
        transaction_1.save()

        transaction_2 = self._create_inquiry(
            startdate=date(2017, 5, 26).strftime("%d.%m.%Y"), enddate=date(2017, 5, 27).strftime("%d.%m.%Y"))
        self.assertFalse(transaction_2.is_first())
        transaction_2.status = Transactions.STATUS_FINISHED
        transaction_2.save()

        transaction_3 = self._create_inquiry(
            startdate=date(2017, 5, 29).strftime("%d.%m.%Y"), enddate=date(2017, 5, 30).strftime("%d.%m.%Y"))
        self.assertFalse(transaction_3.is_first())
        transaction_3.status = Transactions.STATUS_FINISHED
        transaction_3.save()

        transaction_4 = self._create_inquiry(
            startdate=date(2017, 6, 1).strftime("%d.%m.%Y"), enddate=date(2017, 6, 2).strftime("%d.%m.%Y"))
        self.assertFalse(transaction_4.is_first())
        transaction_4.status = Transactions.STATUS_FINISHED
        transaction_4.save()

        transaction_5 = self._create_inquiry(
            startdate=date(2017, 6, 4).strftime("%d.%m.%Y"), enddate=date(2017, 6, 7).strftime("%d.%m.%Y"))
        self.assertTrue(transaction_5.is_first())
        transaction_5.status = Transactions.STATUS_FINISHED
        transaction_5.save()

        transaction_6 = self._create_inquiry(
            startdate=date(2017, 6, 10).strftime("%d.%m.%Y"), enddate=date(2017, 6, 12).strftime("%d.%m.%Y"))
        self.assertFalse(transaction_6.is_first())

    def test_transaction_is_first_method_and_insurance_fee_property(self):
        # Test similar to `test_transaction_is_first_method`
        # but with tests for `insurance_fee` property
        transaction_1 = self._create_inquiry(
            startdate=date(2017, 5, 9).strftime("%d.%m.%Y"), enddate=date(2017, 5, 12).strftime("%d.%m.%Y"))
        self.assertTrue(transaction_1.is_first())

        ins_1 = calc_insurance(duration=transaction_1.calculated_duration, ad=transaction_1.ad)
        self.assertEqual(ins_1["insurance_commission"].quantize(D('.01')), transaction_1.insurance_fee)
        transaction_1.status = Transactions.STATUS_FINISHED
        transaction_1.save()

        transaction_2 = self._create_inquiry(
            startdate=date(2017, 5, 14).strftime("%d.%m.%Y"), enddate=date(2017, 5, 17).strftime("%d.%m.%Y"))
        self.assertFalse(transaction_2.is_first())

        ins_2 = calc_insurance(duration=transaction_2.calculated_duration, ad=transaction_2.ad)
        self.assertEqual(ins_2["insurance_debt"].quantize(D('.01')), transaction_2.insurance_fee)
        transaction_2.status = Transactions.STATUS_FINISHED
        transaction_2.save()

        transaction_3 = self._create_inquiry(
            startdate=date(2017, 5, 23).strftime("%d.%m.%Y"), enddate=date(2017, 5, 24).strftime("%d.%m.%Y"))
        self.assertTrue(transaction_3.is_first())

        ins_3 = calc_insurance(duration=transaction_3.calculated_duration, ad=transaction_3.ad)
        self.assertEqual(ins_3["insurance_commission"].quantize(D('.01')), transaction_3.insurance_fee)

    def test_response_limit(self):
        transaction = self._create_inquiry()
        url = reverse_lazy('responselimit', args=(self.ad.id, transaction.id))
        response = self.app.post(url, user=self.applicant, params={'response_limit': '2017-12-01 18:30:00'})
        self.assertTrue(response.json['success'])
        transaction.refresh_from_db()
        self.assertEquals(transaction.get_responselimit(), '01.12.2017 um 18:30:00 Uhr')

    def test_update_response_limit(self):
        response_limit = timezone.now() + timedelta(hours=6)
        transaction = self._create_inquiry()
        url = reverse_lazy('responselimit', args=(self.ad.id, transaction.id))
        params = {'response_limit': response_limit.strftime('%Y-%m-%d %H:%M:%S')}
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertTrue(response.json['success'])

    def test_default_response_limit(self):
        fake_now = datetime(2017, 12, 2, 18, 30, 0, 0)  # 2017-12-02 18:30:00
        with freeze_time(fake_now):
            transaction = self._create_inquiry(
                startdate=date(2017, 12, 15).strftime("%d.%m.%Y"), enddate=date(2017, 12, 17).strftime("%d.%m.%Y"))
        # because calculated time later than 20:00, time should be equal to 21:00
        self.assertEquals(transaction.get_responselimit(), '08.12.2017 um 21:00:00 Uhr')

    def test_default_response_limit_rent_date_is_today(self):
        expected_datetime = datetime.now() + timedelta(hours=3)
        rounded_expected_datetime = round_datetime_to_nearest_30_minutes(expected_datetime)
        transaction = self._create_inquiry()
        self.assertEquals(
            transaction.get_responselimit(), rounded_expected_datetime.strftime("%d.%m.%Y um %H:%M:%S Uhr")
        )

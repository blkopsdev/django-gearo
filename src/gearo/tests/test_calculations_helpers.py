from decimal import Decimal as D
from decimal import getcontext

from constance.test import override_config
from django_webtest import WebTest
from nose_parameterized import parameterized

from gearo.helper import calc_insurance, calc_rental, calc_transaction_data
from gearo.tests.mixins import BaseAdvertisementTestMixin

getcontext().prec = 10


class CalculationsHelpersTest(BaseAdvertisementTestMixin, WebTest):

    @parameterized.expand([
        (1, '16.11', '16.35', '14.88', '1.24'),
        (2, '17.79', '18.34', '14.88', '2.92'),
        (5, '23.94', '25.66', '14.88', '9.06'),
        (11, '40.24', '45.06', '14.88', '25.37'),
        (15, '44.42', '50.03', '14.88', '29.54'),
        (18, '47.83', '51.27', '29.75', '18.08'),
        (30, '64.33', '70.90', '29.75', '34.58'),
        (31, '65.94', '69.99', '44.62', '21.31'),
        (35, '72.78', '78.13', '44.62', '28.16')
    ])
    @override_config(ADDITION_TO_INSURANCE_COMMISSION=0.0)
    def test_insurance_costs_calculation_lessor_from_de(self, duration, *args):
        insurance_costs = calc_insurance(duration=duration, ad=self.ad)
        for idx, k in enumerate(['insurance_debt', 'insurance_debt_sum', 'insurance_sum', 'insurance_commission']):
            calculated_value = insurance_costs[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value, 'Incorrect "%s" value for duration %s days.' % (k, duration))

    @parameterized.expand([
        (1, '16.11', '17.54', '14.88', '2.24'),
        (2, '17.79', '19.53', '14.88', '3.92'),
        (5, '23.94', '26.85', '14.88', '10.06'),
        (11, '40.24', '46.25', '14.88', '26.37'),
        (15, '44.42', '51.22', '14.88', '30.54'),
        (18, '47.83', '52.46', '29.75', '19.08'),
        (30, '64.33', '72.09', '29.75', '35.58'),
        (31, '65.94', '71.18', '44.62', '22.31'),
        (35, '72.78', '79.32', '44.62', '29.16')
    ])
    def test_insurance_costs_calculation_with_insurance_addition_lessor_from_de(self, duration, *args):
        # for case config.ADDITION_TO_INSURANCE_COMMISSION = 1 EUR
        insurance_costs = calc_insurance(duration=duration, ad=self.ad)
        for idx, k in enumerate(['insurance_debt', 'insurance_debt_sum', 'insurance_sum', 'insurance_commission']):
            calculated_value = insurance_costs[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value, 'Incorrect "%s" value for duration %s days.' % (k, duration))

    @parameterized.expand([
        (1, '0.58'),
        (2, '0.65'),
        (5, '0.91'),
        (11, '1.60'),
        (15, '1.78'),
        (18, '1.82'),
        (30, '2.52'),
        (31, '2.49'),
        (35, '2.78'),
    ])
    @override_config(ADDITION_TO_INSURANCE_COMMISSION=0.0)
    def test_insurance_costs_calculation_lessor_from_at(self, duration, expected_value):
        ins = calc_insurance(duration=duration, ad=self.ad_from_at)
        calculated_value = ins['insurance_debt_sum'].quantize(D('.01'))
        self.assertEquals(
            calculated_value, D(expected_value), 'Incorrect value for duration {} days.'.format(duration))

    @parameterized.expand([
        (1, '1.81'),
        (2, '1.88'),
        (5, '2.14'),
        (11, '2.83'),
        (15, '3.01'),
        (18, '3.05'),
        (30, '3.75'),
        (31, '3.71'),
        (35, '4.00'),
    ])
    def test_insurance_costs_calculation_with_insurance_addition_lessor_from_at_(self, duration, expected_value):
        # for case config.ADDITION_TO_INSURANCE_COMMISSION = 1 EUR
        ins = calc_insurance(duration=duration, ad=self.ad_from_at)
        calculated_value = ins['insurance_debt_sum'].quantize(D('.01'))
        self.assertEquals(
            calculated_value, D(expected_value), 'Incorrect value for duration {} days.'.format(duration)
        )

    @parameterized.expand([
        (1, '12', '10.08', '0.00', '12.00'),
        (2, '24', '20.17', '0.00', '24.00'),
        (3, '30', '25.21', '6.00', '36.00'),
        (5, '50', '42.02', '10.00', '60.00'),
        (7, '60', '50.42', '24.00', '84.00'),
        (10, '85.71', '72.03', '34.29', '120.00'),
    ])
    def test_rental_costs_calculation(self, duration, *args):
        rental_costs = calc_rental(duration, self.ad)
        for idx, k in enumerate(['brutto', 'netto', 'long_term_benefit', 'daily_rent_price']):
            calculated_value = rental_costs[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value, 'Incorrect "%s" value for duration %s days.' % (k, duration)
            )

    @parameterized.expand([
        (1, '13', '10.83'),
        (2, '26', '21.67'),
        (3, '32.5', '27.08'),
        (5, '54.17', '45.14'),
        (6, '65', '54.17'),
        (7, '65', '54.17'),
        (10, '92.86', '77.38'),
        (15, '139.29', '116.07'),
        (19, '176.43', '147.02'),
    ])
    def test_rental_costs_calculation_lessor_from_at(self, duration, *args):
        rental_costs = calc_rental(duration, self.ad_from_at)
        for idx, k in enumerate(['brutto', 'netto']):
            calculated_value = rental_costs[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value, 'Incorrect "%s" value for duration %s days.' % (k, duration)
            )

    @parameterized.expand([
        (1, '15', '12.61', '0.00', '15.00'),
        (2, '30', '25.21', '0.00', '30.00'),
        (3, '45', '37.82', '0.00', '45.00'),
        (5, '75', '63.03', '0.00', '75.00'),
        (7, '105', '88.24', '0.00', '105.00'),
        (10, '150', '126.05', '0.00', '150.00'),
        (19, '285', '239.50', '0.00', '285.00'),
    ])
    def test_rental_costs_calculation_just_price_per_day_indicated(self, duration, *args):
        rental_costs = calc_rental(duration, self.ad_just_priceday)
        for idx, k in enumerate(['brutto', 'netto', 'long_term_benefit', 'daily_rent_price']):
            calculated_value = rental_costs[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value, 'Incorrect "%s" value for duration %s days.' % (k, duration)
            )

    @parameterized.expand([
        (1, '13.74', '23.82', '4.53', '28.35'),
        (2, '15.42', '35.58', '6.76', '42.34'),
        (3, '17.27', '42.48', '8.07', '50.55'),
        (5, '21.56', '63.58', '12.08', '75.66'),
        (7, '26.80', '77.22', '14.67', '91.89'),
        (10, '36.88', '108.91', '20.69', '129.61'),
        (15, '42.04', '150.09', '28.52', '178.60'),
        (19, '44.28', '181.13', '34.42', '215.55'),
        (26, '53.53', '240.81', '45.75', '286.56'),
        (32, '60.46', '290.95', '55.28', '346.24'),
    ])
    @override_config(ADDITION_TO_INSURANCE_COMMISSION=0.0)
    def test_transaction_data_calculation_lessor_from_de(self, duration, *args):
        rental = calc_rental(duration, self.ad)
        insurance = calc_insurance(duration=duration, ad=self.ad)
        trans = calc_transaction_data(rental, insurance, self.ad)
        for idx, k in enumerate(['insurance_profit', 'netto_total', 'vat', 'total']):
            calculated_value = trans[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value,
                'Incorrect "%s" value for duration %s days.' % (k, duration)
            )

    @parameterized.expand([
        (1, '14.74', '24.82', '4.72', '29.54'),
        (2, '16.42', '36.58', '6.95', '43.53'),
        (3, '18.27', '43.48', '8.26', '51.74'),
        (5, '22.56', '64.58', '12.27', '76.85'),
        (7, '27.80', '78.22', '14.86', '93.08'),
        (10, '37.88', '109.91', '20.88', '130.80'),
        (15, '43.04', '151.09', '28.71', '179.79'),
        (19, '45.28', '182.13', '34.61', '216.74'),
        (26, '54.53', '241.81', '45.94', '287.75'),
        (32, '61.46', '291.95', '55.47', '347.43'),
    ])
    def test_transaction_data_calculation_with_insurance_addition_lessor_from_de(self, duration, *args):
        # for case config.ADDITION_TO_INSURANCE_COMMISSION = 1 EUR
        rental = calc_rental(duration, self.ad)
        insurance = calc_insurance(duration=duration, ad=self.ad)
        trans = calc_transaction_data(rental, insurance, self.ad)
        for idx, k in enumerate(['insurance_profit', 'netto_total', 'vat', 'total']):
            calculated_value = trans[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value, 'Incorrect "%s" value for duration %s days.' % (k, duration)
            )

    @parameterized.expand([
        (1, '0.48', '11.32', '2.26', '13.58'),
        (2, '0.54', '22.21', '4.44', '26.65'),
        (3, '0.61', '27.69', '5.54', '33.23'),
        (5, '0.76', '45.90', '9.18', '55.08'),
        (7, '0.94', '55.11', '11.02', '66.13'),
        (10, '1.30', '78.68', '15.74', '94.42'),
        (15, '1.48', '117.55', '23.51', '141.06'),
        (19, '1.56', '148.58', '29.72', '178.30'),
        (26, '1.89', '203.08', '40.62', '243.69'),
        (32, '2.13', '249.75', '49.95', '299.70'),
    ])
    @override_config(ADDITION_TO_INSURANCE_COMMISSION=0.0)
    def test_transaction_data_calculation_lessor_from_at(self, duration, *args):
        rental = calc_rental(duration, self.ad_from_at)
        insurance = calc_insurance(duration=duration, ad=self.ad_from_at)
        trans = calc_transaction_data(rental, insurance, self.ad_from_at)
        for idx, k in enumerate(['insurance_profit', 'netto_total', 'vat', 'total']):
            calculated_value = trans[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value,
                'Incorrect "%s" value for duration %s days.' % (k, duration)
            )

    @parameterized.expand([
        (1, '1.51', '12.34', '2.47', '14.81'),
        (2, '1.57', '23.23', '4.65', '27.88'),
        (3, '1.63', '28.71', '5.74', '34.46'),
        (5, '1.78', '46.92', '9.38', '56.31'),
        (7, '1.97', '56.13', '11.23', '67.36'),
        (10, '2.32', '79.70', '15.94', '95.64'),
        (15, '2.50', '118.58', '23.72', '142.29'),
        (19, '2.58', '149.61', '29.92', '179.53'),
        (26, '2.91', '204.10', '40.82', '244.92'),
        (32, '3.15', '250.77', '50.15', '300.93'),
    ])
    def test_transaction_data_calculation_with_insurance_addition_lessor_from_at(self, duration, *args):
        # for case config.ADDITION_TO_INSURANCE_COMMISSION = 1 EUR
        rental = calc_rental(duration, self.ad_from_at)
        insurance = calc_insurance(duration=duration, ad=self.ad_from_at)
        trans = calc_transaction_data(rental, insurance, self.ad_from_at)
        for idx, k in enumerate(['insurance_profit', 'netto_total',  'vat', 'total']):
            calculated_value = trans[k].quantize(D('.01'))
            expected_value = D(args[idx])
            self.assertEquals(
                calculated_value, expected_value, 'Incorrect "%s" value for duration %s days.' % (k, duration)
            )

    def test_insurance_costs_for_non_insured_category(self):
        insurance_costs = calc_insurance(duration=7, ad=self.ad_not_insured)
        for k in ['insurance_debt', 'insurance_debt_sum', 'insurance_sum', 'insurance_commission']:
            self.assertEquals(insurance_costs[k], 0.0, 'Incorrect "%s" value.' % k)

from django.contrib.auth.models import AnonymousUser, User
from django.core import mail
from django.core.management import call_command
from django.core.urlresolvers import reverse_lazy
from django.test.utils import override_settings

from allauth.account import app_settings as account_settings
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.providers.facebook.provider import FacebookProvider
from allauth.socialaccount.tests import OAuth2TestsMixin
from allauth.tests import MockedResponse
from django_webtest import WebTest

from gearo.models import GearoProfile, GearoUser, Tokens, UserPayment


@override_settings(
    SOCIALACCOUNT_AUTO_SIGNUP=True,
    ACCOUNT_EMAIL_VERIFICATION=account_settings.EmailVerificationMethod.NONE,
    ACCOUNT_UNIQUE_EMAIL=True,
    SOCIALACCOUNT_ADAPTER='allauth.socialaccount.adapter.DefaultSocialAccountAdapter',
    SOCIALACCOUNT_PROVIDERS={
        'facebook': {
            'AUTH_PARAMS': {},
            'VERIFIED_EMAIL': False
        }
    }
)
class FacebookTests(OAuth2TestsMixin, WebTest):
    fixtures = ['email_templates.json']

    provider_id = FacebookProvider.id

    facebook_data = """
        {
           "id": "1900444716841292",
           "name": "Alexander Gaevsky",
           "first_name": "Alexander",
           "last_name": "Gaevsky",
           "email": "info@classiccars.com.ua",
           "link": "https://www.facebook.com/test.test.5070",
           "username": "/test.test.5070",
           "timezone": 2,
           "verified": true,
           "updated_time": "2017-01-30T20:40:33+0000"
        }"""

    def get_mocked_response(self, data=None):
        if data is None:
            data = self.facebook_data
        return MockedResponse(200, data)

    def _test_accounts(self):
        self.assertEquals(GearoUser.objects.count(), 1)
        self.assertEquals(GearoProfile.objects.count(), 1)
        self.assertEquals(UserPayment.objects.count(), 1)
        gearo_user = GearoUser.objects.first()
        self.assertTrue(gearo_user.facebook_verified)
        self.assertTrue(gearo_user.email_verified)

    def test_user_creation(self):
        self.login(self.get_mocked_response())
        self.assertEquals(User.objects.count(), 1)
        user = User.objects.first()
        self.assertIsNotNone(user.email)
        self.assertIsNotNone(user.profile.avatar)
        self.assertEquals(user.username, 'AlexanderG')
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to, ['info@classiccars.com.ua'])
        self.assertEquals(mail.outbox[0].subject, 'gearo Registrierung abschließen')
        token = Tokens.objects.get(user=user)
        url = reverse_lazy('confirmmail', args=(token.token,))
        self.app.get(url)
        self._test_accounts()

    @override_settings(SOCIALACCOUNT_ADAPTER='gearo.adapter.SocialAccountAdapter')
    def test_login_confirmation_for_existing_account(self):
        self.assertEquals(Tokens.objects.count(), 0)
        user = User.objects.create(username='test', email='info@classiccars.com.ua')
        user.gearouser.email_verified = True
        user.gearouser.save()
        response = self.login(self.get_mocked_response())
        self.assertRedirects(response, '/suche/')
        self.assertEquals(User.objects.count(), 1)
        response = self.client.get('/')
        self.assertIsInstance(response.context['user'], AnonymousUser)
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to, ['info@classiccars.com.ua'])
        self.assertEquals(Tokens.objects.count(), 1)
        token = Tokens.objects.get(user=user)
        url = reverse_lazy('socialaccount_login_confirmation', args=(token.token,))
        response = self.client.get(url, follow=True)
        self.assertEquals(response.context['user'], user)
        self.assertEquals(SocialAccount.objects.count(), 1)
        self.assertEquals(User.objects.count(), 1)
        self._test_accounts()
        self.assertIsNotNone(user.profile.avatar)

from datetime import timedelta

from django.conf import settings
from django.core import mail
from django.core.management import call_command
from django.utils import timezone

from django_webtest import WebTest
from freezegun import freeze_time

from gearo.models import Transactions
from gearo.tests.mixins import BaseAdvertisementTestMixin
from gearo_tasks.tasks.bookingreminders import (
    send_reminder_emails, send_response_limit_long_term_reminders, send_response_limit_short_term_reminders
)


class CeleryTasksTests(BaseAdvertisementTestMixin, WebTest):
    csrf_checks = False

    def test_booking_reminder(self):
        transaction = self._create_inquiry()
        transaction.status = Transactions.STATUS_ACTIVE
        transaction.save()
        send_reminder_emails.delay(one_day_rental=True)
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 4)
        self.assertEquals(mail.outbox[0].to, ['test.lessor@gearo.de'])
        self.assertEquals(mail.outbox[0].subject, 'Deine Vermietung mit  findet heute statt.')
        self.assertEquals(mail.outbox[1].to, ['test.applicant@gearo.de'])
        self.assertEquals(mail.outbox[1].subject, 'Deine Anmietung mit  findet heute statt.')
        self.assertEquals(mail.outbox[2].to, ['test.lessor@gearo.de'])
        self.assertEquals(mail.outbox[2].subject, 'Deine Vermietung mit  endet heute.')
        self.assertEquals(mail.outbox[3].to, ['test.applicant@gearo.de'])
        self.assertEquals(mail.outbox[3].subject, 'Deine Anmietung mit  endet heute.')

    def test_send_response_limit_long_term_reminders(self):

        term = settings.GEARO_NOTIFICATION_LONG_TERM

        transaction = self._create_inquiry()
        transaction.response_limit = (timezone.now() + timedelta(hours=term, minutes=30))
        transaction.status = Transactions.STATUS_WAITING
        transaction.save()
        send_response_limit_long_term_reminders.delay()
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to, ['test.lessor@gearo.de'])
        self.assertEquals(mail.outbox[0].subject, 'Erinnerung: Buchungsanfrage auf gearo!')

    def test_not_send_response_limit_long_term_reminders(self):

        term = settings.GEARO_NOTIFICATION_LONG_TERM

        fake_now = timezone.now() + timedelta(hours=term - 10)
        with freeze_time(fake_now):
            transaction = self._create_inquiry()

        transaction.response_limit = (timezone.now() + timedelta(hours=term, minutes=30))
        transaction.status = Transactions.STATUS_WAITING
        transaction.save()
        transaction.refresh_from_db()

        # Transaction created less than `term` hours before `response_limit`
        time_difference = transaction.response_limit - transaction.created_at
        self.assertLess(time_difference / timedelta(hours=1), term)
        # Reminders will not be sent
        send_response_limit_long_term_reminders.delay()
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 0)

    def test_send_response_limit_short_term_reminders(self):

        term = settings.GEARO_NOTIFICATION_SHORT_TERM

        transaction = self._create_inquiry()
        transaction.response_limit = (timezone.now() + timedelta(hours=term, minutes=23))
        transaction.status = Transactions.STATUS_WAITING
        transaction.save()
        send_response_limit_short_term_reminders.delay()
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to, ['test.lessor@gearo.de'])
        self.assertEquals(mail.outbox[0].subject, 'Antworte innerhalb von 2 Stunden auf deine Buchunganfrage!')

import json
from datetime import timedelta
from decimal import Decimal as D

from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.urlresolvers import reverse_lazy
from django.test import TestCase
from django.test.utils import override_settings
from django.utils import timezone

import mangopay
import requests
from django_webtest import WebTest
from mangopay.api import APIRequest
from mangopay.constants import PAYMENT_STATUS_CHOICES
from mangopay.resources import CardRegistration, ClientWallet
from nose_parameterized import parameterized

from gearo.helper.mangopay import (
    allowed_zip_code, create_mangopay_user_bank_account, get_gearo_payout,
    get_mangopay_user_bank_account, get_preauthorization, update_platform_fee_wallet_balance
)
from gearo.models import Advertisement, AdvertisementCategory, Transactions, UserPayment
from gearo_tasks.tasks.mangopay import check_mp_payouts_statuses, update_mp_wallet_balance

mangopay.sandbox = settings.MANGOPAY_USE_SANDBOX
mangopay.client_id = settings.MANGOPAY_CLIENT_ID
mangopay.passphrase = settings.MANGOPAY_PASSPHRASE
handler = APIRequest(sandbox=settings.MANGOPAY_USE_SANDBOX)


class MangopayTests(WebTest):
    fixtures = ['advertisement_categories.json', 'email_templates.json']
    csrf_checks = False

    def setUp(self):
        self.lessor = User.objects.create_user(
            username='test_lessor',
            email='test.lessor@gearo.de',
            password='initial_password',
            first_name='John',
            last_name='Lessor',
        )

        self.category = AdvertisementCategory.objects.filter(is_insured=True).first()
        self.ad = Advertisement.objects.create(
            title='Test Ad', owner=self.lessor, category=self.category,
            description='test', priceday=12, pricethree=30, priceweek=60,
            articleofvalue=5000, lat=52.5074592, lng=13.2860642,
            pickup_location='Berlin, Deutschland', slug='test-ad'
        )

    def add_bank_details(self, billing_info):
        billing_info.transferswitch = True
        billing_info.first_name = 'John'
        billing_info.last_name = 'Doe'
        billing_info.iban = 'LT121000011101001000'
        billing_info.bic = 'ABOCJPJTXXX'
        billing_info.save()

    def create_applicant(self):  # create tenant
        self.applicant = User.objects.create_user(
            username='test_tenant',
            email='test.tenant@gearo.de',
            password='initial_password',
            first_name='John',
            last_name='Tenant',
        )
        self.applicant.gearouser.street = 'Test str.'
        self.applicant.gearouser.zip = '1234567'
        self.applicant.gearouser.place = 'Test Pl.'
        self.applicant.gearouser.save()
        return self.applicant

    def create_admin(self):
        self.admin = User.objects.create_user(
            username='test_admin',
            email='test.admin@gearo.de',
            password='initial_password',
            first_name='Admin',
            last_name='Tester',
            is_staff=True,
            is_superuser=True,
        )
        return self.admin

    def create_transaction(self, tenant, duration=1):
        create_inquire_url = reverse_lazy('inquiread', args=(self.ad.id,))

        if duration > 1:
            rent_end = (timezone.now() + timedelta(days=duration - 1)).strftime("%d.%m.%Y")
        else:
            rent_end = timezone.now().strftime("%d.%m.%Y")

        params = {
            'startdate': timezone.now().strftime("%d.%m.%Y"),
            'enddate': rent_end
        }
        self.app.post(create_inquire_url, user=tenant, params=params)
        transaction = Transactions.objects.last()
        return transaction

    def create_card_payin(self, tenant, transaction):
        # Get card registration data
        response = self.app.get(
            reverse_lazy('create_card_payment', kwargs={'ad_id': self.ad.id, 'trans_id': transaction.id}),
            user=tenant,
        )
        card_registration_data = json.loads(str(response.content, 'utf-8'))

        # Register card
        response = requests.post(
            card_registration_data['card_registration_url'],
            data={
                'cardNumber': '3569990000000157',
                'cardCvx': '123',
                'cardExpirationDate': '0120',
                'accessKeyRef': card_registration_data['access_key'],
                'data': card_registration_data['preregistration_data']
            })
        card_registration = CardRegistration.get(card_registration_data['card_registration_id'])
        card_registration.registration_data = response.content
        card_registration.save()
        card_id = card_registration.card.get_pk()

        # Create card payment
        self.app.post(
            reverse_lazy('create_card_payment', kwargs={'ad_id': self.ad.id, 'trans_id': transaction.id}),
            params={'card_id': card_id},
            user=tenant,
        )

    def create_card_payin_and_return_related_transaction(self):
        tenant = self.create_applicant()
        transaction = self.create_transaction(tenant)

        self.create_card_payin(tenant, transaction)

        # Receive "SUCCESS" status of card preauthorization
        response = self.app.get(
            reverse_lazy('card_preauthorization_status', kwargs={'ad_id': self.ad.id, 'trans_id': transaction.id}),
            user=tenant,
        )
        response_data = json.loads(str(response.content, 'utf-8'))
        self.assertTrue(response_data.get('success'))

        # Redirect to `bookingsuccess` URL
        response = self.app.get(response_data.get('redirect_path'), user=tenant)
        self.assertEquals(response.status_code, 200)

        return transaction

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_auto_natural_user_creation_and_bank_account_creation(self):
        billing_info = UserPayment.objects.first()

        self.assertEquals(UserPayment.objects.count(), 1)
        self.assertIsNotNone(billing_info.mangopay_n_user_id)
        self.assertIsNotNone(billing_info.mangopay_n_user_wallet_id)
        self.assertIsNone(billing_info.mangopay_l_user_id)  # because user is not business
        self.assertIsNone(billing_info.mangopay_l_user_wallet_id)  # because user is not business

        self.add_bank_details(billing_info)

        billing_info.refresh_from_db()
        create_mangopay_user_bank_account(billing_info.user)

        self.assertIsNotNone(billing_info.mangopay_n_user_bank_account_id)
        self.assertIsNone(billing_info.mangopay_l_user_bank_account_id)  # because user is not business

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_legal_user_creation_if_user_become_business_and_bank_account_creation(self):
        url = reverse_lazy('address_info_edit')
        response = self.app.get(url, user=self.lessor)
        form = response.forms['account_edit_form']
        # Address fields are required
        form['street'] = 'Street'
        form['zip'] = '00001'
        form['place'] = 'City'
        form['country'] = 'DE'

        form['onoffswitch_trader'] = True
        form['tax_number'] = 'DE-00000000042'
        form['tax_ident_number'] = 'DE-00000000042'
        form['business_type'] = 'ohg'  # Soletrader
        form.submit()

        billing_info = UserPayment.objects.first()

        self.assertIsNotNone(billing_info.mangopay_l_user_id)
        self.assertIsNotNone(billing_info.mangopay_l_user_wallet_id)

        self.add_bank_details(billing_info)

        billing_info.refresh_from_db()
        create_mangopay_user_bank_account(billing_info.user)

        self.assertIsNone(billing_info.mangopay_n_user_bank_account_id)  # because user is business
        self.assertIsNotNone(billing_info.mangopay_l_user_bank_account_id)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_making_card_payin_and_payout_lessor_from_de(self):
        transaction = self.create_card_payin_and_return_related_transaction()

        lessor = self.lessor
        lessor_wallet_balance_initial = lessor.billing_info.mangopay_current_wallet_balance
        if lessor_wallet_balance_initial is None:  # initially balance is None, so we convert it to "0"
            lessor_wallet_balance_initial = D('0')
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_initial = gearo_wallet.balance.amount / 100

        # Accept booking by the lessor
        self.app.get(reverse_lazy('acceptlessor', kwargs={'inqid': transaction.id}), user=lessor)

        # Payment should have status "CONFIRMED"
        payment = transaction.payments.last()
        self.assertEqual(payment.status, 'confirmed')

        # After accepting of booking, lessor should receive `transaction.payout` on his wallet
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_new = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_initial + transaction.payout, lessor_wallet_balance_new)
        self.assertEqual(lessor_wallet_balance_new, D('9.86'))

        # After accepting of booking, gearo should receive fees on FEES wallet
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_new = gearo_wallet.balance.amount / 100
        fees = transaction.total_incl_discount - transaction.payout  # 19.68
        self.assertEqual(gearo_wallet_balance_initial + fees, gearo_wallet_balance_new)
        self.assertEqual(gearo_wallet_balance_new - gearo_wallet_balance_initial, D('19.68'))

        # We cannot make payment if lessor does not have bank account, so we will create it
        lessor.billing_info.refresh_from_db()
        self.add_bank_details(lessor.billing_info)
        create_mangopay_user_bank_account(lessor)

        # Usual users cannot make PyOuts
        resp = self.app.get(reverse_lazy('mangopay_payout', kwargs={'trans_id': transaction.id}), user=lessor)
        self.assertEquals(resp.status_code, 302)  # redirect to login page
        self.assertEquals(resp.location, '/admin/login/?next=/inserat/anfragen/mangopay_payout/1/')

        # PyOuts can be made just by staff member
        admin = self.create_admin()
        resp = self.app.get(reverse_lazy('mangopay_payout', kwargs={'trans_id': transaction.id}), user=admin)
        self.assertEquals(resp.status_code, 302)  # redirect to admin
        self.assertEquals(resp.location, '/admin/gearo/transactions/')

        # Check PayOut status
        gearo_payout = get_gearo_payout(transaction)
        self.assertEqual(gearo_payout.payout_status, 'CREATED')

        check_mp_payouts_statuses()
        update_mp_wallet_balance(lessor.id)

        # After PayOut, balance of lessor's wallet will be lower on `transaction.payout` value
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_after_payout = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_new - transaction.payout, lessor_wallet_balance_after_payout)
        self.assertEqual(lessor_wallet_balance_after_payout, D('0'))

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_making_card_payin_and_payout_lessor_from_at(self):
        lessor = self.lessor
        lessor.gearouser.country = 'AT'
        lessor.gearouser.save()

        transaction = self.create_card_payin_and_return_related_transaction()

        lessor_wallet_balance_initial = lessor.billing_info.mangopay_current_wallet_balance
        if lessor_wallet_balance_initial is None:  # initially balance is None, so we convert it to "0"
            lessor_wallet_balance_initial = D('0')
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_initial = gearo_wallet.balance.amount / 100

        # Accept booking by the lessor
        self.app.get(reverse_lazy('acceptlessor', kwargs={'inqid': transaction.id}), user=lessor)

        # Payment should have status "CONFIRMED"
        payment = transaction.payments.last()
        self.assertEqual(payment.status, 'confirmed')

        # After accepting of booking, lessor should receive `transaction.payout` on his wallet
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_new = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_initial + transaction.payout, lessor_wallet_balance_new)
        self.assertEqual(lessor_wallet_balance_new, D('10.20'))

        # After accepting of booking, gearo should receive fees on FEES wallet
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_new = gearo_wallet.balance.amount / 100
        fees = transaction.total_incl_discount - transaction.payout  # 22.65
        self.assertEqual(gearo_wallet_balance_initial + fees, gearo_wallet_balance_new)
        self.assertEqual(gearo_wallet_balance_new - gearo_wallet_balance_initial, D('22.65'))

        # We cannot make payment if lessor does not have bank account, so we will create it
        lessor.billing_info.refresh_from_db()
        self.add_bank_details(lessor.billing_info)
        create_mangopay_user_bank_account(lessor)

        # Usual users cannot make PyOuts
        resp = self.app.get(reverse_lazy('mangopay_payout', kwargs={'trans_id': transaction.id}), user=lessor)
        self.assertEquals(resp.status_code, 302)  # redirect to login page
        self.assertEquals(resp.location, '/admin/login/?next=/inserat/anfragen/mangopay_payout/1/')

        # PyOuts can be made just by staff member
        admin = self.create_admin()
        resp = self.app.get(reverse_lazy('mangopay_payout', kwargs={'trans_id': transaction.id}), user=admin)
        self.assertEquals(resp.status_code, 302)  # redirect to admin
        self.assertEquals(resp.location, '/admin/gearo/transactions/')

        # Check PayOut status
        gearo_payout = get_gearo_payout(transaction)
        self.assertEqual(gearo_payout.payout_status, 'CREATED')

        check_mp_payouts_statuses()
        update_mp_wallet_balance(lessor.id)

        # After PayOut, balance of lessor's wallet will be lower on `transaction.payout` value
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_after_payout = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_new - transaction.payout, lessor_wallet_balance_after_payout)
        self.assertEqual(lessor_wallet_balance_after_payout, D('0'))

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_making_card_payin_and_then_release_payment(self):
        transaction = self.create_card_payin_and_return_related_transaction()

        lessor = self.lessor
        lessor_wallet_balance_initial = lessor.billing_info.mangopay_current_wallet_balance
        if lessor_wallet_balance_initial is None:  # initially balance is None, so we convert it to "0"
            lessor_wallet_balance_initial = D('0')
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_initial = gearo_wallet.balance.amount / 100

        # Reject booking by the lessor
        self.app.get(reverse_lazy('rejectlessor', kwargs={'inqid': transaction.id}), user=lessor)

        # Payment should have status "REFUNDED"
        payment = transaction.payments.last()
        self.assertEqual(payment.status, 'refunded')

        # Preauthorization should have status "CANCELED"
        preauthorization_id = payment.preauthorization.preauthorization_id
        preauthorization = get_preauthorization(preauthorization_id)
        self.assertEqual(preauthorization.payment_status, PAYMENT_STATUS_CHOICES.canceled)

        # After rejecting of booking, balance on lessor's wallet should not be changed
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_new = lessor.billing_info.mangopay_current_wallet_balance
        if lessor_wallet_balance_new is None:  # new balance should be None as well, so we convert it to "0"
            lessor_wallet_balance_new = D('0')
        self.assertEqual(lessor_wallet_balance_initial, lessor_wallet_balance_new)
        self.assertEqual(lessor_wallet_balance_new, D('0'))

        # After rejecting of booking, balance on gearo fees' wallet should not be changed
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_new = gearo_wallet.balance.amount / 100
        self.assertEqual(gearo_wallet_balance_initial, gearo_wallet_balance_new)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_making_card_payin_and_then_refund_payment_lessor_from_de(self):
        transaction = self.create_card_payin_and_return_related_transaction()

        lessor = self.lessor
        lessor_wallet_balance_initial = lessor.billing_info.mangopay_current_wallet_balance
        if lessor_wallet_balance_initial is None:  # initially balance is None, so we convert it to "0"
            lessor_wallet_balance_initial = D('0')
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_initial = gearo_wallet.balance.amount / 100

        # Accept booking by the lessor
        self.app.get(reverse_lazy('acceptlessor', kwargs={'inqid': transaction.id}), user=lessor)

        # After accepting of booking, lessor should receive `transaction.payout` on his wallet
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_new = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_initial, D('0'))
        self.assertEqual(lessor_wallet_balance_initial + transaction.payout, lessor_wallet_balance_new)
        self.assertEqual(lessor_wallet_balance_new, D('9.86'))

        # After accepting of booking, gearo should receive fees on FEES wallet
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_new = gearo_wallet.balance.amount / 100
        fees = transaction.total_incl_discount - transaction.payout  # 19.68
        self.assertEqual(gearo_wallet_balance_initial + fees, gearo_wallet_balance_new)
        self.assertEqual(gearo_wallet_balance_new - gearo_wallet_balance_initial, D('19.68'))

        # Cancel booking by the lessor
        self.app.get(reverse_lazy('cancel', kwargs={'inqid': transaction.id}), user=lessor)

        # Payment should have status "REFUNDED"
        payment = transaction.payments.last()
        self.assertEqual(payment.status, 'refunded')

        # After canceling of booking, balance of lessor's wallet will be lower on `transaction.payout` value
        # Means balance of lessor's wallet will return to initial state (before capturing of payment was made)
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_after_refund = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_initial, lessor_wallet_balance_after_refund)
        self.assertEqual(lessor_wallet_balance_new - lessor_wallet_balance_after_refund, D('9.86'))

        # After canceling of booking, balance of gearo FEES wallet will be lower on `fees` value
        # Means balance of FEES wallet will return to initial state (before capturing of payment was made)
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_after_refund = gearo_wallet.balance.amount / 100
        self.assertEqual(gearo_wallet_balance_initial, gearo_wallet_balance_after_refund)
        self.assertEqual(gearo_wallet_balance_new - gearo_wallet_balance_after_refund, D('19.68'))

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_making_card_payin_and_then_refund_payment_lessor_from_at(self):
        lessor = self.lessor
        lessor.gearouser.country = 'AT'
        lessor.gearouser.save()

        transaction = self.create_card_payin_and_return_related_transaction()

        lessor = self.lessor
        lessor_wallet_balance_initial = lessor.billing_info.mangopay_current_wallet_balance
        if lessor_wallet_balance_initial is None:  # initially balance is None, so we convert it to "0"
            lessor_wallet_balance_initial = D('0')
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_initial = gearo_wallet.balance.amount / 100

        # Accept booking by the lessor
        self.app.get(reverse_lazy('acceptlessor', kwargs={'inqid': transaction.id}), user=lessor)

        # After accepting of booking, lessor should receive `transaction.payout` on his wallet
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_new = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_initial, D('0'))
        self.assertEqual(lessor_wallet_balance_initial + transaction.payout, lessor_wallet_balance_new)
        self.assertEqual(lessor_wallet_balance_new, D('10.20'))

        # After accepting of booking, gearo should receive fees on FEES wallet
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_new = gearo_wallet.balance.amount / 100
        fees = transaction.total_incl_discount - transaction.payout  # 22.65
        self.assertEqual(gearo_wallet_balance_initial + fees, gearo_wallet_balance_new)
        self.assertEqual(gearo_wallet_balance_new - gearo_wallet_balance_initial, D('22.65'))

        # Cancel booking by the lessor
        self.app.get(reverse_lazy('cancel', kwargs={'inqid': transaction.id}), user=lessor)

        # Payment should have status "REFUNDED"
        payment = transaction.payments.last()
        self.assertEqual(payment.status, 'refunded')

        # After canceling of booking, balance of lessor's wallet will be lower on `transaction.payout` value
        # Means balance of lessor's wallet will return to initial state (before capturing of payment was made)
        lessor.billing_info.refresh_from_db()
        lessor_wallet_balance_after_refund = lessor.billing_info.mangopay_current_wallet_balance
        self.assertEqual(lessor_wallet_balance_initial, lessor_wallet_balance_after_refund)
        self.assertEqual(lessor_wallet_balance_new - lessor_wallet_balance_after_refund, D('10.20'))

        # After canceling of booking, balance of gearo FEES wallet will be lower on `fees` value
        # Means balance of FEES wallet will return to initial state (before capturing of payment was made)
        gearo_wallet = ClientWallet.get(funds_type='FEES', currency='EUR')
        gearo_wallet_balance_after_refund = gearo_wallet.balance.amount / 100
        self.assertEqual(gearo_wallet_balance_initial, gearo_wallet_balance_after_refund)
        self.assertEqual(gearo_wallet_balance_new - gearo_wallet_balance_after_refund, D('22.65'))

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_views_get_only_one_payment(self):
        tenant = self.create_applicant()
        transaction = self.create_transaction(tenant)

        self.create_card_payin(tenant, transaction)
        self.create_card_payin(tenant, transaction)
        self.create_card_payin(tenant, transaction)

        # Receive "SUCCESS" status of card preauthorization
        response = self.app.get(
            reverse_lazy('card_preauthorization_status', kwargs={'ad_id': self.ad.id, 'trans_id': transaction.id}),
            user=tenant,
        )
        response_data = json.loads(str(response.content, 'utf-8'))
        self.assertTrue(response_data.get('success'))  # means we received just one payment

        response = self.app.get(
            reverse_lazy('mangopay_redirect', kwargs={'ad_id': self.ad.id, 'trans_id': transaction.id}),
            user=tenant,
        )

        # data that we check in this view related to Mangopay, so we cannot set it from our side,
        # but if view will get just one payment we will receive 302 instead of `MultipleObjectsReturned`
        self.assertEquals(response.status_code, 302)  # means we received just one payment

        # Redirect to `bookingsuccess` URL
        response = self.app.get(response_data.get('redirect_path'), user=tenant)
        self.assertEquals(response.status_code, 200)  # means we received just one payment on `bookingsuccess` page

    def test_create_card_payin_with_3dsecure_redirect(self):
        tenant = self.create_applicant()
        # For rent period bigger than 4 days, total payment will be bigger than 50 EUR
        transaction = self.create_transaction(tenant, duration=6)

        self.create_card_payin(tenant, transaction)

        # Receive "SUCCESS" status of card preauthorization
        response = self.app.get(
            reverse_lazy('card_preauthorization_status', kwargs={'ad_id': self.ad.id, 'trans_id': transaction.id}),
            user=tenant,
        )
        response_data = json.loads(str(response.content, 'utf-8'))
        self.assertTrue(response_data.get('success'))

        redirect_path = response_data.get('redirect_path')
        self.assertIsNotNone(redirect_path.startswith('https://api.sandbox.mangopay.com/Redirect/'))

    @parameterized.expand(['mangopay_sofort', 'mangopay_paypal'])
    def test_web_payin_payment_view(self, payment_variant):
        tenant = self.create_applicant()
        transaction = self.create_transaction(tenant)

        response = self.app.post(
            reverse_lazy(
                'create_web_payin_payment',
                kwargs={'ad_id': self.ad.id, 'trans_id': transaction.id, 'payment_variant': payment_variant}),
            user=tenant,
        )
        response_data = json.loads(str(response.content, 'utf-8'))
        self.assertTrue(response_data.get('success'))
        redirect_path = response_data.get('redirect_path')
        if payment_variant == 'mangopay_sofort':
            self.assertTrue(redirect_path.startswith('https://homologation-secure-p.payline.com/webpayment/'))
        else:
            self.assertTrue(redirect_path.startswith('https://www.sandbox.paypal.com/cgi-bin/'))

    def test_save_correct_data_on_bank_details_page(self):
        lessor = self.lessor
        url = reverse_lazy('bank_info_edit')
        response = self.app.get(url, user=lessor)
        form = response.forms['payment_edit']
        form['first_name'] = 'Test'
        form['last_name'] = 'Tester'
        form['iban'] = 'LT121000011101001000'
        form['bic'] = 'ABOCJPJTXXX'
        form.submit()

        lessor.billing_info.refresh_from_db()
        self.assertIsNotNone(get_mangopay_user_bank_account(lessor))
        self.assertIsNone(lessor.billing_info.mangopay_bank_account_error)

    def test_save_incorrect_data_on_bank_details_page(self):
        lessor = self.lessor
        url = reverse_lazy('bank_info_edit')
        response = self.app.get(url, user=lessor)
        form = response.forms['payment_edit']
        form['first_name'] = ''
        form['last_name'] = 'Tester'
        form['iban'] = 'LT12100001110100100_'
        form['bic'] = 'ABOCJPJTXX_'
        response = form.submit()

        lessor.billing_info.refresh_from_db()
        self.assertIsNone(get_mangopay_user_bank_account(lessor))
        self.assertIsNotNone(lessor.billing_info.mangopay_bank_account_error)

        self.assertIn('Bitte überprüfe deine IBAN', response.unicode_normal_body)
        self.assertIn('Bitte überprüfe deine BIC', response.unicode_normal_body)
        self.assertIn('Du musst alle Felder ausfüllen', response.unicode_normal_body)  # empty `first_name`


class HelpersTests(TestCase):
    """
    Tests for helpers that not related to Mangopay users
    """

    def test_zip_code_conversion(self):
        self.assertEquals(allowed_zip_code('jkldhiö'), 'jkldhio')
        self.assertEquals(allowed_zip_code('Aq_13'), 'Aq 13')
        self.assertEquals(allowed_zip_code('ABC-42'), 'ABC-42')
        self.assertEquals(allowed_zip_code('CV23 0PB'), 'CV23 0PB')  # Rugby, UK
        self.assertEquals(allowed_zip_code('33028'), '33028')  # Rivne, Ukraine
        self.assertEquals(allowed_zip_code('test'), 'test')
        self.assertEquals(allowed_zip_code('pAt_zip'), 'pAt zip')
        self.assertEquals(allowed_zip_code('dasasddsa'), 'dasasddsa')
        self.assertEquals(allowed_zip_code('plz vermieter'), 'plz vermieter')
        self.assertEquals(allowed_zip_code('ḦḸṠÖ ä'), 'HLSO a')

    def test_platform_fee_wallet_balance(self):
        update_platform_fee_wallet_balance()
        fee_wallet_balance = cache.get('fee_wallet_balance')
        self.assertIsNotNone(fee_wallet_balance)

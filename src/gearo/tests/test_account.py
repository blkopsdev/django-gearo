from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy

from django_webtest import WebTest
from nose_parameterized import parameterized

from gearo.models import GearoProfile, GearoUser, UserPayment


class AccountTests(WebTest):

    def setUp(self):
        self.user = User.objects.create_user(
            username='test_lessor',
            email='test.lessor@gearo.de',
            password='initial_password',
            first_name='TestUser',
            last_name='Tester',
        )
        super(AccountTests, self).setUp()

    def test_account_created(self):
        self.assertEquals(GearoProfile.objects.count(), 1)
        self.assertEquals(GearoUser.objects.count(), 1)
        self.assertEquals(UserPayment.objects.count(), 1)

    def test_password_change(self):
        url = reverse_lazy('change_password')
        response = self.app.get(url, user=self.user)
        form = response.forms['account_edit_form']
        form['old_password'] = 'initial_password'
        form['new_password'] = 'changed_password'
        response = form.submit()
        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password('changed_password'), response.context['form'].errors)

    @parameterized.expand([
        ('+4930182720',),
        ('+43-1-21145-0',),
        ('+43-1-2160886',),
        ('+43 81 000 0225',),
        ('+49 703 464 095 42',),
        ('+43 1-21145-7800',),
        ('+43 1 61064-0',),
        ('+43 1 61064-200',),
        ('+49(0)30 18 272-0',),
        ('+49(0)30 18 272-2739',),
        ('+4915140530174',),
    ])
    def test_gearouser_saving_valid_phone_mobile(self, phone_mobile):
        url = reverse_lazy('contact_info_edit')
        response = self.app.get(url, user=self.user)
        form = response.forms['account_edit_form']
        form['phone_mobile'] = phone_mobile
        form.submit()
        self.user.gearouser.refresh_from_db()
        self.assertEqual(self.user.gearouser.phone_mobile, phone_mobile)

    @parameterized.expand([
        ('+49',),
        ('+43-1-21145-111111110',),
        ('+43',),
        ('+43 81 0225',),
        ('+43 _____',),
        ('+43 1 0000000000000000-0',),
        ('+43 1 number+-200',),
        ('+49(0)30 (18))= 272-0',),
        ('+49(0)30 18 27--_2739',),
        ('+49-15-14-05-30-1-874',),
    ])
    def test_gearouser_saving_invalid_phone_mobile(self, phone_mobile):
        url = reverse_lazy('contact_info_edit')
        response = self.app.get(url, user=self.user)
        form = response.forms['account_edit_form']
        form['phone_mobile'] = phone_mobile
        response = form.submit()
        self.assertIn('Bitte füge eine gültige Handynummer ein.', response.unicode_normal_body)

    def test_gearouser_saving_empty_phone_mobile(self):
        url = reverse_lazy('contact_info_edit')
        response = self.app.get(url, user=self.user)
        form = response.forms['account_edit_form']
        form['phone_mobile'] = ''
        response = form.submit()
        self.assertIn('Bitte gib deine Nummer an.', response.unicode_normal_body)

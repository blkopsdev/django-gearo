from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.db.models import signals
from django.utils import timezone

from constance import config
from payments import get_payment_model

from gearo.models import Advertisement, AdvertisementCategory, Notifications, Transactions
from gearo.receivers import send_add_notifications

Payment = get_payment_model()


class BaseAdvertisementTestMixin(object):
    fixtures = ['advertisement_categories.json', 'email_templates.json']

    def setUp(self):
        signals.post_save.disconnect(receiver=send_add_notifications, sender=Notifications)
        self.user = self.lessor = User.objects.create_user(username='test_lessor', email='test.lessor@gearo.de')

        self.applicant = User.objects.create_user(username='test_applicant', email='test.applicant@gearo.de')
        self.applicant.gearouser.street = 'Test str.'
        self.applicant.gearouser.zip = '1234567'
        self.applicant.gearouser.place = 'Test Pl.'
        self.applicant.gearouser.save()

        self.category = AdvertisementCategory.objects.filter(is_insured=True).first()
        self.ad = Advertisement.objects.create(
            title='Test Ad', owner=self.lessor, category=self.category,
            description='test', priceday=12, pricethree=30, priceweek=60,
            articleofvalue=5000, lat=52.5074592, lng=13.2860642,
            pickup_location='Berlin, Deutschland', slug='test-ad'
        )

        self.category_not_insured = AdvertisementCategory.objects.filter(is_insured=False).first()
        self.ad_not_insured = Advertisement.objects.create(
            title='Test Ad', owner=self.lessor,
            category=self.category_not_insured,
            description='test', priceday=12, pricethree=30, priceweek=60,
            articleofvalue=5000, lat=52.5074592, lng=13.2860642,
            pickup_location='Berlin, Deutschland', slug='test-ad-not-ins'
        )

        self.lessor_from_at = User.objects.create_user(username='test_lessor_AT', email='test.lessor.at@gearo.de')
        self.lessor_from_at.gearouser.street = 'Obere Donaustrasse 95'
        self.lessor_from_at.gearouser.zip = 'A-1020'
        self.lessor_from_at.gearouser.place = 'Wien'
        self.lessor_from_at.gearouser.country = 'AT'  # 'Österreich'
        self.lessor_from_at.gearouser.save()

        self.ad_from_at = Advertisement.objects.create(
            title='Test Ad from Austria', owner=self.lessor_from_at,
            category=self.category, description='Some test description',
            priceday=13, pricethree=32.50, priceweek=65, articleofvalue=148,
            lat=52.5074592, lng=13.2860642,
            pickup_location='Wien, Österreich', slug='test-ad-from-at',
        )

        self.ad_just_priceday = Advertisement.objects.create(
            title='Test Ad with just price per day indicated', owner=self.lessor,
            category=self.category, description='Some test description',
            priceday=15, articleofvalue=136,
            lat=52.5074592, lng=13.2860642,
            pickup_location='Berlin, Deutschland', slug='test-ad-from-de-just-priceday',
        )

        config.FIRST_RATE = 10.4
        config.SECOND_RATE = 2.5
        config.INSURANCE_RATE = 6.0
        config.INSURANCE_COMMISSION = 0.5

    def _create_inquiry(self, **extra_params):
        existing_transactions_number = Transactions.objects.count()
        create_inquire_url = reverse_lazy('inquiread', args=(self.ad.id,))
        params = {
            'startdate': timezone.now().strftime("%d.%m.%Y"),
            'enddate': timezone.now().strftime("%d.%m.%Y")
        }
        params.update(**extra_params)
        response = self.app.post(
            create_inquire_url, user=self.applicant, params=params)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Transactions.objects.count(), existing_transactions_number + 1)
        transaction = Transactions.objects.last()
        return transaction

    def _create_payment(self, transaction):
        existing_payments_number = Payment.objects.count()
        url = reverse_lazy('createpayment', args=(self.ad.id, transaction.id))
        response = self.app.post(url, user=self.applicant)
        self.assertTrue(response.json['success'])
        self.assertEquals(Payment.objects.count(), existing_payments_number + 1)
        payment = Payment.objects.last()
        return payment


class AttachmentTestMixin(object):
    csrf_checks = False

    def setUp(self):
        super(AttachmentTestMixin, self).setUp()
        self.user = User.objects.create(username='test', email='info@classiccars.com.ua')

from datetime import date, timedelta
from decimal import Decimal

from django.core import mail
from django.core.management import call_command
from django.core.urlresolvers import reverse_lazy
from django.test.utils import override_settings
from django.utils import timezone

from constance.test import override_config
from django_webtest import WebTest

from gearo.models import AdvertisementQuestions, Transactions, Voucher
from gearo.tests.mixins import BaseAdvertisementTestMixin
from gearo_tasks.tasks.advertisement import update_advertisement


class AdvertisementTest(BaseAdvertisementTestMixin, WebTest):
    csrf_checks = False

    def test_prefill_ad_fields_from_latest(self):
        url = reverse_lazy('advertise')
        response = self.app.get(url, user=self.lessor)
        form = response.forms['advertise-form']
        self.assertEquals(form['pickup_location'].value, 'Berlin, Deutschland')
        self.assertEquals(form['lat'].value, '52.5074592')
        self.assertEquals(form['lng'].value, '13.2860642')

    def test_remove_advertisement(self):
        ad_detail_url = reverse_lazy('addetail', args=(self.ad.slug,))
        remove_ad_url = reverse_lazy('adremove')
        response = self.app.get(ad_detail_url)
        self.assertEquals(response.status_code, 200)
        response = self.app.post(remove_ad_url, user=self.lessor, params={'id': self.ad.id})
        self.assertTrue(response.json['success'])
        response = self.app.get(ad_detail_url, status=404)
        self.assertEquals(response.status_code, 404)

    @override_config(ADDITION_TO_INSURANCE_COMMISSION=0.0)
    def test_rent_calc_view(self):
        url = reverse_lazy('calc', args=(self.ad.id,))
        start_date = timezone.now()
        end_date = timezone.now() + timedelta(days=4)
        params = {'start_date': start_date.strftime("%Y-%m-%d"), 'end_date': end_date.strftime("%Y-%m-%d")}
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['duration'], 3.0)
        self.assertEquals(response.json['rent'], 30.0)
        self.assertEquals(response.json['total'], 50.55)

        params['pickup_after_3pm'] = True
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['duration'], 4.0)
        self.assertEquals(response.json['rent'], 40.0)
        self.assertEquals(response.json['total'], 62.98)

        params['return_before_12pm'] = True
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['duration'], 5.0)
        self.assertEquals(response.json['rent'], 50.0)
        self.assertEquals(response.json['total'], 75.66)

    def test_rent_calc_view_with_insurance_addition(self):
        # for case config.ADDITION_TO_INSURANCE_COMMISSION = 1 EUR
        url = reverse_lazy('calc', args=(self.ad.id,))
        start_date = timezone.now()
        end_date = timezone.now() + timedelta(days=4)
        params = {'start_date': start_date.strftime("%Y-%m-%d"), 'end_date': end_date.strftime("%Y-%m-%d")}
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['duration'], 3.0)
        self.assertEquals(response.json['rent'], 30.0)
        self.assertEquals(response.json['total'], 51.74)

        params['pickup_after_3pm'] = True
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['duration'], 4.0)
        self.assertEquals(response.json['rent'], 40.0)
        self.assertEquals(response.json['total'], 64.17)

        params['return_before_12pm'] = True
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['duration'], 5.0)
        self.assertEquals(response.json['rent'], 50.0)
        self.assertEquals(response.json['total'], 76.85)

    def test_duration_calculation(self):
        url = reverse_lazy('calc', args=(self.ad.id,))
        start_date = date(2017, 3, 22)
        end_date = date(2017, 3, 27)
        params = {'start_date': start_date.strftime("%Y-%m-%d"), 'end_date': end_date.strftime("%Y-%m-%d")}
        response = self.app.post(url, user=self.applicant, params=params)
        self.assertEquals(response.json['duration'], 4.0)
        transaction = self._create_inquiry(
            startdate=start_date.strftime("%d.%m.%Y"),
            enddate=end_date.strftime("%d.%m.%Y")
        )
        self.assertEquals(transaction.calculated_duration, 4.0)

    @override_settings(CELERY_ALWAYS_EAGER=True)
    def test_ad_slug_generation(self):
        update_advertisement.delay(self.ad.id)
        self.ad.refresh_from_db()
        self.assertEquals(self.ad.pickup_city, 'Berlin')
        self.assertEquals(self.ad.slug, 'berlin-test-ad-mieten')

    @override_config(ADDITION_TO_INSURANCE_COMMISSION=0.0)
    def test_create_payment(self):
        transaction = self._create_inquiry()
        payment = self._create_payment(transaction)
        self.assertEquals(payment.description, 'Anmietung auf Gearo, gearo Buchung Nr. 1')
        self.assertEquals(payment.total, Decimal('28.35'))

    def test_create_payment_with_insurance_addition(self):
        # for case config.ADDITION_TO_INSURANCE_COMMISSION = 1 EUR
        transaction = self._create_inquiry()
        payment = self._create_payment(transaction)
        self.assertEquals(payment.description, 'Anmietung auf Gearo, gearo Buchung Nr. 1')
        self.assertEquals(payment.total, Decimal('29.54'))

    def test_booking_success(self):
        transaction = self._create_inquiry()
        payment = self._create_payment(transaction)
        payment.status = 'preauth'
        payment.save()
        url = reverse_lazy('bookingsuccess', args=(self.ad.id, payment.publicid))
        self.app.get(url, user=self.applicant)
        transaction.refresh_from_db()
        self.assertEquals(transaction.status, Transactions.STATUS_WAITING)
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 2)
        self.assertEquals(mail.outbox[0].to, ['test.lessor@gearo.de'])
        self.assertEquals(mail.outbox[1].to, ['test.applicant@gearo.de'])

    @override_config(ADDITION_TO_INSURANCE_COMMISSION=0.0)
    def test_voucher(self):
        voucher = Voucher.objects.create(code='promodiscount', value=10, owner=self.applicant)
        transaction = self._create_inquiry()
        url = reverse_lazy('addvoucher', args=(self.ad.id, transaction.id))
        response = self.app.post(url, user=self.applicant, params={'code': voucher.code})
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['discount'], '1.20')
        self.assertEquals(response.json['new_debt'], '10.80')

    def test_voucher_with_insurance_addition(self):
        # for case config.ADDITION_TO_INSURANCE_COMMISSION = 1 EUR
        voucher = Voucher.objects.create(code='promodiscount', value=10, owner=self.applicant)
        transaction = self._create_inquiry()
        url = reverse_lazy('addvoucher', args=(self.ad.id, transaction.id))
        response = self.app.post(url, user=self.applicant, params={'code': voucher.code})
        self.assertTrue(response.json['success'])
        self.assertEquals(response.json['discount'], '1.20')
        self.assertEquals(response.json['new_debt'], '10.80')

    def test_ask_question(self):
        existing_questions = AdvertisementQuestions.objects.count()
        url = reverse_lazy('asked', args=(self.ad.id,))
        response = self.app.post(url, user=self.applicant, params={'question': 'test'})
        self.assertTrue(response.json['success'])
        self.assertEquals(AdvertisementQuestions.objects.count(), existing_questions + 1)
        question = AdvertisementQuestions.objects.last()
        self.assertEquals(question.ad, self.ad)
        self.assertEquals(question.questioner, self.applicant)
        self.assertEquals(question.question, 'test')
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].to, ['test.lessor@gearo.de'])

    def test_reply_question(self):
        question = AdvertisementQuestions.objects.create(
            ad=self.ad, questioner=self.applicant, question='test question'
        )
        url = reverse_lazy('reply', args=(self.ad.id, question.id))
        response = self.app.post(url, user=self.lessor, params={'answer': 'test answer'})
        self.assertTrue(response.json['success'])
        question.refresh_from_db()
        self.assertEquals(question.answer, 'test answer')
        self.assertIsNotNone(question.answered_date)

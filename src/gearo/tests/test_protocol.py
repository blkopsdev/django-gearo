from datetime import time

from django.contrib.auth.models import User
from django.core import mail
from django.core.management import call_command
from django.core.urlresolvers import reverse_lazy

from django_webtest import WebTest

from gearo.models import Rohprotocol
from gearo.tests.mixins import BaseAdvertisementTestMixin


class ProtocolTest(BaseAdvertisementTestMixin, WebTest):
    csrf_checks = False

    def setUp(self):
        super(ProtocolTest, self).setUp()
        self.admin_user = User.objects.create_user(username='testadmin', email='test.admin@gearo.de')
        self.admin_user.is_superuser = True
        self.admin_user.is_staff = True
        self.admin_user.save()

    def _fill_in_handing_over_protocol(self):
        self.transaction = self._create_inquiry()
        url = reverse_lazy('handingover', args=(self.transaction.id,))
        response = self.app.get(url, user=self.user)
        form = response.forms['handing_over_protocol_form']
        form['serial_number'] = 'SN1234567'
        form['identity_number'] = 'ID1234567'
        form['identity_card'] = Rohprotocol.ID_PERSONAL
        form['lessor_signature'] = 'lessor_fakesign'
        form['applicant_signature'] = 'applicant_fakesign'
        form['return_time'] = '12:00'
        form.submit()
        self.transaction.refresh_from_db()
        self.assertIsNotNone(self.transaction.handing_over_protocol)
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 2)
        self.assertEquals(mail.outbox[0].to, ['test.applicant@gearo.de'])
        self.assertEquals(
            mail.outbox[0].subject, 'Deine Kopie des Übergabeprotokolls mit  für das Inserat Test Ad')
        self.assertEquals(mail.outbox[1].to, ['test.lessor@gearo.de'])
        self.assertEquals(
            mail.outbox[1].subject, 'Deine Kopie des Übergabeprotokolls mit test_applicant für das Inserat Test Ad')

    def _fill_in_return_protocol(self, undamaged='True'):
        self.transaction = self._create_inquiry()
        url = reverse_lazy('returnproto', args=(self.transaction.id,))
        response = self.app.get(url, user=self.user)
        form = response.forms['return_protocol_form']
        form['lessor_signature'] = 'lessor_fakesign'
        form['applicant_signature'] = 'applicant_fakesign'
        form['undamaged'] = undamaged
        form.submit()
        self.transaction.refresh_from_db()
        self.assertIsNotNone(self.transaction.return_protocol)
        self.assertEquals(self.transaction.return_protocol.lessor_signature, 'lessor_fakesign')
        self.assertEquals(self.transaction.return_protocol.applicant_signature, 'applicant_fakesign')

    def test_handing_over_protocol(self):
        self._fill_in_handing_over_protocol()
        protocol = self.transaction.handing_over_protocol
        self.assertEquals(protocol.serial_number, 'SN1234567')
        self.assertEquals(protocol.identity_number, 'ID1234567')
        self.assertEquals(protocol.identity_card, Rohprotocol.ID_PERSONAL)
        self.assertEquals(protocol.lessor_signature, 'lessor_fakesign')
        self.assertEquals(protocol.applicant_signature, 'applicant_fakesign')
        self.assertEquals(protocol.return_time, time(12, 0))

    def test_serial_number_reuse(self):
        self._fill_in_handing_over_protocol()
        self.ad.refresh_from_db()
        self.assertEquals(self.ad.serial_number, 'SN1234567')
        url = reverse_lazy('handingover', args=(self.transaction.id,))
        response = self.app.get(url, user=self.user)
        form = response.forms['handing_over_protocol_form']
        self.assertEquals(form['serial_number'].value, 'SN1234567')

    def test_return_protocol_no_damages(self):
        self._fill_in_return_protocol()
        protocol = self.transaction.return_protocol
        self.assertTrue(protocol.undamaged)
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 2)
        self.assertEquals(mail.outbox[0].to, ['test.applicant@gearo.de'])
        self.assertEquals(
            mail.outbox[0].subject, 'Deine Kopie des Rückgabeprotokolls mit  für das Inserat Test Ad')
        self.assertEquals(mail.outbox[1].to, ['test.lessor@gearo.de'])
        self.assertEquals(
            mail.outbox[1].subject, 'Deine Kopie des Rückgabeprotokolls mit test_applicant für das Inserat Test Ad')

    def test_return_protocol_damages(self):
        self._fill_in_return_protocol(undamaged='False')
        protocol = self.transaction.return_protocol
        self.assertFalse(protocol.undamaged)
        call_command('send_queued_mail')
        self.assertEquals(len(mail.outbox), 3)
        self.assertEquals(mail.outbox[0].to, ['test.applicant@gearo.de'])
        self.assertEquals(
            mail.outbox[0].subject, 'Deine Kopie des Rückgabeprotokolls mit  für das Inserat Test Ad')
        self.assertEquals(mail.outbox[1].to, ['test.lessor@gearo.de'])
        self.assertEquals(
            mail.outbox[1].subject, 'Deine Kopie des Rückgabeprotokolls mit test_applicant für das Inserat Test Ad')
        self.assertEquals(mail.outbox[2].to, ['test.admin@gearo.de'])
        self.assertEquals(mail.outbox[2].subject, 'transactiondamage')

from django.conf import settings as django_settings
from django.utils.safestring import mark_safe

from gearo.models.notifications import Notifications


def settings(request):
    context = {
        'ATTACHMENT_PLACEHOLDER': django_settings.ATTACHMENT_PLACEHOLDER
    }
    return context


def notifications(request):
    if request.user.is_authenticated():
        notifications_count = Notifications.objects.filter(user=request.user).count()
    else:
        notifications_count = 0
    context = {
        'notifications_count': notifications_count
    }
    return context


def allowed_countries(request):
    return {'allowed_countries': mark_safe(django_settings.ALLOWED_COUNTRIES)}

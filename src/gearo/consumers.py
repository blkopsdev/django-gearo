import datetime
import hashlib
import hmac
import json
import logging

from django.conf import settings
from django.core.cache import cache

from channels import Group
from channels.generic.websockets import WebsocketConsumer

from gearo.helper.notifications import NOTIFICATIONS_PEPPER
from gearo.models.chatrooms import ChatroomMember, Chatrooms
from gearo.models.messages import Messages
from gearo.models.notifications import Notifications

logger = logging.getLogger('gearo.consumers')


class NotificationConsumer(WebsocketConsumer):
    channel_session_user = True

    def connect(self, message, **kwargs):
        notification_room = message['path'].strip('/').split('/')[2]
        Group('notification-' + notification_room, channel_layer=message.channel_layer).add(message.reply_channel)
        message.channel_session['room'] = notification_room
        super(NotificationConsumer, self).connect(message, **kwargs)

    def receive(self, text=None, bytes=None, **kwargs):
        data = json.loads(text)
        notification_room = data['room']
        pepper_index = datetime.datetime.today().day - 1
        key = settings.NOTIFICATIONS_SERVER["secret"] + NOTIFICATIONS_PEPPER[pepper_index] + data['time']
        key = key.encode('ascii')

        message = data['room'].encode('ascii')
        message_id = hmac.new(key, message, hashlib.sha256).hexdigest()

        if data.get("id", None) == message_id:
            Group('notification-' + notification_room, channel_layer=self.message.channel_layer).send({'text': text})

    def disconnect(self, message, **kwargs):
        try:
            notification_room = message.channel_session['room']
            Group(
                'notification-' + notification_room, channel_layer=message.channel_layer
            ).discard(message.reply_channel)
        except KeyError:
            pass


class ChatConsumer(WebsocketConsumer):
    channel_session_user = True
    http_user = True

    def get_chat_tokens(self, chatroom_slug):
        chat_status_key = 'chat-%s' % chatroom_slug
        tokens = cache.get(chat_status_key, [])
        return chat_status_key, set(tokens)

    def add_chat_token(self, chatroom_slug, token):
        chat_status_key, current_tokens = self.get_chat_tokens(chatroom_slug)
        current_tokens.add(token)
        cache.set(chat_status_key, current_tokens)
        return current_tokens

    def remove_chat_token(self, chatroom_slug, token):
        chat_status_key, current_tokens = self.get_chat_tokens(chatroom_slug)
        current_tokens.discard(token)
        cache.set(chat_status_key, current_tokens)
        return current_tokens

    def send_online_status(self, chatroom_slug, tokens):
        message = {
            'type': 'online_status',
            'tokens': list(tokens)
        }
        Group(
            'chat-' + chatroom_slug, channel_layer=self.message.channel_layer
        ).send({'text': json.dumps(message)})

    def connect(self, message, **kwargs):
        chatroom_slug = message['path'].strip('/').split('/')[2]
        user_token = message['path'].strip('/').split('/')[3]
        logger.debug('User with token "%s" connected to the chat "%s".', user_token, chatroom_slug)
        Group('chat-' + chatroom_slug, channel_layer=message.channel_layer).add(message.reply_channel)
        message.channel_session['room'] = chatroom_slug
        message.channel_session['token'] = user_token
        tokens = self.add_chat_token(chatroom_slug, user_token)
        self.send_online_status(chatroom_slug, tokens)
        super(ChatConsumer, self).connect(message, **kwargs)

    def receive(self, text=None, bytes=None, **kwargs):
        chatroom_slug = self.message.channel_session['room']
        room = Chatrooms.objects.get(slug=chatroom_slug)
        token = self.message.channel_session['token']
        logger.debug('Received message from user "%s" in the chat "%s".', token, chatroom_slug)

        if ChatroomMember.objects.filter(chatroom=room, token=token).exists():
            tokens = self.get_chat_tokens(chatroom_slug)[1]
            chat_tokens = tokens.copy()
            chat_tokens.remove(token)
            is_read = len(chat_tokens) > 0
            recipient_member = ChatroomMember.objects.filter(chatroom=room).exclude(user=self.message.user).first()
            recipient = recipient_member.user
            transaction = room.transaction
            message_params = {
                'sender': self.message.user, 'message': text, 'room': room,
                'is_read': is_read, 'recipient': recipient
            }

            new_message = Messages(**message_params)
            new_message.save()
            chat_message = {
                'type': 'message',
                'user_id': self.message.user.id,
                'message': text,
                'avatar_thumbnail_url': new_message.sender.profile.avatar_thumbnail_url,
                'sended_date': new_message.sended_at.strftime('%d.%m.%Y'),
                'sended_time': new_message.sended_at.strftime('%H:%M'),
                'token': token,
                'tokens': list(tokens)
            }

            if not Notifications.objects.filter(user=recipient, transaction=transaction, type="message").exists():
                Notifications.objects.create(user=recipient, transaction=transaction, type="message")
                if transaction.applicant == recipient:
                    transaction.viewed_by_applicant = False
                else:
                    transaction.viewed_by_lessor = False
                transaction.save()
            Group(
                'chat-' + chatroom_slug, channel_layer=self.message.channel_layer
            ).send({'text': json.dumps(chat_message)})

    def disconnect(self, message, **kwargs):
        try:
            chatroom_slug = self.message.channel_session['room']
            room = Chatrooms.objects.get(slug=chatroom_slug)
            user_token = message.channel_session['token']
            logger.debug('User "%s" disconnected from the chat "%s".', user_token, chatroom_slug)
            tokens = self.remove_chat_token(chatroom_slug, user_token)
            self.send_online_status(chatroom_slug, tokens)
            Group('chat-' + chatroom_slug, channel_layer=message.channel_layer).discard(message.reply_channel)
        except (KeyError, Chatrooms.DoesNotExist):
            pass

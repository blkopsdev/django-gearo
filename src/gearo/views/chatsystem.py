import copy
import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.utils import timezone
from django.views.generic.base import TemplateView, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView

from payments import RedirectNeeded, get_payment_model
from post_office import mail

from gearo.forms.protocols import HandingOverProtocolForm, ReturnProtocolForm
from gearo.helper.absolute_url import absolute_url
from gearo.helper.gentoken import gentoken
from gearo.models.chatrooms import ChatroomMember, Chatrooms
from gearo.models.messages import Messages
from gearo.models.notifications import Notifications
from gearo.models.rohprotocol import ProtocolAttachment, Rohprotocol
from gearo.models.transactions import Transactions
from gearo.views.mixins import AttachmentMixin, MessageErrorMixin
from gearo_tasks.tasks.sms import send_sms

logger = logging.getLogger(__name__)


class NotificationDeleteMixin(object):
    def delete_notifications(self, trans):
        if not all([trans.viewed_by_applicant, trans.viewed_by_lessor]):
            trans.notifications.filter(user=self.request.user).delete()
            if trans.applicant == self.request.user:
                trans.viewed_by_applicant = True
            if trans.lessor == self.request.user:
                trans.viewed_by_lessor = True
            trans.save()


class PostboxView(LoginRequiredMixin, TemplateView):
    template_name = "gearo/user/postbox.html"

    def get_context_data(self, **kwargs):
        ctx = super(PostboxView, self).get_context_data(**kwargs)
        incoming_inquiries = Transactions.objects.filter(ad__owner=self.request.user)\
            .exclude(status__in=[Transactions.STATUS_INCOMPLETE, Transactions.STATUS_REJECTED])\
            .order_by("-created_at")
        outgoing_inquiries = Transactions.objects.filter(applicant=self.request.user)\
            .exclude(status__in=[Transactions.STATUS_INCOMPLETE, Transactions.STATUS_REJECTED])\
            .order_by("-created_at")
        copy_incoming_inquiries = copy.deepcopy(incoming_inquiries)
        copy_outgoing_inquiries = copy.deepcopy(outgoing_inquiries)
        new_incoming_inquiries = copy_incoming_inquiries.filter(viewed_by_lessor=False).count()
        new_outgoing_inquiries = copy_outgoing_inquiries.filter(viewed_by_applicant=False).count()
        ctx.update(
            incoming_inquiries=incoming_inquiries, outgoing_inquiries=outgoing_inquiries,
            new_incoming_inquiries=new_incoming_inquiries, new_outgoing_inquiries=new_outgoing_inquiries
        )
        return ctx


class CompleteInquiryView(LoginRequiredMixin, NotificationDeleteMixin, TemplateView):
    template_name = "gearo/advertisements/booking.html"

    def get_context_data(self, **kwargs):
        ctx = super(CompleteInquiryView, self).get(**kwargs)
        trans = Transactions.objects.get(id=kwargs['inqid'])
        self.delete_notifications(trans)
        submit = None
        Payment = get_payment_model()
        payment = Payment.objects.get(gearo_transaction=kwargs['inqid'], status='preauth')
        try:
            form = payment.get_form()
        except RedirectNeeded as redirect_to:
            submit = str(redirect_to)

        ctx.update(ad=trans.ad, trans=trans, submit=submit, publicid=payment.publicid)
        return ctx


class ManageInquiryView(LoginRequiredMixin, DetailView):
    model = Transactions
    pk_url_kwarg = 'inqid'
    context_object_name = 'inquire'
    template_name = 'gearo/user/manage_inquiries.html'

    def get(self, request, *args, **kwargs):
        object = self.get_object()
        if request.user != object.lessor:
            messages.warning(request, "Du hast leider keinen Zugriff auf diese Anfrage.")
            return redirect("postbox")

        if object.is_response_limit_exceeded or object.status != Transactions.STATUS_WAITING:
            return redirect('showinq', object.id)
        return super(ManageInquiryView, self).get(request, *args, **kwargs)


class InquiryDetailView(LoginRequiredMixin, NotificationDeleteMixin, DetailView):
    model = Transactions
    pk_url_kwarg = 'inqid'
    context_object_name = 'trans'
    template_name = 'gearo/advertisements/booking.html'

    def get_context_data(self, **kwargs):
        ctx = super(InquiryDetailView, self).get_context_data(**kwargs)
        self.delete_notifications(self.object)
        ctx.update(
            bookingsuccess=True,
            ad=self.object.ad
        )
        return ctx


class CancelInquiryView(LoginRequiredMixin, MessageErrorMixin, View):
    url = reverse_lazy("postbox")

    def get(self, request, *args, **kwargs):
        inqid = kwargs['inqid']
        trans = Transactions.objects.get(id=inqid)
        if request.user == trans.lessor or request.user == trans.applicant or request.user.is_superuser:
            if not trans.is_cancellable:
                messages.error(request, 'Du kannst die Buchung jetzt nicht stornieren!')
                return HttpResponseRedirect(self.url)

            try:
                trans.cancel()
            except Exception as e:
                logger.error('Error on inquiry cancellation - %s', e)
                return self.message_and_redirect_to_referrer(
                    message='Error on payment processing. Please try again.',
                    default_path=self.url
                )

            if request.user == trans.lessor:
                trans.viewed_by_applicant = False
                trans.save()

                Notifications.objects.create(user=trans.applicant, transaction=trans, type="message")

                logger.debug('Transaction #%s cancelled by lessor "%s"', trans.id, request.user.email)
                canceledby = "Vermieter"
                context = {
                    'firstname': trans.applicant.first_name, 'firstname_lessor': trans.lessor.first_name}
                mail.send(trans.applicant.email, 'no-reply@gearo.de', template='cancledbyad', context=context)

                gearo_user = trans.applicant.gearouser
                if gearo_user.phone_mobile and gearo_user.phone_verified:
                    logger.debug(
                        'Sent SMS confirmation for transaction #%s cancellation to lessor "%s',
                        trans.id, request.user.email,
                    )
                    sms_text = '%s hat deine Buchung leider storniert. Deine Zahlung wird dir komplett erstattet. ' \
                               'Finde Ersatz auf gearo.de' % trans.lessor.first_name
                    send_sms.delay(gearo_user.phone_mobile.raw_input, sms_text)

            elif request.user == trans.applicant:
                trans.viewed_by_lessor = False
                trans.save()

                Notifications.objects.create(user=trans.lessor, transaction=trans, type="message")

                logger.debug('Transaction #%s cancelled by tenant "%s"', trans.id, request.user.email)
                canceledby = "Mieter"
                mail.send(trans.lessor.email, 'no-reply@gearo.de', template='cancledbyapp',
                          context={'firstname': trans.lessor.first_name,
                                   'firstname_applicant': trans.applicant.first_name})

                gearo_user = trans.lessor.gearouser
                if gearo_user.phone_mobile and gearo_user.phone_verified:
                    logger.debug(
                        'Sent SMS confirmation for transaction #%s cancellation to tenant "%s',
                        trans.id, request.user.email,
                    )
                    sms_text = '%s hat die Buchung leider storniert.' % trans.applicant.first_name
                    send_sms.delay(gearo_user.phone_mobile.raw_input, sms_text)

            trans.chat.active = False
            trans.chat.save()

            trans.status = Transactions.STATUS_CANCELLED
            trans.save()

            mail.send("hi@gearo.de", 'no-reply@gearo.de', template='gearostornomail',
                      context={'trans': trans,
                               'canceledby':  canceledby})

        return HttpResponseRedirect(self.url)


class AcceptLessorView(LoginRequiredMixin, MessageErrorMixin, View):
    url = reverse_lazy("postbox")

    def get(self, request, *args, **kwargs):
        inqid = kwargs['inqid']
        trans = Transactions.objects.get(id=inqid)
        if request.user == trans.lessor and trans.status == Transactions.STATUS_WAITING:
            try:
                trans.accept()
            except Exception as e:
                logger.error('Error on lessor accept - %s', e)
                return self.message_and_redirect_to_referrer(
                    message='Fehler beim Zahlungsprozess. Bitte versuche es erneut.', default_path=self.url,
                )

            trans.status = Transactions.STATUS_ACCEPTED
            trans.viewed_by_applicant = False
            trans.processed_at = timezone.now()
            trans.save()

            logger.debug('Transaction #%s accepted by lessor "%s"', trans.id, request.user.email)

            if not trans.chat:
                new_room = Chatrooms(slug=gentoken(12, True, "Chatrooms", "slug"))
                new_room.save()

                lessor_token = gentoken(12, True, "ChatroomMember", "token")
                ChatroomMember.objects.create(user=trans.lessor, chatroom=new_room, token=lessor_token)
                tenant_token = gentoken(12, True, "ChatroomMember", "token")
                ChatroomMember.objects.create(user=trans.applicant, chatroom=new_room, token=tenant_token)

                trans.chat = new_room
                trans.save()

                Notifications.objects.create(user=trans.applicant, transaction=trans, type="message")

                # send mail to applicant
                lessor_phone_number = getattr(trans.lessor.gearouser.phone_mobile, 'raw_input', '')
                mail.send(trans.applicant.email, 'no-reply@gearo.de', template='transactionaccept',
                          context={'firstname': trans.applicant.first_name,
                                   'firstname_lessor': trans.lessor.first_name,
                                   'phone_lessor': lessor_phone_number,
                                   "chatroom": trans.get_chat_url()})
                # send mail to lessor
                applicant_phone_number = getattr(trans.applicant.gearouser.phone_mobile, 'raw_input', '')
                mail.send(trans.lessor.email, 'no-reply@gearo.de', template='transactioninfo',
                          context={'firstname': trans.lessor.first_name,
                                   'firstname_applicant': trans.applicant.first_name,
                                   'phone_applicant': applicant_phone_number,
                                   "chatroom": trans.get_chat_url()})

                gearo_user = trans.applicant.gearouser
                if gearo_user.phone_mobile and gearo_user.phone_verified:
                    logger.debug(
                        'Sent SMS confirmation for transaction #%s acceptance to lessor "%s',
                        trans.id, request.user.email
                    )
                    sms_text = '%s hat deine Anfrage angenommen. ' \
                               'Sieh in deinen Posteingang und klaere die Details.' % trans.lessor.first_name
                    send_sms.delay(gearo_user.phone_mobile.raw_input, sms_text)

            return redirect("chat", slug=trans.chat.slug, trans_id=trans.id)
        else:
            return HttpResponseRedirect(self.url)


class RejectLessorView(LoginRequiredMixin, MessageErrorMixin, View):
    url = reverse_lazy("postbox")

    def get(self, request, *args, **kwargs):
        inqid = kwargs['inqid']
        trans = Transactions.objects.get(id=inqid)
        if (request.user == trans.lessor or request.user.is_superuser) and trans.status == Transactions.STATUS_WAITING:
            trans.status = Transactions.STATUS_REJECTED
            trans.processed_at = timezone.now()
            trans.save()

            try:
                trans.reject()
            except Exception as e:
                logger.error('Error on lessor reject - %s', e)
                messages.error(self.request, 'Fehler beim Zahlungsprozess.')

            logger.debug('Transaction #%s rejected by lessor "%s"', trans.id, request.user.email)

            mail.send(trans.applicant.email, 'no-reply@gearo.de', template='transactionreject',
                      context={'firstname': trans.applicant.first_name,
                               'firstname_lessor': trans.lessor.first_name})

            gearo_user = trans.applicant.gearouser
            if gearo_user.phone_mobile and gearo_user.phone_verified:
                logger.debug(
                    'Sent SMS confirmation for transaction #%s rejection to lessor "%s',
                    trans.id, request.user.email,
                )
                sms_text = '%s hat deine Anfrage leider abgelehnt. Deine Zahlung wird nicht eingezogen. ' \
                           'Buche jetzt neues Equipment auf gearo.de' % trans.lessor.first_name
                send_sms.delay(gearo_user.phone_mobile.raw_input, sms_text)

            trans.notifications.all().delete()
        return HttpResponseRedirect(self.url)


class ChatroomView(LoginRequiredMixin, NotificationDeleteMixin, TemplateView):
    template_name = "gearo/user/chat.html"

    def get(self, request, *args, **kwargs):
        self.room = Chatrooms.objects.get(slug=kwargs['slug'])

        # check if user is a member
        if not self.room.members.filter(id=request.user.id).exists():
            messages.warning(request, "You don't have access to this chat.")
            return redirect("postbox")
        return super(ChatroomView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(ChatroomView, self).get_context_data(**kwargs)
        trans = Transactions.objects.get(id=kwargs['trans_id'])
        self.delete_notifications(trans)
        chat_messages = Messages.objects.filter(room=self.room)
        chat_messages.filter(recipient=self.request.user).update(is_read=True)
        chatroom_member = self.request.user.chatroom_members.get(chatroom=self.room)
        ctx.update(trans=trans, chat_messages=chat_messages, chatroom_member=chatroom_member)
        return ctx


class BaseProtocolView(AttachmentMixin):
    model = Rohprotocol
    transaction = protocol = None
    is_created = False
    related_field_name = None
    related_attachment_field = 'protocol'
    attachment_model = ProtocolAttachment

    def dispatch(self, request, *args, **kwargs):
        self.transaction = get_object_or_404(Transactions, id=kwargs['inqid'])
        if not any([self.transaction.lessor == request.user, request.user == self.transaction.applicant,
                    request.user.is_superuser]):
            return redirect('postbox')
        if self.transaction and not getattr(self.transaction, self.related_field_name, None):
            protocol_params = {'protocol_type': self.protocol_type}
            if self.protocol_type == "overhanding":
                protocol_params['pickup_time'] = timezone.now()
            elif self.protocol_type == "return":
                protocol_params['return_time'] = timezone.now()
            self.protocol = Rohprotocol(**protocol_params)
        else:
            self.protocol = getattr(self.transaction, self.related_field_name, None)
            self.is_created = True
        return super(BaseProtocolView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(BaseProtocolView, self).get_form_kwargs()
        kwargs['is_created'] = self.is_created
        kwargs['instance'] = self.protocol
        return kwargs

    def get_success_url(self):
        return self.transaction.get_chat_url()

    def get_context_data(self, **kwargs):
        ctx = super(BaseProtocolView, self).get_context_data(**kwargs)
        ctx['trans'] = self.transaction
        ctx['protocol'] = self.protocol
        ctx['is_created'] = self.is_created
        return ctx

    def form_valid(self, form):
        result = super(BaseProtocolView, self).form_valid(form)
        self.set_attachments()
        self.object.save()
        return result


class HandingOverProtocolView(BaseProtocolView, LoginRequiredMixin, CreateView):
    template_name = "gearo/protocol/handing_over_protocol.html"
    form_class = HandingOverProtocolForm
    protocol_type = "overhanding"
    related_field_name = "handing_over_protocol"

    def get_initial(self):
        initial = super(HandingOverProtocolView, self).get_initial()
        initial['serial_number'] = self.transaction.ad.serial_number
        return initial

    def form_valid(self, form):
        super(HandingOverProtocolView, self).form_valid(form)

        self.transaction.handing_over_protocol = self.object
        if self.transaction.status == Transactions.STATUS_ACCEPTED:
            self.transaction.status = Transactions.STATUS_ACTIVE
        self.transaction.save()

        self.transaction.ad.serial_number = form.cleaned_data['serial_number']
        self.transaction.ad.save()

        # send mail to applicant
        mail.send(self.transaction.applicant.email, 'no-reply@gearo.de', template='protocolapplicant',
                  context={'firstname': self.transaction.applicant.first_name,
                           'firstname_lessor': self.transaction.lessor.first_name,
                           'adname': self.transaction.ad.title,
                           "protolink": absolute_url("handingover", self.transaction.id)})
        # send mail to lessor
        mail.send(self.transaction.lessor.email, 'no-reply@gearo.de', template='protocollessor',
                  context={'firstname': self.transaction.lessor.first_name,
                           'firstname_applicant': self.transaction.applicant,
                           'adname': self.transaction.ad.title,
                           "protolink": absolute_url("handingover", self.transaction.id)})

        messages.success(self.request, 'Protokoll erfolgreich übertragen')
        return super(HandingOverProtocolView, self).form_valid(form)


class ReturnProtocolView(BaseProtocolView, LoginRequiredMixin, CreateView):
    template_name = "gearo/protocol/return_protocol.html"
    form_class = ReturnProtocolForm
    protocol_type = "return"
    related_field_name = "return_protocol"

    def form_valid(self, form):
        super(ReturnProtocolView, self).form_valid(form)

        self.transaction.return_protocol = self.object
        self.transaction.save()

        # send mail to applicant
        mail.send(self.transaction.applicant.email, 'no-reply@gearo.de', template='rprotocolapplicant',
                  context={'firstname': self.transaction.applicant.first_name,
                           'firstname_lessor': self.transaction.lessor.first_name,
                           'adname': self.transaction.ad.title,
                           "protolink": absolute_url("returnproto", self.transaction.id)})
        # send mail to lessor
        mail.send(self.transaction.lessor.email, 'no-reply@gearo.de', template='rprotocollessor',
                  context={'firstname': self.transaction.lessor.first_name,
                           'firstname_applicant': self.transaction.applicant, 'adname': self.transaction.ad.title,
                           "protolink": absolute_url("returnproto", self.transaction.id)})

        if not self.object.undamaged:
            admin_emails = list(User.objects.filter(is_staff=True).values_list('email', flat=True))
            mail.send(admin_emails, 'no-reply@gearo.de', template='transactiondamage',
                      context={'trans': self.transaction,
                               'handing_over_protocol_link':  absolute_url("handingover", self.transaction.id),
                               'return_protocol_link': absolute_url("returnproto", self.transaction.id)})
        return super(ReturnProtocolView, self).form_valid(form)

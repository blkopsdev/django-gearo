import logging

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.views.generic.edit import UpdateView

from mangopay.constants import DOCUMENTS_STATUS_CHOICES

from gearo.forms.account import (
    AddressInfoForm, BankInfoForm, ContactInfoForm, EmailChangeForm, IdentificationInfoForm, PasswordChangeForm,
    PersonalInfoForm, ProfileForm,
)
from gearo.helper.mangopay import BUSINESS_DOCUMENTS_FIELDS_NAMES, KYC_DOCUMENTS, create_temp_file, get_mp_document
from gearo.models.gearo_profile import GearoProfile
from gearo.models.gearo_user import GearoUser
from gearo.models.user_payment import UserPayment
from gearo_tasks.tasks.advertisement import update_advertisements
from gearo_tasks.tasks.mangopay import create_mangopay_user_and_wallet, send_mp_document_for_verification, update_mp_user_info

logger = logging.getLogger(__name__)

VALIDATION_ASKED = DOCUMENTS_STATUS_CHOICES.validation_asked


class AccountPageMixin(object):

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        try:
            billing_info = request.user.billing_info
            if not billing_info.is_mangopay_user_id_existing:
                create_mangopay_user_and_wallet.delay(user.id)
        except User.billing_info.RelatedObjectDoesNotExist:
            pass
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['profile_site'] = self.page_name
        return ctx

    def form_valid(self, form):
        form.save()
        ctx = self.get_context_data(form=form)
        messages.success(self.request, 'Einstellungen erfolgreich &uuml;bernommen')
        return self.render_to_response(ctx)


class EmailRequiredMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.email:
            messages.warning(
                self.request, 'Gib deine E-Mail ein, und vervollständige danach deine Profilinformationen.')
            return redirect('change_email')
        return super().dispatch(request, *args, **kwargs)


class ProfileUpdateView(LoginRequiredMixin, EmailRequiredMixin, AccountPageMixin, UpdateView):
    form_class = ProfileForm
    model = GearoProfile
    template_name = 'gearo/user/profile_edit.html'
    success_url = reverse_lazy('profile_edit')
    page_name = 'profile_edit'

    def get_object(self, queryset=None):
        return GearoProfile.objects.get_or_create(user=self.request.user)[0]


class PersonalInfoUpdateView(LoginRequiredMixin, EmailRequiredMixin, AccountPageMixin, UpdateView):
    form_class = PersonalInfoForm
    model = User
    template_name = 'gearo/user/personal_info_edit.html'
    success_url = reverse_lazy('personal_info_edit')
    page_name = 'personal_info_edit'

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        form.save()

        if form.has_changed():
            params_for_update = {}
            if 'birthday' in form.changed_data:
                params_for_update['birthday'] = form.cleaned_data['birthday']

            if params_for_update:
                update_mp_user_info.delay(self.request.user.id, params_for_update)

        ctx = self.get_context_data(form=form)
        billing_info = self.request.user.billing_info
        if billing_info.mangopay_bank_account_error is None:
            messages.success(self.request, 'Einstellungen erfolgreich &uuml;bernommen')
        return self.render_to_response(ctx)


class ContactInfoView(LoginRequiredMixin, EmailRequiredMixin, AccountPageMixin, UpdateView):
    form_class = ContactInfoForm
    model = GearoUser
    template_name = 'gearo/user/contact_info_edit.html'
    success_url = reverse_lazy('contact_info_edit')
    page_name = 'contact_info_edit'

    def get_object(self, queryset=None):
        return GearoUser.objects.get_or_create(user=self.request.user)[0]


class IdentificationInfoUpdateView(LoginRequiredMixin, EmailRequiredMixin, AccountPageMixin, UpdateView):
    form_class = IdentificationInfoForm
    model = UserPayment
    template_name = 'gearo/user/identification_info_edit.html'
    success_url = reverse_lazy('identification_info_edit')
    page_name = 'identification_info_edit'

    def get_object(self, queryset=None):
        return UserPayment.objects.get_or_create(user=self.request.user)[0]

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        billing_info = self.get_object()
        gearo_user = billing_info.user.gearouser
        ctx['business_type'] = gearo_user.get_business_type_name()
        ctx['identity_proof_document'] = get_mp_document(billing_info.user, KYC_DOCUMENTS['identity_proof'])
        ctx['is_id_document_validation_asked'] = billing_info.identity_proof_status == VALIDATION_ASKED
        ctx['registration_proof_document'] = get_mp_document(billing_info.user, KYC_DOCUMENTS['registration_proof'])
        ctx['is_registration_proof_validation_asked'] = billing_info.registration_proof_status == VALIDATION_ASKED
        ctx['articles_of_association_document'] = (
            get_mp_document(billing_info.user, KYC_DOCUMENTS['articles_of_association']))
        ctx['is_articles_of_association_validation_asked'] = (
            billing_info.articles_of_association_status == VALIDATION_ASKED)
        ctx['shareholder_declaration_document'] = (
            get_mp_document(billing_info.user, KYC_DOCUMENTS['shareholder_declaration']))
        ctx['is_shareholder_declaration_validation_asked'] = (
            billing_info.shareholder_declaration_status == VALIDATION_ASKED)
        ctx['is_payment_limit_reached'] = billing_info.is_payment_limit_reached
        return ctx

    def form_valid(self, form):
        self.object = form.save()

        if form.has_changed():
            params_for_update = {}
            if 'nationality' in form.changed_data:
                params_for_update['nationality'] = form.cleaned_data['nationality']
            if 'country_of_residence' in form.changed_data:
                params_for_update['country_of_residence'] = form.cleaned_data['country_of_residence']

            if params_for_update:
                update_mp_user_info.delay(self.request.user.id, params_for_update)

            if 'document_upside' in form.changed_data and 'document_downside' in form.changed_data:
                if self.object.identity_proof_status != VALIDATION_ASKED:
                    document_upside_name = create_temp_file(form.cleaned_data['document_upside'])
                    document_downside_name = create_temp_file(form.cleaned_data['document_downside'])
                    pages = [document_upside_name, document_downside_name]
                    send_mp_document_for_verification.delay(
                        self.request.user.id, KYC_DOCUMENTS['identity_proof'], *pages)
                    # After next set user will not be able to upload ID documents again.
                    # Buttons for document uploading will become active again just
                    # if previously uploaded document's status will be set to "REFUSED" by Mangopay
                    self.object.identity_proof_status = VALIDATION_ASKED
                    self.object.save()

        for document in BUSINESS_DOCUMENTS_FIELDS_NAMES:
            model_field = '{}_status'.format(document)
            document_status = getattr(self.object, model_field)
            if document in form.changed_data and document_status != VALIDATION_ASKED:
                pages = []
                for document_page in self.request.FILES.getlist(document):
                    page = create_temp_file(document_page)
                    pages.append(page)
                send_mp_document_for_verification.delay(self.request.user.id, KYC_DOCUMENTS[document], *pages)

                setattr(self.object, model_field, VALIDATION_ASKED)
                self.object.save()

        ctx = self.get_context_data(form=form)
        billing_info = self.request.user.billing_info
        if billing_info.mangopay_bank_account_error is None:
            messages.success(self.request, 'Einstellungen erfolgreich &uuml;bernommen')
        return self.render_to_response(ctx)


class AddressInfoUpdateView(LoginRequiredMixin, EmailRequiredMixin, AccountPageMixin, UpdateView):
    form_class = AddressInfoForm
    model = GearoUser
    template_name = 'gearo/user/address_info_edit.html'
    success_url = reverse_lazy('address_info_edit')
    page_name = 'address_info_edit'

    def get_object(self, queryset=None):
        return GearoUser.objects.get_or_create(user=self.request.user)[0]

    def form_valid(self, form):
        self.object = form.save()
        update_advertisements.delay(self.object.user.id)
        ctx = self.get_context_data(form=form)
        messages.success(self.request, 'Einstellungen erfolgreich &uuml;bernommen')
        return self.render_to_response(ctx)


class BankInfoUpdateView(LoginRequiredMixin, EmailRequiredMixin, AccountPageMixin, UpdateView):
    form_class = BankInfoForm
    model = UserPayment
    template_name = 'gearo/user/bank_info_edit.html'
    success_url = reverse_lazy('bank_info_edit')
    page_name = 'bank_info_edit'

    def get_object(self, queryset=None):
        return UserPayment.objects.get_or_create(user=self.request.user)[0]


class EmailChangeView(LoginRequiredMixin, AccountPageMixin, UpdateView):
    form_class = EmailChangeForm
    model = User
    template_name = 'gearo/user/email_change.html'
    success_url = reverse_lazy('change_email')
    page_name = 'change_email'

    def get_object(self, queryset=None):
        return self.request.user


class PasswordChangeView(LoginRequiredMixin, EmailRequiredMixin, AccountPageMixin, UpdateView):
    form_class = PasswordChangeForm
    model = User
    template_name = 'gearo/user/password_change.html'
    success_url = reverse_lazy('change_password')
    page_name = 'change_password'

    def get_object(self, queryset=None):
        return self.request.user

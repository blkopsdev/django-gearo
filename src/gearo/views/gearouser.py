import copy
import logging
import re
from datetime import timedelta
from urllib.parse import urlparse

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.files import File
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count
from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from django.utils.timezone import now
from django.views.generic import FormView, RedirectView, TemplateView, View
from django.views.generic.detail import DetailView

from allauth.socialaccount.adapter import get_adapter
from allauth.socialaccount.helpers import complete_social_signup
from allauth.socialaccount.models import SocialLogin
from post_office import mail

from gearo.forms.login_form import LoginForm
from gearo.forms.register_form import RegisterForm
from gearo.forms.resetpassword import ResetPasswordForm
from gearo.helper.absolute_url import absolute_url
from gearo.helper.adrandom import get_random_ads
from gearo.helper.generateusername import generateusername
from gearo.helper.gentoken import gentoken
from gearo.helper.images import crop_image, generate_avatar_thumbnail
from gearo.models.advertisement import Advertisement
from gearo.models.discount_voucher import Offer
from gearo.models.gearo_profile import GearoProfile
from gearo.models.gearo_user import GearoUser
from gearo.models.tokens import Tokens
from gearo.models.userreviews import UserReviews
from gearo_tasks.tasks.sms import send_sms
from gearo_tasks.tasks.usertasks import send_welcome_email

logger = logging.getLogger(__name__)


class BaseFacebookRedirectionMixin(RedirectView):
    url = reverse_lazy('search_results')


class RegisterView(FormView):
    form_class = RegisterForm
    template_name = 'gearo/user/registration.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect("index")
        return super(RegisterView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        referer_page_url = self.request.META['HTTP_REFERER']
        username = generateusername(form.cleaned_data['firstname'], form.cleaned_data['lastname'])
        user = User.objects.create_user(
            username=username,
            email=form.cleaned_data['email'],
            password=form.cleaned_data['password'],
            first_name=form.cleaned_data['firstname'],
            last_name=form.cleaned_data['lastname'],
            is_active=False,
        )

        user.gearouser.is_terms_accepted = True
        user.gearouser.save()

        send_welcome_email.delay(user.id)

        referer_page_path = urlparse(referer_page_url).path
        referer_path_slug = re.sub('[^\w\s-]', '', referer_page_path, flags=re.U).strip().lower()
        offer_lookup_kwargs = {
            'source_page__slug': referer_path_slug,
            'start_date__lt': now(),
            'expired_date__gte': now()
        }
        if Offer.objects.filter(**offer_lookup_kwargs).count() > 0:
            offer = Offer.objects.filter(**offer_lookup_kwargs).last()
            if offer.is_active:
                voucher = offer.generate_voucher(owner=user)
                mail.send(
                    form.cleaned_data["email"],
                    'no-reply@gearo.de',
                    template='vouchergifted',
                    context={
                        'firstname': form.cleaned_data['firstname'],
                        'voucher_code': voucher.code,
                        'search_url': absolute_url("search_results"),
                    })
        return JsonResponse({'success': True})

    def form_invalid(self, form):
        return JsonResponse({'success': False})


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'gearo/user/login.html'

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['white'] = True
        context['ads'] = get_random_ads(4)
        return context

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            return redirect("index")
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        success = is_terms_accepted = False
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        user = authenticate(email=email, password=password)
        if user is not None:
            login(self.request, user)
            success = True
            is_terms_accepted = user.gearouser.is_terms_accepted
        return JsonResponse({'success': success, 'is_terms_accepted': is_terms_accepted})

    def form_invalid(self, form):
        return JsonResponse({'success': False})


class ResetPasswordView(TemplateView):
    template_name = 'gearo/user/reset_password.html'

    def post(self, request, *args, **kwargs):
        success = False
        email = request.POST.get('email', None)
        token = None
        if email:
            try:
                user = User.objects.get(email__iexact=email)

                token = Tokens(user=user, token=gentoken(16), kind=2, expire_date=(now() + timedelta(days=1)))
                token.save()

                mail.send(
                    user.email,
                    'no-reply@gearo.de',
                    template='resetpwmail',
                    context={
                        'firstname': user.first_name,
                        "resetmail": absolute_url("resetpasswordconfirm", token.token),
                    })
                success = True
            except User.DoesNotExist:
                pass
        return JsonResponse({'success': success})


class ResetPasswordConfirmView(FormView):
    template_name = 'gearo/user/reset_password_confirmation.html'
    form_class = ResetPasswordForm
    success_url = reverse_lazy('search_results')

    def get_context_data(self, **kwargs):
        ctx = super(ResetPasswordConfirmView, self).get_context_data(**kwargs)
        ctx['token'] = self.kwargs['token']
        return ctx

    def dispatch(self, request, *args, **kwargs):
        try:
            self.token_db = Tokens.objects.get(token=kwargs['token'], kind=2)
        except Tokens.DoesNotExist:
            self.token_db = None
        if self.token_db and self.token_db.expire_date >= now():
            return super(ResetPasswordConfirmView, self).dispatch(request, *args, **kwargs)
        return render(request, 'gearo/error/expiredlink.html')

    def form_valid(self, form):
        self.token_db.user.set_password(form.cleaned_data['password'])
        self.token_db.user.save()
        self.token_db.delete()
        messages.success(
            self.request, 'Passwort erfolgreich geändert. Du kannst dich jetzt mit deinem neuen Passwort anmelden.'
        )
        return super(ResetPasswordConfirmView, self).form_valid(form)


class LogoutView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        logout(self.request)
        return reverse_lazy('index')


class ConfirmMailView(TemplateView):
    template_name = 'gearo/home/confirmsuccess.html'

    def get(self, request, *args, **kwargs):
        try:
            token = Tokens.objects.get(token=kwargs['token'], kind=1)
        except Tokens.DoesNotExist:
            return redirect('index')

        token.user.is_active = True
        token.user.save()
        notification_token = gentoken(12, True, "GearoUser", "notification_token")
        gearo_user = token.user.gearouser
        gearo_user.email_verified = True
        gearo_user.notification_token = notification_token
        gearo_user.save()
        token.delete()
        return super(ConfirmMailView, self).get(request, *args, **kwargs)


class MailCheckView(View):
    def post(self, request, *args, **kwargs):
        email = request.POST.get("data", None)
        is_email_in_use = User.objects.filter(email=email).exists()
        return JsonResponse({'is_email_in_use': is_email_in_use})


class FacebookLoginCancelledView(BaseFacebookRedirectionMixin):

    def get_redirect_url(self, *args, **kwargs):
        messages.warning(self.request, 'Du hast den Facebook-Login abgebrochen.')
        return super(FacebookLoginCancelledView, self).get_redirect_url(*args, **kwargs)


class FacebookLoginErrorView(BaseFacebookRedirectionMixin):

    def get_redirect_url(self, *args, **kwargs):
        messages.error(self.request, 'Huch, es gab ein Fehler beim Facebook-login. Bitte versuche es erneut!')
        return super(FacebookLoginErrorView, self).get_redirect_url(*args, **kwargs)


class FacebookConfirmAuthentication(BaseFacebookRedirectionMixin):

    def get_redirect_url(self, *args, **kwargs):
        token_hash = kwargs.get('token', None)
        token = is_token_valid = None
        try:
            token = Tokens.objects.get(token=token_hash, kind=5)
            is_token_valid = token.checktoken(self.request.user, False)
        except Tokens.DoesNotExist:
            messages.error(self.request, 'Huch, es gab ein Fehler beim Facebook-login. Bitte versuche es erneut!')
        if is_token_valid:
            data = self.request.session.get('socialaccount_sociallogin')
            if data:
                sociallogin = SocialLogin.deserialize(data)
                sociallogin.save(self.request)
                adapter = get_adapter(self.request)
                messages.success(self.request, 'Du hast dich erfolgreich mit facebook eingelogged!')
                ret = complete_social_signup(self.request, sociallogin)
        return super(FacebookConfirmAuthentication, self).get_redirect_url(*args, **kwargs)


class UploadAvatarView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        upfile = request.FILES['avatar'] if request.FILES else None
        if upfile is None:
            return JsonResponse({'success': False})

        try:
            user = GearoProfile.objects.get(user=request.user)
            user.avatar = upfile
        except GearoProfile.DoesNotExist:
            user = GearoProfile(user=request.user, avatar=upfile)

        user.save()
        file_payload = {
            'name': str(user.avatar),
            'size': str(user.avatar.size),
            'url': user.avatar.url,
            'thumbnailUrl': user.avatar.url,
            'id': user.id,
            'deleteUrl': "",
            'deleteType': 'POST',
            'success': True
        }
        return JsonResponse(file_payload)


class SendPhoneVerificationView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        success = False
        phone_number = request.POST.get('phone_number', None)
        if phone_number:
            gearouser = GearoUser.objects.get(user=request.user)
            if gearouser.phone_verified_status in [GearoUser.STATUS_NONE, GearoUser.STATUS_WAITING]:
                gearouser.phone_mobile = phone_number

                if gearouser.phone_verified_status == GearoUser.STATUS_NONE:
                    gearouser.phone_verified_status = GearoUser.STATUS_WAITING
                elif gearouser.phone_verified_status == GearoUser.STATUS_WAITING:
                    gearouser.phone_verified_status = GearoUser.STATUS_RESENT
                gearouser.save()

                new_token = Tokens(user=request.user, token=gentoken(5, True, "Tokens", "token", True), kind=4)
                new_token.save()

                sms_text = 'Hallo, dein Verifizierungscode lautet: %s' % new_token.token
                send_sms.delay(phone_number, sms_text)
                messages.success(request, 'Der Verifizierungscode wurde soeben an dein Handy geschickt.')

                success = True

        return JsonResponse({'success': success})


class CheckPhoneVerificationView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        success = False
        token_value = request.POST.get('token', None)
        if token_value:
            try:
                token = Tokens.objects.get(user=request.user, kind=4, token=token_value)
            except Tokens.DoesNotExist:
                token = None

            if token:
                gearouser = GearoUser.objects.get(user=request.user)
                gearouser.phone_verified_status = GearoUser.STATUS_SUCCESS
                gearouser.phone_verified = True
                gearouser.save()
                token.delete()

                success = True
                messages.success(request, 'Deine Nummer wurde erfolgreich verifiziert.')

        return JsonResponse({'success': success})


class BaseUserProfileMixin(object):
    model = User
    slug_url_kwarg = 'username'
    slug_field = 'username'
    context_object_name = 'userprofil'


class UserProfileView(BaseUserProfileMixin, DetailView):
    template_name = "gearo/user/profile.html"

    def get_context_data(self, **kwargs):
        ctx = super(UserProfileView, self).get_context_data(**kwargs)
        results = Advertisement.objects.filter(owner=self.object, is_removed=False)
        copy_results = copy.deepcopy(results)
        ctx['results'] = results
        ctx['all_ads'] = list(copy_results.values('pk', 'lat', 'lng'))
        return ctx


class UserProfileReviewsView(BaseUserProfileMixin, DetailView):
    template_name = 'gearo/review/reviews.html'

    def get_rating(self, reviews, rating):
        for review in reviews:
            if review['rating'] == rating:
                return review['total']
        return 0

    def get_context_data(self, **kwargs):
        ctx = super(UserProfileReviewsView, self).get_context_data(**kwargs)
        reviews_count = list(
            self.object.userreviews.values('rating').annotate(total=Count('rating')).order_by('rating')
        )
        ratings = {}
        for rating, __ in UserReviews.RATING_TYPES:
            ratings[rating] = self.get_rating(reviews_count, rating)
        ctx['ratings'] = ratings
        return ctx


class CropAvatarView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        crop_x = request.POST.get('x', None)
        crop_y = request.POST.get('y', None)
        width = request.POST.get('width', None)
        height = request.POST.get('height', None)
        rotate = request.POST.get('rotate', None)
        container_width = request.POST.get('container_width', None)
        container_height = request.POST.get('container_height', None)
        success = False
        data = {}

        if all([crop_x, crop_y, width, height]):
            crop_x = int(crop_x)
            crop_y = int(crop_y)
            width = int(width)
            height = int(height)
            profile = GearoProfile.objects.get_or_create(user=request.user)[0]
            cropped_image_filename, temp_image = crop_image(profile.avatar, crop_x, crop_y,
                                                            width, height, rotate)
            profile.avatar.save(cropped_image_filename, File(temp_image))
            data['thumbnail_url'] = generate_avatar_thumbnail(profile.avatar)
            success = True
        return JsonResponse({'success': success, 'data': data})


class AcceptTermsView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            user.gearouser.is_terms_accepted = True
            user.gearouser.save()
        return JsonResponse({'success': True})

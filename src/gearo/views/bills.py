import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic import RedirectView, TemplateView, View

from gearo.helper.bills import generate_invoice
from gearo.models.billsystem import Billsystem
from gearo.models.transactions import Transactions

logger = logging.getLogger('gearo.views.bills')
User = get_user_model()


class GetBillView(View):
    """Class based view to generate pdf files, simplified to save time."""
    def get(self, request, *args, **kwargs):
        self.bill = Billsystem.objects.get(id=self.kwargs.pop("billid"))
        if request.user == self.bill.transaction.lessor or \
                request.user == self.bill.transaction.applicant:
            if not self.bill.invoice:
                self.bill.generate_invoice()
            return HttpResponse(
                self.bill.invoice, content_type='application/pdf')
        else:
            return redirect("mybills")

    def get_file(self):
        return self.bill.invoice


class GenerateInvoiceView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        trans = Transactions.objects.get(id=kwargs['inqid'])
        if self.request.user == trans.lessor:
            # check if an invoice already exists
            invoice_exists = Billsystem.objects.filter(
                type="invoice", transaction=trans, creator=trans.lessor
            ).exists()
            if invoice_exists:
                bill = Billsystem.objects.get(
                    type="invoice", transaction=trans, creator=trans.lessor)
                return redirect("renderpdf", bill.id)

            generate_invoice(trans)
            return reverse_lazy(
                "chat", kwargs={'slug': trans.chat.slug, 'trans_id': trans.id})
        else:
            return reverse_lazy("postbox")


class BillsApplicantView(LoginRequiredMixin, TemplateView):
    template_name = 'gearo/bills/bills.html'

    def get_context_data(self, **kwargs):
        ctx = super(BillsApplicantView, self).get_context_data(**kwargs)
        invoices = Billsystem.objects.filter(
            transaction__applicant=self.request.user, type="invoice")
        ctx['profile_site'] = 'mybillsapp'
        ctx['invoices'] = invoices
        return ctx


class BillsLessorView(LoginRequiredMixin, TemplateView):
    template_name = 'gearo/bills/bills.html'

    def get_context_data(self, **kwargs):
        ctx = super(BillsLessorView, self).get_context_data(**kwargs)
        invoices = Billsystem.objects.filter(
            transaction__ad__owner=self.request.user, type="invoice")
        ctx['profile_site'] = 'mybillsles'
        ctx['invoices'] = invoices
        return ctx

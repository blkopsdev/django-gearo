import json
import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse, reverse_lazy
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.defaultfilters import linebreaksbr
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.utils.html import strip_tags
from django.utils.timezone import now
from django.views.generic import CreateView, DetailView, FormView, RedirectView, TemplateView, UpdateView, View

import mangopay
from dateutil.parser import parse
from mangopay.api import APIRequest
from mangopay.constants import PAYMENT_STATUS_CHOICES, STATUS_CHOICES
from mangopay.resources import Card, Money, PreAuthorization
from payments import RedirectNeeded, get_payment_model
from post_office import mail
from taggit.models import Tag

from gearo.forms.addadquesiton import AddAdQuestionForm
from gearo.forms.addadvert import AddAdvertFrom, UnavailabilityPeriodFormSet
from gearo.forms.inquire_form import InquireForm
from gearo.forms.rent_calc import RentCalcForm
from gearo.helper.absolute_url import absolute_url
from gearo.helper.alldays import alldays
from gearo.helper.calc_insurance import calc_insurance
from gearo.helper.calc_payout import calc_payout
from gearo.helper.calc_rental import calc_rental
from gearo.helper.calc_transaction_data import calc_transaction_data
from gearo.helper.mangopay import (
    WEB_PAYIN_MAPPING, get_card_pre_registration_data, get_mangopay_user,
    get_mangopay_wallet, get_preauthorization, money_to_cents
)
from gearo.helper.response_limit import get_response_limit
from gearo.models.advertisement import Advertisement, AdvertisementCategory
from gearo.models.advertisement_questions import AdvertisementQuestions
from gearo.models.discount_voucher import Voucher, VoucherApplication
from gearo.models.notifications import Notifications
from gearo.models.transactions import Transactions
from gearo.models.user_payment import GearoPayOut, MangopayPreAuthorization
from gearo.signals import advertisement_removed, advertisement_updated
from gearo.views.mixins import AttachmentMixin, BaseAdvertisementMixin, PaymentMixin
from gearo_tasks.tasks.mangopay import make_mp_payout, update_mp_wallet_balance
from gearo_tasks.tasks.sms import send_sms

logger = logging.getLogger(__name__)
Payment = get_payment_model()

mangopay.sandbox = settings.MANGOPAY_USE_SANDBOX
mangopay.client_id = settings.MANGOPAY_CLIENT_ID
mangopay.passphrase = settings.MANGOPAY_PASSPHRASE
handler = APIRequest(sandbox=settings.MANGOPAY_USE_SANDBOX)


class AdvertisementCreateUpdateMixin(AttachmentMixin):
    form_class = AddAdvertFrom
    formset_class = UnavailabilityPeriodFormSet
    model = Advertisement
    template_name = 'gearo/advertisements/advertise.html'
    related_attachment_field = 'ad'

    def post(self, request, *args, **kwargs):
        is_updating = kwargs.get(self.pk_url_kwarg, None)
        if is_updating:
            self.object = self.get_object()
        else:
            self.object = None
        form = self.get_form()
        formset = self.get_formset()
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def get_formset(self):
        formset_kwargs = self.get_form_kwargs()
        formset_kwargs.pop('initial', None)
        return self.formset_class(**formset_kwargs)

    def form_valid(self, form, formset):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        tags = [t.lower() for t in form.cleaned_data['tags']]
        self.object.tags.set(*tags, clear=True)
        self.set_attachments()
        formset.instance = self.object
        formset.save()
        advertisement_updated.send(instance=self.object, sender=Advertisement)
        messages.success(self.request, 'Deiner Änderungen wurden gespeichert.')
        return redirect(self.get_success_url())

    def form_invalid(self, form, formset):
        messages.error(self.request, 'Leider konnten deine Änderungen nicht gespeichert werden.')
        return super(AdvertisementCreateUpdateMixin, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        form = kwargs.get('form', None)
        context = super(AdvertisementCreateUpdateMixin, self).get_context_data(**kwargs)
        categories_terms = AdvertisementCategory.objects.values_list('pk', 'terms_of_service')
        context['schedule_formset'] = self.get_formset()
        context['daily_billed_categories'] = json.dumps(
            list(AdvertisementCategory.objects.filter(is_billed_daily=True).values_list('pk', flat=True)))
        context['categories_terms'] = json.dumps({k: v for (k, v) in categories_terms})
        if form and not form.is_valid():
            context['error'] = True
        return context


class AdvertisementCreateView(LoginRequiredMixin, BaseAdvertisementMixin, AdvertisementCreateUpdateMixin, CreateView):
    initial_fields = [
        'pickup_location',
        'time_mo_start', 'time_mo_end',
        'time_di_start', 'time_di_end',
        'time_mi_start', 'time_mi_end',
        'time_do_start', 'time_do_end',
        'time_fr_start', 'time_fr_end',
        'time_sa_start', 'time_sa_end',
        'time_so_start', 'time_so_end',
        'lat', 'lng',
    ]

    def get_success_url(self):
        return reverse_lazy("adpreview", kwargs={'ad_id': self.object.id})

    def get_context_data(self, **kwargs):
        context = super(AdvertisementCreateView, self).get_context_data(**kwargs)
        context['add'] = True
        return context

    def get_initial(self):
        last_advertisement = Advertisement.objects.filter(is_removed=False, owner=self.request.user).last()
        if last_advertisement:
            initial = {}
            for field in self.initial_fields:
                initial[field] = getattr(last_advertisement, field, '')
            return initial
        return super(AdvertisementCreateView, self).get_initial()


class AdvertisementUpdateView(LoginRequiredMixin, BaseAdvertisementMixin, AdvertisementCreateUpdateMixin, UpdateView):
    context_object_name = 'ad'
    pk_url_kwarg = 'ad_id'

    def dispatch(self, request, *args, **kwargs):
        r = super(AdvertisementUpdateView, self).dispatch(request, *args, **kwargs)
        if getattr(self, 'object', None) and self.object.owner != self.request.user:
            return redirect("addetail", ad_slug=self.object.slug)
        return r

    def get_initial(self):
        return {'attachments': ','.join([str(aid) for aid in self.object.attachment_ids])}


class AdvertisementDetailView(BaseAdvertisementMixin, DetailView):
    model = Advertisement
    context_object_name = 'ad'
    slug_url_kwarg = 'ad_slug'
    template_name = 'gearo/advertisements/detail.html'
    payload_fields = ['title', 'thumbnail_url', 'priceday', 'include_mwst']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.is_ajax():
            payload = {}
            for field_name in self.payload_fields:
                payload[field_name] = getattr(self.object, field_name, '')
            payload['absolute_url'] = self.object.get_absolute_url()
            return JsonResponse(payload)

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(AdvertisementDetailView, self).get_context_data(**kwargs)
        open_questions = AdvertisementQuestions.objects.filter(answer__isnull=True, ad=self.object)
        questions = AdvertisementQuestions.objects.filter(answer__isnull=False, ad=self.object) \
                                                  .order_by("-answered_date")
        context.update(
            form=InquireForm(instance=self.object),
            form_question=AddAdQuestionForm(),
            open_quests=open_questions,
            quests=questions,
        )
        return context


class AdvertisementPreviewView(AdvertisementDetailView):
    slug_url_kwarg = 'ad_id'
    pk_url_kwarg = 'ad_id'


class AdvertisementRedirectView(RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        ad_id = kwargs.get('ad_id', None)
        try:
            ad = Advertisement.objects.get(id=ad_id)
        except Advertisement.DoesNotExist:
            ad = None
        if ad:
            return ad.get_absolute_url()
        return reverse_lazy('search_results')


class CreateInquireView(LoginRequiredMixin, TemplateView):
    template_name = "gearo/advertisements/booking.html"
    ad_id = ad = None

    def dispatch(self, request, *args, **kwargs):
        self.ad_id = kwargs.get('ad_id')
        self.ad = get_object_or_404(Advertisement, id=self.ad_id, is_removed=False)
        return super(CreateInquireView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return redirect('addetail', self.ad.slug)

    def post(self, request, *args, **kwargs):
        lessor = self.ad.owner
        if request.user == lessor:
            return redirect("adedit", ad_id=self.ad_id)

        rent_start = parse(request.POST.get("startdate"), dayfirst=True).date()
        rent_end = parse(request.POST.get("enddate"), dayfirst=True).date()

        # return to detail if wrong date format
        try:
            affected_days = alldays(rent_start, rent_end)
        except ValueError:
            return redirect("addetail", self.ad_id)

        # return to detail site if ad not availability
        # - maybe find a better solution.
        if not self.ad.check_availability(affected_days):
            return redirect("addetail", self.ad_id)

        td = rent_end - rent_start
        pickup_after_3pm = request.POST.get('pickup_after_3pm', None)
        return_before_12pm = request.POST.get('return_before_12pm', None)
        payday = self.ad.calculate_rent_duration(
            td, pickup_after_3pm=pickup_after_3pm, return_before_12pm=return_before_12pm,
        )

        # need to check if user expelled vat
        rental = calc_rental(payday, self.ad)
        ins = calc_insurance(duration=payday, ad=self.ad)
        trans_data = calc_transaction_data(rental=rental, insurance=ins, ad=self.ad)
        payout = calc_payout(lessor, rental['brutto'])

        new_trans = Transactions(
            ad=self.ad,
            applicant=request.user,  # tenant
            lessor=lessor,
            response_limit=get_response_limit(rent_start),
            rent_start=rent_start,
            rent_end=rent_end,
            priceperday=(rental["brutto"]/payday),
            calculated_duration=payday,

            insurance=ins["insurance"],
            insurance_vat=ins["insurance_vat"],
            insurance_sum=ins["insurance_sum"],  # insurance brutto
            insurance_commission=ins["insurance_commission"],  # commission netto
            insurance_commission_vat=ins["insurance_commission_vat"],  # commission vat
            insurance_commission_sum=ins["insurance_commission_sum"],  # commission brutto
            insurance_debt=ins["insurance_debt"],  # total insurance costs netto
            insurance_debt_sum=ins["insurance_debt_sum"],  # total insurance costs brutto

            vat_special=trans_data['vat_special'],
            netto_total_special=trans_data['netto_total_special'],
            subgoal=trans_data['subgoal'],
            insurance_profit=trans_data['insurance_profit'],
            netto_total=trans_data['netto_total'],
            vat=trans_data['vat'],
            total=trans_data['total'],

            payoutdaily=payout / payday,
            articlecost=rental["netto"],
            articlecost_sum=rental["brutto"],
            long_term_benefit=rental['long_term_benefit'],
            daily_rent_price=rental['daily_rent_price'],

            payout=payout,
            pickup_after_3pm=pickup_after_3pm,
            return_before_12pm=return_before_12pm
        )
        new_trans.save()

        context = self.get_context_data(**kwargs)
        context.update(ad=self.ad, trans=new_trans, submit=True)
        logger.debug(
            'Created transaction #%s by user "%s" for Ad #%s', new_trans.id, self.request.user.email, self.ad.id)

        return self.render_to_response(context)


class UpdateResponseLimitView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        success = False
        ad_id = kwargs.get('ad_id', None)
        trans_id = kwargs.get('trans_id', None)
        response_limit = request.POST.get("response_limit", None)
        ad = get_object_or_404(Advertisement, id=ad_id, is_removed=False)
        trans = get_object_or_404(Transactions, ad=ad, applicant=request.user, id=trans_id)
        if response_limit:
            response_limit_datetime = parse(response_limit)
            trans.response_limit = response_limit_datetime
            trans.save()
            success = True
        return JsonResponse({'success': success})


class AddVoucherView(LoginRequiredMixin, View):

    def post(self, request, *args, ad_id=None, trans_id=None, **kwargs):
        ad = get_object_or_404(Advertisement, id=ad_id, is_removed=False)
        code = request.POST.get('code', None)
        transaction = get_object_or_404(Transactions, ad=ad, applicant=request.user, id=trans_id)
        success = False
        data = {}
        if transaction.voucher_applications.filter(voucher__code=code).exists():
            return JsonResponse({'success': success, 'is_already_used': True})
        if code:
            try:
                voucher = Voucher.objects.get(code=code)
            except Voucher.DoesNotExist:
                voucher = None

            if voucher and voucher.is_active and voucher.validate_voucher(request.user):
                transaction.apply_voucher(voucher, user=request.user)
                success = True
                data.update(
                    discount=transaction.discount,
                    discount_value=transaction.discount_value,
                    new_total=transaction.total_incl_discount,
                    new_debt=transaction.subtotal_incl_discount,
                )
        data['success'] = success
        return JsonResponse(data)


class CreatePaymentView(LoginRequiredMixin, PaymentMixin, View):

    def post(self, request, *args, **kwargs):
        tenant = request.user
        payment = self.create_payment(request, 'paypal', tenant, **kwargs)

        try:
            form = payment.get_form()
        except RedirectNeeded as redirect_to:
            return JsonResponse({'success': True, 'redirect_path': str(redirect_to)})
        except Exception as e:
            logger.debug('Error during PayPal payment creation - %s', e)
            return JsonResponse({'success': False})

        return JsonResponse({'success': False})


class CreateCardPaymentView(LoginRequiredMixin, PaymentMixin, View):

    def get(self, request, *args, **kwargs):
        transaction_id = kwargs.get('trans_id', None)
        tenant = request.user

        data = get_card_pre_registration_data(tenant)
        data['success'] = True

        logger.debug('Get card preregistration data for transaction #%s by user "%s"', transaction_id, tenant.email)

        return JsonResponse(data)

    def post(self, request, *args, **kwargs):
        ad_id = kwargs.get('ad_id', None)
        transaction_id = kwargs.get('trans_id', None)

        tenant = request.user
        mangopay_user = get_mangopay_user(tenant)
        payment = self.create_payment(request, 'mangopay_card', tenant, **kwargs)

        try:
            preauthorization = PreAuthorization(
                author=mangopay_user,
                debited_funds=Money(amount=money_to_cents(payment.total), currency='EUR'),
                card=Card.get(request.POST['card_id']),
                secure_mode_return_url=self.get_return_url(ad_id, transaction_id)
            )
            preauthorization.save()
        except Exception as e:
            logger.debug('Error during Mangopay Card PreAuthorization - %s', e)
            return JsonResponse({'success': False})

        MangopayPreAuthorization.objects.create(payment=payment, preauthorization_id=preauthorization.get_pk())

        logger.debug(
            'Get PreAuthorization for payment #%s for transaction #%s by user "%s"',
            payment.id, transaction_id, tenant.email,
        )
        return JsonResponse({'success': True})


class CardPreAuthorizationStatusView(LoginRequiredMixin, PaymentMixin, View):

    def get(self, request, *args, **kwargs):
        tenant = request.user
        transaction = self.get_transaction(tenant, **kwargs)
        payment = Payment.objects.filter(gearo_transaction=transaction).last()

        preauthorization = get_preauthorization(payment.preauthorization.preauthorization_id)
        is_status_succeeded = preauthorization.status == STATUS_CHOICES.succeeded
        is_status_failed = preauthorization.status == STATUS_CHOICES.failed
        is_payment_status_waiting = preauthorization.payment_status == PAYMENT_STATUS_CHOICES.waiting

        if is_status_succeeded and is_payment_status_waiting:
            payment.change_status('preauth')
            logger.debug(
                'Created Card PreAuthorization for payment #%s for transaction #%s by user "%s"',
                payment.id, transaction.id, tenant.email)
            return JsonResponse({'success': True, 'redirect_path': settings.BASE_URL + payment.get_success_url()})

        if is_status_failed:
            payment.change_status('rejected')
            logger.debug(
                'Failed Card PreAuthorization for payment #%s for transaction #%s by user "%s"',
                payment.id, transaction.id, tenant.email)
            return JsonResponse({'error': True})

        # Some payments (e.g. over 50 EUR) should be made in "3D Secure" mode. In this case user will be redirected
        # to `secure_mode_redirect_url`. In case of success after `secure_mode_redirect_url`,
        # user will be redirected to `payment.get_success_url()` (as set during PreAuthorization creation).
        if preauthorization.secure_mode_needed:
            payment.change_status('preauth')
            redirect_path = preauthorization.secure_mode_redirect_url
            logger.debug(
                'Card PreAuthorization for payment #%s for transaction #%s by user "%s" redirected for Secure Mode',
                payment.id, transaction.id, tenant.email)
            return JsonResponse({'success': True, 'redirect_path': redirect_path})

        return JsonResponse({'success': False})


class CreateWebPayInPaymentView(LoginRequiredMixin, PaymentMixin, View):

    def post(self, request, *args, **kwargs):
        ad_id = kwargs.get('ad_id', None)
        payment_variant = kwargs.pop('payment_variant', None)

        tenant = request.user
        mangopay_user = get_mangopay_user(tenant)
        payment = self.create_payment(request, payment_variant, tenant, **kwargs)
        transaction = payment.gearo_transaction
        lessor = payment.gearo_transaction.lessor

        try:
            debited_funds = Money(amount=money_to_cents(payment.total), currency='EUR')
            fees_in_cents = money_to_cents(payment.total - transaction.payout)
            fees = Money(amount=fees_in_cents, currency='EUR')

            payin = WEB_PAYIN_MAPPING[payment_variant]
            WebPayIn = payin['class']

            web_payin = WebPayIn(
                author=mangopay_user,
                credited_wallet=get_mangopay_wallet(lessor),
                return_url=self.get_return_url(ad_id, transaction.id),
                culture='DE',  # The language to use for the payment page (SOFORT)
                direct_debit_type=payin['mp_payment_variant'],
                debited_funds=debited_funds,
                fees=fees,
            )
            web_payin.save()
        except Exception as e:
            logger.debug('Error during %s payment creation - %s', payment_variant, e)
            return JsonResponse({'success': False})

        payment.web_payin_id = web_payin.get_pk()
        payment.save()
        redirect_path = web_payin.redirect_url

        logger.debug(
            'Create %s payment for payment #%s for transaction #%s by user "%s"',
            payment_variant, payment.id, transaction.id, tenant.email,
        )

        return JsonResponse({'success': True, 'redirect_path': redirect_path})


class MangopayRedirectView(LoginRequiredMixin, PaymentMixin, View):
    """
    Tenant will be redirected here after:
    - Web PayIn payment (whether successful or not).
    - Mangopay "3D Secure" (for card payments).

    Here we will check status of Card PreAuthorization or Web PayIn and redirect tenant based on it.
    """

    def get(self, request, *args, **kwargs):
        tenant = request.user
        transaction = self.get_transaction(tenant, **kwargs)
        payment = Payment.objects.filter(gearo_transaction=transaction).last()
        payin = self.get_payin_info(payment)

        if payin['is_status_failed']:
            payment.change_status('rejected')
            logger.debug(
                'Failed %s #%s for payment #%s for transaction #%s by user "%s"',
                payin['payin_method'], payin['payin_id'], payment.id, transaction.id, tenant.email,
            )
            return redirect(payment.get_failure_url())

        if payin['is_status_succeeded'] and payin['is_payment_status_waiting']:
            self.set_preauth_payment_status(payment)
            logger.debug(
                'Created %s #%s for payment #%s for transaction #%s by user "%s"',
                payin['payin_method'], payin['payin_id'], payment.id, transaction.id, tenant.email,
            )

            # Update lessor's and tenant's wallets balances
            # useful just for Web PayIns, because tenant will be charged at this point
            lessor = transaction.lessor
            update_mp_wallet_balance.delay(lessor.id)
            update_mp_wallet_balance.delay(tenant.id)

            return redirect(payment.get_success_url())


class MangopayPayOutView(LoginRequiredMixin, View):

    @method_decorator(staff_member_required())
    def dispatch(self, *args, **kwargs):
        return super(MangopayPayOutView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        transaction_id = kwargs.get('trans_id', None)
        transaction = get_object_or_404(Transactions, id=transaction_id)

        # Before making "PayOut", we will create instance of `GearoPayOut` and then update it after "PayOut"
        # will be make. So after redirecting to "admin", user will not see "PayOut Button" for this transaction.
        GearoPayOut.objects.create(transaction=transaction)
        make_mp_payout.delay(transaction_id)

        messages.success(request, 'Payout for transaction #{} has successfully initiated.'.format(transaction_id))

        referrer_page = request.META.get('HTTP_REFERER', None)
        if referrer_page is None:
            referrer_page = reverse('admin:gearo_transactions_changelist')

        return redirect(referrer_page)


class BookingSuccessView(LoginRequiredMixin, TemplateView):
    template_name = "gearo/advertisements/booking.html"

    def get(self, request, *args, **kwargs):
        trans_id = kwargs['trans_id']
        ad_id = kwargs['ad_id']
        allowed_statuses = ['preauth', 'confirmed']
        payment = Payment.objects.filter(status__in=allowed_statuses, publicid=trans_id).last()
        ad = get_object_or_404(Advertisement, id=ad_id, is_removed=False)
        trans = Transactions.objects.get(id=payment.gearo_transaction.id, applicant=request.user, ad=ad)

        # Update status and send notifications *only* if this is
        # new transaction, to prevent status overwrite from "accepted",
        # "rejected" and etc.
        if trans.status != Transactions.STATUS_INCOMPLETE:
            messages.error(self.request, 'Die Anfrage wurde bereits angenommen.')
            return redirect('search_results')
        else:
            # TODO: update this part when all payments will be through Mangopay
            payment_status_for_log = 'charged'
            if payment.variant in ['paypal', 'mangopay_card']:
                payment_status_for_log = 'pre-authorized'
            logger.debug(
                'Successfully %s payment #%s for transaction #%s', payment_status_for_log, payment.id, trans_id,
            )

            trans.status = Transactions.STATUS_WAITING
            trans.viewed_by_lessor = False
            trans.save()
            trans.voucher_applications.update(status=VoucherApplication.STATUS_USED)

            gearo_user = trans.lessor.gearouser
            if gearo_user.phone_mobile and gearo_user.phone_verified:
                logger.debug('Sent SMS confirmation for transaction #%s, lessor %s', trans_id, trans.lessor.email)
                sms_text = render_to_string("gearo/advertisements/booking_sms.txt", {'trans': trans})
                send_sms.delay(trans.lessor.gearouser.phone_mobile.raw_input, sms_text)

            logger.debug('Sent email confirmation for transaction #%s', trans_id)

            # send info to ad owner
            mail.send(
                ad.owner.email,
                'no-reply@gearo.de',
                template='bookinglessorinfo',
                context={
                    'firstname_lessor': ad.owner.first_name,
                    'firstname_tenant': trans.applicant.first_name,
                    'bookingmanlink': absolute_url("maninq", trans.id),
                    'responselimit': trans.get_responselimit(),
                })

            Notifications.objects.create(user=ad.owner, transaction=trans, type="inquire")
            # send confirmation to applicant
            mail.send(
                trans.applicant.email,
                'no-reply@gearo.de',
                template='bookingconfirmation',
                context={
                    'firstname_lessor': ad.owner.first_name,
                    'firstname_tenant': trans.applicant.first_name,
                    'showtransaction': absolute_url("showinq", trans.id),
                })

        context = self.get_context_data(**kwargs)
        context.update(ad=ad, trans=trans, bookingsuccess=True)
        return self.render_to_response(context)


class BookingFailureView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        logger.debug('Transaction payment failed, transaction #%s, ad #%s', kwargs['trans_id'], kwargs['ad_id'])
        messages.error(self.request, "Leider ist ein Fehler aufgetreten")
        return reverse_lazy('addetail', args=(kwargs['ad_id'],))


class AskQuestionView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        success = False
        ad = get_object_or_404(Advertisement, id=kwargs['ad_id'], is_removed=False)
        question = request.POST.get('question', None)
        if question:
            quest = AdvertisementQuestions(ad=ad, questioner=request.user, question=strip_tags(question))
            quest.save()

            mail.send(
                ad.owner.email,
                'no-reply@gearo.de',
                template='mailasked',
                context={
                    'firstname': ad.owner.first_name,
                    "answerlink": absolute_url("addetail", ad.slug) + "#frage-" + str(quest.id),
                })
            success = True
        return JsonResponse({'success': success})


class ReplyQuestionView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        success = False
        ad = get_object_or_404(Advertisement, id=kwargs['ad_id'], is_removed=False)
        question = AdvertisementQuestions.objects.get(ad=ad, id=kwargs['quest_id'])
        answer = request.POST.get('answer', None)
        response_data = {}
        if answer and ad.owner == request.user:
            question.answer = strip_tags(answer)
            question.answered_date = now()
            question.save()
            ad.ad_points += 4
            ad.save()
            mail.send(
                question.questioner.email,
                'no-reply@gearo.de',
                template='mailanswered',
                context={
                    'firstname': question.questioner.first_name,
                    'adurl': absolute_url("addetail", ad.slug),
                    'answer': question.answer,
                })

            success = True
            response_data['questioner'] = question.questioner.first_name
            response_data['question'] = linebreaksbr(question.question)
            response_data['answer'] = question.ad.owner.first_name
            response_data['answer'] = linebreaksbr(question.answer)
        response_data['success'] = success
        return JsonResponse(response_data)


class RentCalcView(FormView):
    form_class = RentCalcForm
    ad = None

    def dispatch(self, request, *args, **kwargs):
        ad_id = kwargs.get('ad_id', None)
        self.ad = get_object_or_404(Advertisement, id=ad_id, is_removed=False)
        return super(RentCalcView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        raise PermissionDenied

    def get_template_names(self):
        return

    def form_valid(self, form):
        ad_timedelta = form.cleaned_data['end_date'] - form.cleaned_data['start_date']
        pickup_after_3pm = form.cleaned_data['pickup_after_3pm']
        return_before_12pm = form.cleaned_data['return_before_12pm']
        payday = self.ad.calculate_rent_duration(
            ad_timedelta, pickup_after_3pm=pickup_after_3pm, return_before_12pm=return_before_12pm)
        rental = calc_rental(payday, self.ad)
        insurance = calc_insurance(duration=payday, ad=self.ad)
        payload = dict(
            success=True,
            rent=float("%.2f" % rental.get("brutto")),
            insurance=float("%.2f" % insurance.get("insurance_debt_sum")),
            total=float("%.2f" % (rental.get("brutto") + insurance.get("insurance_debt_sum"))),
            duration=float("%.2f" % rental['duration']),
            long_term_benefit=float("%.2f" % rental['long_term_benefit']),
            daily_rent_price=float("%.02f" % rental['daily_rent_price']),
        )
        return JsonResponse(payload)

    def form_invalid(self, form):
        payload = {
            'success': False,
            'errors': form.errors,
        }
        return JsonResponse(payload)


class AdvertisementRemoveView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        success = False
        ad_id = request.POST.get('id', None)
        try:
            ad = Advertisement.objects.get(id=ad_id, is_removed=False)
            if ad.owner == request.user:
                logger.debug('Ad #%s removed by user "%s"', ad.id, request.user.email)
                ad.is_removed = True
                ad.save()
                advertisement_removed.send(instance=ad, sender=Advertisement)
                success = True
        except Advertisement.DoesNotExist:
            pass
        return JsonResponse({'success': success})


class TagAutocompleteView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        limit = settings.TAG_AUTOSUGGEST_MAX_ITEMS
        success = False
        tag = request.GET.get('tag', None)
        tags = []
        if tag:
            tags = list(Tag.objects.filter(name__icontains=tag).values_list('name', flat=True))

        return JsonResponse({'success': success, 'tags': tags[:limit]})

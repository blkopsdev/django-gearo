"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
import copy

from django.conf import settings
from django.core.cache import cache
from django.core.paginator import EmptyPage, Paginator
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import QueryDict
from django.http.response import JsonResponse
from django.utils.http import urlquote, urlunquote
from django.utils.text import slugify
from django.views.generic.base import RedirectView, TemplateView

import googlemaps
from haystack.query import SearchQuerySet
from haystack.utils.geo import D, Point

from gearo.helper.alldays import alldays
from gearo.models.advertisement_categories import AdvertisementCategory


class BaseSearchMixin(object):
    search_sorting_choices = ['-priceday', 'priceday', '-ranking']


class SearchRedirectView(BaseSearchMixin, RedirectView):
    search_post_fields = ['term', 'cat_slug', 'place', 'distance']

    def get_redirect_url(self, *args, **kwargs):
        redirect_url_kwargs = {}
        post_data = self.request.POST.copy()
        pickup_date = self.request.POST.get('pickup', None)
        return_date = self.request.POST.get('return', None)
        sorting = self.request.POST.get('sorting', None)
        if all([pickup_date, return_date]):
            self.search_post_fields += ['pickup', 'return']
        else:
            post_data['day'] = pickup_date or return_date
            post_data.pop('pickup', None)
            post_data.pop('return', None)
            self.search_post_fields += ['day']

        for field_name in self.search_post_fields:
            value = post_data.get(field_name, None)
            if value:
                redirect_url_kwargs[field_name] = urlquote(value)

        url = reverse_lazy('search_results', kwargs=redirect_url_kwargs)

        if sorting and sorting in self.search_sorting_choices:
            query_str = QueryDict(mutable=True)
            query_str['sorting'] = sorting
            url += '?' + query_str.urlencode()

        return url


class SearchResultsView(BaseSearchMixin, TemplateView):
    template_name = 'gearo/search/results.html'
    payload_fields = ['profile_url', 'profile_avatar_url', 'absolute_url',
                      'title', 'thumbnail_url', 'priceday', 'include_mwst']
    has_next_page = False

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            context = self.get_context_data(**kwargs)
            results = []
            for r in context['results']:
                result = {}
                for field_name in self.payload_fields:
                    result[field_name] = getattr(r, field_name, '')
                results.append(result)
            payload = {
                'results': results,
                'has_next': self.has_next_page
            }
            return JsonResponse(payload)
        return super(SearchResultsView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SearchResultsView, self).get_context_data(**kwargs)
        search_params = {}
        term = kwargs.get('term', None)
        cat_slug = kwargs.get('cat_slug', None)
        place = kwargs.get('place', None)
        pickup_date = kwargs.get('pickup', None)
        return_date = kwargs.get('return', None)
        day = kwargs.get('day', None)
        distance = kwargs.get('distance', None)
        sorting = self.request.GET.get('sorting', None)
        page_num = self.request.GET.get('page_num', 1)
        dates = None

        if term:
            term = urlunquote(term)
            search_params['content__contains'] = term
            context['term'] = term

        if cat_slug:
            cat = AdvertisementCategory.objects.get(secret_name=cat_slug)
            search_params['category__secret_name'] = cat.name
            context['category'] = cat
            context['cat'] = cat_slug

        results = SearchQuerySet().filter(**search_params)

        if place:
            place = urlunquote(place)
            place_key = slugify(urlunquote(place), allow_unicode=True)
            place_location = cache.get(place_key, None)
            longitude = latitude = None
            if not place_location:
                gmaps_client = googlemaps.Client(key=settings.GOOGLE_MAPS_KEY)
                points = gmaps_client.geocode(place)
                if len(points) > 0:
                    point = points[0]
                    longitude = point["geometry"]["location"]["lng"]
                    latitude = point["geometry"]["location"]["lat"]

                    place_location = {
                        'lat': latitude,
                        'lng': longitude
                    }
                    cache.set(place_key, place_location)
            else:
                latitude = place_location['lat']
                longitude = place_location['lng']

            if longitude and latitude:
                geopoint = Point(longitude, latitude)
                if distance:
                    context['distance'] = distance

                max_distance = D(km=distance or 15)
                results = results.dwithin('location', geopoint, max_distance)
                context['lat'] = latitude
                context['lng'] = longitude
                context['place'] = place

        if day:
            dates = [day]
            context['start'] = day

        if pickup_date and return_date:
            dates = alldays(str(pickup_date), str(return_date))
            context['start'] = pickup_date
            context['end'] = return_date

        if dates:
            results = results.exclude(Q(not_available_dates__in=dates) | Q(hired_dates__in=dates))

        if sorting and sorting in self.search_sorting_choices:
            context['sorting'] = sorting
        else:
            sorting = '-ranking'

        results = results.order_by(sorting)

        p = Paginator(results, settings.ITEMS_PER_PAGE)
        try:
            self.page = p.page(page_num)
            context['results'] = self.page.object_list
            self.has_next_page = self.page.has_next()
        except EmptyPage:
            context['results'] = []
        if not self.request.is_ajax():
            copy_results = copy.deepcopy(results)
            context['all_ads'] = list(copy_results.values('pk', 'lat', 'lng'))
        return context

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.views.generic import TemplateView

from gearo.models.faq_categorie import FaqCategory


class FAQView(TemplateView):
    template_name = 'gearo/faq.html'

    def get_context_data(self, **kwargs):
        ctx = super(FAQView, self).get_context_data(**kwargs)
        ctx['tenant_categories'] = FaqCategory.objects.filter(parent=FaqCategory.TENANT_CATEGORY)
        ctx['lessor_categories'] = FaqCategory.objects.filter(parent=FaqCategory.LESSOR_CATEGORY)
        return ctx

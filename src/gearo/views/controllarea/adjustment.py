"""
@copyright: 2016 Gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import FormView

from constance.admin import ConstanceForm, get_values

from gearo.views.mixins import StaffMemberRequiredMixin


class ConstanceEditView(StaffMemberRequiredMixin, FormView):
    template_name = 'gearo/controllarea/constance.html'
    form_class = ConstanceForm
    success_url = reverse_lazy('settings')

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Einstellungen erfolgreich aktualisiert')
        return super(ConstanceEditView, self).form_valid(form)

    def get_initial(self):
        return get_values()

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect, render

from post_office.models import EmailTemplate

from gearo.forms.emailtemplate import EmailTemplateFrom


@staff_member_required
def emailover_view(request):
    templates = EmailTemplate.objects.all()
    return render(request, "gearo/controllarea/email.html", {"section": "", "templates": templates})


@staff_member_required
def emailadd_view(request):
    if request.method == "POST":
        form = EmailTemplateFrom(request.POST)
        if form.is_valid():
            new_template = form.save()
            return render(request, "gearo/controllarea/email.html", {"section": "add", "form": form, "success": True})
    form = EmailTemplateFrom() 
    return render(request, "gearo/controllarea/email.html", {"section": "add", "form": form})


@staff_member_required
def emailedit_view(request, mail_id):
    if request.method == "POST":
        template = EmailTemplate.objects.get(id=mail_id)
        form = EmailTemplateFrom(request.POST, instance=template)
        if form.is_valid():
            form.save()
            return render(request, "gearo/controllarea/email.html", {"section": "edit", "form": form,
                                                                     "template": template, "success": True})
        else:
            return render(request, "gearo/controllarea/email.html", {"section": "edit", "form": form,
                                                                     "template": template})
    else:
        template = EmailTemplate.objects.get(id=mail_id)
        form = EmailTemplateFrom(instance=template)
        return render(request, "gearo/controllarea/email.html", {"section": "edit", "form": form,
                                                                 "template": template})
    return render(request, "gearo/controllarea/email.html", {"section": "edit"})


@staff_member_required
def emaildel_view(request):
    return redirect("emailtemplates")

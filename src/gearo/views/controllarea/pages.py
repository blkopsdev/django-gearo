"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render

from gearo.forms.addsite_form import SiteForm
from gearo.models.sites import Sites


@staff_member_required
def dashboard_view(request):
    return render(request, "gearo/controllarea/not_constructed.html", {})
 

@staff_member_required
def pages_view(request):
    sites = Sites.objects.all()
    return render(request, 'gearo/controllarea/pages.html', {"sites": sites})


@staff_member_required
def pageadd_view(request):
    if request.method == "POST":
        form = SiteForm(request.POST)
        if form.is_valid():
            new_site = form.save()
            
            return render(request, 'gearo/controllarea/pages.html', {"section": "add", "success": True, "form": form})
        else:
            return render(request, 'gearo/controllarea/pages.html', {"section": "add", "form": form})
    else:
        form = SiteForm()
        return render(request, 'gearo/controllarea/pages.html', {"section": "add", "form": form})


@staff_member_required
def page_edit_view(request, site_id):
    if request.method == "POST":
        site = Sites.objects.get(id=site_id)
        form = SiteForm(request.POST, instance=site)
        if form.is_valid():
            form.save()
            return render(request, 'gearo/controllarea/pages.html', {"section": "edit", "site": site, "form": form,
                                                                     "success": True})
    site = Sites.objects.get(id=site_id)
    form = SiteForm(instance=site)
    return render(request, 'gearo/controllarea/pages.html', {"section": "edit", "site": site, "form": form})

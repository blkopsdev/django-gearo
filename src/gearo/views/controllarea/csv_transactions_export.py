from django.views.generic.base import View
from django.views.generic.list import ListView

from gearo.models import Transactions
from gearo.views.controllarea.csv_constants import *
from gearo.views.mixins import CSVResponseMixin, StaffMemberRequiredMixin


class TransactionsList(StaffMemberRequiredMixin, ListView):
    model = Transactions
    template_name = 'gearo/controllarea/transactions.html'
    context_object_name = 'transactions'


class ExportTodayTransactionsView(StaffMemberRequiredMixin, CSVResponseMixin, View):

    def get(self, request, *args, **kwargs):
        export = Transactions.objects.filter(status=Transactions.STATUS_FINISHED)
        return self.render_to_csv_response(
            export,
            fields=TODAY_TRANSACTIONS_CSV_EXPORT_FIELDS,
            headers=TODAY_TRANSACTIONS_CSV_HEADERS,
            filename="auszahlungen",
        )


class ExportFinishedTransactionsView(StaffMemberRequiredMixin, CSVResponseMixin, View):

    def get(self, request, *args, **kwargs):
        queryset = Transactions.objects.filter(status=Transactions.STATUS_FINISHED)
        response = self.render_to_csv_response(
            queryset=queryset,
            fields=ALL_TRANSACTIONS_CSV_EXPORT_FIELDS,
            headers=ALL_TRANSACTIONS_CSV_HEADERS,
            filename="finished_transactions",
        )
        return response


class ExportAllTransactionsView(StaffMemberRequiredMixin, CSVResponseMixin, View):

    def get(self, request, *args, **kwargs):

        all_fields = ALL_TRANSACTIONS_CSV_FULL_LIST_EXPORT_FIELDS
        special_fields = ALL_TRANSACTIONS_CSV_FULL_LIST_SPECIAL_FIELDS

        response = self.render_to_csv_response(
            queryset=Transactions.objects.all(),
            fields=all_fields,
            headers=ALL_TRANSACTIONS_CSV_FULL_LIST_HEADERS,
            special_fields=special_fields,
            filename="all_transactions",
        )
        return response

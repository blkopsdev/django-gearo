"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
from django.views.generic.detail import DetailView

from gearo.forms.addvoucher import AddvoucherFrom
from gearo.models.discount_voucher import Voucher
from gearo.views.mixins import StaffMemberRequiredMixin


@staff_member_required
def addvoucher_view(request):
    if request.method == "POST":
        form = AddvoucherFrom(request.POST)
        if form.is_valid():
            form.save()
            return render(request, "gearo/controllarea/addvoucher.html", {"form": form, "section": "add",
                                                                          "success": True})
        else:
            return render(request, "gearo/controllarea/addvoucher.html", {"form": form, "section": "add"})
    
    form = AddvoucherFrom()
    return render(request, "gearo/controllarea/addvoucher.html", {"form": form, "section": "add"})


@staff_member_required
def editvoucher_view(request, vid):
    if request.method == "POST":
        voucher = Voucher.objects.get(id=vid)
        form = AddvoucherFrom(request.POST, instance=voucher)
        if form.is_valid():
            form.save()
            return render(request, "gearo/controllarea/addvoucher.html", {"form": form, "voucher": voucher,
                                                                          "section": "edit", "success": True})
        else:
            return render(request, "gearo/controllarea/addvoucher.html", {"form": form, "voucher": voucher,
                                                                          "section": "edit"})
    
    voucher = Voucher.objects.get(id=vid)
    form = AddvoucherFrom(instance=voucher)
    return render(request, "gearo/controllarea/addvoucher.html", {"form": form, "voucher": voucher, "section": "edit"})


@staff_member_required
def vouchers_view(request):    
    vouchers = Voucher.objects.all()
    return render(request, "gearo/controllarea/addvoucher.html", {"vouchers": vouchers})


class VoucherApplications(StaffMemberRequiredMixin, DetailView):
    model = Voucher
    pk_url_kwarg = 'vid'
    template_name = 'gearo/controllarea/voucher_applications.html'
    context_object_name = 'voucher'

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render


@staff_member_required
def support_view(request):
    return render(request, "gearo/controllarea/support.html")

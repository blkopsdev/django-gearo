"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect, render

from gearo.forms.add_question import AddQuestionFrom
from gearo.forms.faq_category import FaqCategoryFrom
from gearo.models.faq import Faq
from gearo.models.faq_categorie import FaqCategory


@staff_member_required
def faqoverview_view(request):
    quests_tenant = Faq.objects.filter(category__parent=1)
    quests_lessor = Faq.objects.filter(category__parent=2)
    return render(request, "gearo/controllarea/faq.html", {"questions_v": quests_lessor, "questions_m": quests_tenant})


@staff_member_required
def addquestion_view(request):
    if request.method == "POST":
        form = AddQuestionFrom(request.POST)
        if form.is_valid():
            new_question = form.save()
            return render(request, "gearo/controllarea/faq.html", {"section": "add", "form": form, "success": True})
        else:
            return render(request, "gearo/controllarea/faq.html", {"section": "add", "form": form})
    else:
        form = AddQuestionFrom()
        return render(request, "gearo/controllarea/faq.html", {"section": "add", "form": form})
    

@staff_member_required
def editquestion_view(request, quest_id):
    if request.method == "POST":
        question = Faq.objects.get(id=quest_id)
        form = AddQuestionFrom(request.POST, instance=question)
        if form.is_valid():
            form.save()
            return render(request, "gearo/controllarea/faq.html", {"section": "edit", "form": form,
                                                                   "question": question, "success": True})
        else:
            return render(request, "gearo/controllarea/faq.html", {"section": "edit", "form": form,
                                                                   "question": question})
    else:
        question = Faq.objects.get(id=quest_id)
        form = AddQuestionFrom(instance=question)
        return render(request, "gearo/controllarea/faq.html", {"section": "edit", "form": form, "question": question})


@staff_member_required
def togglequestion_view(request, quest_id):
    question = Faq.objects.get(id=quest_id)
    if question.active == 1:
        question.active = 0
    else:
        question.active = 1
    question.save()
    return redirect("supportfaq")


@staff_member_required
def faqcategoryoverview_view(request):
    categories = FaqCategory.objects.all()
    return render(request, "gearo/controllarea/faqcat.html", {"section": "catover", "cats": categories})


@staff_member_required
def faqaddcategory_view(request):
    if request.method == "POST":
        form = FaqCategoryFrom(request.POST)
        if form.is_valid():
            new_cat = form.save()
            return render(request, "gearo/controllarea/faqcat.html", {"section": "add", "form": form, "success": True})
        else:
            return render(request, "gearo/controllarea/faqcat.html", {"section": "add", "form": form})
    else:   
        form = FaqCategoryFrom() 
        return render(request, "gearo/controllarea/faqcat.html", {"section": "add", "form": form})
    

@staff_member_required
def faqeditcategory_view(request, faqcat_id):
    if request.method == "POST":
        cat = FaqCategory.objects.get(id=faqcat_id)   
        form = FaqCategoryFrom(request.POST, instance=cat)
        if form.is_valid():
            new_cat = form.save()
            return render(request, "gearo/controllarea/faqcat.html", {"section": "edit", "form": form, "cat": cat,
                                                                      "success": True})
        else:
            return render(request, "gearo/controllarea/faqcat.html", {"section": "edit", "form": form, "cat": cat})
    else:
        cat = FaqCategory.objects.get(id=faqcat_id)   
        form = FaqCategoryFrom(instance=cat) 
        return render(request, "gearo/controllarea/faqcat.html", {"section": "edit", "form":form, "cat": cat})

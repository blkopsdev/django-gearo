"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.shortcuts import render


@staff_member_required
def alluser_view(request):
    user = User.objects.all()
        
    return render(request, 'gearo/controllarea/user.html', {"users": user})

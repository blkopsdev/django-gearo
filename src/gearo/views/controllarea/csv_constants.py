TODAY_TRANSACTIONS_CSV_HEADERS = {
    "ad__id": "Inserats Nummer",
    "id": "Nummer",
    "ad__title": "Titel des Angebots",
    "rent_start": "Anmietung Beginn",
    "rent_end": "Anmietung Ende",
    "calculated_duration": "Berechnete Mietdauer",
    "ad__owner__profile__business_name": "Firma Vermieter",
    "ad__owner__first_name": "Vorname Vermieter",
    "ad__owner__last_name": "Nachname Vermieter",
    "applicant__profile__business_name": "Firma Mieter",
    "applicant__first_name": "Vorname Mieter",
    "applicant__last_name": "Nachname Mieter",
    "articlecost_sum": "Mietobjekt (Brutto)",
    "insurance_debt_sum": "Versicherung (Brutto)",
    "insurance_sum": "Versicherung inkl. 19% Vers.Steuer",
    "insurance_vat": "Versicherungssteuer",
    "insurance_commission": "Versicherungsprovision",
    "insurance_commission_vat": "Steuer auf Versicherungsprovision",
    "insurance_commission_sum": "Versicherungsprovion inklusive Mwst.",
    "insurance_debt": "Preis fur Mieter (6.5%) mit Versicherungsprovision Netto",
    "insurance_profit": "Versicherung Netto + Versicherungsprovision Netto",
    "netto_total_special": "netto Rechnungsbetrag Sonderfall",
    "netto_total": "netto Rechnungsbetrag",
    "subgoal": "Zwischensumme",
    "articlecost": "Mietobjekt (Netto)",
    "vat": "19% Mwst.",
    "vat_special": "19% Mwst. Sonderfall",
    "total": "Brutto Rechnungsbetrag",
    "total_special": "Brutto Rechnungsbetrag Sonderfall",
    "discount": "Rabatt in Prozent",
    "discount_value": "Rabatt Wert",
    "payout": "Auszahlungsbetrag gearo an Vermieter",
    "ad__owner__billing_info__paypal": "Auszahlungsmethode Paypal",
    "ad__owner__billing_info__iban": "IBAN",
    "ad__owner__billing_info__bic": "BIC",
    "ad__owner__billing_info__first_name": "Vorname",
    "ad__owner__billing_info__last_name": "Nachname",
}
TODAY_TRANSACTIONS_CSV_EXPORT_FIELDS = (
    "id", "ad__id", "ad__title", "rent_start", "rent_end",
    "calculated_duration", "applicant__profile__business_name",
    "applicant__first_name", "applicant__last_name",
    "ad__owner__profile__business_name", "ad__owner__first_name",
    "ad__owner__last_name", "articlecost_sum", "insurance_debt_sum",
    "insurance_sum", "insurance_vat", "insurance_commission",
    "insurance_commission_vat", "insurance_commission_sum", "insurance_debt",
    "insurance_debt_sum", "insurance_profit", "netto_total_special",
    "netto_total", "subgoal", "articlecost", "articlecost_sum", "vat",
    "vat_special", "total", "total_special", "discount", "discount_value",
    "payout", "ad__owner__billing_info__paypal",
    "ad__owner__billing_info__iban", "ad__owner__billing_info__bic",
    "ad__owner__billing_info__first_name",
    "ad__owner__billing_info__last_name",
)
ALL_TRANSACTIONS_CSV_HEADERS = {
    "id": "Nummer", "ad__owner__profile__business_name": "Firma Vermieter",
    "ad__owner__first_name": "Vorname Vermieter",
    "ad__owner__last_name": "Nachname Vermieter",
    "applicant__profile__business_name": "Firma Mieter",
    "applicant__first_name": "Vorname Mieter",
    "applicant__last_name": "Nachname Mieter",
    "rent_start": "Anmietung Beginn",
    "rent_end": "Anmietung Ende",
    "ad__title": "Titel des Angebots",
    "ad__articleofvalue": "Wiederbeschaffungswert",
}
ALL_TRANSACTIONS_CSV_EXPORT_FIELDS = (
    "id", "ad__owner__profile__business_name", "ad__owner__first_name",
    "ad__owner__last_name", "applicant__profile__business_name",
    "applicant__first_name", "applicant__last_name", "rent_start", "rent_end",
    "ad__title", "ad__articleofvalue",
)

ALL_TRANSACTIONS_CSV_FULL_LIST_HEADERS = {
    'id': 'ID Vermietung',
    'ad__owner__first_name': 'Vorname (Vermieter)',
    'ad__owner__last_name': 'Nachname (Vermieter)',
    'ad__owner__profile__business_name': 'Firmierung (Vermieter)',
    'applicant__first_name': 'Vorname (Mieter)',
    'applicant__last_name': 'Nachname (Mieter)',
    'applicant__profile__business_name': 'Firmierung (Mieter)',
    'rent_start': 'Datum (Abholung)',
    'rent_end': 'Datum (Rückgabe)',
    'ad__title': 'Inserat Titel',
    'ad__category__name': 'Kategorie',
    'ad__articleofvalue': 'Wiederbeschaffungswert',
    'lessor_is_business': 'Gewerblich',  # 'special' field
    'lessor_is_business_with_vat': 'Gewerblich mit MwSt',  # 'special' field
    'transaction_is_first': 'Erste Vermietung innerhalb 15 Tage',  # 'special' field
    'articlecost_sum': 'Mietobjekt (Brutto)',
    'insurance_debt_sum': 'Versicherung (Brutto)',
    'total': 'gesamt / Brutto Rechnungsbetrag',
    'articlecost': 'Mietobjekt 1 (netto)',
    'insurance_profit': 'Versicherung + Versicherungsprovision (Netto)',
    'netto_total': 'netto Rechnungsbetrag',
    'vat': '19% gesetzliche Mwst.',
    'insurance_commission': 'Versicherungs Provision',
    'netto_total_special': 'netto Rechnungsbetrag',
    'vat_special': '19% gesetzliche Mwst.',
    'subgoal': 'Zwischensumme',
    'insurance_sum': 'Versicherung (inkl. 19% Versicherungssteuer)',
    'status': 'Status',
}

ALL_TRANSACTIONS_CSV_FULL_LIST_EXPORT_FIELDS = (
    'id', 'ad__owner__first_name', 'ad__owner__last_name', 'ad__owner__profile__business_name',
    'applicant__first_name', 'applicant__last_name', 'applicant__profile__business_name', 'rent_start', 'rent_end',
    'ad__title', 'ad__category__name', 'ad__articleofvalue',
    # These keys are 'special' because they cannot be received directly from `Transactions` model fields (DB)
    'lessor_is_business', 'lessor_is_business_with_vat', 'transaction_is_first',
    
    'articlecost_sum', 'insurance_debt_sum', 'total', 'articlecost', 'insurance_profit', 'netto_total', 'vat',
    'insurance_commission', 'netto_total_special', 'vat_special', 'subgoal', 'insurance_sum', 'status',
)

ALL_TRANSACTIONS_CSV_FULL_LIST_SPECIAL_FIELDS = (
    'lessor_is_business', 'lessor_is_business_with_vat', 'transaction_is_first'
)

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
from django.views.generic.base import View

from gearo.models.advertisement import Advertisement
from gearo.views.mixins import CSVResponseMixin, StaffMemberRequiredMixin

ADVERSTISEMENT_CSV_HEADERS = {
    "id": "Nummer", "title": "Angebotsname",
    "owner__profile__business_name": "Firma Vermieter",
    "owner__first_name": "Vorname Vermieter",
    "owner__last_name": "Nachname Vermieter",
    "creation_date": "Erstellungsdatum",
    "articleofvalue": "Wiederbeschaffungswert"
}
ADVERTISEMENT_CSV_EXPORT_FIELDS = (
    "id", "title", "owner__profile__business_name", "owner__first_name", "owner__last_name", "creation_date",
    "articleofvalue"
)


@staff_member_required
def advertisements_view(request):
    ads = Advertisement.objects.all()
    return render(request, 'gearo/controllarea/ads.html', {"ads": ads})


class AdvertisementExportView(StaffMemberRequiredMixin, CSVResponseMixin, View):

    def get(self, request, *args, **kwargs):
        ads = Advertisement.objects.all()
        return self.render_to_csv_response(
            ads, fields=ADVERTISEMENT_CSV_EXPORT_FIELDS, headers=ADVERSTISEMENT_CSV_HEADERS, filename='advertisement'
        )

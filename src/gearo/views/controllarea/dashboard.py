import calendar

from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count, F, Sum
from django.utils import timezone
from django.views.generic import TemplateView
from django.views.generic.edit import FormMixin, FormView

from braces import views
from graphos.renderers import flot

from gearo.forms.dashboard import FEES, DashboardDateForm, DashboardFeeDateForm, DashboardStatusForm
from gearo.helper import genchart
from gearo.models.advertisement import Advertisement
from gearo.models.transactions import Transactions


class DashboardView(views.LoginRequiredMixin, views.StaffuserRequiredMixin, FormView):
    """Custom dashboard view to visualize data."""
    template_name = "gearo/controllarea/dashboard.html"
    form_class = DashboardDateForm

    def form_valid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_month_year(self, form):
        now = timezone.now()
        try:
            month = form.cleaned_data.get('month')
        except AttributeError:
            month = now.month

        try:
            year = form.cleaned_data.get('year')
        except AttributeError:
            year = now.year
        return {'month': month, 'year': year}

    def get_time_string(self, form):
        month = self.get_month_year(form).get('month')
        year = self.get_month_year(form).get('year')
        if month:
            time_period_string = (str(calendar.month_name[month]) + ' ' +
                                  str(year))
        else:
            time_period_string = str(year)
        return time_period_string

    def get_initial(self):
        now = timezone.now()
        return {"month": now.month, "year": now.year}

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        form = context['form']
        context['obj_name'] = self.obj_name
        context['time_period_string'] = self.get_time_string(form)
        context['active_tab'] = self.active_tab
        return context


class DashboardUsersView(DashboardView):
    """View to visualize fluctuations of the number of users over time."""
    obj_name = "Number of users"
    model = get_user_model()
    queryset = get_user_model().objects.all()
    active_tab = "users_tab"

    def get_list_of_data(self):
        list_of_dates = self.queryset.annotate(
            num=Count('id')).values_list('date_joined', 'num')
        list_of_dates = list(map(lambda v: (v[0].date(), v[1]), list_of_dates))
        return list_of_dates

    def get_context_data(self, **kwargs):
        context = super(DashboardUsersView, self).get_context_data(**kwargs)
        context['num_obj'] = get_user_model().objects.filter(
            is_active=True).count()
        form = context['form']
        chart, chart_total = genchart.generate_dashboard_line_charts(
            obj_name=self.obj_name,
            vlist=self.get_list_of_data(),
            **self.get_month_year(form)
        )
        context['chart'] = chart
        context['chart_total'] = chart_total
        return context


class DashboardAdsView(DashboardView):
    """View to visualize fluctuations of the number of ads over time."""
    obj_name = "Number of ads"
    queryset = Advertisement.objects.all()
    model = Advertisement
    active_tab = "ads_tab"

    def get_list_of_data(self):
        list_of_dates = self.queryset.annotate(
            num=Count('id')).values_list('creation_date', 'num')
        list_of_dates = list(map(lambda v: (v[0].date(), v[1]), list_of_dates))
        return list_of_dates

    def get_context_data(self, **kwargs):
        context = super(DashboardAdsView, self).get_context_data(**kwargs)
        context['num_obj'] = Advertisement.objects.filter(
            is_removed=False).count()
        form = context['form']
        chart, chart_total = genchart.generate_dashboard_line_charts(
            obj_name=self.obj_name,
            vlist=self.get_list_of_data(),
            **self.get_month_year(form)
        )
        context['chart'] = chart
        context['chart_total'] = chart_total
        return context


class DashboardGearValueView(DashboardView):
    """View to visualize fluctuations of the total gear price over time."""
    obj_name = "Gear Value"
    model = Advertisement
    queryset = Advertisement.objects.all()
    active_tab = "gear_tab"

    def get_list_of_data(self):
        list_of_dates = [[trans.creation_date, trans.articleofvalue] for trans
                         in self.queryset if trans.articleofvalue is not None]
        list_of_dates = list(map(lambda v: (v[0].date(), v[1]), list_of_dates))
        return list_of_dates

    def get_context_data(self, **kwargs):
        context = super(DashboardGearValueView, self).get_context_data(**kwargs)
        context['num_obj'] = Advertisement.objects.filter(
            is_removed=False).aggregate(price=Sum(F('articleofvalue')))['price']
        form = context['form']
        chart, chart_total = genchart.generate_dashboard_line_charts(
            obj_name=self.obj_name,
            vlist=self.get_list_of_data(),
            **self.get_month_year(form)
        )
        context['chart'] = chart
        context['chart_total'] = chart_total
        return context


class DashboardFinishedTransactionsView(DashboardView):
    """View to visualize fluctuations of the finished transactions over time."""
    obj_name = "Number of finished transactions"
    model = Transactions
    queryset = Transactions.objects.filter(status=Transactions.STATUS_FINISHED)
    active_tab = "fin_trans_tab"

    def get_list_of_data(self):
        list_of_data = self.queryset.annotate(
            num=Count('id')).values_list('finished_at', 'num')
        list_of_data = list(map(lambda v: (v[0].date(), v[1]), list_of_data))
        return list_of_data

    def get_context_data(self, **kwargs):
        context = super(
            DashboardFinishedTransactionsView, self).get_context_data(**kwargs)
        context['num_obj'] = self.queryset.count()
        form = context['form']
        chart, chart_total = genchart.generate_dashboard_line_charts(
            obj_name=self.obj_name,
            vlist=self.get_list_of_data(),
            **self.get_month_year(form)
        )
        context['chart'] = chart
        context['chart_total'] = chart_total
        return context


class DashboardIncomeView(DashboardView):
    """View to visualize the money gearo earned from rental fees over time."""
    obj_name = "Income"
    queryset = Transactions.objects.filter(status=Transactions.STATUS_FINISHED)
    active_tab = "income_tab"
    template_name = "gearo/controllarea/dashboard-fees.html"
    form_class = DashboardFeeDateForm
    success_url = reverse_lazy('staffdash-income')

    def get_data_lists(self, form):
        rent_data, rent_data_total = genchart.get_data_for_month_year(
            vlist=self.get_list_rent_fees(),
            **self.get_month_year(form)
        )
        ins_data, ins_data_total = genchart.get_data_for_month_year(
            vlist=self.get_list_ins_fees(),
            **self.get_month_year(form)
        )
        total_data = genchart.elementwise_sum_two_data_lists(
            main_list=rent_data,
            aux_list=ins_data,
        )
        total_data_total = genchart.elementwise_sum_two_data_lists(
            main_list=rent_data_total,
            aux_list=ins_data_total,
        )
        return {'rent_data': rent_data, 'rent_data_total': rent_data_total,
                'ins_data': ins_data, 'ins_data_total': ins_data_total,
                'total_data': total_data, 'total_data_total': total_data_total}

    def get_current_income(self, form):
        income = []
        rental = sum([t.rental_fee for t in self.queryset])
        insurance = sum([t.insurance_fee for t in self.queryset])
        total = rental + insurance
        try:
            fee_types = form.cleaned_data.get('fees')
        except AttributeError:
            income.append(['total', round(total, 2)])
        else:
            if FEES[0][0] in fee_types:
                income.append(['total', round(total, 2)])
            if FEES[1][0] in fee_types:
                income.append(['insurance', round(insurance, 2)])
            if FEES[2][0] in fee_types:
                income.append(['rental', round(rental, 2)])
        return income

    def get_chart_data(self, form):
        data_labels = ['date']
        list_data = []
        list_data_total = []
        data_lists = self.get_data_lists(form)
        try:
            fee_types = form.cleaned_data.get('fees')
        except AttributeError:
            list_data.append(data_lists['total_data'])
            data_labels.append('total')
            list_data_total.append(data_lists['total_data_total'])
        else:
            if FEES[0][0] in fee_types:
                list_data.append(data_lists['total_data'])
                data_labels.append('total')
                list_data_total.append(data_lists['total_data_total'])
            if FEES[1][0] in fee_types:
                list_data.append(data_lists['ins_data'])
                data_labels.append('insurance')
                list_data_total.append(data_lists['ins_data_total'])
            if FEES[2][0] in fee_types:
                list_data.append(data_lists['rent_data'])
                data_labels.append('rental')
                list_data_total.append(data_lists['rent_data_total'])

        chart_data = genchart.chart_data_from_data_lists(list_data)
        chart_data.insert(0, data_labels)

        chart_data_total = genchart.chart_data_from_data_lists(list_data_total)
        chart_data_total.insert(0, data_labels)

        return chart_data, chart_data_total

    def get_initial(self):
        now = timezone.now()
        return {'month': now.month, 'year': now.year, 'fees': ['total']}

    def get_list_ins_fees(self):
        ins_fees_list = [[trans.finished_at, trans.insurance_fee] for trans in
                         self.queryset]

        list_of_data = list(map(lambda v: (v[0].date(), v[1]), ins_fees_list))
        return list_of_data

    def get_list_rent_fees(self):
        rent_fees_list = [[trans.finished_at, trans.rental_fee] for trans in
                          self.queryset]

        list_of_data = list(map(lambda v: (v[0].date(), v[1]), rent_fees_list))
        return list_of_data

    def get_context_data(self, **kwargs):
        context = super(DashboardIncomeView, self).get_context_data(**kwargs)
        form = context['form']
        chart_data, chart_data_total = self.get_chart_data(form)
        chart, chart_total = genchart.generate_dashboard_line_charts(
            obj_name=self.obj_name,
            data=chart_data,
            data_total=chart_data_total,
            label=True,
            **self.get_month_year(form)
        )
        context['chart'] = chart
        context['chart_total'] = chart_total
        context['num_obj'] = self.get_current_income(form)
        return context


class DashboardTopAdsView(views.LoginRequiredMixin,
                          views.StaffuserRequiredMixin,
                          FormMixin,
                          TemplateView):
    """View to show top ads based on the number of transactions of a particular
    status."""
    template_name = "gearo/controllarea/dashboard-top-ads.html"
    form_class = DashboardStatusForm

    def get_status(self):
        choices = list(Transactions.STATUS_CHOICES)
        choices.insert(0, ('all', 'Alle'))

        if self.request.GET.get('status'):
            status = self.request.GET.get('status')
            if not any(status in choice for choice in choices):
                status = 'all'
        else:
            status = 'all'
        return {'status': status}

    def get_initial(self):
        return self.get_status()

    def get_ads_stats(self):
        status = self.get_status()['status']
        if status == 'all':
            ads = Advertisement.objects.annotate(
                count=Count('transactions'),
            ).order_by('-count')
        else:
            ads = Advertisement.objects.filter(
                transactions__status=self.get_status()['status']
            ).annotate(
                count=Count('transactions__status'),
            ).order_by('-count')
        return ads

    def get_context_data(self, **kwargs):
        context = super(DashboardTopAdsView, self).get_context_data(**kwargs)
        context['ads'] = self.get_ads_stats()
        context['active_tab'] = 'top_ads_tab'
        return context


class DashboardStatusesView(views.LoginRequiredMixin,
                            views.StaffuserRequiredMixin,
                            TemplateView):
    """View to illustrate distribution of transaction statuses."""
    template_name = "gearo/controllarea/dashboard-statuses.html"

    @staticmethod
    def get_chart_data():
        data = []
        stats = Transactions.objects.values(
            'status'
        ).annotate(
            count=Count('status'),
        ).order_by(
            '-count'
        )
        for stat in stats:
            data.append([stat['status'], stat['count']])

        data.insert(0, ['status', 'count'])
        return data

    def get_table_data(self):
        data = self.get_chart_data()[1:]
        total = 0
        for row in data:
            total += row[1]

        for row in data:
            row.append(round(row[1]/total*100))
        return data

    def get_context_data(self, **kwargs):
        context = super(DashboardStatusesView, self).get_context_data(**kwargs)
        data = self.get_chart_data()
        context['chart'] = genchart.generate_chart(
            chart_class=flot.PieChart,
            data=data,
            height=400,
            id='pie_chart_div'
        )
        context['data'] = self.get_table_data()
        context['active_tab'] = 'stat_tab'
        return context

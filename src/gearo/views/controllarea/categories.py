"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import redirect, render

from gearo.forms.categorie_add import CategorieAdd
from gearo.models.advertisement_categories import AdvertisementCategory


@staff_member_required
def listcat_view(request):
    cats = AdvertisementCategory.objects.all()
    return render(request, "gearo/controllarea/categorie.html", {"cats": cats})


@staff_member_required
def addcat_view(request):
    if request.method == "POST":
        form = CategorieAdd(request.POST)
        if form.is_valid():
            new_cat = form.save()
            return render(request, "gearo/controllarea/categorie.html", {"section": "add", "form": form,
                                                                         "success": True})
        else:
            return render(request, "gearo/controllarea/categorie.html", {"section": "add", "form": form})
    else:
        form = CategorieAdd()
        return render(request, "gearo/controllarea/categorie.html", {"section": "add", "form": form})
    

@staff_member_required
def editcat_view(request, cat_id):
    if request.method == "POST":
        cat = AdvertisementCategory.objects.get(id=cat_id)
        form = CategorieAdd(request.POST, instance=cat)
        if form.is_valid():
            form.save()
            return render(request, "gearo/controllarea/categorie.html", {"section": "edit", "form": form,
                                                                         "cat": cat, "success": True})
        else:
            return render(request, "gearo/controllarea/categorie.html", {"section": "edit", "form": form, "cat": cat})
    else:
        cat = AdvertisementCategory.objects.get(id=cat_id)
        form = CategorieAdd(instance=cat)
        return render(request, "gearo/controllarea/categorie.html", {"section": "edit", "form":form, "cat": cat})


@staff_member_required
def togglecat_view(request, cat_id):
    question = AdvertisementCategory.objects.get(id=cat_id)
    if question.active == 1:
        question.active = 0
    else:
        question.active = 1
    question.save()
    return redirect("supportfaq")

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
import os.path

from gearo.helper.adrandom import get_random_ads
from gearo.views.controllarea.pages import *
from gearo.views.gearouser import *

from .account import *
from .advertisements import *
from .bills import *
from .chatsystem import *
from .controllarea import *
from .faq import *
from .imageupload import *
from .reviews import *
from .search import *


# Start page is saved here, because nowhere suitable
def index_view(request):
    form = LoginForm()
    ads = get_random_ads(4)
    return render(request, 'gearo/home/index.html', {"form": form, "ads": ads, "white": True})


class SitesView(DetailView):
    context_object_name = 'site'
    slug_url_kwarg = 'site'
    model = Sites

    def get_template_names(self):
        page_template_name = getattr(self.object, 'template_name', 'site.html') or 'site.html'
        return [os.path.join('gearo/home', page_template_name)]

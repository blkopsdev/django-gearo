"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView

from gearo.models.tokens import Tokens
from gearo.models.transactions import Transactions
from gearo.models.userreviews import UserReviews

logger = logging.getLogger('gearo.views.reviews')


class UserReviewView(LoginRequiredMixin, TemplateView):
    template_name = 'gearo/review/addreview.html'
    is_already_reviewed = False
    is_token_valid = False
    rating = None
    transaction = None
    token = None

    def dispatch(self, request, *args, **kwargs):
        transaction_id = kwargs.get('inqid', None)
        token_hash = kwargs.get('token', None)
        self.transaction = get_object_or_404(Transactions, id=transaction_id)
        if UserReviews.objects.filter(token=token_hash, transaction=self.transaction).exists():
            self.is_already_reviewed = True
        else:
            try:
                self.token = Tokens.objects.get(token=token_hash)
                self.is_token_valid = self.token.checktoken(request.user, False)
            except Tokens.DoesNotExist:
                pass
        return super(UserReviewView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UserReviewView, self).get_context_data(**kwargs)
        if self.is_already_reviewed:
            context['is_already_reviewed'] = True
        if not self.is_token_valid:
            context['tokenfail'] = True
        context['rating'] = self.rating
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        comment = request.POST.get('text', '')
        token_hash = kwargs.get('token', None)
        if not self.is_already_reviewed and self.is_token_valid:
            params = {
                'transaction': self.transaction, 'rating': self.rating, 'token': token_hash, 'comment': comment
            }
            if request.user == self.transaction.applicant:
                params['user'] = self.transaction.lessor
            else:
                params['user'] = self.transaction.applicant
            new_review = UserReviews(**params)
            new_review.save()
            self.token.delete()
            context['reviewed'] = True
            logger.debug('User "%s" reviewed transaction #%s', request.user.email, self.transaction.id)
        return self.render_to_response(context)

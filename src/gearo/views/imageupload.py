"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
import logging

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files import File
from django.http.response import JsonResponse
from django.views.generic import View

from easy_thumbnails.files import generate_all_aliases
from easy_thumbnails.templatetags.thumbnail import thumbnail_url

from gearo.helper.images import crop_image
from gearo.models.attachments import Attachments
from gearo.models.rohprotocol import ProtocolAttachment

logger = logging.getLogger('gearo.views.imageupload')


class AttachmentTypeMixin(object):
    attachment_types_mapping = {
        'ad': Attachments,
        'protocol': ProtocolAttachment
    }

    def get_attachment_model(self):
        attachment_type = self.kwargs['attachment_type']
        return self.attachment_types_mapping[attachment_type]


class UploadAttachmentView(LoginRequiredMixin, AttachmentTypeMixin, View):

    def post(self, request, *args, **kwargs):
        upfile = request.FILES['attachmentupload'] if request.FILES else None
        if upfile is None:
            return JsonResponse({'success': False})

        model = self.get_attachment_model()
        new_attach = model(original_image=upfile, image=upfile, owner=request.user, status=model.STATUS_TEMPORARY)
        new_attach.save()
        generate_all_aliases(new_attach.image, include_global=True)

        file_payload = {
            'name': new_attach.original_image.name,
            'size': str(new_attach.original_image.size),
            'url': new_attach.original_image.url,
            'id': new_attach.id,
            'thumbnail_url': thumbnail_url(new_attach.image, settings.THUMBNAIL_AD_ALIAS),
            'success': True
        }
        return JsonResponse(file_payload)


class AttachmentDeleteView(LoginRequiredMixin, AttachmentTypeMixin, View):

    def post(self, request, *args, **kwargs):
        success = False
        attachment_id = request.POST.get('attachment_id', None)
        model = self.get_attachment_model()
        if attachment_id:
            try:
                attachment = model.objects.get(id=attachment_id)
                attachment.delete()
                success = True
            except model.DoesNotExist:
                pass
        return JsonResponse({'success': success})


class CropAttachmentView(LoginRequiredMixin, AttachmentTypeMixin, View):

    def post(self, request, *args, **kwargs):
        crop_x = request.POST.get('x', None)
        crop_y = request.POST.get('y', None)
        width = request.POST.get('width', None)
        height = request.POST.get('height', None)
        rotate = request.POST.get('rotate', None)
        container_width = request.POST.get('container_width', None)
        container_height = request.POST.get('container_height', None)
        model = self.get_attachment_model()
        success = False
        data = {}

        if all([crop_x, crop_y, width, height]):
            crop_x = int(crop_x)
            crop_y = int(crop_y)
            width = int(width)
            height = int(height)
            image_id = request.POST.get('image_id', None)

            try:
                attachment = model.objects.get(pk=image_id)
                try:
                    cropped_image_filename, temp_image = crop_image(
                        attachment.original_image, crop_x, crop_y, width, height, rotate
                    )
                    attachment.image.save(cropped_image_filename, File(temp_image))
                    generate_all_aliases(attachment.image, include_global=True)
                    data['thumbnail_url'] = thumbnail_url(attachment.image, settings.THUMBNAIL_AD_ALIAS)
                    success = True
                except Exception as e:
                    logger.error('Error on image cropping - %s', e)
            except model.DoesNotExist:
                pass
        return JsonResponse({'success': success, 'data': data})

import logging
from decimal import Decimal as D

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.timezone import now

from mangopay.constants import PAYMENT_STATUS_CHOICES, STATUS_CHOICES
from mangopay.resources import DirectDebitWebPayIn
from payments import get_payment_model

from gearo.helper.csv_formatter import CSVWriter
from gearo.helper.mangopay import get_mangopay_user, get_preauthorization
from gearo.models.advertisement import Advertisement
from gearo.models.attachments import Attachments
from gearo.models.transactions import Transactions

logger = logging.getLogger(__name__)
Payment = get_payment_model()


class BaseAdvertisementMixin(object):
    def get_queryset(self):
        return super(BaseAdvertisementMixin, self).get_queryset().filter(is_removed=False)


class StaffMemberRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_active and self.request.user.is_staff


class CSVResponseMixin(object):

    def render_to_csv_response(self, queryset, headers, fields, special_fields=None, filename='export'):
        writer = CSVWriter(queryset, headers, fields, special_fields)
        gearo_csv = writer.get_csv()
        gearo_csv.filename = '{}_{}.csv'.format(filename, now().strftime('%Y%m%d'))
        return gearo_csv.http_response()


class RedirectToReferrerMixin(object):

    def redirect_to_referrer(self, default_path='/'):
        url = self.request.META.get('HTTP_REFERER', default_path)
        return HttpResponseRedirect(url)


class MessageErrorMixin(RedirectToReferrerMixin):

    def message_and_redirect_to_referrer(self, message, default_path):
        messages.error(self.request, message)
        return self.redirect_to_referrer(default_path=default_path)


class AttachmentMixin(object):
    attachment_model = Attachments

    def claimattachment(self, attachment_id, owner, instance, related_field):
        attachment = None
        model = self.attachment_model
        try:
            attachment = model.objects.get(pk=attachment_id, owner=owner, status=model.STATUS_TEMPORARY)
        except model.DoesNotExist:
            pass

        if attachment:
            attachment.status = model.STATUS_ACTIVE
            setattr(attachment, related_field, instance)
            attachment.save()

    def set_attachments(self):
        attachment_ids = self.request.POST['attachments'].split(",")
        attachment_ids = map(lambda a: a.strip(' '), attachment_ids)
        attachment_ids = filter(None, attachment_ids)
        for attachment_id in attachment_ids:
            self.claimattachment(
                attachment_id,
                self.request.user,
                self.object,
                related_field=self.related_attachment_field,
            )


class PaymentMixin(object):

    def get_transaction(self, tenant, **kwargs):
        ad_id = kwargs.get('ad_id', None)
        trans_id = kwargs.get('trans_id', None)
        ad = get_object_or_404(Advertisement, id=ad_id, is_removed=False)
        transaction = get_object_or_404(Transactions, ad=ad, applicant=tenant, id=trans_id)
        return transaction

    def create_payment(self, request, payment_variant, tenant, **kwargs):
        transaction = self.get_transaction(tenant, **kwargs)
        address = tenant.gearouser.get_bill_address()

        # TODO: in future all payments will be made through Mangopay, so this check should be deleted then
        mangopay_payment_variants = ['mangopay_card', 'mangopay_sofort', 'mangopay_paypal']
        mangopay_user_id = None
        if payment_variant in mangopay_payment_variants:
            mangopay_user = get_mangopay_user(tenant)
            mangopay_user_id = mangopay_user.id

        payment = Payment.objects.create(
            variant=payment_variant,
            description='Anmietung auf Gearo, gearo Buchung Nr. %s' % transaction.id,
            total=transaction.total_incl_discount,
            tax=D('0'),
            currency='EUR',
            delivery=D('0'),
            billing_first_name=address.get("first_name"),
            billing_last_name=address.get("last_name"),
            billing_address_1=address.get("street"),
            billing_address_2='',
            billing_city=address.get("place"),
            billing_postcode=address.get("zip"),
            billing_country_code=address.get("country_code"),
            billing_country_area=address.get("country"),
            customer_ip_address=request.META.get("REMOTE_ADDR"),
            gearo_transaction=transaction,
            publicid=Payment.gen_public_token(),

            mangopay_user_id=mangopay_user_id,
        )

        logger.debug(
            'Created payment (%s) #%s for transaction #%s by user "%s"',
            payment_variant, payment.id, transaction.id, tenant.email,
        )

        return payment

    def get_payin_info(self, payment):
        WEB_PAYINS = payment.gearo_transaction.MANGOPAY_WEB_PAYINS
        payin_info = {}
        if payment.variant in WEB_PAYINS:
            payin_id = payment.web_payin_id
            payin = DirectDebitWebPayIn.get(payin_id)
            # we have `payment_status` only for PreAuthorization so for Web PayIns we set it to True
            payin_info['is_payment_status_waiting'] = True
            payin_info['payin_method'] = payment.variant
        else:
            payin_id = payment.preauthorization.preauthorization_id
            payin = get_preauthorization(payin_id)
            payin_info['is_payment_status_waiting'] = payin.payment_status == PAYMENT_STATUS_CHOICES.waiting
            payin_info['payin_method'] = 'card preauthorization'

        payin_info['payin_id'] = payin_id
        payin_info['payin'] = payin
        status = payin.status
        payin_info['is_status_succeeded'] = status == STATUS_CHOICES.succeeded
        payin_info['is_status_failed'] = status == STATUS_CHOICES.failed

        return payin_info

    def set_preauth_payment_status(self, payment):
        WEB_PAYINS = payment.gearo_transaction.MANGOPAY_WEB_PAYINS
        if payment.variant in WEB_PAYINS:
            # Web PayIns don't have PreAuthorization, so payment status should be set to `confirmed` w/o capturing
            # Only charged (means captured) payments can be then refunded
            payment.captured_amount = payment.total
            payment.change_status('confirmed')
        else:
            payment.change_status('preauth')

    def get_return_url(self, advertisement_id, transaction_id):
        return_url = reverse('mangopay_redirect', kwargs={'ad_id': advertisement_id, 'trans_id': transaction_id})
        return settings.BASE_URL + return_url


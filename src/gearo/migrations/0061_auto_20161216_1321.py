# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-12-16 12:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0060_remove_advertisementcategory_parent_cat_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notifications',
            name='transact',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to='gearo.Transactions'),
        ),
    ]

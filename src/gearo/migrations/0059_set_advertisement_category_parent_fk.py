# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-12-14 21:01
from __future__ import unicode_literals

from django.db import migrations


def set_parent_fk(apps, schema_editor):
    AdvertisementCategory = apps.get_model('gearo', 'AdvertisementCategory')
    for category in AdvertisementCategory.objects.exclude(parent_cat_id=0):
        category.parent = AdvertisementCategory.objects.get(id=category.parent_cat_id)
        category.save()


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0058_advertisementcategory_parent'),
    ]

    operations = [
        migrations.RunPython(set_parent_fk)
    ]

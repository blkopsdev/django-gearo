# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gearouser',
            name='phone_mobile',
            field=models.CharField(default=b'49', max_length=50, null=True, blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-10-27 14:00
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0044_auto_20161026_1009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userreviews',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='userreviews', to=settings.AUTH_USER_MODEL),
        ),
    ]

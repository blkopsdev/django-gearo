# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-19 11:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0024_auto_20160919_1218'),
    ]

    operations = [
        migrations.RenameField(
            model_name='voucher',
            old_name='voucher',
            new_name='code',
        ),
    ]

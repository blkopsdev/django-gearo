# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-10-25 20:39
from __future__ import unicode_literals

from django.db import migrations

PROFILE_URL_FIELD_NAMES = ['homepage', 'vimeo', 'youtube', 'instagram', 'facebook', 'twitter', 'linkedin', 'flickr']


def cleanup_profile_urls(apps, schema_editor):
    """Set template name to rent features static page."""
    GearoProfile = apps.get_model('gearo', 'GearoProfile')
    for profile in GearoProfile.objects.all():
        for fname in PROFILE_URL_FIELD_NAMES:
            fvalue = getattr(profile, fname, None)
            if fvalue == 'http://':
                setattr(profile, fname, None)
                profile.save()


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0042_set_vermiete_template_name'),
    ]

    operations = [
        migrations.RunPython(cleanup_profile_urls)
    ]

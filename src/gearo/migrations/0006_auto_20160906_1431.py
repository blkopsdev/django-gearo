# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-06 12:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0005_auto_20160906_1429'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachments',
            name='image',
            field=models.ImageField(upload_to='attachments/'),
        ),
    ]

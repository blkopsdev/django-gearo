# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-10-21 11:37
from __future__ import unicode_literals
import datetime

from django.db import migrations


def create_post_signup_offer(apps, schema_editor):
    Offer = apps.get_model('gearo', 'Offer')
    Sites = apps.get_model('gearo', 'Sites')
    rent_features_page = Sites.objects.get(slug='miete')
    Offer.objects.create(
        name='Post-signup voucher gift',
        benefit=10,
        application_limit=1,
        duration=14,
        start_date=datetime.datetime.today(),
        expired_date=datetime.date(2017, 1, 15),
        source_page=rent_features_page
    )


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0040_offer_source_page'),
    ]

    operations = [
        migrations.RunPython(create_post_signup_offer)
    ]

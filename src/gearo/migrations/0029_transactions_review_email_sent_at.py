# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-22 13:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0028_voucherapplication_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='transactions',
            name='review_email_sent_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]

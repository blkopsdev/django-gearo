# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-22 15:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0029_transactions_review_email_sent_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userreviews',
            name='comment',
            field=models.TextField(blank=True),
        ),
    ]

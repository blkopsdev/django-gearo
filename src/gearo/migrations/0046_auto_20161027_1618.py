# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-10-27 14:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0045_auto_20161027_1600'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userreviews',
            options={'ordering': ['-created_at']},
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-14 13:02
from __future__ import unicode_literals

from django.db import migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('gearo', '0016_auto_20160914_1451'),
    ]

    operations = [
        migrations.AddField(
            model_name='advertisement',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
    ]

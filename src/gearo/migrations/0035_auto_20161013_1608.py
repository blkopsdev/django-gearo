# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-10-13 14:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gearo', '0034_advertisement_pickup_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sites',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]

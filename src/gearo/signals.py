from django.dispatch import Signal

advertisement_updated = Signal(providing_args=['instance'])
advertisement_removed = Signal(providing_args=['instance'])

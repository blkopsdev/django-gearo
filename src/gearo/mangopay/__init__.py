from __future__ import unicode_literals

import logging

from django.conf import settings

import mangopay
from mangopay.api import APIRequest
from mangopay.constants import PAYMENT_STATUS_CHOICES, SECURE_MODE_CHOICES
from mangopay.resources import DirectDebitWebPayIn, Money, PayInRefund, PreAuthorizedPayIn
from payments.core import BasicProvider

from gearo.helper.mangopay import get_mangopay_user_by_id, get_mangopay_wallet, get_preauthorization, money_to_cents
from gearo_tasks.tasks.mangopay import update_mp_wallet_balance

logger = logging.getLogger(__name__)

mangopay.sandbox = settings.MANGOPAY_USE_SANDBOX
mangopay.client_id = settings.MANGOPAY_CLIENT_ID
mangopay.passphrase = settings.MANGOPAY_PASSPHRASE
handler = APIRequest(sandbox=settings.MANGOPAY_USE_SANDBOX)


class MangopayProvider(BasicProvider):

    def __init__(self, **kwargs):
        super(MangopayProvider, self).__init__(**kwargs)

    def get_payin(self, payment):
        WEB_PAYINS = payment.gearo_transaction.MANGOPAY_WEB_PAYINS
        if payment.variant in WEB_PAYINS:
            payin_id = payment.web_payin_id
            return DirectDebitWebPayIn.get(payin_id)
        else:
            preauthorized_payin_id = payment.preauthorization.preauthorized_payin_id
            return PreAuthorizedPayIn.get(preauthorized_payin_id)

    def update_wallets_balances(self, payment):
        lessor = payment.gearo_transaction.lessor
        update_mp_wallet_balance.delay(lessor.id)
        tenant = payment.gearo_transaction.applicant
        update_mp_wallet_balance.delay(tenant.id)

    def capture(self, payment, amount=None):
        amount = amount or payment.total
        amount_in_cents = money_to_cents(amount)
        fees_in_cents = money_to_cents(amount - payment.gearo_transaction.payout)
        tenant = payment.gearo_transaction.applicant
        lessor = payment.gearo_transaction.lessor
        mangopay_user = get_mangopay_user_by_id(tenant, payment.mangopay_user_id)
        preauthorization_id = payment.preauthorization.preauthorization_id

        preauthorized_payin = PreAuthorizedPayIn(
            author=mangopay_user,
            debited_funds=Money(amount=amount_in_cents, currency='EUR'),
            fees=Money(amount=fees_in_cents, currency='EUR'),
            credited_wallet=get_mangopay_wallet(lessor),
            preauthorization=get_preauthorization(preauthorization_id),
            secure_mode=SECURE_MODE_CHOICES.default,
            secure_mode_return_url=payment.get_success_url(),
        )
        preauthorized_payin.save()

        payment.preauthorization.preauthorized_payin_id = preauthorized_payin.get_pk()
        payment.preauthorization.save()

        logger.debug(
            'Successfully captured payment #%s (%s) for transaction #%s',
            payment.id, payment.variant, payment.gearo_transaction.id,
        )

        self.update_wallets_balances(payment)

        return amount

    def release(self, payment):
        preauthorization = get_preauthorization(payment.preauthorization.preauthorization_id)
        if not preauthorization.payment_status == PAYMENT_STATUS_CHOICES.canceled:
            preauthorization.payment_status = PAYMENT_STATUS_CHOICES.canceled
            preauthorization.save()

        logger.debug(
            'Successfully released payment #%s (%s) for transaction #%s',
            payment.id, payment.variant, payment.gearo_transaction.id,
        )

    def refund(self, payment, amount=None):
        tenant = payment.gearo_transaction.applicant
        mangopay_user = get_mangopay_user_by_id(tenant, payment.mangopay_user_id)
        payin = self.get_payin(payment)

        payin_refund = PayInRefund(author=mangopay_user, payin=payin)
        payin_refund.save()

        logger.debug(
            'Successfully refunded payment #%s (%s) for transaction #%s',
            payment.id, payment.variant, payment.gearo_transaction.id,
        )

        self.update_wallets_balances(payment)

        amount = amount or payment.total
        return amount

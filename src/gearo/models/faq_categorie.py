"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.db import models


class FaqCategory(models.Model):
    TENANT_CATEGORY = 1
    LESSOR_CATEGORY = 2

    PARENTS = (
        (LESSOR_CATEGORY, "Mieter"),
        (TENANT_CATEGORY, "Vermieter")
    )
    
    id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=100)
    parent = models.IntegerField(null=True, choices=PARENTS)
    create_date = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.category

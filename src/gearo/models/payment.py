from decimal import Decimal

from django.core.urlresolvers import reverse
from django.db import models

from payments import PurchasedItem
from payments.models import BasePayment

from gearo.helper.gentoken import gentoken
from gearo.models.transactions import Transactions


class Payment(BasePayment):
    gearo_transaction = models.ForeignKey(Transactions, related_name='payments')
    publicid = models.CharField(max_length=12, default=0)
    mangopay_user_id = models.CharField(null=True, blank=True, max_length=255)

    # If payment will be made as Web PayIn we will keep PayIn id in this field
    web_payin_id = models.CharField(null=True, blank=True, max_length=255)

    @staticmethod
    def gen_public_token():
        tok = gentoken(12)
        while Payment.objects.filter(publicid=tok).exists():
            tok = gentoken(12)
        return tok
    
    def get_failure_url(self):
        return reverse(
            "bookingfailure", kwargs={"ad_id": int(self.gearo_transaction.ad.id), "trans_id": self.publicid})

    def get_success_url(self):
        return reverse(
            "bookingsuccess", kwargs={"ad_id": int(self.gearo_transaction.ad.id), "trans_id": self.publicid})

    def get_purchased_items(self):
        yield PurchasedItem(
            name=self.gearo_transaction.ad.title, sku='BSKV', quantity=1, price=Decimal(self.total), currency='EUR')

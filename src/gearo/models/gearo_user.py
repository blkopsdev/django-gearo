import math

from django.contrib.auth.models import User
from django.db import models

from django_countries import Countries
from django_countries.fields import CountryField

from ._custom_fields import GearoPhoneNumberField


class GearoCountries(Countries):
    only = [
        ('DE', 'Deutschland'),
        ('AT', 'Österreich')
    ]


class GearoUser(models.Model):
    STATUS_NONE = 'none'
    STATUS_RESENT = 're_sent'
    STATUS_WAITING = 'waiting'
    STATUS_FAILED = 'wrong'
    STATUS_SUCCESS = 'success'

    PHONE_STATUS = (
        (STATUS_NONE, "nicht verifiziert"),
        (STATUS_WAITING, "warte auf code "),
        (STATUS_RESENT, "erneut gesendet"),
        (STATUS_FAILED, "falscher code"),
        (STATUS_SUCCESS, "verifiziert"),
    )

    BUSINESSES = (
        ('ag', 'AG'),
        ('gmbh', 'GmbH'),
        ('ggmbh', 'gGmbH'),
        ('kgaa', 'KgaA'),
        ('ug', 'UG'),
        ('gmbh_kg', 'GmbH & Co. KG.'),
        ('kg', 'KG'),
    )
    SOLETRADERS = (
        ('gbr', 'Gbr'),
        ('ohg', 'OHG'),
        ('e.k.', 'e.K.'),
    )
    # Next business types are "Soletraiders", but they need only "Identity Proof" for "Regular validation"
    SOLETRAIDERS_SPECIAL = (
        ('freiberufler', 'Freiberufler'),
        ('gewerbeschein', 'Gewerbeschein'),
    )
    BUSINESS_TYPE = BUSINESSES + SOLETRADERS + SOLETRAIDERS_SPECIAL

    GENDER_CHOICES = (
        ('female', 'weiblich'),
        ('male', 'männlich'),
    )

    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_verified = models.BooleanField(default=0)
    phone_verified = models.BooleanField(default=0)
    phone_verified_status = models.CharField(max_length=10, choices=PHONE_STATUS, default="none")
    google_verified = models.BooleanField(default=0)
    facebook_verified = models.BooleanField(default=0)
    linkedin_verified = models.BooleanField(default=0)
    phone = GearoPhoneNumberField(blank=True, null=True)
    phone_mobile = GearoPhoneNumberField(blank=True, null=True)
    street = models.CharField(max_length=120, null=True, blank=True)
    zip = models.CharField(max_length=120, null=True, blank=True)
    place = models.CharField(max_length=120, null=True, blank=True)
    country = CountryField(blank_label='Land wählen', countries=GearoCountries, null=True, blank=True)

    onoffswitch_bill = models.BooleanField(default=False)
    business_name_bill = models.CharField(max_length=100, null=True, blank=True)
    first_name_bill = models.CharField(max_length=120, null=True, blank=True)
    last_name_bill = models.CharField(max_length=120, null=True, blank=True)
    street_bill = models.CharField(max_length=120, null=True, blank=True)
    city_bill = models.CharField(max_length=120, null=True, blank=True)
    zip_bill = models.CharField(max_length=30, null=True, blank=True)
    place_bill = models.CharField(max_length=120, null=True, blank=True)
    country_bill = CountryField(blank_label='Land wählen', countries=GearoCountries, null=True, blank=True)
    trader = models.BooleanField(default=False)
    calc_mwst = models.BooleanField(default=False)
    onoffswitch_trader = models.BooleanField(default=False)
    business_type = models.CharField(choices=BUSINESS_TYPE, max_length=120, null=True, blank=True)
    tax_number = models.CharField(max_length=120, null=True, blank=True)
    tax_ident_number = models.CharField(max_length=120, null=True, blank=True)

    bill_count = models.IntegerField(default=0)
    user_review = models.FloatField(default=0.0)
    user_ranking = models.IntegerField(default=0)
    responsetime = models.FloatField(blank=True, null=True)
    notification_token = models.CharField(max_length=14, default="fail")

    is_terms_accepted = models.BooleanField(default=False)

    gender = models.CharField(choices=GENDER_CHOICES, max_length=6, null=True, blank=True)

    is_corporate_account = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    @property
    def is_completed(self):
        is_everything_completed = all([
            self.is_personal_info_completed,
            self.is_contact_info_completed,
            self.is_address_completed,
            self.user.profile.is_profile_completed,
            self.user.email,
            self.user.is_active,
        ])
        if self.is_business:
            # we need this check just for users that was created before we added `business_type` field
            return is_everything_completed and self.business_type is not None
        return is_everything_completed

    @property
    def is_personal_info_completed(self):
        return all([self.user.first_name, self.user.last_name, self.user.billing_info.birthday, self.gender])

    @property
    def is_contact_info_completed(self):
        return self.phone_mobile is not None and self.phone_verified

    @property
    def is_address_completed(self):
        return all([self.street, self.zip, self.place, self.country])

    def get_review(self):
        if self.user_review <= 0.0:
            return "unbewertet"
        else:
            return "%.0f %% positiv" % self.user_review

    def get_responsetime(self):
        if not self.responsetime or self.responsetime <= 0.0:
            return ""
        elif self.responsetime > 24:
            return "<24 Std. Responsetime"
        else:
            return "%.2f Std. Responsetime" % self.responsetime

    @property
    def fivepoint_review(self):
        return self.user_review / 20

    def render_review(self):
        stars = []
        fullstarts = self.fivepoint_review
        for star in range(int(math.floor(fullstarts))):
            stars.append("full")
        halfstar = self.user_review % 20
        if halfstar >= 10:
            stars.append("half")

        while len(stars) < 5:
            stars.append("empty")
        return stars

    def get_bill_address(self):
        self.address = {}
        if self.onoffswitch_bill:
            self.address["business_name"] = self.business_name_bill
            self.address["first_name"] = self.first_name_bill
            self.address["last_name"] = self.last_name_bill
            self.address["street"] = self.street_bill
            self.address["zip"] = self.zip_bill
            self.address["place"] = self.place_bill
            self.address["country"] = self.country_bill
        else:
            self.address["business_name"] = getattr(self.user.profile, 'business_name', '')
            self.address["first_name"] = self.user.first_name
            self.address["last_name"] = self.user.last_name
            self.address["street"] = self.street
            self.address["zip"] = self.zip
            self.address["place"] = self.place
            self.address["country"] = self.country
        self.address["country_code"] = self.country_code
        return self.address

    @property
    def is_business(self):
        return self.onoffswitch_trader

    @property
    def is_business_with_vat(self):
        if self.is_business and self.calc_mwst:
            return True
        return False

    @property
    def is_business_without_vat(self):
        if self.is_business and not self.calc_mwst:
            return True
        return False

    def get_business_type_name(self):
        if self.is_business:
            if self.business_type in [business[0] for business in self.BUSINESSES]:
                return 'business'
            elif self.business_type in [soletrader[0] for soletrader in self.SOLETRADERS]:
                return 'soletraider'
            else:
                return 'soletraider_special'

    @property
    def is_private(self):
        return not self.is_business

    @property
    def has_business_address(self):
        return self.onoffswitch_bill

    @property
    def country_code(self):
        if self.has_business_address and self.country_bill:
            return self.country_bill.code
        if self.country:
            return self.country.code

        # if all country fields are empty (not filled)
        return 'DE'

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.db import models

from easy_thumbnails.templatetags.thumbnail import thumbnail_url


class GearoProfile(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    avatar = models.ImageField(upload_to="profilebilder/", null=True, blank=True)
    business_name = models.CharField(max_length=100, null=True, blank=True)
    self_description = models.CharField(max_length=200, null=True, blank=True)
    
    homepage = models.URLField(max_length=255, null=True, blank=True)
    vimeo = models.URLField(max_length=255, null=True, blank=True)
    youtube = models.URLField(max_length=255, null=True, blank=True)
    instagram = models.URLField(max_length=255, null=True, blank=True)
    facebook = models.URLField(max_length=255, null=True, blank=True)
    twitter = models.URLField(max_length=255, null=True, blank=True)
    linkedin = models.URLField(max_length=255, null=True, blank=True)
    flickr = models.URLField(max_length=255, null=True, blank=True)

    @property
    def avatar_thumbnail_url(self):
        if self.avatar:
            return thumbnail_url(self.avatar, settings.THUMBNAIL_AVATAR_ALIAS)
        return settings.AVATAR_PLACEHOLDER

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse_lazy('publicprofil', args=(self.user.username,))

    @property
    def is_profile_completed(self):
        return all([self.avatar, self.self_description])

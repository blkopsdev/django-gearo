"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.text import slugify

from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from taggit.managers import TaggableManager

from gearo.helper.alldays import alldays
from gearo.helper.calc_insurance import calc_insurance
from gearo.models.advertisement_categories import AdvertisementCategory


class Advertisement(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(AdvertisementCategory, on_delete=models.CASCADE)
    title = models.CharField(max_length=60)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=255, allow_unicode=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    description = models.TextField()
    priceday = models.FloatField(blank=True)
    pricethree = models.FloatField(blank=True, null=True)
    priceweek = models.FloatField(blank=True, null=True)
    articleofvalue = models.FloatField(blank=True, null=True)
    notAvailable = models.TextField(blank=True)
    hired = models.TextField(blank=True)
    lat = models.FloatField()
    lng = models.FloatField()
    ranking = models.FloatField(default=0)
    ad_points = models.IntegerField(default=50)
    time_mo_start = models.CharField(max_length=10, blank=True, null=True)
    time_mo_end = models.CharField(max_length=10, blank=True, null=True)
    time_di_start = models.CharField(max_length=10, blank=True, null=True)
    time_di_end = models.CharField(max_length=10, blank=True, null=True)
    time_mi_start = models.CharField(max_length=10, blank=True, null=True)
    time_mi_end = models.CharField(max_length=10, blank=True, null=True)
    time_do_start = models.CharField(max_length=10, blank=True, null=True)
    time_do_end = models.CharField(max_length=10, blank=True, null=True)
    time_fr_start = models.CharField(max_length=10, blank=True, null=True)
    time_fr_end = models.CharField(max_length=10, blank=True, null=True)
    time_sa_start = models.CharField(max_length=10, blank=True, null=True)
    time_sa_end = models.CharField(max_length=10, blank=True, null=True)
    time_so_start = models.CharField(max_length=10, blank=True, null=True)
    time_so_end = models.CharField(max_length=10, blank=True, null=True)
    is_removed = models.BooleanField(default=False)
    pickup_location = models.TextField(blank=True, null=True)
    pickup_city = models.CharField(max_length=255, blank=True, null=True)
    amazon_ref_link = models.URLField(blank=True, null=True)
    serial_number = models.TextField(max_length=255, blank=True, null=True)

    tags = TaggableManager()

    @property
    def get_location(self):        
        return Point(self.lng, self.lat)
    
    def get_insurance(self):
        price = calc_insurance(duration=1, ad=self).get("insurance_debt_sum")
        return float("%.2f" % price)

    def get_onedayprice(self):
        price = self.get_insurance() + self.priceday
        return float("%.2f" % price)

    def get_owner_link(self):
        return reverse("publicprofil", args=[self.owner.username])

    @property
    def has_schedule(self):
        return any([self.time_mo_start, self.time_mo_end, self.time_di_start, self.time_di_end,
                    self.time_mi_start, self.time_mi_end, self.time_do_start, self.time_do_end,
                    self.time_fr_start, self.time_fr_end, self.time_sa_start, self.time_sa_end,
                    self.time_so_start, self.time_so_end])

    def check_availability(self, dates):
        # convert single date into list
        if type(dates) is not list:
            semi = list()
            semi.append(dates)
            dates = semi
            
        for date in dates:            
            if date in self.not_available_dates or date in self.hired_dates:
                return False
        return True

    @property
    def hired_dates(self):
        from gearo.models import Transactions
        transactions = self.transactions.filter(status__in=Transactions.ACTIVE_STATUSES)\
                                        .values_list('rent_start', 'rent_end')
        dates = []
        for rent_start, rent_end in transactions:
            dates += alldays(rent_start, rent_end)
        return dates

    @property
    def not_available_dates(self):
        dates = []
        for start, end in self.unavailable_periods.values_list('start', 'end'):
            dates += alldays(start, end)
        return dates

    @property
    def all_not_available_dates(self):
        return list(set(self.hired_dates + self.not_available_dates))

    @property
    def thumbnail_url(self):
        if self.attachments.count() > 0:
            return thumbnail_url(self.attachments.first().image, 'ad_s')
        return settings.ATTACHMENT_PLACEHOLDER

    @property
    def attachment_ids(self):
        return self.attachments.values_list('id', flat=True)

    @property
    def available_attachments_range(self):
        return range(settings.ADVERTISEMENT_MAX_ATTACHMENTS - self.attachments.count())

    def calculate_rent_duration(self, date_range, pickup_after_3pm=None, return_before_12pm=None):
        # Use daily count for Ad of daily billed categories.
        # For other categories, reduce duration because pick up and return day not calculated.
        payday = date_range.days
        if self.category.is_billed_daily:
            # Increment duration, because date of rent start also counts
            # for Ad with daily billing.
            payday += 1
        else:
            if date_range.days <= 1 and not all([pickup_after_3pm, return_before_12pm]):
                payday = 1
            elif all([pickup_after_3pm, return_before_12pm]):
                payday += 1
            elif not any([pickup_after_3pm, return_before_12pm]):
                payday -= 1
        return payday

    def get_absolute_url(self):
        if self.slug:
            return reverse('addetail', args=(self.slug,))
        return

    @property
    def three_days_benefit(self):
        return 100 - (self.pricethree / ((self.priceday * 3) / 100))

    @property
    def weekly_benefit(self):
        return 100 - (self.priceweek / ((self.priceday * 7) / 100))

    def get_slug_base(self, append_id=False):
        slug_base = [self.pickup_city, self.title]
        if append_id:
            slug_base.append(self.id)
        slug_base.append('mieten')
        return slug_base

    def set_slug(self):
        slug = slugify(self.get_slug_base(), allow_unicode=True)
        if Advertisement.objects.filter(slug=slug).exclude(id=self.id).count() == 0:
            self.slug = slug
        else:
            self.slug = slugify(self.get_slug_base(append_id=True), allow_unicode=True)

    @property
    def include_mwst(self):
        return self.owner.gearouser.calc_mwst

    def __str__(self):
        return "Ad %s - %i" % (self.title, self.id)


class UnavailabilityPeriod(models.Model):
    start = models.DateField()
    end = models.DateField()
    advertisement = models.ForeignKey(Advertisement, related_name='unavailable_periods')

    def __str__(self):
        return 'Ad #%s not available on %s - %s' % (self.advertisement.id, self.start, self.end)

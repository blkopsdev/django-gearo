"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.auth.models import User
from django.db import models

from gearo.models.advertisement import Advertisement


class AdvertisementQuestions(models.Model):
    id = models.AutoField(primary_key=True)
    ad = models.ForeignKey(Advertisement)
    questioner = models.ForeignKey(User)
    question = models.TextField()
    answer = models.TextField(max_length=200, blank=True, null=True)
    asked_date = models.DateTimeField(auto_now_add=True)
    answered_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return 'Question #%s for Ad #%s' % (self.id, self.ad_id)

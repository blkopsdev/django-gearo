"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.auth.models import User
from django.db import models

from gearo.models.transactions import Transactions

TYPE_CHOICES = (
    ('inquire', "Du hast eine Anfrage erhalten."),
    ('message', "Du hast eine Nachricht erhalten."),
)


class Notifications(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User)
    transaction = models.ForeignKey(Transactions, related_name='notifications')
    type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Notification #%i from user %s for transaction %s" % (self.pk,  self.user, self.transaction)

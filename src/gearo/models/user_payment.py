from django.contrib.auth.models import User
from django.db import models

from django_countries.fields import CountryField
from mangopay.constants import DOCUMENTS_STATUS_CHOICES, DOCUMENTS_TYPE_CHOICES

from gearo.helper.mangopay import GEARO_STATUS_CHOICES
from gearo.models.payment import Payment
from gearo.models.transactions import Transactions

VALIDATION_ASKED = DOCUMENTS_STATUS_CHOICES.validation_asked
VALIDATED = DOCUMENTS_STATUS_CHOICES.validated
ACTIVE_STATUSES = [VALIDATION_ASKED, VALIDATED]
BUSINESS_TYPES = ['business', 'soletraider']


class UserPayment(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='billing_info')

    # TODO: after Mangopay integration remove `paypalswitch` & `transferswitch`, keep `paypal` with comment:
    # This field outdated and unused now (because we use PayPal through Mangopay, and not directly)
    # but we keep it keep for reference
    paypal = models.EmailField(null=True, blank=True)
    paypalswitch = models.BooleanField(default=False)
    transferswitch = models.BooleanField(default=False)

    first_name = models.CharField(null=True, blank=True, max_length=50)
    last_name = models.CharField(null=True, blank=True, max_length=50)
    # IBAN - International Bank Account Number (ISO 13616)
    iban = models.CharField(null=True, blank=True, max_length=34)
    # BIC - Business Identifier Codes (also known as SWIFT-BIC, BIC code, SWIFT ID or SWIFT code) (ISO 9362)
    bic = models.CharField(null=True, blank=True, max_length=11)

    # Fields needed for Mangopay
    birthday = models.DateField(null=True, blank=True)
    nationality = CountryField(blank_label='Staatsangehörigkeit', null=True, blank=True)
    country_of_residence = CountryField(blank_label='Wohnsitz', null=True, blank=True)

    id_document_number = models.CharField(null=True, blank=True, max_length=30)

    # Mangopay NaturalUser id and wallet id
    mangopay_n_user_id = models.CharField(null=True, blank=True, max_length=255)
    mangopay_n_user_wallet_id = models.CharField(null=True, blank=True, max_length=255)
    mangopay_n_user_bank_account_id = models.CharField(null=True, blank=True, max_length=255)
    # Mangopay LegalUser id and wallet id
    mangopay_l_user_id = models.CharField(null=True, blank=True, max_length=255)
    mangopay_l_user_wallet_id = models.CharField(null=True, blank=True, max_length=255)
    mangopay_l_user_bank_account_id = models.CharField(null=True, blank=True, max_length=255)
    # If we will receive some error during Mangopay BankAccount creation, we will keep it here
    mangopay_bank_account_error = models.TextField(null=True, blank=True)

    # Field to keep current wallet balance amount (w/o currency, because everywhere on site we use "EUR")
    mangopay_current_wallet_balance = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)

    identity_proof_status = models.CharField(
        choices=DOCUMENTS_STATUS_CHOICES, default=DOCUMENTS_STATUS_CHOICES.created, max_length=50)

    registration_proof_status = models.CharField(
        choices=DOCUMENTS_STATUS_CHOICES, default=DOCUMENTS_STATUS_CHOICES.created, max_length=50)

    articles_of_association_status = models.CharField(
        choices=DOCUMENTS_STATUS_CHOICES, default=DOCUMENTS_STATUS_CHOICES.created, max_length=50)

    shareholder_declaration_status = models.CharField(
        choices=DOCUMENTS_STATUS_CHOICES, default=DOCUMENTS_STATUS_CHOICES.created, max_length=50)

    is_payment_limit_reached = models.BooleanField(default=False)
    payin_limit_reached_email_sent_at = models.DateTimeField(blank=True, null=True)
    payout_limit_reached_email_sent_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.user.username

    @property
    def is_completed(self):
        return self.is_identification_info_completed and self.is_bank_info_completed

    @property
    def is_identification_info_completed(self):
        if self.is_payment_limit_reached and self.user.gearouser.is_business:
            is_registration_proof_active = self.registration_proof_status in ACTIVE_STATUSES
            is_articles_of_association_active = self.articles_of_association_status in ACTIVE_STATUSES
            is_shareholder_declaration_active = self.shareholder_declaration_status in ACTIVE_STATUSES
            is_additional_documents_active = all([is_articles_of_association_active, is_shareholder_declaration_active])

            business_type = self.user.gearouser.get_business_type_name()
            if business_type in BUSINESS_TYPES and not is_registration_proof_active:
                return False
            if business_type == BUSINESS_TYPES[0] and not is_additional_documents_active:
                return False

        is_identity_proof_active = self.identity_proof_status in ACTIVE_STATUSES
        return all([
            self.nationality, self.country_of_residence, self.id_document_number, is_identity_proof_active])

    @property
    def is_bank_info_completed(self):
        return all([self.first_name, self.last_name, self.iban, self.bic, self.is_mangopay_bank_account_id_existing])

    @property
    def is_mangopay_bank_account_id_existing(self):
        if self.user.gearouser.is_business:
            return self.mangopay_l_user_bank_account_id
        return self.mangopay_n_user_bank_account_id

    @property
    def is_mangopay_user_id_existing(self):
        if self.user.gearouser.is_business:
            return self.mangopay_l_user_id
        return self.mangopay_n_user_id

    @property
    def is_mangopay_user_wallet_id_existing(self):
        if self.user.gearouser.is_business:
            return self.mangopay_l_user_wallet_id
        return self.mangopay_n_user_wallet_id


class MangopayPreAuthorization(models.Model):
    """
    This model keeps information related to PreAuthorized Card payment:
    `preauthorization_id` for payment releasing (cancel `PreAuthorization`) or capturing (`PreAuthorizedPayIn`)
    `preauthorized_payin_id` for payment refunding (`PayInRefund`)

    """
    payment = models.OneToOneField(Payment, related_name='preauthorization')
    preauthorization_id = models.CharField(null=True, blank=True, max_length=255)
    preauthorized_payin_id = models.CharField(null=True, blank=True, max_length=255)


class GearoUserDocument(models.Model):
    """
    This model keeps information related to required KYC documents, that needed for Mangopay Regular Authentification.
    KYC means "Know Your Customer" and are a set of legal obligations related to Mangopay license as an electronic
    money issuer and are necessary in order to fight fraud, money laundering and financing of terrorism.

    Levels of validation (Light, Regular and Strong), types of documents etc.: https://docs.mangopay.com/guide/kyc

    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='documents')
    document_type = models.CharField(choices=DOCUMENTS_TYPE_CHOICES, max_length=50)
    document_id = models.CharField(null=True, blank=True, max_length=255)
    document_status = models.CharField(choices=DOCUMENTS_STATUS_CHOICES, max_length=50)
    refused_reason_type = models.CharField(null=True, blank=True, max_length=255)
    refused_reason_message = models.CharField(null=True, blank=True, max_length=255)

    def __str__(self):
        return '{} | {} | {} | {}'.format(self.user.email, self.document_id, self.document_type, self.document_status)

    @property
    def is_validation_asked(self):
        return self.document_status == DOCUMENTS_STATUS_CHOICES.validation_asked

    @property
    def is_validated(self):
        return self.document_status == DOCUMENTS_STATUS_CHOICES.validated

    @property
    def is_refused(self):
        return self.document_status == DOCUMENTS_STATUS_CHOICES.refused

    @property
    def is_active(self):
        return self.is_validation_asked or self.is_validated


class GearoPayOut(models.Model):

    transaction = models.ForeignKey(Transactions, on_delete=models.CASCADE, related_name='payouts')
    payout_id = models.CharField(null=True, blank=True, max_length=255)
    payout_status = models.CharField(
        choices=GEARO_STATUS_CHOICES, default=GEARO_STATUS_CHOICES.created, max_length=15
    )
    result_code = models.CharField(null=True, blank=True, max_length=255)
    result_message = models.CharField(null=True, blank=True, max_length=255)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return 'PayOut (Mangopay id - {}) for transaction {}'.format(self.payout_id, self.transaction.id)

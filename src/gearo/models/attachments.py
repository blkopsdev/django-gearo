"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
import os
import uuid

from django.db import models

from gearo.models.mixins import AttachmentMixin


def get_upload_path(instance, filename):
    ext = os.path.splitext(filename)[1]
    filename = "%s%s" % (uuid.uuid4(), ext)
    return os.path.join('attachments/', filename)


class Attachments(AttachmentMixin):
    ad = models.ForeignKey('gearo.Advertisement', blank=True, null=True, related_name='attachments')

    def __str__(self):
        return 'Attachment #%s for Ad #%s' % (self.id, self.ad_id)

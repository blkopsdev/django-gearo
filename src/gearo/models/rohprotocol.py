"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.db import models
from django.utils.safestring import mark_safe

from gearo.models.mixins import AttachmentMixin


class Rohprotocol(models.Model):
    ID_PERSONAL = 'personal'
    ID_DRIVER = 'driver'
    ID_TRAVEL = 'travel'

    ID_CHOICES = (
        (ID_PERSONAL, "Personalausweis"),
        (ID_DRIVER, mark_safe("F&uuml;hrerschein")),
        (ID_TRAVEL, "Reisepass"),
    )

    PROTOCOL_TYPES = (
        ('overhanding', mark_safe('&Uuml;bergabeprotokoll')),
        ('return', mark_safe('R&uuml;ckgabeprotokoll')),
    )

    entry = models.AutoField(primary_key=True)
    identity_card = models.CharField(max_length=10, choices=ID_CHOICES, blank=True, null=True)
    identity_number = models.CharField(max_length=80, blank=True, null=True)
    protocol_type = models.CharField(max_length=12, choices=PROTOCOL_TYPES)
    serial_number = models.TextField(max_length=255, blank=True, null=True)
    defects = models.TextField(blank=True, null=True)
    other_defects = models.TextField(blank=True, null=True)
    pickup_time = models.TimeField(blank=True, null=True)
    return_time = models.TimeField(blank=True, null=True)
    questions = models.TextField(blank=True, null=True)
    marked_defects = models.TextField(blank=True, null=True)
    undamaged = models.NullBooleanField(blank=True, null=True)
    lessor_signature = models.TextField()
    applicant_signature = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Protocol #%s' % self.pk


class ProtocolAttachment(AttachmentMixin):
    protocol = models.ForeignKey('gearo.Rohprotocol', blank=True, null=True, related_name='protocol_attachments')

    def __str__(self):
        return 'Attachment #%s for Protocol #%s' % (self.id, self.protocol_id)

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.auth.models import User
from django.db import models


class Chatrooms(models.Model):
    entry = models.AutoField(primary_key=True)
    slug = models.SlugField()
    members = models.ManyToManyField(User, through='ChatroomMember', related_name='member_chatrooms')
    created_at = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    
    def __str__(self):
        return "Room %i" % self.entry
    
    def close(self):
        self.active = False
        self.save()


class ChatroomMember(models.Model):
    chatroom = models.ForeignKey(Chatrooms)
    user = models.ForeignKey(User, related_name='chatroom_members')
    token = models.CharField(max_length=14)

    def __str__(self):
        return self.user.email

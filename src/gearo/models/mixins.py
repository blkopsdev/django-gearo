import os
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from easy_thumbnails.fields import ThumbnailerImageField


def get_upload_path(instance, filename):
    ext = os.path.splitext(filename)[1]
    filename = "%s%s" % (uuid.uuid4(), ext)
    return os.path.join('attachments/', filename)


class AttachmentMixin(models.Model):
    STATUS_TEMPORARY = 1
    STATUS_ACTIVE = 2
    STATUS_INACTIVE = 3

    STATUSES = (
        (STATUS_ACTIVE, 'Temporary'),
        (STATUS_ACTIVE, 'Active'),
        (STATUS_INACTIVE, 'Inactive')
    )

    original_image = models.ImageField(upload_to=get_upload_path)
    image = ThumbnailerImageField(upload_to='attachments/', blank=True, null=True)
    status = models.IntegerField(choices=STATUSES, default=STATUS_TEMPORARY)
    updatedate = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User)

    class Meta:
        abstract = True


class IsActiveMixin(models.Model):
    start_date = models.DateTimeField(blank=True, null=True)
    expired_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True

    @property
    def is_active(self):
        if all([self.start_date, self.expired_date]):
            return self.start_date < timezone.now() < self.expired_date
        return True

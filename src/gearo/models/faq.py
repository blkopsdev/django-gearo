"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.db import models

from gearo.models.faq_categorie import FaqCategory


class Faq(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(FaqCategory, on_delete=models.CASCADE, related_name='faqs')
    question = models.CharField(max_length=200)
    answer = models.TextField()
    create_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=1)

    def __str__(self):
        return 'FAQ #%s' % self.id

from django.core.exceptions import ValidationError

from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.phonenumber import to_python


class GearoPhoneNumberField(PhoneNumberField):

    default_error_messages = {
        'invalid': 'Bitte füge eine gültige Handynummer ein.',
        'required': 'Bitte gib deine Nummer an.',
    }

    def to_python(self, value):
        phone_number = to_python(value)
        if phone_number and not phone_number.is_valid():
            raise ValidationError(self.error_messages['invalid'])
        # Because of known issue with django-phonenumber-field
        # https://github.com/stefanfoulis/django-phonenumber-field/issues/136
        # we need next two lines to show '' when model field is empty (None)
        if phone_number == '+NoneNone':
            phone_number = ''
        return phone_number

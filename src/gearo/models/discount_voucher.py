"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from datetime import timedelta

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from gearo.helper.gentoken import gentoken
from gearo.models.mixins import IsActiveMixin


class Offer(IsActiveMixin, models.Model):
    name = models.CharField(max_length=255)
    benefit = models.DecimalField(max_digits=12, decimal_places=2)
    application_limit = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    duration = models.IntegerField()  # voucher lifetime in days
    source_page = models.ForeignKey('gearo.Sites', blank=True, null=True)

    def __str__(self):
        return self.name

    def generate_voucher(self, owner=None):
        values = {
            'value': self.benefit,
            'application_limit': self.application_limit,
            'start_date': timezone.now(),
            'expired_date': timezone.now() + timedelta(days=self.duration),
            'owner': owner,
            'code': gentoken(16, unique=True, model="Voucher", field='code'),
            'offer': self
        }
        return Voucher.objects.create(**values)


class Voucher(IsActiveMixin, models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=20, unique=True)
    value = models.DecimalField(max_digits=12, decimal_places=2)
    owner = models.ForeignKey(User, blank=True, null=True)
    application_limit = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    offer = models.ForeignKey(Offer, null=True, blank=True, related_name='vouchers')

    def __str__(self):
        return self.code

    def validate_voucher(self, user=None):
        if self.owner and self.owner != user:
                return False
        if self.application_limit != 0:
            if self.application_limit - self.num_applications < 1:
                return False
        if self.applications.filter(voucher=self, user=user, status=VoucherApplication.STATUS_USED).count() == 1:
            return False
        return True

    @property
    def num_applications(self):
        return self.applications.filter(status=VoucherApplication.STATUS_USED).count()


class VoucherApplication(models.Model):
    STATUS_APPLIED = 'applied'
    STATUS_USED = 'used'

    STATUSES = (
        ('applied', 'Angewandt'),
        ('used', 'Benutzt')
    )

    voucher = models.ForeignKey(Voucher, related_name='applications')
    user = models.ForeignKey(User, related_name='voucher_applications')
    advertisement = models.ForeignKey('gearo.Advertisement', related_name='voucher_applications')
    transaction = models.ForeignKey('gearo.Transactions', related_name='voucher_applications')
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50, choices=STATUSES, default=STATUS_APPLIED)

    def __str__(self):
        return self.voucher.code

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.auth.models import User
from django.db import models

from gearo.models.transactions import Transactions


class UserReviews(models.Model):
    RATING_TYPES = (
        ("great", "Super!"),
        ("alright", "In Ordnung"),
        ("nogo", "Geht garnicht!")
    )

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='userreviews')
    transaction = models.ForeignKey(Transactions)
    rating = models.CharField(max_length=10, choices=RATING_TYPES)
    token = models.CharField(max_length=120)
    comment = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    @property
    def reviewer(self):
        if self.user == self.transaction.applicant:  
            return self.transaction.ad.owner
        else:
            return self.transaction.applicant

    def __str__(self):
        return 'Review #%s' % self.id

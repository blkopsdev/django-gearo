"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.db import models

from autoslug import AutoSlugField


class Sites(models.Model):
    slug = AutoSlugField(populate_from='title', unique=True)
    title = models.CharField(max_length=120)
    content = models.TextField()
    template_name = models.CharField(max_length=255, editable=False, null=True, blank=True)

    def __str__(self):
        return self.title

"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

KINDS = (
   (1, "mail"),
   (2, "password"),
   (3, "review"),
   (4, "smsver"),
   (5, "facebook authorization")
)


class Tokens(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User)
    token = models.CharField(max_length=120)
    creation_date = models.DateTimeField(auto_now_add=True)
    expire_date = models.DateTimeField(blank=True, null=True)
    kind = models.IntegerField(choices=KINDS)

    def __str__(self):
        return 'Token #%s' % self.pk

    def checktoken(self, usertotest, testuser=True):
        if testuser:
            if usertotest == self.user:
                if (self.expire_date - timezone.now()).days >= 0:
                    return True
                else:
                    return False
            else:
                return False
        else:
            if (self.expire_date - timezone.now()).days >= 0:
                return True
            else:
                return False

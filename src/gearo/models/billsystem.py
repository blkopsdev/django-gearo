"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from tempfile import NamedTemporaryFile

from django import template
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files import File
from django.db import models
from django.template.loader import render_to_string

from weasyprint import CSS, HTML
from weasyprint.fonts import FontConfiguration

from gearo.models.transactions import Transactions

BILL_TYPES = (
    ("invoice", "rechnung"),
    ("payout", "auszahlung"),
    ("credit", "auszahlung-gearo")
)


class Billsystem(models.Model):
    id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=10, default="invoice", choices=BILL_TYPES)
    transaction = models.ForeignKey(Transactions, blank=True, null=True)
    count = models.IntegerField(default=0)
    creator = models.ForeignKey(User, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    invoice = models.FileField(upload_to='invoices/', blank=True, null=True)

    def lessor_billnumber(self):
        return str(self.transaction.lessor.id) + "-" + "%05d" % self.count
    
    def filename(self):
        return "%s-%s-%05d.pdf" % (self.get_type_display(),
                                   self.transaction.id, self.count)

    def render_invoice(self):
        template_context = {
            "trans": self.transaction,
            "bill": self,
            "lessor": self.transaction.lessor.gearouser,
            "lessor_user": self.transaction.lessor,
            "lessor_profile": self.transaction.lessor.profile,
            "tenant": self.transaction.applicant.gearouser,
            "tenant_user": self.transaction.applicant,
            "tenant_profile": self.transaction.applicant.profile,
            "BASE_URL": settings.BASE_URL,
        }
        c = template.Context(template_context)
        if self.transaction.lessor.gearouser.country_code == "DE":
            template_name = 'gearo/bills/invoice_lessor_de.html'
        else:
            template_name = 'gearo/bills/invoice_lessor_at.html'
        return render_to_string(template_name, c)

    def generate_invoice(self):
        tmp_pdf_file = NamedTemporaryFile()
        HTML(string=self.render_invoice()).write_pdf(
            tmp_pdf_file.name,
            stylesheets=[CSS(
                string='@page { size: A4; margin: 1.5cm }',
                font_config=FontConfiguration(),
            )],
        )
        tmp_pdf_file.seek(0)
        self.invoice.save(self.filename(), File(tmp_pdf_file))

    def __str__(self):
        return "bill %i " % self.id
    
    class Meta:
        ordering = ['-created_at']

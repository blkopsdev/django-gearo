"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.db import models

from autoslug import AutoSlugField


class AdvertisementCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)
    meta_description = models.TextField(null=True, blank=True)
    secret_name = models.CharField(max_length=150)
    slug = AutoSlugField(populate_from='name', unique=True)
    parent = models.ForeignKey('self', null=True, blank=True)
    active = models.BooleanField(default=True)
    # "Drone and Pilot", "Studio" categories does not have pickup and return date.
    # Thus, needs to be billed for every day of rent.
    is_billed_daily = models.BooleanField(default=False)
    is_insured = models.BooleanField(default=True)
    terms_of_service = models.TextField(null=True, blank=True)
    
    def __str__(self):
        return self.name

import datetime
from decimal import ROUND_HALF_UP, Decimal

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils.safestring import mark_safe
from django.utils.timezone import localtime, now, utc

from constance import config

from gearo.helper.absolute_url import absolute_url
from gearo.models.advertisement import Advertisement
from gearo.models.chatrooms import Chatrooms
from gearo.models.discount_voucher import Voucher, VoucherApplication
from gearo.models.rohprotocol import Rohprotocol


class Transactions(models.Model):
    STATUS_FINISHED = 'finished'
    STATUS_INCOMPLETE = 'incomplete'
    STATUS_WAITING = 'waiting'
    STATUS_REJECTED = 'rejected'
    STATUS_EXPIRED = 'expired'
    STATUS_ACCEPTED = 'accepted'
    STATUS_CANCELLED = 'canceled'
    STATUS_HANDING = 'handing'
    STATUS_ACTIVE = 'active'
    STATUS_RETURN = 'return'
    STATUS_INSURED = 'insured'
    STATUS_DISPUTE = 'dispute'

    STATUS_CHOICES = (
        (STATUS_INCOMPLETE, mark_safe("Unvollst&auml;ndig")),
        (STATUS_WAITING, "Ausstehend"),
        (STATUS_REJECTED, "Abgelehnt"),
        (STATUS_EXPIRED, "Bearbeitungszeit abgelaufen"),
        (STATUS_ACCEPTED, "Angenommen"),
        (STATUS_CANCELLED, "Stoniert"),
        (STATUS_HANDING, mark_safe("&Uuml;bergabe")),
        (STATUS_ACTIVE, "Mietung aktiv"),
        (STATUS_RETURN, mark_safe("R&uuml;ckgabe")),
        (STATUS_FINISHED, "Beendet"),
        (STATUS_INSURED, "Versicherungsfall"),
        (STATUS_DISPUTE, "Streitfall"),
    )

    CANCELD_CHOICES = (
        ("tenant", (
            ("r1", "Grund1"),
            ("r2", "grund2"))),
        ("lessor", (
            ("r1", "Grund1"),
            ("r2", "Grund2")))
    )

    ACTIVE_STATUSES = (STATUS_ACCEPTED, STATUS_HANDING, STATUS_ACTIVE)
    WAITING_STATUSES = (STATUS_INCOMPLETE, STATUS_WAITING, STATUS_ACCEPTED)

    MANGOPAY_WEB_PAYINS = ['mangopay_sofort', 'mangopay_paypal']

    id = models.AutoField(primary_key=True)
    ad = models.ForeignKey(Advertisement, related_name='transactions')
    applicant = models.ForeignKey(User, related_name='applicant_transactions')
    lessor = models.ForeignKey(User, related_name='lessor_transactions')
    response_limit = models.DateTimeField(blank=True, null=True)
    rent_start = models.DateField()
    rent_end = models.DateField()
    
    priceperday = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    calculated_duration = models.IntegerField()
    insurance = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)  # insurance netto
    insurance_vat = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)  # insurance vat
    insurance_sum = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    # commission netto
    insurance_commission = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    # commission vat
    insurance_commission_vat = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    # commission netto
    insurance_commission_sum = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    # total insurance costs netto
    insurance_debt = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    # total insurance costs brutto
    insurance_debt_sum = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    insurance_profit = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    netto_total_special = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    netto_total = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    subgoal = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    # netto
    articlecost = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    articlecost_sum = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)

    vat = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    vat_special = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)

    total = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    total_special = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    
    discount = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, default=Decimal(0))
    discount_value = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True, default=Decimal(0))

    payoutdaily = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    payout = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)

    long_term_benefit = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    daily_rent_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default=STATUS_INCOMPLETE)
    canceled_reason = models.CharField(max_length=2, choices=CANCELD_CHOICES, blank=True)
    canceled_other = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    processed_at = models.DateTimeField(blank=True, null=True)
    finished_at = models.DateTimeField(blank=True, null=True)
    last_modified = models.DateTimeField(auto_now=True)
    review_email_sent_at = models.DateTimeField(blank=True, null=True)
    viewed_by_applicant = models.BooleanField(default=True)
    viewed_by_lessor = models.BooleanField(default=True)
    bill_created = models.BooleanField(default=False)
    bill_number = models.IntegerField(blank=True, null=True)
    chat = models.OneToOneField(Chatrooms, blank=True, null=True, related_name='transaction')
    handing_over_protocol = models.ForeignKey(Rohprotocol, related_name='overhaning', blank=True, null=True)
    return_protocol = models.ForeignKey(Rohprotocol, related_name='returnprot', blank=True, null=True)
    voucher = models.ForeignKey(Voucher, blank=True, null=True)
    has_payout = models.BooleanField(default=0)
    exported = models.BooleanField(default=0)
    pickup_after_3pm = models.NullBooleanField(blank=True, null=True, default=True)
    return_before_12pm = models.NullBooleanField(blank=True, null=True, default=True)
    pickup_reminders_sent_at = models.DateTimeField(blank=True, null=True)
    return_reminders_sent_at = models.DateTimeField(blank=True, null=True)
    response_limit_reminder_sent_at = models.DateTimeField(blank=True, null=True)
    response_limit_reminder_short_sms_sent_at = models.DateTimeField(blank=True, null=True)
    response_limit_reminder_short_email_sent_at = models.DateTimeField(blank=True, null=True)
    response_limit_reminder_long_email_sent_at = models.DateTimeField(blank=True, null=True)

    def get_responselimit(self):
        try:
            return localtime(self.response_limit).strftime("%d.%m.%Y um %H:%M:%S Uhr")
        except AttributeError:
            return False

    def get_chat_url(self):
        if self.chat:
            return absolute_url('chat', self.chat.slug, self.id)
        return

    def apply_voucher(self, voucher, user):
        self.voucher = voucher
        self.discount_value = voucher.value
        self.discount = (
            (self.articlecost_sum * self.discount_value) / Decimal(100.0)).quantize(Decimal('0.01'), ROUND_HALF_UP)
        VoucherApplication.objects.create(voucher=voucher, advertisement=self.ad, transaction=self, user=user)
        self.save()

    @property
    def discount_amount(self):
        return getattr(self, 'discount', Decimal(0)) or Decimal(0)

    @property
    def total_incl_discount(self):
        return (Decimal(self.total) - self.discount_amount).quantize(Decimal('0.01'), ROUND_HALF_UP)

    @property
    def subgoal_incl_discount(self):
        return (Decimal(self.subgoal) - self.discount_amount).quantize(Decimal('0.01'), ROUND_HALF_UP)

    @property
    def subtotal_incl_discount(self):
        return (Decimal(self.subtotal) - self.discount_amount).quantize(Decimal('0.01'), ROUND_HALF_UP)

    @property
    def subtotal(self):
        return self.total - self.insurance_debt_sum

    @property
    def rental_fee(self):
        discount = getattr(self, 'discount_value', Decimal(0)) or Decimal(0)
        fee = Decimal(config.GEARO_FEE)
        return self.articlecost * Decimal(100) / (Decimal(100) - discount) * (fee - discount) / Decimal(100)

    @property
    def insurance_fee(self):
        if self.is_first():
            return self.insurance_commission
        else:
            return self.insurance_debt

    def ended(self):
        if (self.rent_end - datetime.date.today()).days <= 0:
            return True
        else:
            return False

    def is_first(self):
        period_start = self.rent_end - datetime.timedelta(days=14)
        period_end = self.rent_start
        transactions = Transactions.objects.filter(
            rent_start__gte=period_start,
            rent_end__lt=period_end,
            ad=self.ad,
            status=Transactions.STATUS_FINISHED,
        )
        if transactions.count():
            if any([trans.is_first() for trans in transactions]):
                return False
        return True

    @property
    def is_active(self):
        return self.status not in self.WAITING_STATUSES

    @property
    def is_cancellable(self):
        if self.pickup_after_3pm:
            pickup_time = datetime.time(hour=15)
            pickup_datetime = datetime.datetime.combine(self.rent_start, pickup_time).replace(tzinfo=utc)
            timedelta_hours = settings.TRANSACTION_CANCELLATION_DOWNTIME
            calcellation_deadline = pickup_datetime + datetime.timedelta(hours=timedelta_hours)
            return not self.is_active and calcellation_deadline >= now()
        return not self.is_active

    @property
    def is_response_limit_exceeded(self):
        return self.response_limit <= now()

    def __str__(self):
        return str(self.ad)

    def get_payment(self):
        return self.payments.last()

    def accept(self):
        payment = self.get_payment()
        if payment.variant not in self.MANGOPAY_WEB_PAYINS:
            payment.capture()
        else:
            payment.change_status('confirmed')

    def reject(self):
        payment = self.get_payment()
        if payment.variant in self.MANGOPAY_WEB_PAYINS:
            payment.refund(payment.captured_amount)
        else:
            payment.release()

    def cancel(self):
        payment = self.get_payment()
        payment.refund(payment.captured_amount)

    def set_status(self, new_status):
        if not self.status == new_status:
            self.status = Transactions.STATUS_REJECTED
            self.processed_at = now()
            self.save()

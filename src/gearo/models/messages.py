"""
@copyright: 2016 gearo GmbH
@author: Obtrusiv Solutions UG (haftungsbeschraenkt)
@license: proprietary
"""
from django.contrib.auth.models import User
from django.db import models

from gearo.models.chatrooms import ChatroomMember, Chatrooms


class Messages(models.Model):
    entry = models.AutoField(primary_key=True)
    sender = models.ForeignKey(User, related_name='sent_messages', on_delete=models.CASCADE)
    recipient = models.ForeignKey(User, related_name='received_message', null=True, blank=True)
    message = models.TextField()
    room = models.ForeignKey(Chatrooms, related_name='messages')
    sended_at = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    is_notification_sent = models.BooleanField(default=False)

    class Meta:
        ordering = ['sended_at']

    def __str__(self):
        return 'Message #%s' % self.pk

    @property
    def chatroom_member(self):
        return ChatroomMember.objects.get(chatroom=self.room, user=self.sender)

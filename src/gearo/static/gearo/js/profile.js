$(function() {
    if (ads.length > 0) {
        initAutocomplete();
    }
});

function initAutocomplete() {
    var distance = 5;
    var latitude = ads[0].lat;
    var longitude = ads[0].lng;
    var options = {
        types: ['geocode'],
        componentRestrictions: {country: 'de'}
    };

    autocomplete = new google.maps.places.Autocomplete((document.getElementById('searchplace')), options);
    initMap(latitude, longitude, distance);
}

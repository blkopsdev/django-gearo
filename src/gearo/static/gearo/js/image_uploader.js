var ImageUploader = function (options) {
    this.cropper = {};
    this.options = {
        'cropUploadedImage': true,
        'attachmentType': 'ad'
    };
    $.extend(this.options, options);
    this.finishUpload = function(thumbnail_url, attachment_id) {
        this.thumbnail_container.attr("src", thumbnail_url);
        this.preview_container.find('.link_ad_remove').removeClass('hide');
        this.preview_container.data('attachmentid', attachment_id);
    };

    this.initUploader = function() {
        var options = this.options;
        var uploader = this;
        $('.attachment_upload').fileupload({
            url: Urls.imageupload(options.attachmentType),
            dataType: 'json',
            add: function(e, data) {
                $('.lightbox').show();
                $('body').css('overflow', 'hidden');
                data.submit();
            },
            done: function(e, upload_data){
                $('.lightbox').hide();
                $('body').css('overflow', 'auto');
                uploader.thumbnail_container = $(this).next();
                uploader.preview_container = $(this).parent();
                if (options.cropUploadedImage) {
                    swal({
                        title: 'Bild beschneiden',
                        customClass: "cropper_modal",
                        html: $('<div><img id="cropper_image" src="' + upload_data.result.url + '" data-imageid="' + upload_data.result.id + '"></div>' +
                            '<p>Sollte dein Bild nicht genau passen, benutze einfach dein Scrollrad um zu zoomen!</p>'),
                        showCancelButton: true,
                        closeOnConfirm: true,
                        closeOnCancel: true,
                        confirmButtonColor: '#343a3d',
                        cancelButtonColor: '#979899'
                    }).then(function () {
                        var cropper_data = uploader.cropper.getData(rounded = true);
                        var container_data = uploader.cropper.getContainerData();
                        var data = {
                            'x': cropper_data.x,
                            'y': cropper_data.y,
                            'width': cropper_data.width,
                            'height': cropper_data.height,
                            'rotate': cropper_data.rotate,
                            'container_width': container_data.width,
                            'container_height': container_data.height,
                            'image_id': $('#cropper_image').data("imageid")
                        };
                        $.post(Urls.imagecrop(options.attachmentType), data, function (crop_data) {
                            if (crop_data.success) {
                                uploader.finishUpload(crop_data.data.thumbnail_url, upload_data.result.id);
                            }
                            else {
                                swal({
                                    title: 'Fehler beim Skalieren',
                                    html: "Bitte lade dein Bild erneut hoch und skaliere es nach deinen Wünschen.",
                                    type: 'error'
                                });
                            }
                        });
                    }, function (dismiss) {
                        uploader.finishUpload(upload_data.result.thumbnail_url, upload_data.result.id);
                    });
                }
                else {
                    uploader.finishUpload(upload_data.result.thumbnail_url, upload_data.result.id);
                }

                if (options.cropUploadedImage) {
                    var image = document.getElementById('cropper_image');
                    uploader.cropper = new Cropper(image, {
                        aspectRatio: 16 / 9
                    });
                }

                var attachments = $("#attachments_field").val().split(',');
                attachments.push(upload_data.result.id);
                $("#attachments_field").val(attachments.join(','));
                if (attachments.length == 2){
                    $("#chooseimages div").removeClass("hide");
                }
            }
        });
    };

    this.initImageDelete = function() {
        options = this.options;
        $('.link_ad_remove').on('click', function () {
            var preview_container = $(this).parent();
            var link_ad_remove = $(this);
            var attachment_id = preview_container.data('attachmentid');
            swal({
                title: 'Bild löschen',
                customClass: 'remove_attachment_modal',
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true,
                text: "Bist du sicher, dass du dieses Bild löschen möchtest",
                confirmButtonColor: '#343a3d',
                cancelButtonColor: '#979899'
            }).then(function() {
                $.post(Urls.uploaddelete(options.attachmentType), {'attachment_id': attachment_id}, function (data) {
                    preview_container.find('.preview-thumbnail').attr('src', chooseImgUrl);
                    link_ad_remove.addClass('hide');
                    var attachments = $("#attachments_field").val().split(',');
                    var updated_attachments = jQuery.grep(attachments, function(value) {
                        return value != attachment_id;
                    });
                    $("#attachments_field").val(updated_attachments.join(','));
                });
            });

        });
    };

    this.initUploader();
    this.initImageDelete();
};
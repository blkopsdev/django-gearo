var $sections = $('.form-section');
var isBilledDaily;
var dailyBilledCategoryIds;

$('#advertise-form').find('.form-control').keypress(function(e){
    if ( e.which == 13 ) // Enter key = keycode 13
    {
        return false;
    }
});

function updateTags() {
    var tagLabels = new Array();
    $('.tagit-label').each(function() {
        tagLabels.push($(this).text());
    });
    $('#id_tags').val(tagLabels.join());
}

function navigateTo(index) {
    // Mark the current section with the class 'current'
    $sections.removeClass('current').eq(index).addClass('current');
    // Show only the navigation buttons that make sense for the current section:
    $('.form-navigation .previous').toggle(index > 0);
    var atTheEnd = index >= $sections.length - 1;
    $('.form-navigation .next').toggle(!atTheEnd);
    $('.form-navigation [type=submit]').toggle(atTheEnd);

    // change progress image
    switch(index+1){
        case 1:
            $('.form-navigation img').attr("src", stepOneImg);
            break;
        case 2:
            $('.form-navigation img').attr("src", stepTwoImg);
            break;
        case 3:
            $('.form-navigation img').attr("src", stepThreeImg);
            initMap();
            break;
    }
}

function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
}

function calculateRentPrice() {
    var priceday = parseInt($("#id_priceday").val());
    if (isBilledDaily) {
        $("#id_pricethree").val(priceday * 3);
        $("#id_priceweek").val(priceday * 7);
    }
    else {
        $("#id_pricethree").val(priceday * 2.5);
        $("#id_priceweek").val(priceday * 5);
    }
}

function updateFieldsAndLabels(skipRentPriceUpdate) {
    var selectedCategory = $('#id_category option:selected').val();
    var selectedCategoryId = parseInt(selectedCategory);
    isBilledDaily = $.inArray(selectedCategoryId, dailyBilledCategoryIds) > -1;
    if (selectedCategory != '') {
        $('#id_priceday').prop('disabled', false);
        $('#id_pricethree').prop('disabled', false);
        $('#id_priceweek').prop('disabled', false);
        if (!skipRentPriceUpdate) {
            calculateRentPrice();
        }
    }
    else {
        $('#id_priceday').prop('disabled', true);
        $('#id_pricethree').prop('disabled', true);
        $('#id_priceweek').prop('disabled', true);
    }
    if (isBilledDaily) {
        var termsOfService = categoriesTerms[selectedCategoryId];
        $('#id_articleofvalue').prop('required', false);
        $('#advertise_prices_label').text('Tagessätze in Euro');
        $('label[for=notAvailable]').text('Trage ein, wann du nicht verfügbar bist.');
        if (termsOfService) {
            $('#articleofvalue_not_specified_container p').text(termsOfService);
        }
        $('#articleofvalue_not_specified_container').show();
        $('#articleofvalue_specified_container').hide();
        $('#pickup_times_container').hide();
    }
    else {
        $('#id_articleofvalue').prop('required', true);
        $('#advertise_prices_label').text('Mietpreise in Euro');
        $('label[for=notAvailable]').text('Trage ein, ob und wann dein Gear nicht verfügbar ist');
        $('#articleofvalue_not_specified_container').hide();
        $('#articleofvalue_specified_container').show();
        $('#pickup_times_container').show();
    }
}

$(function(){
    initAutocomplete();
    new ImageUploader();
    var is_ad_add = $('#advertise-form').hasClass('advertise-add');
    dailyBilledCategoryIds = $.parseJSON(dailyBilledCategories);

    $('#id_category').change(function() {
        updateFieldsAndLabels(false)
    });

    $sections = $('.form-section');
    if (is_ad_add) {
        // Previous button is easy, just go back
        $('.form-navigation .previous').click(function() {
            navigateTo(curIndex() - 1);
        });

        // Next button goes forward if current block validates
        $('.form-navigation .next').click(function() {
            if ($('.advertise-form').parsley().validate({
                group : 'block-' + curIndex()
            }))
                navigateTo(curIndex() + 1);
        });

        // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
        $sections.each(function(index, section) {
            $(section).find(':input').attr('data-parsley-group',
                    'block-' + index);
        });
        navigateTo(0); // Start at the beginning
    }
    else {
        updateFieldsAndLabels(true);
    }

    $("#tags").tagit({
        autocomplete: {
            source: function(search, showChoices) {
                $.get(tagsAutocompleteUrl, {tag: search.term.toLowerCase()}, function (data) {
                    showChoices(data.tags);
                });
            }
        },
        singleField: true,
        fieldName : "tags",
        singleFieldNode: $('#id_tags'),
        afterTagRemoved: function(event, ui) {
            updateTags();
        },
        afterTagAdded : function(event, ui) {
            updateTags();
        }
    });

    $("#id_priceday").focusout(function(){
        calculateRentPrice();
    });

    var datepickerRent = new DatepickerCreate({
        container: $("#notAvailableSchedule"),
        editable: true
    });


    datepickerRent.init();

    $(".times").each(function(){
        $(this).timepicker({ 'timeFormat': 'H:i' });
    });

    $('#tags').popover({ placement: "right",   content:"Trage hier mindestens 3 Tags ein, um dein Angebot noch besser auffindbar zu machen.", trigger: "focus"});
    $("#id_priceday").popover({ placement: "left",   content:"<p>Trage hier den Mietpreis für dein Equipment ein. Berücksichtige dass alle Mietpreise auf gearo als Brutto Endpreise inkl. Mwst. ausgewiesen werden. Rabattiere längere Anmietungen und erhalte so mehr Anfragen für dein Inserat.</p><p>Der hier eingetragene Mietpreis enthält den 15-prozentigen gearo-fee.</p>", html: true,trigger: "focus"});
    $("#id_articleofvalue").popover({ placement: "right",   content:"Die Sicherheit deines Equipments hat für gearo oberste Priorität. Sollte wirklich einmal etwas passieren, erstatten wir dir über unsere exklusive Versicherung selbstverständlich die Reparatur- oder Wiederbeschaffungskosten.", trigger: "focus"});

    $("#chooseimages").popover({ placement: "right",   content:"Füge mindestens ein Bild hinzu, um das Interesse des Mieters auf den ersten Blick zu wecken. Du kannst bis zu 6 Bilder hochladen.", trigger: "hover"});
    $("#placesearch").popover({ placement: "right",   content:"Gib' die Adresse ein, an der das Equipment abgeholt werden kann. Keine Sorge, öffentlich wird nur der ungefähre Standort angezeigt.", trigger: "hover"});
    $('.action-new').popover({ placement: "right",   content:"Falls du dein Equipment _mal selber benötigst, kannst du hier eingeben, wann es für andere Nutzer nicht verfügbar sein soll.", trigger: "hover"});
    $("#times").popover({ placement: "right",   content:"Trage ein, wann dein Equipment üblicherweise verfügbar ist. Die Abholzeiten sind nicht bindend. Du entscheidest bei jeder Anfrage individuell.", trigger: "hover"});

});

function initAutocomplete() {
    var options = {
        types: ['geocode'],
        componentRestrictions: {country: allowedCountries}  // from base.html
    };
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('placesearch')), options);

    // show mark on map
    autocomplete.addListener('place_changed',initMap);
    initMap();
}

//lat, lng
function initMap() {
    var is_ad_add = $('#advertise-form').hasClass('advertise-add');
    var place = autocomplete.getPlace();
    var latitude = $('#id_lat').val();
    var longitude = $('#id_lng').val();
    if (place !== undefined){
        document.getElementById("id_lat").value = place.geometry.location.lat();
        document.getElementById("id_lng").value = place.geometry.location.lng();
        var LatLng = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
        var zoom = 10;
    } else {
        if (latitude != '' && longitude != '') {
            var LatLng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            var zoom = 10;
        }
        else {
            var LatLng = {lat: 51.165691, lng: 10.451526000000058};
            var zoom = 5;
        }
    }
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: zoom,
      center: LatLng
    });

    if ((is_ad_add && place !== undefined) || (is_ad_add && latitude != '' && longitude != '') || !is_ad_add ){
        var marker = new google.maps.Marker({
          position: LatLng,
          map: map,
          icon: markerImage
        });
    }
  }
var chatMessageTemplate = '<div class="message {{#is_own_message}}type1{{/is_own_message}} {{^is_own_message}}type2{{/is_own_message}}">' +
    '<img src="{{ avatar_thumbnail_url }}" class="uavatar-large">' +
    '<span class="online-status chatroom-member-{{token}}"></span>' +
    '<div class="content">{{ message }} </div>' +
    '<div class="greyinfo timestamp">geschrieben am {{ sended_date }} um {{ sended_time }} Uhr </div> ' +
    '</div>';

function update_online_status(tokens) {
    $('.online-status.online').removeClass('online');
    tokens.forEach(function (token) {
        $('.chatroom-member-' + token).addClass('online');
    });
}

$(function(){
    $('.match').matchHeight();

    $("#reject").click(function(){
        $("#stornoModal").modal('hide');
    });
    $("#messages").animate({ scrollTop: $('#messages').prop('scrollHeight') }, "slow");

    var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    var chatSocket = new RobustWebSocket(ws_scheme + '://' + window.location.host + "/ws/chat/" + chatRoom + "/" + chatRoomToken + "/");

    chatSocket.addEventListener('message', function(message) {
        var data = JSON.parse(message.data);
        if (data['type'] == 'message') {
            var chat = $("#messages");
            data['is_own_message'] = data.token == chatRoomToken;
            var content = Mustache.render(chatMessageTemplate, data);
            chat.append(content);
            $("#messages").animate({ scrollTop: $('#messages').prop('scrollHeight') }, "slow");
        }
        update_online_status(data['tokens']);
    });

    $('#btn_send_message').click(function (e) {
        var message_input = $('textarea[name=message]');
        var is_valid = $('#message').parsley().validate();
        if (is_valid) {
            chatSocket.send(message_input.val());
            message_input.val('');
            e.preventDefault();
        }
        else {
            message_input.blur();
        }
    });

    $('#geninvoice, #renderpdf').click(function(e) {
        if ($(this).hasClass('status-active') && isLessor) {
            if (!isBillingInfoCompleted) {
                swal({
                    "title": "Bevor du deine Rechnung abschickst...",
                    "html": "Sag uns kurz, wie du deine Mieteinnahmen erhalten möchtest. Vervollständige hierzu einfach deine " +
                    "<a href='" + paymentEditUrl + "' target='_blank'> Bankinformation</a>.",
                    "type": "warning"
                });
                e.preventDefault();
            } else if (!isProfileCompleted) {
                swal({
                    "title": "Profilinformation",
                    "html": "Du musst deinen <a href='" + accountEditUrl + "' target='_blank'> Account</a>  vervollständigen bevor du etwas anmieten kannst.",
                    "type": "warning"
                });
                this.onclick = undefined;
            }
        }
    });

    if (device.desktop()) {
        $('.btn-href.btn-handingover-protocol').off('click');
        $('.btn-href.btn-return-protocol').off('click');
        $('.btn-handingover-protocol.status-active').click(function (e) {
            var url = $(this).data('href');
            swal({
                title: "Übergabeprotokoll",
                html: "Fülle das Übergabeprotokoll mit deinem Smartphone oder Tablet aus. Log dich hierzu einfach mit deinem Smartphone oder Tablet auf gearo ein.",
                type: "warning",
                confirmButtonText:  'zum Protokoll',
                cancelButtonText: 'zurück',
                showCancelButton: true
            }).then(function () {
                location.href = url;
            });
            e.preventDefault();
        });
        $('.btn-return-protocol.status-active').click(function (e) {
            var url = $(this).data('href');
            swal({
                title: "Rückgabeprotokoll",
                html: "Fülle das Rückgabeprotokoll mit deinem Smartphone oder Tablet aus. Log dich hierzu einfach mit deinem Smartphone oder Tablet auf gearo ein.",
                type: "warning",
                confirmButtonText:  'zum Protokoll',
                cancelButtonText: 'zurück',
                showCancelButton: true
            }).then(function () {
                location.href = url;
            });
            e.preventDefault();
        })
    }
});
var $sections;
var stepProgressWidth;
var isUndamaged;
var nextStepIdx;
var previousStepIdx;
var signaturePadLessor;
var signaturePadApplicant;

function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
}

function updateProgressBar() {
    var progressBarWidth = stepProgressWidth * (curIndex() + 1);
    if (curIndex() >= ($sections.length - 1)) {
        progressBarWidth = 100;
    }
    $('.progress-indicator').css('width', progressBarWidth + '%');
}

function resizeSignaturePad() {
    var signatureWidth = $('#protocolcontent').width();
    var signatureHeight = (signatureWidth / 3) * 2;
    $('#lessor_signature').attr('width', signatureWidth);
    $('#lessor_signature').attr('height', signatureHeight);
    $('#applicant_signature').attr('width', signatureWidth);
    $('#applicant_signature').attr('height', signatureHeight);
}

$(function(){
    $sections = $('.protocol-steps');
    stepProgressWidth = (100 / $sections.length).toFixed(2);
    autosize($('textarea'));
    if (!isCreated) {
        resizeSignaturePad();
        signaturePadLessor = new SignaturePad(document.getElementById('lessor_signature'), {
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(255, 255, 255)'
        });

        signaturePadApplicant = new SignaturePad(document.getElementById('applicant_signature'), {
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(255, 255, 255)'
        });
    }
    else {
        $('.protocol-steps').addClass('created');
        $('#progressbar').addClass('created');
        $('#undamaged_message, #damaged_message').addClass('created');
        $('.slider').bxSlider({pager: false, mode: "fade", auto: true, captions: true});
    }
    $(".btn-clear-lessor-signature").click(function(e){
        e.preventDefault();
        signaturePadLessor.clear();
    });
    $(".btn-clear-applicant-signature").click(function(e){
        e.preventDefault();
        signaturePadApplicant.clear();
    });

    $(".timepicker").each(function(){
        $(this).timepicker({ 'timeFormat': 'H:i' });
    });

    $("#applicant_signature").popover({ placement: "right", content:"Der Mieter muss unterschreiben", trigger: "manual"});
    $("#lessor_signature").popover({ placement: "right", content: "Der Vermieter muss unterschreiben", trigger: "manual"});

    $('.protocol-steps').each(function(index, container) {
        $(container).find(':input').attr('data-parsley-group', 'block-' + index);
    });

    $('#id_defects').on('keyup', function() {
        if (!isUndamaged && $(this).val() == '') {
            $('.protocol-step5 .btn-protocol-next').addClass('invisible');
        }
        else {
            $('.protocol-step5 .btn-protocol-next').removeClass('invisible');
        }
    });

    $('button[type="submit"]').click(function(e){
        $('input[name="applicant_signature"]').val(signaturePadApplicant.toDataURL('image/png'));
        $('input[name="lessor_signature"]').val(signaturePadLessor.toDataURL('image/png'));
    });

    new ImageUploader({'cropUploadedImage': false, 'attachmentType': 'protocol'});
});
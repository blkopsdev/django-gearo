$(document).ready(function () {
    $("#chart_div").UseTooltip();
    $("#chart_div_total").UseTooltip();
    $("#pie_chart_div").showMemo();
    $('input[type="checkbox"][name="fees"]').on('change',function(){
      var getArrVal = $('input[type="checkbox"][name="fees"]:checked').map(function(){
        return this.value;
      }).toArray();

      if(getArrVal.length){
        $('#cont').html('');
      } else {
        $(this).prop("checked",true);
        $('#cont').html("At least one value must be checked!");
        return false;
      }
    });
});

var previousPoint = null, previousLabel = null;

$.fn.UseTooltip = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                $("#tooltip").remove();

                var y = parseFloat(item.datapoint[1]).toFixed(2);
                var color = item.series.color;

                showTooltip(item.pageX, item.pageY, color, "<strong>" + y);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
};


function showTooltip(x, y, color, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 40,
        left: x - 0,
        border: '2px solid ' + color,
        padding: '3px',
        'font-size': '9px',
        'border-radius': '5px',
        'background-color': '#fff',
        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(200);
}

$.fn.showMemo = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (!item) { return; }
        var html = [];
        var percent = parseFloat(item.series.percent).toFixed(0);

        html.push("<div style=\"border:1px solid grey;background-color:",
             item.series.color,
             "\">",
             "<span style=\"color:white\">",
             item.series.label,
             " : ",
             item.series.data[0][1],
             " (", percent, "%)",
             "</span>",
             "</div>");
        $("#flot-memo").html(html.join(''));
    });
};

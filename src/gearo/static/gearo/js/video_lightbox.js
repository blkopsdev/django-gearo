function adjustLightboxTopMargin() {
    var margin_top = $(window).height() - $('#lightbox_container').outerHeight();
    if (margin_top < 0) {
        margin_top = 0;
    }

    if ($(window).width() <= 991) {
        $('#lightbox_container').css('margin-top', margin_top / 2);
    }
}

$(function() {
    $('.play-button > a').click(function () {
        var video_id = $(this).data('videoid');
        $('#lightbox_container').html('<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/' + video_id + '?autoplay=1" frameborder="0" allowfullscreen></iframe>');
        $('#lightbox').show();
        adjustLightboxTopMargin();
        $('body').css('overflow', 'hidden');
    });
    $(window).resize(function () {
        adjustLightboxTopMargin();
    });
    $('.lightbox').click(function (e) {
        $('#lightbox_container').empty();
        $(this).hide();
        $('body').css('overflow', 'auto');
    });
    $('.lightbox.close-button').click(function () {
        $('#lightbox_container').empty();
        $('.lightbox').hide();
        $('body').css('overflow', 'auto');
        return false;
    });
    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            $('#lightbox_container').empty();
            $('.lightbox').hide();
            $('body').css('overflow', 'auto');
        }
    });
});
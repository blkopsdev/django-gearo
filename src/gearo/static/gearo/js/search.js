// First page already rendered, thus fetching for infinite pagination pages starting from 2.
var currentPageNum = 2;
var hasNext = true;
var scene;

var searchResultsTemplate = '{{#results}}' +
    '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
        '<a href="{{ profile_url }}"><img src="{{ profile_avatar_url }}" class="uavatar-small"></a>' +
        '<a href="{{ absolute_url }}" class="noUnderline">' +
            '<div class="panel panel-gearo">' +
                '<div class="panel-heading">' +
                '<span>{{ title }}</span>' +
                '</div>' +
                '<div class="panel-body">' +
                    '<img src="{{ thumbnail_url }}" alt="alt text" style="text-decoration: none;" class="img-responsive">' +
                        '<span class="price">{{ priceday }} &euro; ' +
                        '<small>' +
                        '{{#include_mwst}}inkl. MwSt{{/include_mwst}}' +
                        '{{^include_mwst}}Mwst. frei.{{/include_mwst}}' +
                        '</small>' +
                        '</span>' +
                '</div>' +
            '</div>' +
        '</a>' +
    '</div>' +
    '{{/results}}';

function loadNewPaginationItems() {
    if (hasNext) {
        var url = window.location.pathname;
        var sorting = $('input[name=sorting]').val();
        var params = {
            'page_num': currentPageNum
        };
        if (sorting != '-ranking') {
            params['sorting'] = sorting;
        }

        $.get(url + '?' + $.param(params), function (data) {
            hasNext = data.has_next;
            if (data.has_next) {
                currentPageNum += 1;
            }
            else {
                $('#loader').hide();
            }
            var content = Mustache.render(searchResultsTemplate, data);
            $('.search-results').append(content);
            scene.update();
        });
        $(".nano").nanoScroller();
    }
    else {
        $('#loader').hide();
    }
}

$(function(){
    initAutocomplete();
    var test = window.location.pathname.split("/");

    $('#startpage-categories li a[data-category="'+ test[test.indexOf("kategorie") + 1] + '"]').addClass("active");
    $("#startpage-categories li a").click(function(e){
        e.preventDefault();
        if($('input[name="cat_slug"]').val() == $(this).data("category")){
            $('input[name="cat_slug"]').val("")
        }else{
            $('input[name="cat_slug"]').val($(this).data("category"));
        }
        $('#searchbar').submit();
    });

    $('.distance-options li a').click(function() {
        var distance_text = $(this).text();
        var distance_value = $(this).data('distance');
        $('#distance_placeholder').text(distance_text);
        $('.distance-dropdown').addClass('distance-selected');
        $('input[name=distance]').val(distance_value);
        $('#searchbar').submit();
    });

    $('.sorting-options li a').click(function() {
        var sorting_text = $(this).text();
        var sorting_value = $(this).data('sorting');
        $('#sorting_placeholder').text(sorting_text);
        $('input[name=sorting]').val(sorting_value);
        $('#searchbar').submit();
    });

    $('#searchplace').change(function() {
        if ($(this).val() !== '') {
            $('.distance-dropdown').removeClass('disabled');
        }
        else {
            $('#distance_placeholder').text('Umkreis');
            $('.distance-dropdown').removeClass('distance-selected');
            $('input[name=distance]').val('');
            if (!$(this).hasClass('disabled')) {
                $('.distance-dropdown').addClass('disabled');
            }
        }
    });

    var controller = new ScrollMagic.Controller();

    scene = new ScrollMagic.Scene({triggerElement: "#loader", triggerHook: "onEnter"}).addTo(controller).on("enter", function (e) {
        loadNewPaginationItems();
    });

    $(".nano").nanoScroller();
    $(".nano").bind("scrollend", function(e){
        scene.update();
    });

    $('#searchpickup').daterangepicker({
        "singleDatePicker": true,
        minDate: moment(),
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
    $('#searchpickup').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.YYYY'));
    });
    $('#searchreturn').daterangepicker({
        "singleDatePicker": true,
        minDate: moment(),
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
    $('#searchreturn').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.YYYY'));
    });

    // Some js adds inline styles to .container.nano-content and this breaks
    // style of advertisement search page. Next line removes useless styles
    $(".container.nano-content").css('margin-right', '').css('right', '');

});

function initAutocomplete() {
    var distance = $('#distance').val();
    var latitude = $('#lat').val();
    var longitude = $('#lng').val();
    var options = {
        types: ['geocode'],
        componentRestrictions: {country: allowedCountries}  // from base.html
    };

    autocomplete = new google.maps.places.Autocomplete((document.getElementById('searchplace')), options);
    initMap(latitude, longitude, distance);
}
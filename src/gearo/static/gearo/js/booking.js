var time;
var date;
var datetime;

// Card PreRegistration variables
var paymentVariant;
var mangopayClientId;
var mangopayBaseURL;
var accessKey;
var preregistrationData;
var cardRegistrationURL;
var cardPreRegistrationId;
var cardType;

function limitTimeResponse() {
    var timeStart = moment(defaultResponseLimit, "DD.MM.YYYY HH:mm");
    $('#timeresponse').timepicker('option', 'minTime', '00:00');
    $('#timeresponse').timepicker('option', 'maxTime', '23:30');
    $('#timeresponse').attr('value', timeStart.format('HH:mm'));
}

function showError(message) {
    swal({
        text: message, type: 'error'
    }).then(function() {
        $('.lightbox').hide();
    });
}

function showErrorDuringCardRegistration() {
    showError("Fehler bei der Kreditkarten-Vorregistrierung.");
}

function showErrorDuringPayment() {
    showError("Fehler bei der Transaktion. Bitte versuche es erneut.");
}

function makeCardPayment(post_data) {
    $.post(createCardPaymentUrl, post_data, function(data) {
        if (data.success) {
            hideCardForm();
            unsetCardPreRegistrationVariables();
            checkCardPreAuthorizationStatus()
        } else {
            showErrorDuringPayment();
        }
    }).fail(function() {
        showErrorDuringPayment();
    });
}

function makeWebPayInPayment(url) {
    $('.lightbox').show();
    $.post(url, {}, function(data) {
        if (data.success) {
            window.location.href = data.redirect_path;
        } else {
            showErrorDuringPayment();
        }
    }).fail(function() {
        showErrorDuringPayment();
    });
}

function checkCardPreAuthorizationStatus() {
    var interval = setInterval(function(){
        $.get(checkCardPreAuthorizationStatusUrl, {}, function(data) {
            if (data.success) {
                window.location.href = data.redirect_path;
            } else if (data.error) {
                clearInterval(interval);
                showErrorDuringPayment();
            }
        });
    }, 2000);
}

function receiveCardPreRegistrationData() {
    var cardPreRegistration = $.get(createCardPaymentUrl, {}, function (data) {
        if (data.success) {
            mangopayClientId = data.mp_client_id;
            mangopayBaseURL = data.mp_base_url;
            accessKey = data.access_key;
            preregistrationData = data.preregistration_data;
            cardRegistrationURL = data.card_registration_url;
            cardPreRegistrationId = data.card_registration_id;
            cardType = data.card_type;
        }
        else {
            showErrorDuringCardRegistration();
        }
    }).fail(function() {
        showErrorDuringCardRegistration();
    });
    return cardPreRegistration;
}

function unsetCardPreRegistrationVariables() {
    paymentVariant = undefined;
    mangopayClientId = undefined;
    mangopayBaseURL = undefined;
    accessKey = undefined;
    preregistrationData = undefined;
    cardRegistrationURL = undefined;
    cardPreRegistrationId = undefined;
    cardType = undefined;
}

function cardFormIsValid() {
    $("#card_form_errors").html('');

    var card_number = $(".card-number").val();
    var card_month = $(".card-month").val();
    var card_year = $(".card-year").val();
    var card_cvc = $(".card-cvc").val();

    if (card_number == '' || card_month == '' || card_year == '' || card_cvc == '') {
        // "All card form fields should be filled."
        $("#card_form_errors").append('<ul class="errorlist"><li>Fülle alle Kreditkartenfelder aus.</li></ul>');
        $('.lightbox').hide();
    } else {
        if (card_number.length < 16) {
            $("#card_form_errors").append('<ul class="errorlist"><li>Card number too short!</li></ul>');
            $('.lightbox').hide();
        } else if (card_month.length < 2 || card_year.length < 2) {
            // "Enter the expiration date of your credit card in MM / YY format."
            $("#card_form_errors").append('<ul class="errorlist"><li>Gib das Ablaufdatum deiner Kreditkarte im Format MM/JJ an.</li></ul>');
            $('.lightbox').hide();
        } else if (card_cvc.length < 3) {
            // "Enter CVC number consisting of 3 digits."
            $("#card_form_errors").append('<ul class="errorlist"><li>Gib CVC Nummer bestehend aus 3 Stellen ein.</li></ul>');
            $('.lightbox').hide();
        } else {
            $("#card_form_errors").html('');
            return true;
        }
    }
    return false;
}

function registerCard() {

    // Card register data prepared on the server
    var cardRegisterData = {
        cardRegistrationURL: cardRegistrationURL,
        preregistrationData: preregistrationData,
        accessKey: accessKey,
        Id: cardPreRegistrationId,
    };

    // Card data collected from the user
    var cardData = {
         cardNumber: $(".card-number").val(),
         cardExpirationDate: $(".card-month").val() + $(".card-year").val(),
         cardCvx: $(".card-cvc").val(),
         cardType: cardType,
    };

    // Set MangoPay API base URL and Client ID
    mangoPay.cardRegistration.baseURL = mangopayBaseURL;
    mangoPay.cardRegistration.clientId = mangopayClientId;

    // Initialize the CardRegistration
    mangoPay.cardRegistration.init(cardRegisterData);

    // Register card
    mangoPay.cardRegistration.registerCard(cardData,
        function(res) {
            post_data = {'card_id': res.CardId};
            makeCardPayment(post_data);
        },
        function(res) {
            // "Credit card registration error."
            swal({
                title: 'Fehler bei der Kreditkartenregistrierung.',
                text: 'Code: ' + res.ResultCode + ', message: ' + res.ResultMessage,
                type: 'error',
            }).then(function() {
                $('.lightbox').hide();
            });
        }
    );
}

function hideCardForm() {
    $('.card-number').val('');
    $('.card-expiration-date').val('');
    $('.card-month').val('');
    $('.card-year').val('');
    $('.card-cvc').val('');
    $('#card_form').hide('slow');
    $(".pay-with-card").removeClass('active-payment-box');
}

function showPaymentExplanation(option) {
    var paymentExplanation = $('#payment_explanation');
    if (option == 'preauth') {
        paymentExplanation.html(
            'Der Rechnungsbetrag wird erst von deinem Konto abgebucht, wenn der Vermieter deine Anfrage bestätigt hat.'
        );
    } else if (option == 'charge') {
        paymentExplanation.html(
            'Der Rechnungsbetrag wird direkt von deinem Konto abgebucht. ' +
            'Sollte deine Anfrage abgelehnt werden buchen wir den Betrag unverzüglich auf dein Konto zurück.'
        );
    }
}

$(function(){
    $('#timeresponse').timepicker({'timeFormat': 'H:i'});
    limitTimeResponse();

    $('#dateresponse').daterangepicker({
        "locale": {
            "format": "DD.MM.YYYY",
            "separator": ".",
            "daysOfWeek": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
            "monthNames": [
                "Januar", "Februar", "März", "April", "Mai", "Juni",
                "Juli", "August", "September", "Oktober", "November", "Dezember",
            ],
        },
        "singleDatePicker": true,
        "minDate": minDate,
        autoUpdateInput: false,
            locale: {
              cancelLabel: 'Clear'
            }
    });

    $('#dateresponse').on('apply.daterangepicker', function(ev, picker) {
        var currentDate = new Date();
        if (picker.startDate.isSame(currentDate, 'day')) {
            limitTimeResponse();
        }
        else {
            $('#timeresponse').timepicker('option', 'minTime', '00:00');
            $('#timeresponse').attr('placeholder', '14:00');
        }
        $(this).val(picker.startDate.format('DD.MM.YYYY'));
    });

    function updateResponse (datetime){
        $.ajax({
            url : updateResponseLimitUrl,
            type : "POST",
            data : {
                response_limit: datetime.format("YYYY-MM-DD HH:mm:ss")
            },

            // handle a successful response
            success : function(data) {
                if (!data.success){
                    console.log("Oppla, leider konnte dein Antwort limit nicht gespeichert werden :(");
                }
            }
        });
    }

    var initDate;

    setInterval(function(){
        var transactionTime = $("#timeresponse").val();
        var transactionDate = $("#dateresponse").val();

        if(transactionTime && transactionDate){
            datetime = moment(transactionDate + " " + transactionTime , "DD.MM.YYYY HH:mm");
            // check if end date isn't before start date
            if(datetime.isBefore(moment(), "day")){
                // add hint... maybe
                return;
            }
            // set init values on first call
            if(initDate === undefined  ){
                initDate = datetime;
                // updateResponse new rent and insurance
                updateResponse(datetime);
            }
            // check if user change values
            if(!initDate.isSame(datetime) ){
                initDate = datetime;
                // update
                updateResponse(datetime);
            }
        }
    }, 1000);

    $("#timeresponse_error").popover({
        placement: "right",
        content:"gearo gestaltet den Mietprozess für dich so einfach wie möglich. " +
                "Sobald der Vermieter die Buchung bestätigt hat, ist dein Equipment verbindlich gebucht . " +
                "So hast du Planungssicherheit bei deiner Produktion.",
        trigger: "manual"});
    $("#timeresponse_error").popover('show');

    $(".pay-with-sofort").click(function() {
        showPaymentExplanation('charge')
        $(this).addClass('active-payment-box');
        paymentVariant = 'sofort';
        hideCardForm();
        $(".pay-with-paypal").removeClass('active-payment-box');
    });

    $(".pay-with-paypal").click(function() {
        showPaymentExplanation('charge')
        $(this).addClass('active-payment-box');
        paymentVariant = 'paypal';
        hideCardForm();
        $(".pay-with-sofort").removeClass('active-payment-box');
    });

    $(".pay-with-card").click(function() {
        showPaymentExplanation('preauth')
        $(this).addClass('active-payment-box');
        paymentVariant = 'card';
        $('#card_form').show('slow');
        $(".pay-with-sofort").removeClass('active-payment-box');
        $(".pay-with-paypal").removeClass('active-payment-box');
    });

    $("#payment_button").click(function(e){
        if($("#timeresponse").val().length === 0){
            e.preventDefault();
            $("#timeresponse").css("border", "1px solid #be5b5b");
            if($("#timeresponse_error .errorlist li").length == 0){
                $("#timeresponse_error").append('<ul class="errorlist"><li>Pflichtangabe</li></ul>');
            }
        }
        else {
            if (moment() > datetime) {
                limitTimeResponse();
                $("#timeresponse").css("border", "1px solid #be5b5b");
                $("#timeresponse_error").html('<ul class="errorlist"><li>Bitte wähle einen Zeitraum in der Zukunft</li></ul>');
            }
            else {
                $("#timeresponse").css("border", "1px solid #979899");
                $("#timeresponse_error").html('');
                if (paymentVariant == "sofort") {
                    makeWebPayInPayment(createSofortPaymentUrl);
                } else if (paymentVariant == "card") {
                    if (cardFormIsValid()) {
                        $('.lightbox').show();
                        d1 = receiveCardPreRegistrationData();
                        $.when(d1).done(function() {
                            registerCard();
                        });
                    }
                }
            }
        }
    });

    $('#discountenter').click(function(){
        $.ajax({
            url : addVoucherUrl,
            type : "POST",
            data : {
                code: $('input[name="discountinput"]').val()
            },

            // handle a successful response
            success : function(data) {
                if (data.success) {
                    $(".vore").remove();
                    $("#discountenter").parent().append('<div class="vosuc vore">Rabattcode akzeptiert.</div>');
                    $('#discount_value').html(data.discount_value + ' % Rabatt');
                    $('#discount').html("- " + data.discount + " €");
                    $("#rentdetail").html(data.new_debt + " €");
                    $("#totaldetail").html(data.new_total + " €");
                }
                else {
                    if (data.already_used) {
                        $(".vore").remove();
                        $("#discountenter").parent().append('<div class="voerr vore">Du kannst nur einen Rabattcode pro Buchung nutzen.</div>');
                    }
                    else {
                        $(".vore").remove();
                        $("#discountenter").parent().append('<div class="voerr vore">Rabattcode ungültig.</div>');
                    }
                }
            }
        });
    });

    $(".only-digits").keypress(function (e) {
        $("#card_form_errors").html('');
        // If the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            // Display error message
            $("#card_form_errors").html('<ul class="errorlist"><li>Only digits!</li></ul>');
            return false;
        }
   });

});
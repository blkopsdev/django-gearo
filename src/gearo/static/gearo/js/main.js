Parsley.addMessages('de', {
    defaultMessage : "Die Eingabe scheint nicht korrekt zu sein.",
    type : {
        email : "Die Eingabe muss eine gültige E-Mail-Adresse sein.",
        url : "Die Eingabe muss eine gültige URL sein.",
        number : "Die Eingabe muss eine Zahl sein.",
        integer : "Die Eingabe muss eine Zahl sein.",
        digits : "Die Eingabe darf nur Ziffern enthalten.",
        alphanum : "Die Eingabe muss alphanumerisch sein."
    },
    notblank : "Die Eingabe darf nicht leer sein.",
    required : "Dies ist ein Pflichtfeld.",
    pattern : "Die Eingabe scheint ungültig zu sein.",
    min : "Die Eingabe muss größer oder gleich %s sein.",
    max : "Die Eingabe muss kleiner oder gleich %s sein.",
    range : "Die Eingabe muss zwischen %s und %s liegen.",
    minlength : "Die Eingabe ist zu kurz. Es müssen mindestens %s Zeichen eingegeben werden.",
    maxlength : "Die Eingabe ist zu lang. Es dürfen höchstens %s Zeichen eingegeben werden.",
    length : "Die Länge der Eingabe ist ungültig. Es müssen zwischen %s und %s Zeichen eingegeben werden.",
    mincheck : "Wählen Sie mindestens %s Angaben aus.",
    maxcheck : "Wählen Sie maximal %s Angaben aus.",
    check : "Wählen Sie zwischen %s und %s Angaben.",
    equalto : "Dieses Feld muss dem anderen entsprechen."
});

Parsley.setLocale('de');

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these https methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function acceptTermsPopUp() {
    swal({
        title: 'Wir haben unsere AGB aktualisiert.',
        text: 'Bitte lese und akzeptiere unsere <a href="/agb/" target="_blank"> AGB</a>.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'AGB akzeptieren',
        cancelButtonText: 'abbrechen',
        allowOutsideClick: false,
    }).then(function () {
        setTermsAsAccepted();
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            swal({
                title: 'Abbrechen',
                html: 'Akzeptiere die AGB, um weiterhin Angebote auf gearo erstellen und buchen zu können.',
                type: 'error',
                confirmButtonText: 'Nein, danke!',
                allowOutsideClick: false,
            }).then(function () {
                location.reload();
            });
        }
    })
}

function setTermsAsAccepted() {
    $.ajax({
        url: acceptTermsUrl,
        type: "GET",
        success : function(data) {
            location.reload();
        }
    });
}

$(function(){
    $('.btn-href').click(function() {
        var redirection_target = $(this).data('href');
        if (typeof(redirection_target) !== 'undefined') {
            location.href = $(this).data('href');
        }
        return false;
    });

    $('#login button[type="submit"]').click(function(e){
        e.preventDefault();
        $.ajax({
            url : loginUrl,
            type : "POST",
            dataType: "json",
            data : { email : $('input[name="email"]').val(),
            password: $('input[name="password"]').val()},

            // handle a successful response
            success : function(data) {
                if(data.success){
                    isTermsAccepted = data.is_terms_accepted;
                    if (isTermsAccepted) {
                        location.reload();
                    } else {
                        $("#loginModal").modal("hide");
                        acceptTermsPopUp();
                    }
                }else{
                    if($("#login .errorlist li").length == 0){
                        $('#login input[name="email"]').addClass("parsley-error");
                        $("#login .errorlist").append("<li>Password und E-Mail müssen übereinstimmen - probier's nochmal.</li>");
                    }
                }
            }
        });
    });

    $('#register button[type="submit"]').click(function(e){
        var is_valid = $("#register form").parsley().validate();
        e.preventDefault();
        if(is_valid){
            $.ajax({
                url : mailcheckUrl,
                type : "POST",
                data : { data : $('#register .email').val() },

                // handle a successful response
                success : function(data, e) {
                   if(data.is_email_in_use){
                       $('#register .email').removeClass("parsley-success");
                        $('#register .email').addClass("parsley-error");
                        if($("#register #error_email .errorlist li").length == 0){
                            $("#register #error_email").append('<ul class="errorlist"><li>Es existiert bereits ein ' +
                                'Account mit dieser E-Mail-Adresse</li></ul>');
                        }
                     }else{
                         $("#register #error_email .errorlist li").detach();
                         $.ajax({

                            url : registerUrl,
                            type : "POST",
                            data : $("#register form").serialize(),

                            // handle a successful response
                            success : function(data) {
                                if(data.success){
                                    $("#registerModal").modal("hide");
                                    swal('Fast geschafft!',
                                            'Schaue in deine E-Mails und bestätige deine E-Mail-Adresse. Kontrolliere ' +
                                            'deinen Spam Ordner, wenn du keine E-Mail angezeigt bekommst.',
                                            'success');
                                     fbq('track', 'Registration');
                                } else{
                                    swal('Huch...',
                                            'Leider ist etwas schief gelaufen, versuche es einfach später noch einmal',
                                            'error');
                                }
                            },
                            error : function() {
                                swal('Huch...',
                                        'Leider ist etwas schief gelaufen, versuche es einfach später noch einmal',
                                        'error');
                            }
                         });
                   }

                }
         });
        }

    });

    $('#forgetpassword button[type="submit"]').click(function(e) {
        var is_valid = $("#forgetpassword form").parsley().validate();
        e.preventDefault();
        if(is_valid){
            $.ajax({
                url : passwordResetUrl,
                type : "POST",
                data : { email: $('#forgetpassword input[name=email]').val() },

                // handle a successful response
                success : function(data, e) {
                    if (data.success) {
                        swal('Fast geschafft!',
                            'Wir haben dir eine E-Mail zum Zurücksetzen des Passwortes geschickt. Kontrolliere deinen ' +
                            'Spam Ordner, wenn du keine E-Mail angezeigt bekommst.',
                            'success')
                    }
                    else {
                        swal('Huch...',
                             'Leider ist etwas schief gelaufen, versuche es einfach später noch einmal',
                             'error');
                    }
                },
                error : function() {
                    swal('Huch...',
                         'Leider ist etwas schief gelaufen, versuche es einfach später noch einmal',
                         'error');
                }
            });
        }
        else {
            $('#forgetpassword input').blur();
        }
    });


    $("#registerlink").click(function(e){
        e.preventDefault();
        $("#loginModal").modal("toggle");
        $("#registerModal").modal("toggle");
    });
    $("#forgetpassword").click(function(e){
        e.preventDefault();
        $("#loginModal").modal("toggle");
        $("#passwordModal").modal("toggle");
    });

    $('#registerModal').on('shown.bs.modal', function () {
        $(this).find('input[name=firstname]').focus();
    });

    $('#loginModal').on('shown.bs.modal', function () {
        $(this).find('input[name=email]').focus();
    });

    $('#passwordModal').on('shown.bs.modal', function () {
        $(this).find('input[name=email]').focus();
    });

    $(".not").click(function(e){
        e.preventDefault();
            swal({
              text: "not implemented yet",
              type: 'error'
            });
    });
    $("#insert, #resultinsert").click(function(e){
        if (isUserAuthenticated && !isTermsAccepted) {
            e.preventDefault();
            acceptTermsPopUp();
        }

        if(isUserAuthenticated && !isProfileCompleted){
            swal({
                "title": "Profilinformation",
                "html": "Bevor du dein Equipment inserieren kannst, benötigen wir noch einige wenige Angaben. " +
                "Bitte vervollständige deine <a href='" + accountEditUrl + "' target='_blank'> Profilinformationen</a>",
                "type": "warning"
            });
            e.preventDefault();
        }
    });

    var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    if (isUserAuthenticated && notificationRoom != '') {
        var notificationSocket = new RobustWebSocket(ws_scheme + '://' + window.location.host + "/ws/notification/" + notificationRoom + "/");
        notificationSocket.addEventListener('message', function(event) {
            var msg = JSON.parse(event.data);

            var selct = $(".badge");
            if (msg.type == 'add') {
                if (!selct.text()) {
                    selct.text(1);
                }
                else {
                    var newvalue = parseInt(selct.html()) + 1;
                    selct.html(newvalue);
                }
            }
            if (msg.type == 'remove') {
                if (selct.text()) {
                    selct.text(parseInt(selct.html()) - 1);
                    if (parseInt(selct.html()) == 0) {
                        selct.empty();
                    }
                }
            }
        });
    }

    $(window).on("scroll", function() {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            // do not change anything for mobile devices
        } else {
            var scrollHeight = $(document).height();
            var scrollPosition = $(window).height() + $(window).scrollTop();
            var livechatContainer = $('#livechat-compact-container');
            var livechatView = $('#livechat-compact-view');

            if ((scrollHeight - scrollPosition) === 0) {
                livechatContainer.css('bottom', '30px');
                livechatView.css('top', '10px');
            } else {
                livechatContainer.css('bottom', '0');
                livechatView.css('top', '20px');
            }
        }
    });

    $('#id_birthday').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
        locale: {
            cancelLabel: 'Clear',
        }
    });
    $('#id_birthday').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.YYYY'));
    });

});

try {
    Typekit.load();
}
catch (e) {}

$(function () {
    $('.upload').fileupload({
        url: uploadAvatarUrl,
        dataType: 'json',
         done: function(e, upload_data){
            swal({
              title: 'Bild beschneiden',
              customClass: "cropper_modal",
              html: $('<div><img id="cropper_image" src="' + upload_data.result.url + '"></div>' +
                      '<p>Sollte dein Bild nicht genau passen, benutze einfach dein Scrollrad um zu zoomen!</p>'),
              showCancelButton: true,
              closeOnConfirm: true,
              closeOnCancel: true,
              confirmButtonColor: '#343a3d',
              cancelButtonColor: '#979899'
            }).then(function() {
                var cropper_data = cropper.getData(rounded = true);
                var container_data = cropper.getContainerData();
                var data = {
                    'x': cropper_data.x,
                    'y': cropper_data.y,
                    'width': cropper_data.width,
                    'height': cropper_data.height,
                    'rotate': cropper_data.rotate,
                    'container_width': container_data.width,
                    'container_height': container_data.height,
                    'image_id': $('#cropper_image').data("imageid")
                };
                $.post(cropAvatarUrl, data, function (crop_data) {
                    if (crop_data.success) {
                        $(avatarprof).attr("src", crop_data.data.thumbnail_url);
                    }
                    else {
                        swal({
                            title: 'Fehler beim Skalieren',
                            html: "Bitte lade dein Bild erneut hoch und skaliere es nach deinen Wünschen.",
                            type: 'error',
                        });
                    }
                });
            });

            var image = document.getElementById('cropper_image');
            cropper = new Cropper(image, {aspectRatio: 1 / 1});
        }
    });

    $("#avatarprof").popover({placement: "right",
                              content:"Gestalte dein Profil so aussagekräftig wie möglich. " +
                              "Das Hinzufügen eines interessanten Profilfotos, deiner Website und deiner " +
                              "Social Media Seiten erhöhen deine Chancen für An- und Vermietungen.", trigger: "hover"});
	$("#id_business_name").popover({placement: "right",
                                    content:"Trage deine Firmierung hier ein, wenn du gewerblich tätig bist. " +
                                    "Die Firmierung erscheint auf deinen Rechnungen. So bist du rechtlich auf " +
                                    "der sicheren Seite.", trigger: "hover"});
	$("#trigger_social").popover({placement: "right",
                                  content:"Vervollständige deine Social Sharing Links. Zeige den anderen Mitgliedern " +
                                  "deine Arbeiten und deine Profile. So erhöhst du das Vertrauen und die " +
                                  "Wahrscheinlichkeit für erfolgreiche An- und Vermietungen.", trigger: "hover"});
});
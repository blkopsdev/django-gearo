window.ParsleyConfig = {
    excluded: 'input[type=button], input[type=submit], input[type=reset]',
    inputs: 'input, textarea, select, input[type=hidden], :hidden'
};
window.Parsley.addValidator('isCoordinatesSet', {
    requirementType: 'string',
    validateString: function(value, requirement) {
        return $('#id_lat').val() != '' && $('#id_lng').val() != ''
    },
    messages: {
        de: 'Bitte wähle eine Adresse aus den Vorschlägen aus'
    }
});

window.Parsley.addValidator('minTags', {
    requirementType: 'string',
    validateString: function(value, requirement) {
        return value.split(',').length >= requirement
    },
    messages: {
        de: 'Bitte füge mindestens 3 Tags hinzu'
    }
});

window.Parsley.addValidator('minAttachments', {
    requirementType: 'string',
    validateString: function(value, requirement) {
        return value.split(',').length >= requirement
    }
});
$(function(){
    $("#id_first_name").popover({
        placement: "right",
        content:"Hinterlege deine Bankdaten und wir zahlen dir die Einnahmen aus Vermietungen auf dein Bankkonto ein.",
        trigger: "focus",
    });

    $('.btn-image-id').change(function() {
        var button = $(this);
        var input = button.children('input');
        var button_label = button.children('span');

        if (input.val()) {
            button.css("background", "#999999");
            button_label.html('ausgewählt <i class="fa fa-check" aria-hidden="true"></i>');
        }
    });

    // File uploading can take some time.
    // We will show "animation" so that the user will see that work is in progress.
    $("#save_button").click(function() {
        $('.lightbox').show();
    });

});

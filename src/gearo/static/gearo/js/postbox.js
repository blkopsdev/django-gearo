$(function () {
   $('.tabs li').click(function() {
        $('.tabs li.active').removeClass('active');
        $(this).addClass('active');
        var active_tab = $(this).data('tab');

        $('.tab').hide();
        $('#' + active_tab).show();

   });
   $('.mail-body').click(function() {
        location.href = $(this).data('href');
   });
});
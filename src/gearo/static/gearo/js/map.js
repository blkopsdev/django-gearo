var destination_to_zoom_mapping = {
    100: 10,
    50: 11,
    30: 11,
    20: 11,
    15: 12,
    10: 12,
    5: 13
};

var loadedAds = {};
var map;
var infoWindow;
var markerSpiderfier;

var infoWindowTemplate = '<div class="info_content">' +
    '<a href="{{ absolute_url }}" class="noUnderline">'+
    '<div class="panel panel-gearo">'+
    '<div class="panel-heading">'+
        '<span>{{ title }}</span>'+
            '</div>'+
            '<div class="panel-body">'+
                '<img src="{{ thumbnail_url }}" alt="alt text" style="text-decoration: none;"> ' +
                '<span class="price">{{ priceday }} &euro; ' +
                '<small>' +
                '{{#include_mwst}}inkl. MwSt{{/include_mwst}}' +
                '{{^include_mwst}}Mwst. frei.{{/include_mwst}}' +
                '</small>' +
                '</span>'+
            '</div>'+
            '</div>'+
    '</a>'+
    '</div>';

function renderInfoWindow(marker, data) {
    var content = Mustache.render(infoWindowTemplate, data);
    infoWindow.setContent(content);
    infoWindow.open(map, marker);
    // Reference to the DIV which receives the contents of the infowindow using jQuery
    var iwOuter = $('.gm-style-iw');

    /* The DIV we want to change is above the .gm-style-iw DIV.
     * So, we use jQuery and create a iwBackground variable,
     * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
     */
    var iwBackground = iwOuter.prev();

    // Remove the background shadow DIV
    iwBackground.children(':nth-child(2)').css({'display': 'none'});

    // Remove the white background DIV
    iwBackground.children(':nth-child(4)').css({'display': 'none'});

    // Taking advantage of the already established reference to
    // div .gm-style-iw with iwOuter variable.
    // You must set a new variable iwCloseBtn.
    // Using the .next() method of JQuery you reference the following div to .gm-style-iw.
    // Is this div that groups the close button elements.
    var iwCloseBtn = iwOuter.next();

    // Apply the desired effect to the close button
    iwCloseBtn.css({
        right: '60px', top: '25px', // button repositioning
    });

    // The API automatically applies 0.7 opacity to the button after the mouseout event.
    // This function reverses this event to the desired value.
    iwCloseBtn.mouseout(function () {
        $(this).css({opacity: '1'});
    });
}

function initMap(latitude, longitude, distance) {
    var mapOptions = {mapTypeId: 'roadmap'};
    if (latitude != '' && longitude != '') {
        mapOptions['center'] = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
        if (distance != '') {
            distance = parseInt(distance);
            mapOptions['zoom'] = destination_to_zoom_mapping[distance];
        }
        else {
            mapOptions['zoom'] = 13;
        }
    }
    else {
        mapOptions['center'] = {lat: 51.165691, lng: 10.451526000000058};
        mapOptions['zoom'] = 5;
    }

    // Display a map on the page
    map = new google.maps.Map(document.getElementById("resultmap"), mapOptions);

    infoWindow = new google.maps.InfoWindow();
    markerSpiderfier = new OverlappingMarkerSpiderfier(map);

    var markers = [];

    for (var idx in ads) {
        var ad = ads[idx];
        var marker = new google.maps.Marker({
            position: {'lat': ad.lat, 'lng': ad.lng},
            map: map,
            icon: image,
            adId: ad.pk
        });

        markers.push(marker);
        markerSpiderfier.addMarker(marker);
    }

    markerSpiderfier.addListener('click', function(marker, e) {
        var adId = marker.adId;
            if (typeof(loadedAds[adId]) == 'undefined') {
                var adDetailUrl = Urls.addetail(marker.adId);
                $.get(adDetailUrl, function (data) {
                    renderInfoWindow(marker, data);
                    loadedAds[adId] = data;
                });
            }
            else {
                renderInfoWindow(marker, loadedAds[adId]);
            }

    });

    markerSpiderfier.addListener('spiderfy', function(markers) {
        infoWindow.close();
    });


    var markerCluster = new MarkerClusterer(map, markers, {imagePath: imagePath});
    markerCluster.setMaxZoom(15);

}
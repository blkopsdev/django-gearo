var $sections;
var isUndamaged;
var nextStepIdx;
var previousStepIdx;
var isOfferViewed = false;
var isEquipmentChecked = false;

$(function(){
    $('button.btn-protocol-next, .btn-no, .btn-yes').click(function() {
        if ($(this).hasClass('btn-yes')) {
            $('.has-no-defects').show();
            $('.has-defects').hide();
            $('input[name=undamaged]').val('True');
            $('#id_defects').hide();
            $('#id_defects').removeAttr('required');
            $('#attachments_field').removeAttr('required');
            $('#undamaged_message').show();
            $('#damaged_message').hide();
            $('.protocol-step5 .btn-protocol-next').text('Bestätigungsklausel lesen');
            $('.protocol-step5 .btn-protocol-next').removeClass('damaged').addClass('undamaged');
            $('.protocol-step5 .btn-protocol-next').removeClass('invisible');

            isUndamaged = true;
        }
        if ($(this).hasClass('btn-no')) {
            $('.has-defects').show();
            $('.has-no-defects').hide();
            $('input[name=undamaged]').val('False');
            $('#id_defects').show();
            $('#id_defects').attr('required', '');
            $('#attachments_field').attr('required', '');
            $('#undamaged_message').hide();
            $('#damaged_message').show();
            $('.protocol-step5 .btn-protocol-next').text('Schäden fotografieren');
            $('.protocol-step5 .btn-protocol-next').removeClass('undamaged').addClass('damaged');
            $('.protocol-step5 .btn-protocol-next').addClass('invisible');
            isUndamaged = false;
        }
        if ($('#protocolcontent form').parsley().validate({group : 'block-' + curIndex()})) {
            var currentIdx = curIndex();
            if (currentIdx == 8 && signaturePadLessor.isEmpty()) {
                $('#lessor_signature_errors').show();
                return false;
            }
            if (currentIdx == 10 && signaturePadApplicant.isEmpty()) {
                $('#applicant_signature_errors').show();
                return false;
            }

            $('#lessor_signature_errors').hide();
            $('#applicant_signature_errors').hide();
            if (currentIdx == 6) {
                nextStepIdx = currentIdx + 2;
            }
            else if (currentIdx == 4 && isUndamaged) {
                nextStepIdx = currentIdx + 3;
            }
            else {

                nextStepIdx = currentIdx + 1;
            }
            $sections.removeClass('current').eq(nextStepIdx).addClass('current');
            $('body').removeClass();
            $('body').addClass('step' + (nextStepIdx + 1));
            updateProgressBar();
        }
    });

    $('.btn-protocol-back').click(function() {
        var currentIdx = curIndex();
        if ((currentIdx == 6 && isUndamaged) || (currentIdx == 8 && !isUndamaged)) {
            previousStepIdx = currentIdx - 2;
        }
        else if ((currentIdx == 7 && isUndamaged)) {
            previousStepIdx = currentIdx - 3;
        }
        else {
            previousStepIdx = currentIdx - 1;
        }
        $sections.removeClass('current').eq(previousStepIdx).addClass('current');
        $('body').removeClass();
        $('body').addClass('step' + (previousStepIdx + 1));
        updateProgressBar();
    });

    $('.btn-check-offer').click(function() {
        isOfferViewed = true;
        $(this).addClass('checked');
    });

    $('.btn-check-equipment').click(function (e) {
        if (!isOfferViewed) {
            return false;
        }
        isEquipmentChecked = true;
        $(this).addClass('checked');
        $(this).html('bestätigt <i class="fa fa-check-circle-o" aria-hidden="true"></i>');
        $('.protocol-step6 .btn-protocol-next').removeClass('invisible');
    });
});
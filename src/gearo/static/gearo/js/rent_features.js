$(function() {
    var rent_equipment_value_slider = $('#rent_equipment_value_slider').bootstrapSlider().on('change', function (e) {
        $('#rent_equipment_value_current').html(e.value.newValue + ' €');
        calculate_yearly_income();
    });

    var rent_duration_slider = $('#rent_duration_slider').bootstrapSlider().on('change', function (e) {
        $('#rent_duration_current').html(e.value.newValue + ' Tage');
        calculate_yearly_income();
    });

    var calculate_yearly_income = function() {
        var equipment_value = rent_equipment_value_slider.bootstrapSlider('getValue');
        var duration = rent_duration_slider.bootstrapSlider('getValue');
        var rent_yearly_income = equipment_value * duration * 0.017 * 12;
        $('#rent_yearly_income').html(rent_yearly_income.toFixed(0) + ' €');
    };
});
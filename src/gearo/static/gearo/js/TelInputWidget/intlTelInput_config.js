$("#id_phone_mobile").intlTelInput({
      initialCountry: "de",
      onlyCountries: allowedCountries,  // from base.html
      // separateDialCode: true,
      nationalMode: false,
      utilsScript: utilsScript,  // from base.html
});
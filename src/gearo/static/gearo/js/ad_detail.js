$(function(){
    initAutocomplete();
    $('.slider').bxSlider({pager: false, mode: "fade", auto: true, captions: true});
    $("#startdate").popover({
        placement: "left",
        content: startDatePopoverContent,
        trigger: "manual"
    });
    $("#enddate").popover({
        placement: "left",
        content: endDatePopoverContent,
        trigger: "manual"
    });

    $("#applybooking").click(function(e){

        if (isUserAuthenticated && !isTermsAccepted) {
            e.preventDefault();
            acceptTermsPopUp(); // from `main.js`
        }

        if(!$("#id_startdate").val().length > 0){
            $("#startdate").popover("toggle");
            e.preventDefault();
        }
        if(!$("#id_enddate").val().length > 0){
            $("#enddate").popover("toggle");
            e.preventDefault();
        }

        if(!isProfileCompleted){
            swal({
                "title": "Profilinformation",
                "html": "Du musst deinen <a href='" + accountEditUrl + "' target='_blank'> Account</a>  vervollständigen bevor du etwas anmieten kannst.",
                "type": "warning"
            });
            e.preventDefault();
        }
    });

    $('.more-qa').click(function(e){
        e.preventDefault();

        if ($('.question.hide').length > 0){
            $(this).text("Weniger Fragen und Antworten anzeigen");
            $('.question.hide').removeClass('hide');
        }else{
            $('.question:gt(2)').addClass('hide');
            $(this).addClass("questions-hidden");
            $(this).text("Weitere Fragen und Antworten lesen");
        }
    });

    function calcRent(start, end ){
        $.ajax({
            url: calcRentUrl,
            type: "POST",
            data: {
                ad_id : adId,
                start_date: start.format("YYYY-MM-DD"),
                end_date: end.format("YYYY-MM-DD"),
                pickup_after_3pm: $('#id_pickup_after_3pm:checked').val(),
                return_before_12pm: $('#id_return_before_12pm:checked').val()
            },

            // handle a successful response
            success : function(data) {
                if (data.success) {
                    var rent_label = '';
                    if (data.duration == 1) {
                        rent_label = '1 Tag';
                    }
                    else {
                        rent_label = data.duration + ' Tage';
                    }
                    rent_label += ' Miete';
                    $("#rentlabel").text(rent_label);
                    $("#rentdetail").text(data.daily_rent_price + " €");
                    if (data.long_term_benefit) {
                        $("#long_term_benefit_value").text("- " + data.long_term_benefit + " €");
                        $(".long_term_benefit").removeClass("hide");
                    }
                    else {
                        $(".long_term_benefit").addClass("hide");
                    }
                    $("#insurancedetail").text(data.insurance + " €");
                    $("#totaldetail").text(data.total + " €");
                }
            }
        });

    }

    $('#id_pickup_after_3pm, #id_return_before_12pm').change(function() {
        var startDate = $("#id_startdate").val();
        var endDate = $("#id_enddate").val();
        if (startDate != '' && endDate != '') {
            var curStart = moment(startDate, "DD.MM.YYYY");
            var curEnd = moment(endDate, "DD.MM.YYYY");
            calcRent(curStart, curEnd);
        }
    });

    var initStart;
    var initEnd;

    setInterval(function(){
        var startDate = $("#id_startdate").val();
        var endDate = $("#id_enddate").val();

        if(startDate && endDate){
            var curStart = moment(startDate, "DD.MM.YYYY");
            var curEnd = moment(endDate, "DD.MM.YYYY");

            // check if start date isn't before today
            if(curEnd.isBefore(curStart, "day")){
                // add hint... maybe
                return;
            }


            // check if end date isn't before start date
            if(curStart.isBefore(moment(), "day")){
                // add hint... maybe
                return;
            }

            // set init values on first call
            if(initStart === undefined || initEnd === undefined ){
                initStart = curStart;
                initEnd = curEnd;

                // calculate new rent and insurance
                calcRent(curStart, curEnd );
            }

            // check if user change values
            if(!initStart.isSame(curStart) || !initEnd.isSame(curEnd)){
                initStart = curStart;
                initEnd = curEnd;

                // calculate new rent and insurance
                calcRent(curStart, curEnd );
            }

        }
    }, 1000);

    var datepickerRent = new DatepickerRent({
        container: $("#observe"),
        autoclose: true,
        notAvailable: notAvailable
    });

    datepickerRent.init();
    $("#id_startdate").val('');
    $("#id_enddate").val('');

    $("a#advertisement_remove").click(function(e){
        e.preventDefault();
        swal({
            title: 'Angebot löschen',
            html: "Möchtest du dieses Angebot wirklich löschen? Alle bis jetzt angenommenen Buchungen werden trotzdem aktiv bleiben.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#343a3d',
            cancelButtonColor: '#979899',
            confirmButtonText: 'ja',
            cancelButtonText: 'nein',
            closeOnConfirm: true
        }).then(function () {
            $.ajax({
                url: adRemoveUrl,
                type: "POST",
                data: {id: adId},

                // handle a successful response
                success : function(data) {
                    if (data.success) {
                        window.location = searchResultsUrl;
                    }
                    else {
                        swal('Huch...',
                         'Leider konnte dein Inserat nicht gelöscht werden. Versuche es bitte noch einmal. Sollte das Problem weiterhin bestehen, schreibe uns eine Mail!',
                          'error');
                    }
                },
                // handle a non-successful response
                error : function() {
                    swal('Huch...',
                         'Leider konnte dein Inserat nicht gelöscht werden. Versuche es bitte noch einmal. Sollte das Problem weiterhin bestehen, schreibe uns eine Mail!',
                          'error');
                }
            });
        });
    });

    $("#ad_question_button").click(function(){
        var is_valid = $('.question-form').parsley().validate();
        if (is_valid) {
            $.ajax({
                url: askQuestionUrl,
                type: "POST",
                data: {question: $('#id_question').val()},

                // handle a successful response
                success: function (data) {
                    if (data.success) {
                        $('#id_question').val(''); // remove the value from the input
                        swal('Frage gestellt',
                            adOwner + ' hat deine Frage erhalten, wir informieren dich, sobald sie beantwortet wurde. ',
                            'success');
                    }
                    else {
                        swal('Huch...',
                             'Leider konnten wir deine Frage nicht stellen, versuche es einfach später noch einmal',
                             'error');
                    }
                },

                // handle a non-successful response
                error: function () {
                    swal('Huch...',
                        'Leider konnten wir deine Frage nicht stellen, versuche es einfach später noch einmal',
                        'error');
                }
            });
        }
        else {
            $("#id_question").blur();
        }
        return false;
    });

    $(".reply form").each(function(form){
        $(this).find("#ad_answer_button").click(function(){
            var selector = $(this).offsetParent().offsetParent().offsetParent().offsetParent();
            var submitterForm = $(this).closest("form");
            var is_valid = submitterForm.parsley().validate();
            if (is_valid) {
                $.ajax({
                    url: submitterForm.attr("action"),
                    type: "POST",
                    data: {answer: submitterForm.find('.answerField').val()},

                    // handle a successful response
                    success: function (answer) {
                        if (answer.success) {
                            $('#answer').val(''); // remove the value from the input

                            selector.modal("hide");
                            selector.prev().fadeOut();

                            $('<div>\
                            <table> \
                                <tr> \
                                    <td>' + answer.questioner + '</td> \
                                    <td>' + answer.question + '</td> \
                                </tr> \
                                <tr> \
                                    <td>' + answer.answer + '</td> \
                                    <td>' + answer.answer + '</td> \
                                </tr>' +
                                ' </table> \
                            </div>').prependTo("#questions");

                            swal('Frage beantwortet',
                                'Deine Antwort wurde eingetragen und ' + answer.questioner + ' wurde benachrichtigt.',
                                'success');
                        }
                        else {
                            swal('Huch...',
                                 'Leider konnten wir deine Antwort nicht speichern, versuche es einfach später noch einmal',
                                 'error');
                        }
                    },

                    // handle a non-successful response
                    error: function () {
                        swal('Huch...',
                            'Leider konnten wir deine Antwort nicht speichern, versuche es einfach später noch einmal',
                            'error');
                    }
                });
            }
            else {
                $(this).blur();
            }
            return false;
        });
    });

});

function initAutocomplete(){
    var options = {
        types: ['geocode'],
        componentRestrictions: {country: allowedCountries},
    };
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('searchplace')), options);
    initMap();
}

function initMap() {
    var zoom = 10;

    var map = new google.maps.Map(document.getElementById('admap'), {
      zoom: zoom,
      center: LatLng
    });

    var marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        icon: imageUrl
    });

}

$(function(){
    $('#clock').countdown(new Date(responseLimit), function(event) {
        $(this).html(event.strftime(' %H Stunden %M Minuten %S Sekunden'));
    }).on('finish.countdown', function(event) {
        $("button#accept").prop("disabled", true);
        location.href = showInquiryUrl;
    });
    $("button#reject").click(function(e){
        e.preventDefault();
        swal({
            title: 'Bist du sicher?',
            html: "Du kannst das Angebot danach nicht mehr annehmen.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#343a3d',
            cancelButtonColor: '#979899',
            confirmButtonText: 'ablehnen',
            cancelButtonText: 'abbrechen',
            closeOnConfirm: true
        }).then(function() {
            window.location = rejectLessorUrl;
        })
    });
});
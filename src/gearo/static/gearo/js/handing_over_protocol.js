var $sections;
var isUndamaged;
var nextStepIdx;
var previousStepIdx;
var isOfferViewed = false;
var isEquipmentChecked = false;

$(function(){
    $('button.btn-protocol-next, .btn-no, .btn-yes').click(function() {
        if ($(this).hasClass('btn-yes')) {
            $('.has-no-defects').show();
            $('.has-defects').hide();
            $('input[name=undamaged]').val('True');
            $('.protocol-step5 .btn-protocol-next').removeClass('invisible');
            $('#id_defects').attr('placeholder', 'Besonderheiten und Notizen hier eintragen');
            $('.protocol-step5 h4').text('Trage Besonderheiten oder Notizen hier ein.');
            isUndamaged = true;
        }
        if ($(this).hasClass('btn-no')) {
            $('.has-defects').show();
            $('.has-no-defects').hide();
            $('input[name=undamaged]').val('False');
            $('.protocol-step5 .btn-protocol-next').addClass('invisible');
            $('#id_defects').attr('placeholder', 'Mängel oder Besonderheiten hier eintragen');
            $('.protocol-step5 h4').text('Trage Mängel, Besonderheiten und Notizen hier ein.');
            isUndamaged = false;
        }
        if ($('#protocolcontent form').parsley().validate({group : 'block-' + curIndex()})) {
            var currentIdx = curIndex();
            if (currentIdx == 10 && signaturePadLessor.isEmpty()) {
                $('#lessor_signature_errors').show();
                return false;
            }
            if (currentIdx == 11 && signaturePadApplicant.isEmpty()) {
                $('#applicant_signature_errors').show();
                return false;
            }

            $('#lessor_signature_errors').hide();
            $('#applicant_signature_errors').hide();
            nextStepIdx = currentIdx + 1;
            $sections.removeClass('current').eq(nextStepIdx).addClass('current');
            $('body').removeClass();
            $('body').addClass('step' + (nextStepIdx + 1));
            updateProgressBar();
        }
    });

    $('.btn-protocol-back').click(function() {
        var currentIdx = curIndex();
        previousStepIdx = currentIdx - 1;
        $sections.removeClass('current').eq(previousStepIdx).addClass('current');
        $('body').removeClass();
        $('body').addClass('step' + (previousStepIdx + 1));
        updateProgressBar();
    });

    $('.btn-check-offer').click(function() {
        isOfferViewed = true;
        $(this).addClass('checked');
    });

    $('.btn-check-equipment').click(function (e) {
        isEquipmentChecked = true;
        $(this).addClass('checked');
        $(this).html('bestätigt <i class="fa fa-check-circle-o" aria-hidden="true"></i>');
        $('.protocol-step6 .btn-protocol-next').removeClass('invisible');
    });
});
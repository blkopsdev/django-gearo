function hideTaxField() {
    $(".hide-for-at").hide();
    $('[name=tax_ident_number]').attr('placeholder', "UID-Nummer");
}


function showTaxField() {
    $(".hide-for-at").show();
    $('[name=tax_ident_number]').attr('placeholder', "Umsatzsteuer – Identifikationsnummer");
}


$(function(){

    // Updating MP user data can take some time.
    // We will show "animation" so that the user will see that work is in progress.
    $("#save_button").click(function() {
        $('.lightbox').show();
    });

    $("#verifysms, #resend").click(function(e){
        e.preventDefault();
        $.ajax({
            url : sendVerificationUrl,
            type : "POST",
            data : {"phone_number": $("#id_phone_mobile").val()},

            success : function(data) {
                if(data.success){
                    location.reload();
                }
                else {
                    swal('Huch...',
                         'Leider ist etwas schief gelaufen, versuche es einfach später noch einmal',
                         'error');
                }
            },
            error : function() {
                swal('Huch...',
                     'Leider ist etwas schief gelaufen, versuche es einfach später noch einmal',
                     'error');
            }
         });
    });


    $("#applycode").click(function(e){
        e.preventDefault();
        $.ajax({

            url : checkVerificationUrl,
            type : "POST",
            data : {"token": $("#verify").val()},

            // handle a successful response
            success : function(data) {
                if(data.success){
                    location.reload();
                }else{
                    if($("#verifyerror ul li").length == 0){
                        $("#verifyerror ul").append("<li>Ungültiger Code.</li>");
                    }
                }

            },
            error : function() {
                swal('Huch...',
                     'Leider ist etwas schief gelaufen, versuche es einfach später noch einmal',
                     'error');
            }
         });
    });


    $(".onoffswitch-label").click(function(){
		if ($(this).prev().is(':checked')){
			$(this).parent().parent().next().children().val("");
		}
		$(this).parent().parent().next().slideToggle("slow");
	});


    $("#id_phone_mobile").popover({
        placement: "right",
        content: "Durch eine verifizierte Telefonnummer schaffst du Vertrauen bei Mietern " +
                 "und Vermietern. Du musst deine Mobilnummer hinterlegen um inserieren und " +
                 "mieten zu können.",
        trigger: "focus",
    });

    $("#id_street").popover({
        placement: "right",
        content: "Trage deine vollständige Adresse ein. Sie wird aus Sicherheitsgründen und zur " +
                 "Rechnungserstellung bei Vermietungen benötig. Sollte deine Rechnungsadresse " +
                 "abweichen kannst du sie hinzufügen.",
        trigger: "focus",
    });

    $("#id_tax_number").popover({
        placement: "right",
        content: "Du betreibst ein Gewerbe? Dann trage hier einfach deine Steuernummer ein und " +
                 "wir weisen sie auf deiner Rechnung an den Vermieter aus. Sollen wir zusätzlich " +
                 "dazu noch die Mwst. ausweisen, lege einfach den Schalter dazu um.",
        trigger: "hover",
    });


    // Hide useless field for Austria when document is ready.
    // When "Rechnungsadresse" checked and selected country in it is "AT"
    // or selected country in "Adresse" is "AT"
    if ( ($("#billonoffswitch").is(":checked") && $('[name=country_bill]').val() == "AT") || $('[name=country]').val() == "AT"){
        hideTaxField();
    }

    // Hide or show tax field when selected country changed
    $("[name=country], [name=country_bill], #billonoffswitch").change(function(){
        var countryCode;
        if ($("#billonoffswitch").is(":checked")) {
            countryCode = $("[name=country_bill]").val();
        } else {
            countryCode = $("[name=country]").val();
        }
        if (countryCode == "AT") {
            hideTaxField();
        } else {
            showTaxField();
        }
    });


    // "Berechnest du Umsatzsteuer?" should be clickable only if
    // "Bist du Gewerbetreibender oder Freiberufler" is already checked.
    if (!$("#traderonoffswitch").is(":checked")) {
        $('#calctaxonoffswitch').removeAttr('checked');
        $('#calctaxonoffswitch').prop( "disabled", true );
    }

    $("#traderonoffswitch").change(function(){
        if ($(this).is(":checked")) {
            $('#calctaxonoffswitch').prop( "disabled", false );
        } else {
            if ($('#calctaxonoffswitch').is(':checked')) {
                $('#calctaxonoffswitch').removeAttr('checked');
            }
            $('#calctaxonoffswitch').prop( "disabled", true );
        };
    });


    $("label[for='calctaxonoffswitch']").hover(function(){
        if ($(this).prev().is(":disabled")) {
            $(this).popover({
                placement: "right",
                content: "Wenn du Umsatzsteuer berechnest, aktiviere zunächst " +
                         "'Bist du Gewerbetreibender oder Freiberufler' und gib dort deine Daten an.",
                trigger: "hover",
            });
        } else {
            $(this).popover("destroy");
        }
    });

});

try {
    Typekit.load();
} catch (e) {
}

Parsley.addMessages('de', {
    defaultMessage : "Die Eingabe scheint nicht korrekt zu sein.",
    type : {
        email : "Die Eingabe muss eine gültige E-Mail-Adresse sein.",
        url : "Die Eingabe muss eine gültige URL sein.",
        number : "Die Eingabe muss eine Zahl sein.",
        integer : "Die Eingabe muss eine Zahl sein.",
        digits : "Die Eingabe darf nur Ziffern enthalten.",
        alphanum : "Die Eingabe muss alphanumerisch sein."
    },
    notblank : "Die Eingabe darf nicht leer sein.",
    required : "Dies ist ein Pflichtfeld.",
    pattern : "Die Eingabe scheint ungültig zu sein.",
    min : "Die Eingabe muss größer oder gleich %s sein.",
    max : "Die Eingabe muss kleiner oder gleich %s sein.",
    range : "Die Eingabe muss zwischen %s und %s liegen.",
    minlength : "Die Eingabe ist zu kurz. Es müssen mindestens %s Zeichen eingegeben werden.",
    maxlength : "Die Eingabe ist zu lang. Es dürfen höchstens %s Zeichen eingegeben werden.",
    length : "Die Länge der Eingabe ist ungültig. Es müssen zwischen %s und %s Zeichen eingegeben werden.",
    mincheck : "Wählen Sie mindestens %s Angaben aus.",
    maxcheck : "Wählen Sie maximal %s Angaben aus.",
    check : "Wählen Sie zwischen %s und %s Angaben.",
    equalto : "Dieses Feld muss dem anderen entsprechen."
});

Parsley.setLocale('de');

//using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function initAutocomplete() {
    var options = {
        types: ['geocode'],
        componentRestrictions: {country: allowedCountries}  // from index.html
    };
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('searchplace')), options);
    autocomplete.addListener('place_changed',fillGeo);

}

function fillGeo(){
    var place = autocomplete.getPlace();

    document.getElementById("lat").value = place.geometry.location.lat();
    document.getElementById("lng").value = place.geometry.location.lng();
}

$(function(){
    initAutocomplete();
    $('.play-button > a').click(function() {
        $('#lightbox_container').html('<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/N16l60bhWSs?autoplay=1" frameborder="0" allowfullscreen></iframe>');
        $('#lightbox').show();
        adjustLightboxTopMargin();
        $('body').css('overflow', 'hidden');
    });
    $(window).resize(function() {
        adjustLightboxTopMargin();
    });
    $('.lightbox').click(function (e) {
        $('#lightbox_container').empty();
        $(this).hide();
        $('body').css('overflow', 'auto');
    });
    $('.lightbox.close-button').click(function() {
        $('#lightbox_container').empty();
        $('.lightbox').hide();
        $('body').css('overflow', 'auto');
        return false;
    });
    $(document).keyup(function(e) {
      if (e.keyCode === 27) {
          $('#lightbox_container').empty();
          $('.lightbox').hide();
          $('body').css('overflow', 'auto');
      }
    });

    $("#howitworks div a").click(function() {
        $('html, body').animate({
            scrollTop: $("#box-features").offset().top
        }, 2000);
    });
});

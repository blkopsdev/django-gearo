var DatepickerHelpers = function() {};
DatepickerHelpers.createObject = Object.create || function(a) {
    var b = function() {};
    return b.prototype = a, new b
};
var DatepickerModel = function(a) {
    this.$box = a.container, this.notAvailable = "undefined" != typeof a.notAvailable ? a.notAvailable : [], this.dateString = "dd.mm.yyyy"
};
DatepickerModel.prototype.init = function() {
    this.attachHandlers(), this.attachTriggers()
}, DatepickerModel.prototype.getMomentDatestring = function() {
    return this.dateString.toUpperCase()
}, DatepickerModel.prototype.generateSingleDates = function(a) {
    var b = this.getMomentDatestring(),
        c = [];
    return $.each(a, function() {
        var a = this,
            d = a.split(" - ");
        if ("undefined" == typeof d[1]) {
            var e = moment(d[0], b);
            return c.push(e.format(b)), !0
        }
        var f = moment(d[0], b),
            g = moment(d[1], b),
            h = moment.range(f, g);
        h.by("days", function(a) {
            c.push(a.format(b))
        })
    }), c
}, DatepickerModel.prototype.generateNotAvailableDates = function() {
    return this.generateSingleDates(this.notAvailable)
};
var DatepickerCreate = function(a) {
    DatepickerModel.call(this, a), this.editable = "undefined" != typeof a.editable ? a.editable : !0, this.$container = !1, this.dates = []
};
DatepickerCreate.prototype = DatepickerHelpers.createObject(DatepickerModel.prototype),
                             DatepickerCreate.prototype.constructor = DatepickerModel,
                             DatepickerCreate.prototype.attachHandlers = function() {},
                             DatepickerCreate.prototype.attachTriggers = function() {
    this.updateDatesFromInput(), this.addContainer(), this.addList(), this.addCalendar(), this.updateList()
}, DatepickerCreate.prototype.getDatesFromInput = function() {
    var a = this;
    var totalForms = $('#id_unavailable_periods-TOTAL_FORMS').val();
    var dates = [];
    for (var i=0; i<totalForms; i++) {
        var startDateString = $('#id_unavailable_periods-' + i + '-start').val();
        var endDateString =  $('#id_unavailable_periods-' + i + '-end').val();
        if (startDateString && endDateString) {
            var startDate = moment(startDateString).format(a.getMomentDatestring());
            var endDate = moment(endDateString).format(a.getMomentDatestring());
            if (startDate != endDate) {
                var dateRangeString = startDate + " - " + endDate;
            }
            else {
                var dateRangeString = startDate;
            }
            dates.push({'row': i, 'dateString': dateRangeString});
        }
    }
    return dates;
}, DatepickerCreate.prototype.updateDatesFromInput = function() {
    var a = this.getDatesFromInput(),
        b = [];
    return $.each(a, function() {
        var a = this.dateString.toString();
        return "" === a ? !0 : void b.push({
            initial: !0,
            value: a,
            row: this.row
        })
    }), this.dates = b, !0

}, DatepickerCreate.prototype.getOnlyDates = function() {
    var a = [];
    return $.each(this.dates, function() {
        a.push(this.value)
    }), a
}, DatepickerCreate.prototype.updateDatesInInput = function() {
    var a = this.getOnlyDates();
    return this.$box.val(a.join(", ")), !0
}, DatepickerCreate.prototype.updateDates = function() {
    return this.updateList(), this.updateDatesInInput(), !0
}, DatepickerCreate.prototype.addContainer = function() {
    return this.$container = $("<div />"), this.$container.insertAfter(this.$box), !0
}, DatepickerCreate.prototype.getContainer = function() {
    return this.$container
}, DatepickerCreate.prototype.addList = function() {
    var a = this.getContainer();
    return $("<div />").addClass("list").appendTo(a), !0
}, DatepickerCreate.prototype.getList = function() {
    var a = this.getContainer();
    return a.find(".list")
}, DatepickerCreate.prototype.updateList = function() {
    var a = this,
        b = this.getList();
    b.empty();
    var c = $("<ul />").appendTo(b);
    $.each(this.dates, function() {
        var d = this,
            e = $("<li />").data('rowid', d.row).appendTo(c);
        if ($("<span />").addClass("text").text(d.value).appendTo(e), !d.initial || a.editable && d.initial) {
            var f = $("<span />").addClass("delete").html("löschen").appendTo(e);
            f.on("click", function() {
                if (b.hasClass("disabled")) return !1;
                var rowId = $(this).parent('li').data('rowid');
                $('#id_unavailable_periods-' + rowId + '-DELETE').prop('checked', true);
                var c = [];
                $.each(a.dates, function() {
                    var a = this;
                    return a.value === d.value ? !0 : void c.push(a)
                }), a.dates = c, a.updateDates()
            })
        }
    });
    var d = $("<div />").addClass("actions").appendTo(b),
        e = $("<div />").addClass("action-new").text("Neuen Zeitraum hinzufügen").appendTo(d);
    return e.on("click", function() {
        return b.hasClass("disabled") ? !1 : void a.addModule()
    }), !0
}, DatepickerCreate.prototype.addCalendar = function() {
    var a = this.getContainer();
    return $("<div />").addClass("calendar").appendTo(a), !0
}, DatepickerCreate.prototype.getCalendar = function() {
    var a = this.getContainer();
    return a.find(".calendar")
}, DatepickerCreate.prototype.generateStringFromDates = function(a, b) {
    "undefined" == typeof b && (b = a);
    var c = moment(a),
        d = moment(b);
    if (!c.isValid() || !d.isValid()) return !1;
    var e = this.getMomentDatestring(),
        f = c.format(e),
        g = d.format(e);
    return f === g ? f : f + " - " + g
}, DatepickerCreate.prototype.getDatesDisabled = function() {
    var a = this.generateNotAvailableDates(),
        b = this.generateSingleDates(this.getOnlyDates());
    return a.concat(b)
}, DatepickerCreate.prototype.generateStringsFromSingleDateArray = function(a) {
    var b = this,
        c = this.getMomentDatestring(),
        d = !1,
        e = [];
    return $.each(a, function(f) {
        var g = this,
            h = moment(g, c);
        d || (d = h);
        var i = a[f + 1];
        if ("undefined" == typeof i) {
            var j = b.generateStringFromDates(d, h);
            return e.push(j), !1
        }
        var k = moment(g, c);
        k.add(1, "days");
        var l = b.generateStringFromDates(k);
        if (i === l) return !0;
        var j = b.generateStringFromDates(d, h);
        return e.push(j), d = !1, !0
    }), e
}, DatepickerCreate.prototype.addModule = function() {
    var a = this,
        b = this.getList();
    b.addClass("disabled");
    var c = this.getCalendar(),
        d = $("<div />").addClass("range").appendTo(c);
    $("<div />").addClass("range-start").appendTo(d).hide(), $("<div />").addClass("range-end").appendTo(d), d.datepicker({
        language: "de",
        inputs: $(".range-start, .range-end"),
        clearBtn: !0,
        format: this.dateString,
        datesDisabled: this.getDatesDisabled(),
        forceParse: !0
    });
    var e = $("<div />").addClass("save").text("speichern").appendTo(c),
        f = d.find(".range-start").datepicker(),
        g = d.find(".range-end").datepicker();
    e.on("click", function() {
        var d = f.datepicker("getDate"),
            e = g.datepicker("getDate");
        if (d && e) {
            var h = a.generateStringFromDates(d, e),
                i = a.generateSingleDates([h]),
                j = a.getDatesDisabled(),
                k = [];
            $.each(i, function() {
                var a = this.toString();
                return -1 !== $.inArray(a, j) ? !0 : void k.push(a)
            });
            var totalForms = $('#id_unavailable_periods-TOTAL_FORMS').val();
            totalForms = parseInt(totalForms);
            var startFieldName = "unavailable_periods-" + totalForms + "-start";
            var startFieldId = "id_" + startFieldName;
            var endFieldName = "unavailable_periods-" + totalForms + "-end";
            var endFieldId = "id_" + endFieldName;
            var deleteFieldName = "unavailable_periods-" + totalForms + "-DELETE";
            var deleteFieldId = "id_" + deleteFieldName;
            var startFieldValue = moment(d).format('YYYY-MM-DD');
            var endFieldValue = moment(e).format('YYYY-MM-DD');
            $('#schedule_formset').append('<input id="'+ startFieldId +'" name="'+ startFieldName +'" type="text" value="' + startFieldValue +'">');
            $('#schedule_formset').append('<input id="'+ endFieldId +'" name="'+ endFieldName +'" type="text" value="' + endFieldValue +'">');
            $('#schedule_formset').append('<input id="'+ deleteFieldId +'" name="'+ deleteFieldName +'" type="checkbox">');

            totalForms += 1;
            $('#id_unavailable_periods-TOTAL_FORMS').val(totalForms);

            var l = a.generateStringsFromSingleDateArray(k);
            l.length > 0 && ($.each(l, function() {
                a.dates.push({
                    initial: !1,
                    value: this,
                    row: totalForms
                })
            }), a.updateDates())
        }
        c.empty(), b.removeClass("disabled")
    });
    var h = $("<div />").addClass("abort").text("abbrechen").appendTo(c);
    return h.on("click", function() {
        c.empty(), b.removeClass("disabled")
    }), !0
};
var DatepickerRent = function(a) {
    DatepickerModel.call(this, a)
};
DatepickerRent.prototype = DatepickerHelpers.createObject(DatepickerModel.prototype),
                           DatepickerRent.prototype.constructor = DatepickerModel,
                           DatepickerRent.prototype.attachHandlers = function() {},
                           DatepickerRent.prototype.attachTriggers = function() {
    this.addModule()
}, DatepickerRent.prototype.addModule = function() {
    var a = this,
        b = this.$box.find(".datepicker-field");
    b.each(function() {
        var b = $(this);
        return "" !== b.val() ? !0 : void b.val(moment().format(a.getMomentDatestring()))
    }), this.$box.datepicker({
        inputs: b,
        format: this.dateString,
        language: "de",
        autoclose: true,
        startDate: "0",
        datesDisabled: this.generateNotAvailableDates(),
        forceParse: !0
    })
};
from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin.utils import unquote
from django.core.urlresolvers import reverse
from django.db import models
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.safestring import mark_safe

from easy_thumbnails.fields import ThumbnailerField
from easy_thumbnails.widgets import ImageClearableFileInput

from gearo.helper.absolute_url import absolute_url
from gearo.helper.bills import generate_invoice
from gearo.helper.mangopay import get_gearo_payout_status, get_platform_fee_wallet_balance
from gearo.models import (
    Advertisement, AdvertisementCategory, AdvertisementQuestions, Attachments, Billsystem, Chatrooms,
    Faq, FaqCategory, GearoPayOut, GearoProfile, GearoUser, GearoUserDocument, MangopayPreAuthorization,
    Messages, Notifications, Offer, Payment, ProtocolAttachment, Rohprotocol, Sites, Tokens,
    Transactions, UnavailabilityPeriod, UserPayment, UserReviews, Voucher, VoucherApplication
)


class AttachmentAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ImageField: {'widget': ImageClearableFileInput},
        ThumbnailerField: {'widget': ImageClearableFileInput},
    }


class AdvertisementAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'owner', 'priceday', 'pricethree', 'priceweek', 'articleofvalue',
                    'pickup_location', 'ranking', 'ad_points')
    search_fields = ('title',)


class AdvertisementQuestionsAdmin(admin.ModelAdmin):
    list_display = ('id', 'ad', 'questioner', 'asked_date', 'answered_date')


class TransactionAdmin(admin.ModelAdmin):
    readonly_fields = ('subtotal', 'total_incl_discount', 'discount')
    list_display = (
        'ad', 'applicant', 'lessor', 'created_at', 'rent_start', 'rent_end', 'total_incl_discount', 'payout',
        'lessor_wallet_balance', 'status', 'get_action_link', 'get_protocols_link', 'payout_status',
    )
    search_fields = ('ad__title', 'applicant__email', 'lessor__email')

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['fee_wallet_balance'] = get_platform_fee_wallet_balance()
        return super(TransactionAdmin, self).changelist_view(request, extra_context=extra_context)

    def get_urls(self):
        urls = super(TransactionAdmin, self).get_urls()
        my_urls = [
            url(r'^(?P<object_id>[0-9]+)/generate-bill/$', self.get_bill,
                name='gearo_transactions_generate_bill'),
        ]
        return my_urls + urls

    def get_action_link(self, obj):
        link = name = None
        if obj.status == Transactions.STATUS_WAITING:
            link = reverse('rejectlessor', args=(obj.id,))
            name = 'reject'
        elif obj.status in [Transactions.STATUS_ACCEPTED, Transactions.STATUS_ACTIVE]:
            link = reverse('cancel', args=(obj.id,))
            name = 'cancel'

        if link and name:
            return '<a href="%s">%s</a>' % (link, name)

    get_action_link.short_description = 'Action'
    get_action_link.allow_tags = True

    def get_bill(self, request, object_id):
        from django.contrib import messages
        obj = self.get_object(request, unquote(object_id))
        bill = generate_invoice(obj)
        message = 'Bill #{} for transaction #{} has been successfully generated.'.format(bill.id, obj.id)
        messages.success(request, message)
        return redirect('admin:gearo_transactions_changelist')

    get_bill.short_description = 'Generate bill'

    def get_protocols_link(self, obj):
        links = []
        if obj.handing_over_protocol:
            link = reverse('handingover', args=(obj.id,))
            links.append('<a href="%s">%s</a>' % (link, 'Handing over protocol'))
        if obj.return_protocol:
            link = reverse('returnproto', args=(obj.id,))
            links.append('<a href="%s">%s</a>' % (link, 'Return protocol'))
        if obj.status != Transactions.STATUS_CANCELLED and not obj.bill_created and obj.chat:
            link = reverse('admin:gearo_transactions_generate_bill', args=(obj.id,))
            links.append('<a href="%s">%s</a>' % (link, 'Generate invoice'))
        if links:
            return '<br>'.join(links)

    get_protocols_link.short_description = 'Protocols'
    get_protocols_link.allow_tags = True

    def payout_status(self, obj):
        status = get_gearo_payout_status(obj)
        return status

    payout_status.short_description = 'PayOut Status'

    def lessor_wallet_balance(self, obj):
        lessor = obj.lessor
        if obj.status in Transactions.ACTIVE_STATUSES or obj.status == Transactions.STATUS_FINISHED:
            return lessor.billing_info.mangopay_current_wallet_balance

    lessor_wallet_balance.short_description = 'Lessor Wallet Balance'


class UserReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'transaction', 'user', 'rating', 'created_at')


class SitesAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug')


class BillsystemAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_invoice_links')

    def get_urls(self):
        urls = super(BillsystemAdmin, self).get_urls()
        my_urls = [
            url(r'^(?P<object_id>[0-9]+)/invoice/(?P<mimetype>[\w]+)/$', self.get_invoice,
                name='gearo_billsystem_invoice'),
        ]
        return my_urls + urls

    def get_invoice(self, request, object_id, mimetype):
        obj = self.get_object(request, unquote(object_id))
        if mimetype == 'pdf':
            if not obj.invoice:
                obj.generate_invoice()
            return HttpResponse(obj.invoice, content_type='application/pdf')
        return HttpResponse(obj.render_invoice())

    def get_invoice_links(self, obj):
        links = '<a href="%s" target="_blank">HTML</a>' % \
                reverse('admin:gearo_billsystem_invoice', args=(obj.id, 'html'))
        links += '<br><a href="%s" target="_blank">PDF</a>' % \
                 reverse('admin:gearo_billsystem_invoice', args=(obj.id, 'pdf'))
        return mark_safe(links)

    get_invoice_links.short_description = 'Name'


class GearoUserAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'get_email', 'get_full_name', 'street', 'zip', 'place', 'country', 'get_date_joined',
        'user_ranking', 'fivepoint_review', 'is_terms_accepted',
    )
    search_fields = ('user__username', 'user__email')

    def get_email(self, obj):
        return obj.user.email

    get_email.short_description = 'Email'

    def get_full_name(self, obj):
        return '{} {}'.format(obj.user.first_name, obj.user.last_name)

    get_full_name.short_description = 'Full name'

    def get_date_joined(self, obj):
        return obj.user.date_joined.date()

    get_date_joined.short_description = 'Date joined'

    def get_last_login(self, obj):
        return obj.user.last_login

    get_last_login.short_description = 'Last login'


class GearoProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'get_profile_url')

    def get_profile_url(self, obj):
        url = absolute_url('publicprofil', obj.user.username)
        return '<a href="%s" target="_blank">%s</a>' % ((url,)*2)

    get_profile_url.short_description = 'Profile link'
    get_profile_url.allow_tags = True


class GearoPayOutAdmin(admin.ModelAdmin):
    list_display = ('transaction', 'payout_id', 'payout_status', 'result_code', 'created_at')


admin.site.register(Advertisement, AdvertisementAdmin)
admin.site.register(AdvertisementCategory)
admin.site.register(AdvertisementQuestions, AdvertisementQuestionsAdmin)
admin.site.register(Attachments, AttachmentAdmin)
admin.site.register(Billsystem, BillsystemAdmin)
admin.site.register(Chatrooms)
admin.site.register(Voucher)
admin.site.register(VoucherApplication)
admin.site.register(Faq)
admin.site.register(FaqCategory)
admin.site.register(GearoProfile, GearoProfileAdmin)
admin.site.register(GearoUser, GearoUserAdmin)
admin.site.register(Messages)
admin.site.register(Notifications)
admin.site.register(Offer)
admin.site.register(Payment)
admin.site.register(Rohprotocol)
admin.site.register(ProtocolAttachment)
admin.site.register(Sites, SitesAdmin)
admin.site.register(Tokens)
admin.site.register(Transactions, TransactionAdmin)
admin.site.register(UserPayment)
admin.site.register(UserReviews, UserReviewAdmin)
admin.site.register(UnavailabilityPeriod)
admin.site.register(MangopayPreAuthorization)
admin.site.register(GearoUserDocument)
admin.site.register(GearoPayOut, GearoPayOutAdmin)
